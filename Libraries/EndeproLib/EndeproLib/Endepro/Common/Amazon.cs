﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EndeproLib.Endepro.Common
{
    public class AmazonResponse
    {
        public String URL;
        public String Title;
        public String ASIN;
        public String LargeImage;
        public String Year;
        public String Author;
        public String Category;
        public String EAN;
        public String Description;
        public String Language;
        public String Observations;
    }

    public enum SearchType
    {
        Books = 0,
        Series = 1,
        Films = 2,
        Music = 3,
        MusicEs = 4
    };

    public enum SearchCountry
    {
        ES = 0,
        UK = 1
    };

    public class Amazon
    {
        public static List<AmazonResponse> SearchBooks(String AWSAccessKeyId, String AWSSecretAccessKey, String AssociateTag, String SearchText, SearchType SearchType, SearchCountry searchCountry, int itemPage = 1)
        {
            List<AmazonResponse> response = new List<AmazonResponse>();

            SprightlySoftAWS.REST MyREST = new SprightlySoftAWS.REST();

            String RequestURL;

            String domain = String.Empty;

            if (searchCountry == SearchCountry.ES)
                domain = "es";
            else if (searchCountry == SearchCountry.UK)
                domain = "co.uk";

            RequestURL = String.Format("https://webservices.amazon.{0}/onca/xml?Service=AWSECommerceService&Operation=ItemSearch&Version=2011-08-01", domain);
            RequestURL += "&AWSAccessKeyId=" + System.Uri.EscapeDataString(AWSAccessKeyId) + "&AssociateTag=" + System.Uri.EscapeDataString(AssociateTag);
            RequestURL += "&SignatureVersion=2&SignatureMethod=HmacSHA256&Timestamp=" + Uri.EscapeDataString(DateTime.UtcNow.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z"));
            //RequestURL += "&Title=" + System.Uri.EscapeDataString(SearchText);

            RequestURL += "&Keywords=" + System.Uri.EscapeDataString(SearchText);

            String sCategory = String.Empty;

            if (SearchType == SearchType.Books)
            { 
                RequestURL += "&SearchIndex=Books";
                sCategory = "Libros";
            }
            else if (SearchType == SearchType.Series)
            {
                RequestURL += "&SearchIndex=DVD";
                //RequestURL += "&Keywords=TV";
                RequestURL += "&BrowseNode=665293031";
                sCategory = "Series";
            }
            else if (SearchType == SearchType.Films)
            {
                RequestURL += "&SearchIndex=DVD";
                //RequestURL += "&Keywords=Film";
                RequestURL += "&BrowseNode=916701031";
                sCategory = "Cine";
            }
            else if (SearchType == SearchType.Music)
            {
                RequestURL += "&SearchIndex=MP3Downloads";
                //RequestURL += "&Tag=Film";
                sCategory = "Música";
            }
            else if (SearchType == SearchType.MusicEs)
            {
                RequestURL += "&SearchIndex=Music";
                //RequestURL += "&Tag=Film";
                sCategory = "Música";
            }

            RequestURL += "&ResponseGroup=" + System.Uri.EscapeDataString("ItemAttributes,Images,EditorialReview");
            //RequestURL += "&ResponseGroup=" + System.Uri.EscapeDataString("Large");

            //RequestURL += "&Sort=salesrank";
            RequestURL += "&Sort=relevancerank";
            
            if (itemPage > 0)
                RequestURL += "&ItemPage=" + itemPage;
            else
                RequestURL += "&ItemPage=1";

            //Request per buscar BrowseNodes
            /*RequestURL = String.Format("https://webservices.amazon.{0}/onca/xml?Service=AWSECommerceService&Operation=ItemSearch&Version=2011-08-01", domain);
            RequestURL += "&AWSAccessKeyId=" + System.Uri.EscapeDataString(AWSAccessKeyId) + "&AssociateTag=" + System.Uri.EscapeDataString(AssociateTag);
            RequestURL += "&SignatureVersion=2&SignatureMethod=HmacSHA256&Timestamp=" + Uri.EscapeDataString(DateTime.UtcNow.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z"));
            RequestURL += "&Keywords=" + System.Uri.EscapeDataString(SearchText);

            RequestURL += "&SearchIndex=DVD";

            RequestURL += "&ResponseGroup=BrowseNodes";
            //RequestURL += "&Sort=salesrank";
            //RequestURL += "&Sort=relevancerank";
            RequestURL += "&ItemPage=1";*/

            String RequestMethod;
            RequestMethod = "GET";

            String SignatureValue;
            SignatureValue = MyREST.GetSignatureVersion2Value(RequestURL, RequestMethod, "", AWSSecretAccessKey);

            RequestURL += "&Signature=" + System.Uri.EscapeDataString(SignatureValue);

            Boolean RetBool;
            RetBool = MyREST.MakeRequest(RequestURL, RequestMethod, null);

            if (RetBool == true)
            {
                System.Xml.XmlDocument MyXmlDocument;
                System.Xml.XmlNamespaceManager MyXmlNamespaceManager;
                System.Xml.XmlNode MyXmlNode;
                System.Xml.XmlNodeList MyXmlNodeList;

                MyXmlDocument = new System.Xml.XmlDocument();
                MyXmlDocument.LoadXml(MyREST.ResponseString);

                MyXmlNamespaceManager = new System.Xml.XmlNamespaceManager(MyXmlDocument.NameTable);
                MyXmlNamespaceManager.AddNamespace("amz", "http://webservices.amazon.com/AWSECommerceService/2011-08-01");

                MyXmlNodeList = MyXmlDocument.SelectNodes("amz:ItemSearchResponse/amz:Items/amz:Item", MyXmlNamespaceManager);

                if (MyXmlNodeList.Count > 0)
                {
                    //http://docs.amazonwebservices.com/AWSECommerceService/2011-08-01/DG/RG_ItemAttributes.html

                    foreach (System.Xml.XmlNode ItemXmlNode in MyXmlNodeList)
                    {
                        AmazonResponse r = new AmazonResponse();

                        //Title
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Title", MyXmlNamespaceManager);
                        r.Title = MyXmlNode.InnerText;

                        r.Title = r.Title.Replace("\"", "");

                        //URL
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:DetailPageURL", MyXmlNamespaceManager);
                        r.URL = MyXmlNode.InnerText;

                        //ASIN
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ASIN", MyXmlNamespaceManager);
                        r.ASIN = MyXmlNode.InnerText;

                        //EAN
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:EAN", MyXmlNamespaceManager);
                        if (MyXmlNode != null)
                            r.EAN = MyXmlNode.InnerText;

                        //Large Image
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:LargeImage/amz:URL", MyXmlNamespaceManager);
                        if (MyXmlNode != null)
                            r.LargeImage = MyXmlNode.InnerText;

                        //Year
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:Year", MyXmlNamespaceManager);
                        if (MyXmlNode != null)
                        {
                            r.Year = MyXmlNode.InnerText;
                        }
                        else
                        {

                            System.Xml.XmlNode EditionNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Edition", MyXmlNamespaceManager);
                            System.Xml.XmlNode PublicationDateNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:PublicationDate", MyXmlNamespaceManager);
                            System.Xml.XmlNode ReleaseDateNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:ReleaseDate", MyXmlNamespaceManager);

                            if (ReleaseDateNode != null)
                            {
                                r.Year = ReleaseDateNode.InnerText;
                            }
                            else if (EditionNode != null)
                            {
                                r.Year = EditionNode.InnerText;
                            }
                            else if (PublicationDateNode != null)
                            {
                                r.Year = PublicationDateNode.InnerText;
                            }
                        }

                        //Author
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Author", MyXmlNamespaceManager);
                        if (MyXmlNode != null)
                        {
                            r.Author = MyXmlNode.InnerText;
                        }
                        else
                        {
                            System.Xml.XmlNode ArtistNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Artist", MyXmlNamespaceManager);
                            System.Xml.XmlNode DirectorNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Director", MyXmlNamespaceManager);
                            System.Xml.XmlNode CreatorNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Creator", MyXmlNamespaceManager);

                            if (DirectorNode != null)
                            {
                                r.Author = DirectorNode.InnerText;
                            }
                            else if (ArtistNode != null)
                            {
                                r.Author = ArtistNode.InnerText;
                            }
                            else if (CreatorNode != null)
                            {
                                r.Author = CreatorNode.InnerText;
                            }
                        }

                        r.Category = sCategory;

                        //Busquem el Language
                        System.Xml.XmlNodeList MyXmlNodeListLanguages;

                        MyXmlNodeListLanguages = ItemXmlNode.SelectNodes("amz:ItemAttributes/amz:Languages", MyXmlNamespaceManager);

                        if (MyXmlNodeListLanguages.Count > 0)
                        {

                            foreach (System.Xml.XmlNode MyXmlNodeLanguage in MyXmlNodeListLanguages)
                            {
                                System.Xml.XmlNode Language = MyXmlNodeLanguage.SelectSingleNode("amz:Language", MyXmlNamespaceManager);

                                if (Language != null)
                                {
                                    String sLanguage = String.Empty;

                                    System.Xml.XmlNode LanguageName = Language.SelectSingleNode("amz:Name", MyXmlNamespaceManager);
                                    System.Xml.XmlNode LanguageType = Language.SelectSingleNode("amz:Type", MyXmlNamespaceManager);

                                    if (LanguageType.InnerText == "Idioma original")
                                    {
                                        r.Language = LanguageName.InnerText;
                                    }
                                }
                            }
                        }


                        //EditorialReview
                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:EditorialReviews/amz:EditorialReview", MyXmlNamespaceManager);
                        if (MyXmlNode != null)
                        {
                            r.Description = MyXmlNode.InnerText;
                        }

                        //Si és música guardem el format a Observations
                        if (SearchType == SearchType.MusicEs)
                        {
                            MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Binding", MyXmlNamespaceManager);
                            if (MyXmlNode != null)
                            {
                                r.Observations = MyXmlNode.InnerText;

                                if (r.Observations == "CD de audio")
                                { 
                                    r.Observations = "CD";
                                }
                                else if (r.Observations == "Disco de vinilo")
                                {
                                    r.Observations = "Vinilo";
                                }
                            }
                        }
                        else if (SearchType == SearchType.Music)
                        {
                            r.Observations = String.Format("Single");
                        }

                        response.Add(r);

                        /*MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Label", MyXmlNamespaceManager);
                        //ResponseMessage += " Label = " + MyXmlNode.InnerText;

                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Director", MyXmlNamespaceManager);

                        if (MyXmlNode!=null)
                            ResponseMessage += " Director = " + MyXmlNode.InnerText;

                        MyXmlNode = ItemXmlNode.SelectSingleNode("amz:ItemAttributes/amz:Genre", MyXmlNamespaceManager);

                        if (MyXmlNode != null)
                            ResponseMessage += " Genre = " + MyXmlNode.InnerText;
                         */
                    }
                }
            }

            return response;
        }
    }
}
