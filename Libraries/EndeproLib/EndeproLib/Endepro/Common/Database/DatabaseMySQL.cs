using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Globalization;
using MySql.Data.MySqlClient;

namespace Endepro.Common.Database
{
    public class DatabaseMySQL : DatabaseBase
    {
		private string _host = string.Empty;
        private int _port = 3306;
        private string _user = string.Empty;
        private string _password = string.Empty;
        private string _name = string.Empty;
        private MySqlConnection _conn = null;

        public DatabaseMySQL(string host, string user, string password, string name)
        {
            PrivateContructor(host, 3306, user, password, name);
        }

        public DatabaseMySQL(string host, int port, string user, string password, string name)
        {
            PrivateContructor(host,port,user,password,name);
        }

        public DatabaseMySQL(String connectionString)
            : base(connectionString)
        {
            _conn = new MySqlConnection();
        }

        void PrivateContructor(string host, int port, string user, string password, string name)
        {
            _host = host;
            _port = port;
            _user = user;
            _password = password;
            _name = name;
            _conn = new MySqlConnection();
        }

        public override String GetDateFormat()
        {
            return "yyyy-MM-dd";
        }

        public override String GetTimeFormat()
        {
            return "HH:mm:ss";
        }

        public override String GetDateTimeFormat()
        {
            return GetDateFormat() + " " + GetTimeFormat();
        }

        public override String GetConnectionString()
        {
            if (_connectionString != String.Empty)
                return _connectionString;

            return String.Format("Database={0};Data Source={1};User Id={2};Password={3};CharSet=utf8;default command timeout=120;",
                                 _name, _host, _user, _password);
        }

        public override void Open()
        {
            _conn.ConnectionString = GetConnectionString();
            _conn.Open();
        }

        public override void Close()
        {
            _conn.Close();
        }

        public override ConnectionState GetState()
        {
            return _conn.State;
        }

        public override IDbTransaction BeginTransaction()
        {
            return _conn.BeginTransaction();
        }

        public override IDbTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return _conn.BeginTransaction(isolationLevel);
        }

        public override IDbCommand CreateCommand()
        {
            return _conn.CreateCommand();
        }

        public override IDbCommand CreateCommand(String commandText)
        {
            return new MySqlCommand(commandText, _conn);
        }

        public override IDbCommand CreateCommand(String commandText, IDbTransaction transaction)
        {
            return new MySqlCommand(commandText, _conn, (MySqlTransaction)transaction);
        }

        public override bool IsEmptyDatabase()
        {
            return true;
        }

        public override void CreateTables()
        {
        }

        public override DateTime GetReaderValueDateTime(IDataReader reader, string name)
        {
            //La Base de Datos MySQl en un camp Date puede tener el valor 0000-00-00 00:00:00 que da error al intentar
            //obtenerlo. Si se genera un error devolvemos como si el valor fuera NULL.
            try
            {
                return base.GetReaderValueDateTime(reader, name);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        public override Int64 GetLastAutoIncrement()
        {
            IDbCommand command = CreateCommand("SELECT LAST_INSERT_ID()");
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                    return reader.GetInt64(0);

                return 0;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }
        }
    }
}
