using System;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Globalization;

namespace Endepro.Common.Database
{
    public abstract class DatabaseBase
    {
        protected string _connectionString = String.Empty;
        private int _timeZoneOffset = -1;

        public abstract String GetDateFormat();
        public abstract String GetTimeFormat();
        public abstract String GetDateTimeFormat();

        public DatabaseBase()
        {
        }

        public DatabaseBase(String connectionString)
        {
            _connectionString = connectionString;
        }

        public String DateFormat
        {
            get
            {
                return GetDateFormat();
            }
        }

        public String TimeFormat
        {
            get
            {
                return GetTimeFormat();
            }
        }

        public String DateTimeFormat
        {
            get
            {
                return GetDateTimeFormat();
            }
        }

        public int TimeZoneOffset
        {
            get
            {
                if (_timeZoneOffset != -1)
                    return _timeZoneOffset;
                else
                    return 0;
            }
            set
            {
                _timeZoneOffset = value;
            }
        }

        public abstract String GetConnectionString();

        public abstract void Open();

        public abstract void Close();

        public abstract ConnectionState GetState();

        public abstract IDbTransaction BeginTransaction();
        public abstract IDbTransaction BeginTransaction(IsolationLevel isolationLevel);

        public abstract IDbCommand CreateCommand();
        public abstract IDbCommand CreateCommand(String commandText);
        public abstract IDbCommand CreateCommand(String commandText, IDbTransaction transaction);

        public abstract bool IsEmptyDatabase();

        public abstract void CreateTables();

        public bool IsOpened()
        {
            return GetState() == ConnectionState.Open ||
                   GetState() == ConnectionState.Executing ||
                   GetState() == ConnectionState.Fetching;
        }

        public void AddParameter(IDbCommand command, DbType type, Object value)
        {
            IDbDataParameter param = command.CreateParameter();
            param.DbType = type;
            param.Value = value;
            command.Parameters.Add(param);
        }

        public virtual String ParseStringToSQL(String str)
        {
            return str.Replace("\'", "\'\'").Replace("\\","\\\\");
        }

        public virtual string ParseDecimalToSQL(decimal number)
        {
            string result = number.ToString();
            if (NumberFormatInfo.CurrentInfo.NumberDecimalSeparator != ".")
                result = result.Replace(NumberFormatInfo.CurrentInfo.NumberDecimalSeparator, ".");
            return result;
        }

        public virtual string ParseDoubleToSQL(double number)
        {
            string result = number.ToString();
            if (NumberFormatInfo.CurrentInfo.NumberDecimalSeparator != ".")
                result = result.Replace(NumberFormatInfo.CurrentInfo.NumberDecimalSeparator, ".");
            return result;
        }

        public virtual string ParseDateToSQL(DateTime date)
        {
            return date.ToString(GetDateFormat());
        }

        public virtual string ParseTimeToSQL(DateTime date)
        {
            return date.ToString(GetTimeFormat());
        }

        public virtual string ParseDateTimeToSQL(DateTime date)
        {
            return date.ToString(GetDateTimeFormat());
        }

        public virtual string ParseDateToSQLNull(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "null";

            return "'" + ParseDateToSQL(date) + "'";
        }

        public virtual string ParseTimeToSQLNull(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "null";

            return "'" + ParseTimeToSQL(date) + "'";
        }

        public virtual string ParseDateTimeToSQLNull(DateTime date)
        {
            if (date == DateTime.MinValue)
                return "null";

            return "'" + ParseDateTimeToSQL(date) + "'";
        }

        public virtual string ParseIDToSQLNull(Int64 id)
        {
            if (id <= 0)
                return "null";

            return id.ToString();
        }

        public virtual Int64 GetNextPrimaryKeyValue(String table, String numericField)
        {
            return GetMaxFieldValue(table, numericField) + 1;
        }

        public virtual Int64 GetNextPrimaryKeyValue(String table, String numericField, IDbTransaction transaction)
        {
            return GetMaxFieldValue(table, numericField, transaction) + 1;
        }

        public virtual Int64 GetMaxFieldValue(String table, String numericField)
        {
            return GetMaxFieldValue(table, numericField, String.Empty);
        }

        public virtual Int64 GetMaxFieldValue(String table, String numericField, String where)
        {
            Int64 result = 0;
            String sql = String.Format("select max({0}) from {1}", numericField, table);
            if (where != String.Empty)
                sql += " where " + where;
            IDbCommand command = CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Type tipus = reader.GetFieldType(0);

            try
            {
                if (!reader.Read() || reader.IsDBNull(0))
                    result = 0;
                else
                {
                    if (tipus == typeof(System.Int64))
                    {
                        result = reader.GetInt64(0);
                    }
                    else if (tipus == typeof(System.Int32))
                    {
                        result = (Int64)reader.GetInt32(0);
                    }
                    else
                    {
                        result = (Int64)reader.GetInt16(0);
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return result;
        }

        public virtual Int64 GetMaxFieldValue(String table, String numericField, IDbTransaction transaction)
        {
            Int64 result = 0;
            IDbCommand command = CreateCommand(String.Format("select max({0}) from {1};", numericField, table), transaction);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (!reader.Read() || reader.IsDBNull(0))
                    result = 0;
                else
                {
                    Type tipus = reader.GetFieldType(0);

                    if (tipus == typeof(System.Int64))
                    {
                        result = reader.GetInt64(0);
                    }
                    else if (tipus == typeof(System.Int32))
                    {
                        result = (Int64)reader.GetInt32(0);
                    }
                    else
                    {
                        result = (Int64)reader.GetInt16(0);
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return result;
        }

        public virtual DateTime GetMaxFieldValueDateTime(String table, String field)
        {
            DateTime result = DateTime.MinValue;
            IDbCommand command = CreateCommand(String.Format("select max({0}) from {1};", field, table));
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read() && !reader.IsDBNull(0))
                {
                    result = reader.GetDateTime(0);
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return result;
        }

        public virtual bool RecordExists(string table, string field, string where)
        {
            string sql = String.Format("select {0} from {1}", field, table);
            if (where != String.Empty)
                sql += " where " + where;
            IDbCommand command = CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            bool hasRows = false;
            try
            {
                hasRows = reader.Read();
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }
            return hasRows;
        }

        public virtual int GetTableCount(string table, string field)
        {
            return GetTableCount(table, field, String.Empty, null);
        }

        public virtual int GetTableCount(string table, string field, string where)
        {
            return GetTableCount(table, field, where, null);
        }

        public virtual int GetTableCount(string table, string field, IDbTransaction transaction)
        {
            return GetTableCount(table, field, String.Empty, transaction);
        }

        public virtual int GetTableCount(string table, string field, string where, IDbTransaction transaction)
        {
            string sql = String.Format("select count({0}) from {1}", field, table);
            if (where != String.Empty)
                sql += " where " + where;
            IDbCommand command = null;
            if (transaction != null)
                command = CreateCommand(sql, transaction);
            else
                command = CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            int result = 0;
            try
            {
                if (reader.Read() && !reader.IsDBNull(0))
                {
                    Type tipus = reader.GetFieldType(0);
                    if (tipus == typeof(System.Int64))
                    {
                        result = (int)reader.GetInt64(0);
                    }
                    else if (tipus == typeof(System.Int32))
                    {
                        result = (int)reader.GetInt32(0);
                    }
                    else
                    {
                        result = (int)reader.GetInt16(0);
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }
            return result;
        }

        public virtual String GetReaderValueString(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return String.Empty;

            String result = reader.GetString(reader.GetOrdinal(name));
            result = result.Replace("", "€");
            result = result.Replace("[landa]", "λ");
            return result;
        }

        public virtual Int16 GetReaderValueInt16(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return 0;

            return reader.GetInt16(reader.GetOrdinal(name));
        }

        public virtual Int32 GetReaderValueInt32(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return 0;

            return reader.GetInt32(reader.GetOrdinal(name));
        }

        public virtual Int64 GetReaderValueInt64(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return 0;

            return reader.GetInt64(reader.GetOrdinal(name));
        }

        public virtual Int64 GetReaderValueInt(IDataReader reader, string name)
        {
            int ordinal = reader.GetOrdinal(name);
            if (reader.IsDBNull(ordinal))
                return 0;

            Type type = reader.GetFieldType(ordinal);
            if (type == typeof(System.Int64))
            {
                return reader.GetInt64(ordinal);
            }
            else if (type == typeof(System.Int32))
            {
                return (Int64)reader.GetInt32(ordinal);
            }
            else if (type == typeof(System.Int16))
            {
                return (Int64)reader.GetInt16(ordinal);
            }
            else if (type == typeof(System.Decimal))
            {
                return (Int64)reader.GetDecimal(ordinal);
            }
            else if (type == typeof(System.Double))
            {
                return (Int64)reader.GetDouble(ordinal);
            }
            else
            {
                return 0;
            }
        }

        public virtual Decimal GetReaderValueNumeric(IDataReader reader, string name)
        {
            int ordinal = reader.GetOrdinal(name);
            if (reader.IsDBNull(ordinal))
                return 0;

            Type type = reader.GetFieldType(ordinal);
            if (type == typeof(System.Int64))
            {
                return (Decimal)reader.GetInt64(ordinal);
            }
            else if (type == typeof(System.Int32))
            {
                return (Decimal)reader.GetInt32(ordinal);
            }
            else if (type == typeof(System.Int16))
            {
                return (Decimal)reader.GetInt16(ordinal);
            }
            else if (type == typeof(System.Decimal))
            {
                return reader.GetDecimal(ordinal);
            }
            else //if (type == typeof(System.Double))
            {
                return (Decimal)reader.GetDouble(ordinal);
            }
        }

        public virtual Decimal GetReaderValueDecimal(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return 0;

            return reader.GetDecimal(reader.GetOrdinal(name));
        }

        public virtual Double GetReaderValueDouble(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return 0;

            return reader.GetDouble(reader.GetOrdinal(name));
        }

        public virtual Single GetReaderValueSingle(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return 0;

            return reader.GetFloat(reader.GetOrdinal(name));
        }

        public virtual DateTime GetReaderValueDateTime(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return DateTime.MinValue;

            return reader.GetDateTime(reader.GetOrdinal(name));
        }

        public virtual DateTime GetReaderValueDateTimeUTC(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return DateTime.MinValue;

            if (_timeZoneOffset == 0)
                return reader.GetDateTime(reader.GetOrdinal(name));
            else
                return reader.GetDateTime(reader.GetOrdinal(name)).AddMinutes((double)(0 - TimeZoneOffset));
        }

        public virtual Boolean GetReaderValueBoolean(IDataReader reader, string name)
        {
            if (reader.IsDBNull(reader.GetOrdinal(name)))
                return false;

            return reader.GetBoolean(reader.GetOrdinal(name));
        }

        public void ExecuteCommand(String SQL)
        {
            IDbCommand cmd = null;
            try
            {
                cmd = CreateCommand(SQL);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public abstract Int64 GetLastAutoIncrement();
    }
}
