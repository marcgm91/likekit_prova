
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
//using System.Globalization;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Globalization;

namespace Endepro.Common.Xml
{
    public class SettingsParser
    {
        private String _filename = String.Empty;
        private NameValueCollection _settings = null;
        public int Indentation = 4;
        public char IndentChar = ' ';

        public SettingsParser()
        {
            CreateFileName();
        }

        public SettingsParser(String filename)
        {
            PrivateConstructor(filename, Assembly.GetExecutingAssembly());
        }

        public SettingsParser(String filename, Assembly assembly)
        {
            PrivateConstructor(filename, assembly);
        }

        private void PrivateConstructor(String filename, Assembly assembly)
        {
            if (assembly != null)
                _filename = Common.GetAssemblyPath(assembly) + filename;
            else
                _filename = filename;
        }

        public String GetFilename()
        {
            return _filename;
        }

        public void CreateFileName()
        {
            CreateFileName(Assembly.GetExecutingAssembly());
        }

        public void CreateFileName(Assembly assembly)
        {
            string assemPath = assembly.GetName().CodeBase;
            assemPath = assemPath.Replace("file:\\", "");
            _filename = assemPath + ".config.xml";
        }

        private void CheckCollection()
        {
            if (_settings == null)
            {
                _settings = new NameValueCollection(0);
            }
        }

        public int GetSettingsCount()
        {
            return _settings.Count;
        }

        public String GetKey(int index)
        {
            return _settings.Keys[index];
        }

        public String GetValue(int index)
        {
            return _settings[index];
        }

        public String GetString(String name)
        {
            return GetString(name,String.Empty);
        }

        public String GetString(String name, String defaultValue)
        {
            if (_settings == null)
            {
                return defaultValue;
            }

            try
            {
                String result = _settings.Get(name);
                if (result == null || result == String.Empty)
                    return defaultValue;
                
                return result;
            }
            catch
            {
                return defaultValue;
            }
        }

        public int GetInt(String name)
        {
            return GetInt(name,0);
        }

        public int GetInt(String name, int defaultValue)
        {
            if (_settings == null)
            {
                return defaultValue;
            }

            String result = _settings.Get(name);
            if (result == null || result == String.Empty)
                return defaultValue;
            try
            {
                return int.Parse(result);
            }
            catch
            {
                return defaultValue;
            }
        }

        public long GetLong(String name)
        {
            return GetLong(name, 0);
        }

        public long GetLong(String name, long defaultValue)
        {
            if (_settings == null)
            {
                return defaultValue;
            }

            String result = _settings.Get(name);
            if (result == null || result == String.Empty)
                return defaultValue;
            try
            {
                return long.Parse(result);
            }
            catch
            {
                return defaultValue;
            }
        }

        public DateTime GetDateTime(String name, DateTime defaultValue)
        {
            if (_settings == null)
            {
                return defaultValue;
            }

            String result = _settings.Get(name);
            if (result == null || result == String.Empty)
                return defaultValue;
            try
            {
                return DateTime.Parse(result, new CultureInfo("es-ES", false).NumberFormat);
            }
            catch
            {
                return defaultValue;
            }
        }

        public Decimal GetDecimal(String name, Decimal defaultValue)
        {
            if (_settings == null)
            {
                return defaultValue;
            }

            String result = _settings.Get(name);
            if (result == null || result == String.Empty)
                return defaultValue;
            try
            {
                return Decimal.Parse(result, new CultureInfo("es-ES", false).NumberFormat);
            }
            catch
            {
                return defaultValue;
            }
        }

        public void PutString(String name, String value)
        {
            CheckCollection();
            _settings.Set(name, value);
        }

        public void PutInt(String name, int value)
        {
            CheckCollection();
            _settings.Set(name, value.ToString());
        }

        public void PutLong(String name, long value)
        {
            CheckCollection();
            _settings.Set(name, value.ToString());
        }

        public void PutDateTime(String name, DateTime value)
        {
            CheckCollection();
            _settings.Set(name, value.ToString(new CultureInfo("es-ES", false).NumberFormat));
        }
        
        public void PutDecimal(String name, Decimal value)
        {
            CheckCollection();
            _settings.Set(name, value.ToString(new CultureInfo("es-ES", false).NumberFormat));
        }

        public bool ReadFile(String fromNodeName)
        {
            if (_filename == String.Empty)
            {
                CreateFileName();
            }

            if (!File.Exists(_filename))
            {
                return false;
            }

            CheckCollection();

            _settings.Clear();

            StreamReader streamReader = null;
            XmlTextReader xmlTextReader = null;
            streamReader = new StreamReader(_filename);
            xmlTextReader = new XmlTextReader(streamReader);
            xmlTextReader.WhitespaceHandling = WhitespaceHandling.None;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlTextReader);

            HandleNodes(xmlDocument.DocumentElement, fromNodeName);

            if (xmlTextReader != null)
            {
                xmlTextReader.Close();
            }
            if (streamReader == null)
            {
                return false;
            }
            streamReader.Close();
            return true;
        }

        public bool ReadString(String xmlString, String fromNodeName)
        {
            CheckCollection();

            _settings.Clear();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlString);

            HandleNodes(xmlDocument.DocumentElement, fromNodeName);

            return true;
        }

        private void HandleNodes(XmlNode Node, String fromNodeName)
        {
            if (Node != null && Node.Name.ToLower() == fromNodeName.ToLower())
            {
                ReadSettings(Node);
            }
            if (Node.HasChildNodes)
            {
                for (Node = Node.FirstChild; Node != null; Node = Node.NextSibling)
                {
                    HandleNodes(Node, fromNodeName);
                }
            }
        }

        private void ReadSettings(XmlNode parent)
        {
            for (XmlNode xmlNode = parent.FirstChild; xmlNode != null; xmlNode = xmlNode.NextSibling)
            {
                String strKey = xmlNode.Name;
                String strValue = xmlNode.InnerText;

                _settings.Add(strKey, strValue);
            }
        }

        public bool WriteFile(String parentNodeName)
        {
            if (_filename == String.Empty)
            {
                CreateFileName();
            }

            if (_settings == null)
                return false;

            XmlTextWriter tw = new XmlTextWriter(_filename, Encoding.Unicode);
            if (Indentation > 0)
            {
                tw.Formatting = Formatting.Indented;
                tw.Indentation = Indentation;
                tw.IndentChar = IndentChar;
            }
            else
            {
                tw.Formatting = Formatting.None;
            }
            tw.QuoteChar = '\'';
            tw.WriteStartDocument();
            tw.WriteStartElement(parentNodeName);
            for (int i = 0; i < _settings.Count; i++)
            {
                tw.WriteStartElement(_settings.Keys[i]);
                tw.WriteRaw(_settings[i]);
                tw.WriteEndElement();
            }
            tw.WriteEndElement();
            tw.WriteEndDocument();
            tw.Close();

            return true;
        }

        public String WriteString(String parentNodeName)
        {
            if (_settings == null)
                return String.Empty;

            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            if (Indentation > 0)
            {
                tw.Formatting = Formatting.Indented;
                tw.Indentation = Indentation;
                tw.IndentChar = IndentChar;
            }
            else
            {
                tw.Formatting = Formatting.None;
            }
            tw.QuoteChar = '\'';
            tw.WriteStartDocument();
            tw.WriteStartElement(parentNodeName);
            for (int i = 0; i < _settings.Count; i++)
            {
                tw.WriteStartElement(_settings.Keys[i]);
                tw.WriteRaw(_settings[i]);
                tw.WriteEndElement();
            }
            tw.WriteEndElement();
            tw.WriteEndDocument();
            tw.Flush();
            tw.Close();

            sw.Flush();

            return sw.ToString();
        }
    }
}
