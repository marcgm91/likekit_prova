﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Endepro.Common
{
    public abstract class EThread
    {
        private String _name = String.Empty;
        private Thread _thread = null;

        public String Name
        {
            get
            {
                return _name;
            }
        }

        public EThread()
        {
        }

        public EThread(String name)
        {
        }

        public virtual void Start()
        {
            if (_thread == null)
            {
                _thread = new Thread(new ThreadStart(ThreadProc));
                if (_name != String.Empty)
                    _thread.Name = _name;
                _thread.Start();
            }
        }

        public virtual void Abort()
        {
            if (_thread != null)
            {
                _thread.Abort();
                _thread = null;
            }
        }

        protected abstract void ThreadProc();
    }
}
