﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;
using Twitterizer;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;

namespace Endepro.Common.Social
{
    public class InviteContact : IComparable
    {
        public String Name = String.Empty;
        public String Email = String.Empty;

        public InviteContact(String name, String email)
        {
            Name = name;
            Email = email;
        }

        public int CompareTo(object obj)
        {
            InviteContact Compare = (InviteContact)obj;

            int result = 0;

            if ((this.Name != null) && (Compare.Name != null))
            {
                result = this.Name.CompareTo(Compare.Name);
                if (result == 0)
                    result = this.Name.CompareTo(Compare.Name);
            }
            else if ((this.Email != null) && (Compare.Email != null))
            {
                result = this.Email.CompareTo(Compare.Email);
                if (result == 0)
                    result = this.Email.CompareTo(Compare.Email);            
            }

            return result;
        }
    }

    public class FaceBookPageInformation
    {
        public String Name;
        public String AccessToken;
        public String PageID;
    }

    public class Social
    {
        public static void FacebookPublish(String FacebookAccessToken, String Message,
            String Link, String Name, String Caption, String Picture, String Description, FaceBookPageInformation page = null)
        {
            Facebook.FacebookClient fbClient = new Facebook.FacebookClient(FacebookAccessToken);

            Dictionary<string, object> postMessage = new Dictionary<string, object>();
            postMessage["message"] = Message;
            postMessage["link"] = Link;
            postMessage["name"] = Name;
            postMessage["caption"] = Caption;
            postMessage["picture"] = Picture;
            postMessage["description"] = Description;

            if (page==null)
                fbClient.Post("me/feed", postMessage);
            else
                fbClient.Post(page.PageID+"/feed", postMessage);
        }

        public static ArrayList FacebookUserPages(String FacebookAccessToken)
        {
            Facebook.FacebookClient fbClient = new Facebook.FacebookClient(FacebookAccessToken);

            Dictionary<string, object> pMessage = new Dictionary<string, object>();
            pMessage["access_token"] = FacebookAccessToken;

            object resultGet = fbClient.Get("/me/accounts", pMessage);

            JObject result = JObject.Parse(resultGet.ToString());
            
            ArrayList resultArray = new ArrayList();

            foreach (var i in result["data"].Children())
            {
                FaceBookPageInformation fb = new FaceBookPageInformation();

                String category = i["category"].ToString().Replace("\"", "");

                if (category != "Application")
                {
                    fb.AccessToken = i["access_token"].ToString().Replace("\"", "");
                    fb.Name = i["name"].ToString().Replace("\"", "");
                    fb.PageID = i["id"].ToString().Replace("\"", "");

                    resultArray.Add(fb);
                }
            }

            return resultArray;
        }
        
        public static List<InviteContact> FacebookGetFriends(String FacebookAccessToken)
        {
            Facebook.FacebookClient fbClient = new Facebook.FacebookClient(FacebookAccessToken);

            dynamic myInfo = fbClient.Get("/me/friends");

            List<InviteContact> resultList = new List<InviteContact>();

            foreach (dynamic friend in myInfo.data)
                resultList.Add(new InviteContact(friend.name, friend.id));

            return resultList;
        }

        public static bool TwitterPublish(String AccessToken, String AccessTokenSecret, String ConsumerKey, String ConsumerSecret, String Message)
        {
            OAuthTokens tokens = new OAuthTokens();
            tokens.AccessToken = AccessToken;
            tokens.AccessTokenSecret = AccessTokenSecret;
            tokens.ConsumerKey = ConsumerKey;
            tokens.ConsumerSecret = ConsumerSecret;

            if (Message.Length > 140)
                Message = Message.Substring(0, 140);

            TwitterResponse<TwitterStatus> tweetResponse = TwitterStatus.Update(tokens, Message);
            if (tweetResponse.Result == RequestResult.Success)
                return true;
            else
                return false;
        }
    }

}
