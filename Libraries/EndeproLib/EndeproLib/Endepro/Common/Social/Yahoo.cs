﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;
using Twitterizer;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;
using OAuth;
using System.Net;
using System.Web;
using System.IO;
using System.Xml;
using Endepro.Common.Social;

namespace Endepro.Common.Yahoo
{
    public static class YahooAuthSession
    {
        public static String OauthTokenSecret;
        public static String _consumerKey;
        public static String _consumerSecretKey;
        public static String _urlCallback;

        private static String _oauthToken = String.Empty;
        private static String _oauthVerifier = String.Empty;
        private static String _oauthYahooGuid = String.Empty;
        private static String _oauthSessionHandle = String.Empty;

        /*public YahooAuth(String consumerKey, String consumerSecretKey, String urlCallback)
        {
            _consumerKey = consumerKey;
            _consumerSecretKey = consumerSecretKey;
            _urlCallback = urlCallback;
        }*/

        public static void SetAccessToken(String OAuthToken, String OAuthVerifier)
        {
            _oauthToken = OAuthToken;
            _oauthVerifier = OAuthVerifier;

            GetAccessToken();
        }

        public static String GetRequestToken()
        {
            string authorizationUrl = string.Empty;
            OAuthBase oauth = new OAuthBase();

            Uri uri = new Uri("https://api.login.yahoo.com/oauth/v2/get_request_token");
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string normalizedUrl;
            string normalizedRequestParameters;
            string sig = oauth.GenerateSignature(uri, _consumerKey, _consumerSecretKey, string.Empty, string.Empty, "GET", timeStamp, nonce, OAuthBase.SignatureTypes.PLAINTEXT, out normalizedUrl, out normalizedRequestParameters);
            StringBuilder sbRequestToken = new StringBuilder(uri.ToString());
            sbRequestToken.AppendFormat("?oauth_nonce={0}&", nonce);
            sbRequestToken.AppendFormat("oauth_timestamp={0}&", timeStamp);
            sbRequestToken.AppendFormat("oauth_consumer_key={0}&", _consumerKey);
            sbRequestToken.AppendFormat("oauth_signature_method={0}&", "PLAINTEXT"); //HMAC-SHA1
            sbRequestToken.AppendFormat("oauth_signature={0}&", sig);
            sbRequestToken.AppendFormat("oauth_version={0}&", "1.0");
            sbRequestToken.AppendFormat("oauth_callback={0}", HttpUtility.UrlEncode(_urlCallback));

            try
            {
                string returnStr = string.Empty;
                string[] returnData;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sbRequestToken.ToString());
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader streamReader = new StreamReader(res.GetResponseStream());
                returnStr = streamReader.ReadToEnd();
                returnData = returnStr.Split(new Char[] { '&' });

                int index;
                if (returnData.Length > 0)
                {
                    index = returnData[1].IndexOf("=");
                    string oauth_token_secret = returnData[1].Substring(index + 1);
                    OauthTokenSecret = oauth_token_secret;
                    index = returnData[3].IndexOf("=");
                    string oauth_request_auth_url = returnData[3].Substring(index + 1);
                    authorizationUrl = HttpUtility.UrlDecode(oauth_request_auth_url);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return authorizationUrl;
        }

        private static void GetAccessToken()
        {
            OAuthBase oauth = new OAuthBase();

            Uri uri = new Uri("https://api.login.yahoo.com/oauth/v2/get_token");
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string sig = _consumerSecretKey + "%26" + OauthTokenSecret;

            StringBuilder sbAccessToken = new StringBuilder(uri.ToString());
            sbAccessToken.AppendFormat("?oauth_consumer_key={0}&", _consumerKey);
            sbAccessToken.AppendFormat("oauth_signature_method={0}&", "PLAINTEXT"); //HMAC-SHA1
            sbAccessToken.AppendFormat("oauth_signature={0}&", sig);
            sbAccessToken.AppendFormat("oauth_timestamp={0}&", timeStamp);
            sbAccessToken.AppendFormat("oauth_version={0}&", "1.0");
            sbAccessToken.AppendFormat("oauth_token={0}&", _oauthToken);
            sbAccessToken.AppendFormat("oauth_nonce={0}&", nonce);
            sbAccessToken.AppendFormat("oauth_verifier={0}", _oauthVerifier);

            try
            {
                string returnStr = string.Empty;
                string[] returnData;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sbAccessToken.ToString());
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader streamReader = new StreamReader(res.GetResponseStream());
                returnStr = streamReader.ReadToEnd();
                returnData = returnStr.Split(new Char[] { '&' });

                int index;
                if (returnData.Length > 0)
                {
                    index = returnData[0].IndexOf("=");
                    _oauthToken = returnData[0].Substring(index + 1);

                    index = returnData[1].IndexOf("=");
                    string oauth_token_secret = returnData[1].Substring(index + 1);
                    OauthTokenSecret = oauth_token_secret;

                    index = returnData[3].IndexOf("=");
                    string oauth_session_handle = returnData[3].Substring(index + 1);
                    _oauthSessionHandle = oauth_session_handle;

                    index = returnData[5].IndexOf("=");
                    string xoauth_yahoo_guid = returnData[5].Substring(index + 1);
                    _oauthYahooGuid = xoauth_yahoo_guid;
                }
            }
            catch
            {
            }
        }

        public static List<InviteContact> RetriveContacts()
        {
            List<InviteContact> result = new List<InviteContact>();

            OAuthBase oauth = new OAuthBase();

            Uri uri = new Uri("http://social.yahooapis.com/v1/user/" + _oauthYahooGuid + "/contacts?format=XML");
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string normalizedUrl;
            string normalizedRequestParameters;
            string sig = oauth.GenerateSignature(uri, _consumerKey, _consumerSecretKey, _oauthToken, OauthTokenSecret, "GET", timeStamp, nonce, OAuthBase.SignatureTypes.HMACSHA1, out normalizedUrl, out normalizedRequestParameters);

            StringBuilder sbGetContacts = new StringBuilder(uri.ToString());

            try
            {
                string returnStr = string.Empty;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sbGetContacts.ToString());
                req.Method = "GET";

                string authHeader = "Authorization: OAuth " +
                "realm=\"yahooapis.com\"" +
                ",oauth_consumer_key=\"" + _consumerKey + "\"" +
                ",oauth_nonce=\"" + nonce + "\"" +
                ",oauth_signature_method=\"HMAC-SHA1\"" +
                ",oauth_timestamp=\"" + timeStamp + "\"" +
                ",oauth_token=\"" + _oauthToken + "\"" +
                ",oauth_version=\"1.0\"" +
                ",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\"";

                req.Headers.Add(authHeader);

                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader streamReader = new StreamReader(res.GetResponseStream());
                returnStr = streamReader.ReadToEnd();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(returnStr);
                XmlNodeList elemList = xmldoc.DocumentElement.GetElementsByTagName("fields");

                ArrayList emails = new ArrayList();
                for (int i = 0; i < elemList.Count; i++)
                {
                    if (elemList[i].ChildNodes[1].InnerText == "email")
                    {
                        result.Add(new InviteContact(elemList[i].ChildNodes[2].InnerText, elemList[i].ChildNodes[2].InnerText));
                    }
                }
                
            }
            catch
            {
            }
            
            return result;
        }
    }

}
