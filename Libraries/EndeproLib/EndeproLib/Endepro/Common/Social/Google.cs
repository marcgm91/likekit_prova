﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;
using Twitterizer;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;
using Google.GData.Client;
using Google.Contacts;
using Google.GData.Extensions.Apps;
using Endepro.Common.Social;
using Google.GData.Contacts;

namespace Endepro.Common.Google
{
    public class GoogleAuth
    {
        private OAuth2Parameters parameters=null;
        private String applicationName=String.Empty;
        //private static string scopes = "https://www.google.com/m8/feeds/ https://apps-apis.google.com/a/feeds/groups/ https://www.google.com/m8/feeds/contacts/default/full";
        private static string scopes = "https://www.google.com/m8/feeds/contacts";

        public GoogleAuth(String appName, String clientID, String clientSecret, String redirectUri)
        {
            applicationName = appName;

            parameters = new OAuth2Parameters()
            {
                ClientId = clientID,
                ClientSecret = clientSecret,
                RedirectUri = redirectUri,
                Scope = scopes
            };
        }

        public String CreateAuthURL()
        {
            return OAuthUtil.CreateOAuth2AuthorizationUrl(parameters);
        }

        public void SetAccessCode(String accessCode)
        {
            parameters.AccessCode = accessCode;
            OAuthUtil.GetAccessToken(parameters);
        }

        public List<InviteContact> GetContacts()
        {
            List<InviteContact> result = new List<InviteContact>();

            try
            {
                RequestSettings settings = new RequestSettings(applicationName, parameters);
                ContactsRequest cr = new ContactsRequest(settings);

                ContactsQuery query = new ContactsQuery(ContactsQuery.CreateContactsUri("default"));
                query.NumberToRetrieve = 500;

                Feed<Contact> feed = cr.Get<Contact>(query);
                foreach (Contact contact in feed.Entries)
                {
                    result.Add(new InviteContact(contact.Name.FullName, contact.PrimaryEmail.Address));
                }

                /*Feed<Contact> f = cr.GetContacts()(query);
                foreach (Contact c in f.Entries)
                    result.Add(new InviteContact(c.Name.FullName, c.PrimaryEmail.Address));*/
            }
            catch
            {
            }

            return result;
        }
    }

}
