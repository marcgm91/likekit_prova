
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Security.Cryptography;

namespace Endepro.Common
{
    /// <summary>
    /// Encrypts and decrypts data using the crypto APIs.
    /// </summary>
    public class Crypto
    {
        // all static methods
        private Crypto()
        {
        }

        #region MD5
        public static byte[] ComputeMD5(byte[] buffer)
        {
            //Create the md5 hash provider.
            MD5 md = new MD5CryptoServiceProvider();

            //Compute the hash value from the array of bytes.
            return md.ComputeHash(buffer);
        }

        public static String ComputeMD5(String data)
        {
            return Common.ByteArrayToString(ComputeMD5(Common.StringToByteArray(data)));
        }

        public static string GetMD5Hash(string input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            string password = s.ToString();
            return password;
        }

        #endregion
    }
}
