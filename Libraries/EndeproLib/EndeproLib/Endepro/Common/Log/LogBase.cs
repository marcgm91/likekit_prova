
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Endepro.Common
{
    public abstract class LogBase
    {
        private string _datatimeformat = "dd/MM/yyyy HH:mm:ss";
        public string DataTimeFormat
        {
            get
            {
                return _datatimeformat;
            }
        }

        public LogBase()
        {
        }

        public LogBase(string datetimeformat)
        {
            _datatimeformat = datetimeformat;
        }

        public string GetDateTimeFormat()
        {
            return _datatimeformat;
        }

        public string FormatCurrentDateTime()
        {
            return DateTime.Now.ToString(_datatimeformat);
        }

        public abstract void LogLine(string text);

        public virtual void LogLine(Exception ex)
        {
            LogLine("Exception: " + ex.Message + "; StackTrace: " + ex.StackTrace +
                    "; Memory used: " + System.GC.GetTotalMemory(false).ToString() + 
                    "; Version: " + Assembly.GetCallingAssembly().GetName().Version.ToString(4));
        }
    }
}
