
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace Endepro.Common
{
    public class LogFile : LogBase
    {
        private FileStream _file = null;
        private StreamWriter _streamWriter = null;
        private string _filename = String.Empty;
        private const string _separatorLine = "\r\n******************************************\r\n";
        
        public LogFile(string filename)
            : base()
        {
            PrivateContructor(filename, Assembly.GetExecutingAssembly(), true);
        }

        public LogFile(string filename, bool autoOpen)
            : base()
        {
            PrivateContructor(filename, Assembly.GetExecutingAssembly(), autoOpen);
        }

        public LogFile(string filename, string datetimeformat)
            :  base(datetimeformat)
        {
            PrivateContructor(filename, Assembly.GetExecutingAssembly(), true);
        }

        public LogFile(string filename, Assembly assembly)
            : base()
        {
            PrivateContructor(filename, assembly, true);
        }

        public LogFile(string filename, Assembly assembly, bool autoOpen)
            : base()
        {
            PrivateContructor(filename, assembly, autoOpen);
        }

        public LogFile(string filename, Assembly assembly, string datetimeformat)
            : base(datetimeformat)
        {
            PrivateContructor(filename, assembly, true);
        }

        private void PrivateContructor(string filename, Assembly assembly, bool autoOpen)
        {
            if (assembly != null)
            {
                _filename = Common.GetAssemblyPath(assembly) + filename;
            }
            else
            {
                _filename = filename;
            }
            if (autoOpen)
                Open();
        }

        public void Open()
        {
            if (_file == null)
            {
                try
                {
                    _file = new FileStream(_filename, FileMode.Append, FileAccess.Write);
                    _streamWriter = new StreamWriter(_file);
                    _streamWriter.WriteLine(_separatorLine);
                    _streamWriter.WriteLine("Application started: " + DateTime.Now.ToString(DataTimeFormat));
                }
                catch (Exception)
                {
                    _streamWriter = null;
                    _file = null;
                    throw;
                }
            }
        }

        public void Close()
        {
            if (_streamWriter != null)
            {
                _streamWriter.WriteLine(_separatorLine);
                _streamWriter.Flush();
                _streamWriter.Close();
                //_streamWriter.Dispose();
                _streamWriter = null;
            }
            if (_file != null)
            {
                _file.Close();
                //_file.Dispose();
                _file = null;
            }
        }

        public string GetFilename()
        {
            return _filename;
        }

        public override void LogLine(string text)
        {
            if (_streamWriter != null)
            {
                _streamWriter.WriteLine(FormatCurrentDateTime() + "\t" + text);
                _streamWriter.Flush();
            }
        }
    }
}
