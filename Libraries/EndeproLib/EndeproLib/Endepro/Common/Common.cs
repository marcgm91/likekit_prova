
using System;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Net;
using System.Web;

namespace Endepro.Common
{
    public class Common
    {
        // all static methods
        private Common()
		{
        }

        public static string EncodePassword(string originalPassword)
        {
            // Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            Byte[] encodedBytes = new MD5CryptoServiceProvider().ComputeHash(originalBytes);

            return Convert.ToBase64String(encodedBytes);
        }

        #region String, bytes and StringHex Convertion Functions
        public static byte[] StringToByteArray(String str)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetBytes(str);
        }

        public static String ByteArrayToString(byte[] bytes)
        {
            return ByteArrayToString(bytes, 0, bytes.Length);
        }

        public static String ByteArrayToString(byte[] bytes, int index, int count)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetString(bytes, index, count);
        }

        public static String ByteArrayToStringHex(byte[] bytes, int index, int count)
        {
            String result = String.Empty;
            for (int i = 0; i < bytes.Length; i++)
            {
                result += bytes[i].ToString("X2");
            }
            return result;
        }

        public static String ByteArrayToStringHex(byte[] bytes)
        {
            return ByteArrayToStringHex(bytes, 0, bytes.Length);
        }

        public static String StringToStringHex(String str)
        {
            return ByteArrayToStringHex(StringToByteArray(str));
        }

        public static String StringHexToString(String str)
        {
            return Common.ByteArrayToString(StringHexToByteArray(str));
        }

        public static byte[] StringHexToByteArray(String str)
        {
            byte[] data = new byte[str.Length / 2];
            for (int resPos = 0, dataPos = 0; resPos < str.Length; resPos += 2, dataPos++)
            {
                data[dataPos] = Byte.Parse(str.Substring(resPos, 2), NumberStyles.HexNumber);
            }
            return data;
        }

        public static byte[] Copy(byte[] bytes, int index, int count)
        {
            if (count == 0)
                return null;

            int cnt = (index + count - 1) < bytes.Length ? count : bytes.Length - index;
            byte[] result = new byte[cnt];
            for (int i = index; i < index + cnt; i++)
            {
                result[i] = bytes[i];
            }
            return result;
        }

        public static String ToBase64String(String str)
        {
            return Convert.ToBase64String(StringToByteArray(str));
        }

        public static String FromBase64String(String str)
        {
            return ByteArrayToString(Convert.FromBase64String(str));
        }
        #endregion

        #region General Helper Functions
        public static decimal Round(decimal number, int digits)
        {
            //return Math.Round(val, decimals);
            string Temp;
            string Temp2;
            int i, j;
            decimal ResultValue;
            decimal Numbertemp;

            //Obliguem que el separador de decimals sigui el .
            CultureInfo ci = CultureInfo.CurrentCulture;
            NumberFormatInfo ni = (NumberFormatInfo) ci.NumberFormat.Clone();
            //ni.NumberDecimalSeparator = ".";

            Temp = Convert.ToString(number);
            i = Temp.LastIndexOf(ni.NumberDecimalSeparator);
            if (((Temp.Length - (i + 1)) <= digits) || (i == -1))
                return number;

            Temp2 = Temp.Substring(i + digits + 1, 1);
            j = Convert.ToInt32(Temp2);
            Numbertemp = Convert.ToDecimal(Temp.Substring(0, i + digits + 1));
            if (j == 5)
                ResultValue = Numbertemp + (1 / ((decimal)Math.Pow(10, digits)));
            else
                ResultValue = Math.Round(number, digits);

            return ResultValue;
        }

        public static int MonthsBetween(DateTime now, DateTime then)
        {
            //MonthsBetween returns the number of whole months between ANow and AThen. 
            //This number is an approximation, based on an average number of days of 30.4375 per month 
            //(average over 4 years). This means the fractional part of a month is dropped. 
            TimeSpan span = now - then;
            return (int) (span.TotalDays / 30.4375);
        }

        public static int GetDiaSetmana(DateTime data)
        {
            int result = (int)data.DayOfWeek;
            if (result == 0)
                result = 7;
            return result;
        }

        public static String GetAssemblyPath(Assembly assembly)
        {
            Uri u = new Uri(assembly.CodeBase);

            return Path.GetDirectoryName(u.LocalPath) + Path.DirectorySeparatorChar;
        }

        public static String GetAssemblyVersion(Assembly assembly)
        {
            return assembly.GetName().Version.ToString(4);
        }
       
        public static String StringRepeat(String str, int count)
        {
            String result = String.Empty;
            for (int i = 0; i < count; i++)
                result += str;
            return result;
        }
		#endregion

        #region URL
        public static string MakeTinyUrl(string url)
        {
            try
            {
                Uri address = new Uri("http://tinyurl.com/api-create.php?url=" + url); 
                System.Net.WebClient client = new System.Net.WebClient(); 
                string tinyUrl = client.DownloadString(address); 
                return tinyUrl; 
            }
            catch (Exception)
            {
                return url;
            }
        }

        public static string MakeVesCatUrl(string url)
        {
            try
            {
                Uri address = new Uri("http://ves.cat/?url=" + HttpUtility.UrlEncode(url + "&format=text")); 

                System.Net.WebClient client = new System.Net.WebClient();
                string tinyUrl = client.DownloadString(address);
                return tinyUrl;
            }
            catch (Exception)
            {
                return url;
            }
        } 

        #endregion

        #region RSA

        public static String RSA_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----" +
                                                "MIIBOwIBAAJBALCuxAPoKN0Rv2j+1pNfSc4RdtKAcZpOe1dCxeALvnJii4toINr/" +
                                                "RxqOyHOhkFZ1elwCoVS1wZLyvlwoPkVMWs8CAwEAAQJAWj+rGGPKG8IpB7TEXlra" +
                                                "FEyPaU76uFtGXXhaIutcyIf8X92sGdxFKOqzVx2FdrmCKQWerav6MtcBmm/Xs27P" +
                                                "4QIhANxOFL7SGchN56U7q2j7m6JRHPR1BTKuBLL1SGJid98NAiEAzU9L441vpKEI" +
                                                "ryZV7HgSRiTWoLRqmhlR2nLDNhF1iksCIQCndVKfmeSvNUiXeLvamSa8QxvXVfwV" +
                                                "geFghsH2xxKHIQIhALJz7j6HvTaXUfkLlTvI0fluI6/joZT31RHPFqZ0XnopAiB1" +
                                                "7nRJMLtpyB6Mr0l524G6HUGf8Jlltjob05AISJs3vw==" +
                                                "-----END RSA PRIVATE KEY-----";

        public static String RSA_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----" +
                                                "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALCuxAPoKN0Rv2j+1pNfSc4RdtKAcZpO" +
                                                "e1dCxeALvnJii4toINr/RxqOyHOhkFZ1elwCoVS1wZLyvlwoPkVMWs8CAwEAAQ==" +
                                                "-----END PUBLIC KEY-----";
        #endregion

    }
}
