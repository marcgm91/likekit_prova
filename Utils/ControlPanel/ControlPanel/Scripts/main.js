﻿$(document).ready(function () {
    
    $("#product-ws-search-container-btn").live('click', function () {
        KitWSSearch();
    });
});

function KitWSSearch() {

    var search = $('#product-ws-search-text').val();

    $.post('/ControlPanel/Product/WSSearchProduct/?search=' + search, function (data) {
        $('#product-ws-search-container-results-container').html(data);
        $('#product-ws-search-container-results-container').css('display', 'block');
    });
}

function LoadWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL) {

    $('#ASIN').val(ASIN);
    $('#PartnerUrl').val(URL);
}

function ViewWSProductImage(image) {
    window.open(image);
}

function SaveWSProductImage(ImageUrl, amazon_asin) {

    $.post('/ControlPanel/Product/SaveWSImage/?imageurl=' + ImageUrl + '&amazon_asin=' + amazon_asin, function (data) {
        $('#amazon_image').val(data);
    });
}