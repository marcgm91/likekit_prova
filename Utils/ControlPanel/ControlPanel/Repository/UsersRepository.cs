﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControlPanel.Models;
using System.Data;
using Endepro.Common.Database;
using System.Configuration;
using System.IO;
using System.Collections;
using Endepro.Common.Google;
using Endepro.Common.Social;

namespace ControlPanel.Repository
{
    public class UsersRepository : BaseRepository
    {
        public UsersRepository()
        {
        }

        public UsersRepository(DatabaseBase database)
        {
            Database = database;
        }


        public void DeleteUser(User user)
        {
            //Esborrem els kits Comments
            String SQL = String.Format("DELETE FROM KitComment " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Ara hauriem d'esborrar els Kit tags, per evitar errors
            SQL = String.Format("DELETE FROM KitTag WHERE KitID IN (SELECT ID FROM Kit WHERE UserID={0})", user.ID);

            Database.ExecuteCommand(SQL);


            //Ara hauriem d'esborrar els Kit comments, fet als kits de l'usuari
            SQL = String.Format("DELETE FROM KitComment WHERE KitID IN (SELECT ID FROM Kit WHERE UserID={0})", user.ID);

            Database.ExecuteCommand(SQL);


            //Ara hauriem d'esborrar els likes, fet als kits de l'usuari
            SQL = String.Format("DELETE FROM UserLike WHERE KitID IN (SELECT ID FROM Kit WHERE UserID={0})", user.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els kits
            SQL = String.Format("DELETE FROM Kit " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem les notificacions 
            SQL = String.Format("DELETE FROM Notify " +
                                       " WHERE ToUserID={0} OR FromUserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem ProductCategoryTrender
            SQL = String.Format("DELETE FROM ProductCategoryTrender " +
                           " WHERE UserID={0}",
                            user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem els Likes
            SQL = String.Format("DELETE FROM UserLike  " +
                                       " WHERE UserID={0}",
                                        user.ID);
            Database.ExecuteCommand(SQL);

            //Esborrem els Likes de producte
            SQL = String.Format("DELETE FROM UserLikeProduct  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els UserRelation
            SQL = String.Format("DELETE FROM UserRelation  " +
                                       " WHERE UserID={0} OR FollowID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els Wishlist
            SQL = String.Format("DELETE FROM WishList  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem els Nowwith
            SQL = String.Format("DELETE FROM UserStatus  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Finalment esborrem l'usuari
            /*SQL = String.Format("DELETE FROM User " +
                                       " WHERE ID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);*/
        }


        public void updateVendor(User user)
        {
            //Actualitzem el Vendor
            if (user.ID > 0)
            {
                String SQL = String.Empty;

                if (user.VendorID != null)
                    SQL = String.Format("UPDATE User set VendorID={0} WHERE ID={1}", user.VendorID, user.ID);
                else
                    SQL = String.Format("UPDATE User set VendorID=NULL WHERE ID={0}", user.ID);

                Database.ExecuteCommand(SQL);
            }
        }

        public void LoadVendor(User user)
        {
            //Actualitzem el Vendor
            if (user.ID > 0)
            {
                String sql = String.Format("SELECT VendorID FROM User " +
                                       " WHERE ID={0}", user.ID);

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {

                    user.VendorID = Database.GetReaderValueInt64(reader, "VendorID");
                }
            }
        }

    }
}