﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControlPanel.Models;
using System.Data;
using Endepro.Common.Database;
using System.Configuration;
using System.IO;
using System.Collections;
using Endepro.Common.Google;
using Endepro.Common.Social;

namespace ControlPanel.Repository
{
    public class StaticPagesRepository : BaseRepository
    {
        public StaticPagesRepository()
        {
        }

        public StaticPagesRepository(DatabaseBase database)
        {
            Database = database;
        }


        public void updateSEO(StaticPage static_page)
        {
            //Actualitzem el SEO
            if (static_page.ID > 0)
            {
                String SQL = String.Format("UPDATE StaticPage set SeoTitle='{0}', SeoDescription='{1}', SeoKeywords='{2}'" +
                                       " WHERE ID={3}", static_page.SeoTitle, static_page.SeoDescription, static_page.SeoKeywords, static_page.ID);

                Database.ExecuteCommand(SQL);
            }
        }

        public void LoadSEO(StaticPage static_page)
        {
            //Actualitzem el SEO
            if (static_page.ID > 0)
            {
                String sql = String.Format("SELECT SeoTitle, SeoDescription, SeoKeywords FROM StaticPage " +
                                       " WHERE ID={0}", static_page.ID);

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {

                    static_page.SeoTitle = Database.GetReaderValueString(reader, "SeoTitle");
                    static_page.SeoDescription = Database.GetReaderValueString(reader, "SeoDescription");
                    static_page.SeoKeywords = Database.GetReaderValueString(reader, "SeoKeywords");
                }
            }
        }
    }
}