﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Endepro.Common.Database;
using System.Configuration;
using System.Data;

namespace ControlPanel.Repository
{
    public class BaseRepository
    {
        private DatabaseBase _database = null;

        public DatabaseBase Database { get {return _database;} set { _database = value;} }

        public BaseRepository()
        {
        }

        public BaseRepository(DatabaseBase database)
        {
            _database = database;
        }
        
        public void OpenDatabase()
        {
            if (_database == null)
            {
                _database = new DatabaseMySQL(ConfigurationManager.AppSettings["DBHost"],
                                              ConfigurationManager.AppSettings["DBUserName"],
                                              ConfigurationManager.AppSettings["DBPassword"],
                                              ConfigurationManager.AppSettings["DBDatabaseName"]);
                _database.Open();

                //_database.TimeZoneOffset = TimeZoneOffset;
            }
        }

        public void CloseDatabase()
        {
            if (_database!=null)
                _database.Close();
        }

        protected Int64 GetLastAutoIncrement()
        {
            IDbCommand command = Database.CreateCommand("SELECT LAST_INSERT_ID()");
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                    return reader.GetInt64(0);

                return 0;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }
        }
    }
}