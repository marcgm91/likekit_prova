﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using PagedList;

namespace ControlPanel.Controllers
{ 
    public class KitCommentController : Controller
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /KitComment/

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var kitcomment = db.KitComment.Include("Kit").Include("User");

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Username desc" : "";
            ViewBag.DateSortParm = sortOrder == "Created" ? "Created desc" : "Created";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;

            var kitcomments = from s in kitcomment
                       select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                kitcomments = kitcomments.Where(s => s.Comment.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Username desc":
                    kitcomments = kitcomments.OrderByDescending(s => s.User.Username);
                    break;
                default:
                    kitcomments = kitcomments.OrderBy(s => s.Created);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(kitcomments.ToPagedList(pageNumber, pageSize));


        }

        //
        // GET: /KitComment/Details/5

        public ViewResult Details(long id)
        {
            KitComment kitcomment = db.KitComment.Single(k => k.ID == id);
            return View(kitcomment);
        }

        //
        // GET: /KitComment/Create

        public ActionResult Create()
        {
            ViewBag.KitID = new SelectList(db.Kit, "ID", "Comment");
            ViewBag.UserID = new SelectList(db.User, "ID", "Username");
            return View();
        } 

        //
        // POST: /KitComment/Create

        [HttpPost]
        public ActionResult Create(KitComment kitcomment)
        {
            if (ModelState.IsValid)
            {
                db.KitComment.AddObject(kitcomment);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.KitID = new SelectList(db.Kit, "ID", "Comment", kitcomment.KitID);
            ViewBag.UserID = new SelectList(db.User, "ID", "Username", kitcomment.UserID);
            return View(kitcomment);
        }
        
        //
        // GET: /KitComment/Edit/5
 
        public ActionResult Edit(long id)
        {
            KitComment kitcomment = db.KitComment.Single(k => k.ID == id);
            ViewBag.KitID = new SelectList(db.Kit, "ID", "Comment", kitcomment.KitID);
            ViewBag.UserID = new SelectList(db.User, "ID", "Username", kitcomment.UserID);
            return View(kitcomment);
        }

        //
        // POST: /KitComment/Edit/5

        [HttpPost]
        public ActionResult Edit(KitComment kitcomment)
        {
            if (ModelState.IsValid)
            {
                kitcomment.Modified = DateTime.Now;
                db.KitComment.Attach(kitcomment);
                db.ObjectStateManager.ChangeObjectState(kitcomment, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KitID = new SelectList(db.Kit, "ID", "Comment", kitcomment.KitID);
            ViewBag.UserID = new SelectList(db.User, "ID", "Username", kitcomment.UserID);
            return View(kitcomment);
        }

        //
        // GET: /KitComment/Delete/5
 
        public ActionResult Delete(long id)
        {
            KitComment kitcomment = db.KitComment.Single(k => k.ID == id);
            return View(kitcomment);
        }

        //
        // POST: /KitComment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            KitComment kitcomment = db.KitComment.Single(k => k.ID == id);
            db.KitComment.DeleteObject(kitcomment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}