﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using PagedList;
using System.Web.Configuration;
using EndeproLib.Endepro.Common;

namespace ControlPanel.Controllers
{ 
    public class ProductController : Controller
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /Product/

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var product = db.Product.Include("Language").Include("ProductCategory");

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "FirstName desc" : "";
            ViewBag.DateSortParm = sortOrder == "Created" ? "Created desc" : "Created";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;

            var products = from s in product
                        select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(s => s.Name.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Name desc":
                    products = products.OrderByDescending(s => s.Name);
                    break;
                default:
                    products = products.OrderBy(s => s.Name);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(products.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Product/Details/5

        public ViewResult Details(long id)
        {
            Product product = db.Product.Single(p => p.ID == id);
            return View(product);
        }

        //
        // GET: /Product/Create

        public ActionResult Create()
        {
            ViewBag.LanguageID = new SelectList(db.Language, "ID", "Code");
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ID", "Name");
            return View();
        } 

        //
        // POST: /Product/Create

        [HttpPost]
        public ActionResult Create(Product product, HttpPostedFileBase NewImage, String amazon_image)
        {
            if ((NewImage == null || NewImage.ContentLength <= 0) && ((amazon_image == String.Empty)) || (amazon_image == null))
            {
                ModelState.AddModelError("Image", "Product image is required.");
            }

            if (ModelState.IsValid)
            {
                if (NewImage != null)
                {
                    string FilesFolder = WebConfigurationManager.AppSettings["PathImages"].ToString();
                    string fileName = Endepro.Common.Crypto.GetMD5Hash(System.IO.Path.GetFileName(NewImage.FileName) + DateTime.Now.ToString());
                    var path = System.IO.Path.Combine(FilesFolder, fileName);
                    NewImage.SaveAs(path);
                    product.Image = fileName;
                }
                else if (amazon_image != String.Empty)
                {
                    product.Image = amazon_image;
                }
                
                product.Created = DateTime.Now;
                product.Modified = DateTime.Now;

                if (product.PartnerUrl == null)
                    product.PartnerUrl = String.Empty;

                if (product.ASIN == null)
                    product.ASIN = String.Empty;

                if (product.Permalink == null)
                    product.Permalink = String.Empty;
                
                db.Product.AddObject(product);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.LanguageID = new SelectList(db.Language, "ID", "Code", product.LanguageID);
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ID", "Name", product.ProductCategoryID);
            return View(product);
        }
        
        //
        // GET: /Product/Edit/5
 
        public ActionResult Edit(long id)
        {
            Product product = db.Product.Single(p => p.ID == id);
            ViewBag.LanguageID = new SelectList(db.Language, "ID", "Code", product.LanguageID);
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ID", "Name", product.ProductCategoryID);
            return View(product);
        }

        //
        // POST: /Product/Edit/5

        [HttpPost]
        public ActionResult Edit(Product product, HttpPostedFileBase NewImage, String amazon_image)
        {
            if (ModelState.IsValid)
            {
                if (NewImage != null)
                {
                    string FilesFolder = WebConfigurationManager.AppSettings["PathImages"].ToString();
                    string fileName = Endepro.Common.Crypto.GetMD5Hash(System.IO.Path.GetFileName(NewImage.FileName) + DateTime.Now.ToString());
                    var path = System.IO.Path.Combine(FilesFolder, fileName);
                    product.Image = fileName;
                    NewImage.SaveAs(path);
                }
                else if (amazon_image != String.Empty)
                {
                    product.Image = amazon_image;
                }

                product.Modified = DateTime.Now;

                if (product.PartnerUrl == null)
                    product.PartnerUrl = String.Empty;

                if (product.ASIN == null)
                    product.ASIN = String.Empty;

                if (product.Permalink == null)
                    product.Permalink = String.Empty;

                db.Product.Attach(product);
                db.ObjectStateManager.ChangeObjectState(product, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LanguageID = new SelectList(db.Language, "ID", "Code", product.LanguageID);
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ID", "Name", product.ProductCategoryID);
            return View(product);
        }

        //
        // GET: /Product/Delete/5
 
        public ActionResult Delete(long id)
        {
            Product product = db.Product.Single(p => p.ID == id);
            return View(product);
        }

        //
        // POST: /Product/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Product product = db.Product.Single(p => p.ID == id);
            db.Product.DeleteObject(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult WSSearchProduct(String search)
        {
            //var products = Repository.WSSearchProductsKitHtml(search, 20);

            String sHtml = String.Empty;
            int limit = 25;

            String sAssociateTag = "likekit08-21";
            String sAssociateTagUK = "likekit04-21";
            String sAWSAccessKeyID = "AKIAIBDCTHDECKPMLEFQ";
            String sAWSSecretAccessKey = "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";

            List<AmazonResponse> books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
            List<AmazonResponse> series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
            List<AmazonResponse> films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
            List<AmazonResponse> music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
            List<AmazonResponse> musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);

            //Products reorder
            List<AmazonResponse> products = new List<AmazonResponse>();
            List<AmazonResponse> products_hidden = new List<AmazonResponse>();

            int count_tmp = 0;
            int count_products_hidden = 0;
            for (int count_products = 0; (count_products < limit) && (count_tmp < limit); count_products++)
            {
                if (books.Count > count_products)
                {
                    products.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products.Add(musicEs[count_products]);
                    count_tmp++;
                }

                count_products_hidden = count_products;
            }

            //Omplim la segona pàgina
            for (int count_products = count_products_hidden; (count_products < (limit + limit)) && (count_tmp < (limit + limit)); count_products++)
            {
                if (books.Count > count_products)
                {
                    products_hidden.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products_hidden.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products_hidden.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products_hidden.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products_hidden.Add(musicEs[count_products]);
                    count_tmp++;
                }
            }

            int count = 0;

            foreach (AmazonResponse product in products)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                sHtml += "<div class=\"product-ws-search-result\">\n" +
                          "<div class=\"product-ws-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + "</div>\n" +
                          "<div class=\"product-ws-search-result-category\">" + product.Category + "</div>\n" +
                          "<div class=\"product-ws-search-result-load-container\" onclick=\"LoadWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "');\">Cargar datos</div>\n" +
                          "<div class=\"product-ws-search-result-load-container\" onclick=\"ViewWSProductImage('" + product.LargeImage + "');\">Ver imagen</div>\n" +
                          "<div class=\"product-ws-search-result-load-container\" onclick=\"SaveWSProductImage('" + product.LargeImage + "','" + product.ASIN + "');\">Cargar img</div>\n" +
                          "</div>\n";
            }


            /*if (products_hidden.Count > 0)
            {
                sHtml += "<div class=\"timeline-kit-search-result\">\n" +
                            "<div id=\"timeline-kit-search-result-more-results\">Más resultados</div></div>\n" +
                            "</div>\n";
            }

            sHtml += "<div id=\"timeline-kit-search-result-more-results-container\">\n";*/

            foreach (AmazonResponse product in products_hidden)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                sHtml += "<div class=\"product-ws-search-result\">\n" +
                          "<div class=\"product-ws-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + "</div>\n" +
                          "<div class=\"product-ws-search-result-category\">" + product.Category + "</div>\n" +
                          "<div class=\"product-ws-search-result-load-container\" onclick=\"LoadWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "');\">Cargar datos</div>\n" +
                          "<div class=\"product-ws-search-result-load-container\" onclick=\"ViewWSProductImage('" + product.LargeImage + "');\">Ver imagen</div>\n" +
                          "<div class=\"product-ws-search-result-load-container\" onclick=\"SaveWSProductImage('" + product.LargeImage + "','" + product.ASIN + "');\">Cargar img</div>\n" +
                          "</div>\n";
            }

            //sHtml += "</div>\n";

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            {
                sHtml = "<div class=\"product-ws-search-result-noresults-title\">Disculpa, no hemos encontrado nada que corresponda.<br/><br/>Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";
                //"<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";
            }

            return PartialView("Html", (object)sHtml);
        }

        public ActionResult SaveWSImage(String imageurl, String amazon_asin)
        {
            String sFileName = String.Format("{0}", amazon_asin);
            try
            {
                //Tractem la imatge
                System.Drawing.Image image = null;

                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageurl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                System.IO.Stream stream = webResponse.GetResponseStream();

                image = System.Drawing.Image.FromStream(stream);

                webResponse.Close();

                //string imageName = Path.GetFileName(picsquare);

                string fileName = System.IO.Path.Combine(WebConfigurationManager.AppSettings["PathImages"], sFileName);
                image.Save(fileName);
            }
            catch (Exception ex)
            {
            }

            return PartialView("Html", (object)sFileName);
        }
    }
}