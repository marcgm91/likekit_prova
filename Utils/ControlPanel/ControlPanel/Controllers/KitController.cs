﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using PagedList;

namespace ControlPanel.Controllers
{ 
    public class KitController : Controller
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /Kit/

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var kit = db.Kit.Include("User").Include("Product");

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Username desc" : "";
            ViewBag.DateSortParm = sortOrder == "Created" ? "Created desc" : "Created";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;

            var kits = from s in kit
                        select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                kits = kits.Where(s => s.Comment.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Username desc":
                    kits = kits.OrderByDescending(s => s.User.Username);
                    break;
                default:
                    kits = kits.OrderBy(s => s.Created);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(kits.ToPagedList(pageNumber, pageSize));

        }

        //
        // GET: /Kit/Details/5

        public ViewResult Details(long id)
        {
            Kit kit = db.Kit.Single(k => k.ID == id);
            return View(kit);
        }

        //
        // GET: /Kit/Create

        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.User, "ID", "Username");
            ViewBag.ProductID = new SelectList(db.Product, "ID", "Name");
            return View();
        } 

        //
        // POST: /Kit/Create

        [HttpPost]
        public ActionResult Create(Kit kit)
        {
            if (ModelState.IsValid)
            {
                db.Kit.AddObject(kit);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.UserID = new SelectList(db.User, "ID", "Username", kit.UserID);
            ViewBag.ProductID = new SelectList(db.Product, "ID", "Name", kit.ProductID);
            return View(kit);
        }
        
        //
        // GET: /Kit/Edit/5
 
        public ActionResult Edit(long id)
        {
            Kit kit = db.Kit.Single(k => k.ID == id);
            ViewBag.UserID = new SelectList(db.User, "ID", "Username", kit.UserID);
            ViewBag.ProductID = new SelectList(db.Product, "ID", "Name", kit.ProductID);
            return View(kit);
        }

        //
        // POST: /Kit/Edit/5

        [HttpPost]
        public ActionResult Edit(Kit kit)
        {
            if (ModelState.IsValid)
            {
                kit.Modified = DateTime.Now;
                db.Kit.Attach(kit);
                db.ObjectStateManager.ChangeObjectState(kit, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.User, "ID", "Username", kit.UserID);
            ViewBag.ProductID = new SelectList(db.Product, "ID", "Name", kit.ProductID);
            return View(kit);
        }

        //
        // GET: /Kit/Delete/5
 
        public ActionResult Delete(long id)
        {
            Kit kit = db.Kit.Single(k => k.ID == id);
            return View(kit);
        }

        //
        // POST: /Kit/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Kit kit = db.Kit.Single(k => k.ID == id);
            db.Kit.DeleteObject(kit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}