﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;

namespace ControlPanel.Controllers
{ 
    public class LandingUsersController : Controller
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /LandingUsers/

        public ViewResult Index()
        {
            return View(db.BetaUser.ToList());
        }

        //
        // GET: /LandingUsers/Details/5

        public ViewResult Details(long id)
        {
            BetaUser betauser = db.BetaUser.Single(b => b.ID == id);
            return View(betauser);
        }

        //
        // GET: /LandingUsers/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /LandingUsers/Create

        [HttpPost]
        public ActionResult Create(BetaUser betauser)
        {
            if (ModelState.IsValid)
            {
                db.BetaUser.AddObject(betauser);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(betauser);
        }
        
        //
        // GET: /LandingUsers/Edit/5
 
        public ActionResult Edit(long id)
        {
            BetaUser betauser = db.BetaUser.Single(b => b.ID == id);
            return View(betauser);
        }

        //
        // POST: /LandingUsers/Edit/5

        [HttpPost]
        public ActionResult Edit(BetaUser betauser)
        {
            if (ModelState.IsValid)
            {
                db.BetaUser.Attach(betauser);
                db.ObjectStateManager.ChangeObjectState(betauser, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(betauser);
        }

        //
        // GET: /LandingUsers/Delete/5
 
        public ActionResult Delete(long id)
        {
            BetaUser betauser = db.BetaUser.Single(b => b.ID == id);
            return View(betauser);
        }

        //
        // POST: /LandingUsers/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            BetaUser betauser = db.BetaUser.Single(b => b.ID == id);
            db.BetaUser.DeleteObject(betauser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}