﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using ControlPanel.Repository;
using PagedList;

namespace ControlPanel.Controllers
{
    public class UserController : EController<UsersRepository>
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /User/

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var user = db.User.Include("City").Include("Country");

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "FirstName desc" : "";
            ViewBag.DateSortParm = sortOrder == "Created" ? "Created desc" : "Created";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;

            var users = from s in user
                           select s; 


            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.LastName.ToUpper().Contains(searchString.ToUpper())
                                       || s.FirstName.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "FirstName desc":
                    users = users.OrderByDescending(s => s.FirstName);
                    break;
                default:
                    users = users.OrderBy(s => s.FirstName);
                    break;
            } 


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));

            
        }

        //
        // GET: /User/Details/5

        public ViewResult Details(long id)
        {
            User user = db.User.Single(u => u.ID == id);
            return View(user);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            ViewBag.CityID = new SelectList(db.City, "ID", "Name");
            ViewBag.CountryID = new SelectList(db.Country, "ID", "Name");
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name");
            return View();
        } 

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                db.User.AddObject(user);
                db.SaveChanges();

                Repository.updateVendor(user);

                return RedirectToAction("Index");  
            }

            ViewBag.CityID = new SelectList(db.City, "ID", "Name", user.CityID);
            ViewBag.CountryID = new SelectList(db.Country, "ID", "Name", user.CountryID);
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name", user.VendorID);
            return View(user);
        }
        
        //
        // GET: /User/Edit/5
 
        public ActionResult Edit(long id)
        {
            User user = db.User.Single(u => u.ID == id);
            ViewBag.CityID = new SelectList(db.City, "ID", "Name", user.CityID);
            ViewBag.CountryID = new SelectList(db.Country, "ID", "Name", user.CountryID);

            //Carreguem les dades del Vendor
            Repository.LoadVendor(user);
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name", user.VendorID);

            return View(user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                user.Modified = DateTime.Now;
                db.User.Attach(user);
                db.ObjectStateManager.ChangeObjectState(user, EntityState.Modified);
                db.SaveChanges();

                //Modifiquem les dades del Vendor
                Repository.updateVendor(user);

                return RedirectToAction("Index");
            }
            ViewBag.CityID = new SelectList(db.City, "ID", "Name", user.CityID);
            ViewBag.CountryID = new SelectList(db.Country, "ID", "Name", user.CountryID);
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name", user.VendorID);
            return View(user);
        }

        //
        // GET: /User/Delete/5
 
        public ActionResult Delete(long id)
        {
            User user = db.User.Single(u => u.ID == id);
            return View(user);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            //Abans d'esborrar l'usuari esborrem les dades associades
            User user = db.User.Single(u => u.ID == id);

            Repository.DeleteUser(user);

            db.User.DeleteObject(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}