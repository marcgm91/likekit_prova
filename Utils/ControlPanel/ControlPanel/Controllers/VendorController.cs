﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using ControlPanel.Repository;
using System.Web.Configuration;
using PagedList;

namespace ControlPanel.Controllers
{
    public class VendorController : Controller
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /Vendor/

        /*public ViewResult Index()
        {*/
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            //return View(db.Vendor.ToList());
            var vendor = db.Vendor;

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Name desc" : "";
            ViewBag.DateSortParm = sortOrder == "Created" ? "Created desc" : "Created";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;

            var vendors = from s in vendor
                       select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                vendors = vendors.Where(s => s.Name.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Name desc":
                    vendors = vendors.OrderByDescending(s => s.Name);
                    break;
                default:
                    vendors = vendors.OrderBy(s => s.Created);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(vendors.ToPagedList(pageNumber, pageSize));
        }


        //
        // GET: /Vendor/Details/5

        public ViewResult Details(long id)
        {
            Vendor vendor = db.Vendor.Single(s => s.ID == id);
            return View(vendor);
        }

        //
        // GET: /Vendor/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Vendor/Create

        [HttpPost]
        public ActionResult Create(Vendor vendor, HttpPostedFileBase NewImage)
        {
            if (ModelState.IsValid)
            {
                /*if (vendor.Logo == null)
                    vendor.Logo = String.Empty;*/

                vendor.Created = DateTime.Now;
                vendor.Modified = DateTime.Now;

                if (NewImage != null)
                {
                    string FilesFolder = WebConfigurationManager.AppSettings["PathImages"].ToString();
                    string fileName = Endepro.Common.Crypto.GetMD5Hash(System.IO.Path.GetFileName(NewImage.FileName) + DateTime.Now.ToString());
                    var path = System.IO.Path.Combine(FilesFolder, fileName);
                    NewImage.SaveAs(path);
                    vendor.Logo = fileName;
                }


                db.Vendor.AddObject(vendor);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(vendor);
        }

        //
        // GET: /Vendor/Edit/5
        [ValidateInput(false)]
        public ActionResult Edit(long id)
        {
            Vendor vendor = db.Vendor.Single(s => s.ID == id);

            return View(vendor);
        }

        //
        // POST: /Vendor/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Vendor vendor, HttpPostedFileBase NewImage)
        {
            if (ModelState.IsValid)
            {
                /*if (vendor.Logo == null)
                    vendor.Logo = String.Empty;*/

                if (NewImage != null)
                {
                    string FilesFolder = WebConfigurationManager.AppSettings["PathImages"].ToString();
                    string fileName = Endepro.Common.Crypto.GetMD5Hash(System.IO.Path.GetFileName(NewImage.FileName) + DateTime.Now.ToString());
                    var path = System.IO.Path.Combine(FilesFolder, fileName);
                    NewImage.SaveAs(path);
                    vendor.Logo = fileName;
                }

                vendor.Modified = System.DateTime.Now;
                db.Vendor.Attach(vendor);
                db.ObjectStateManager.ChangeObjectState(vendor, EntityState.Modified);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(vendor);
        }

        //
        // GET: /Vendor/Delete/5

        public ActionResult Delete(long id)
        {
            Vendor vendor = db.Vendor.Single(s => s.ID == id);
            return View(vendor);
        }

        //
        // POST: /Vendor/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            Vendor vendor = db.Vendor.Single(s => s.ID == id);
            db.Vendor.DeleteObject(vendor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}