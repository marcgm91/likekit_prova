﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using PagedList;

namespace ControlPanel.Controllers
{ 
    public class ProductVendorController : Controller
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /ProductVendor/

        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var product_vendor = db.ProductVendor.Include("Vendor").Include("Product");

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "Product.Name desc" : "";
            ViewBag.DateSortParm = sortOrder == "Created" ? "Created desc" : "Created";

            if (Request.HttpMethod == "GET")
            {
                searchString = currentFilter;
            }
            else
            {
                page = 1;
            }
            ViewBag.CurrentFilter = searchString;

            var product_vendors = from s in product_vendor
                        select s;


            if (!String.IsNullOrEmpty(searchString))
            {
                product_vendors = product_vendors.Where(s => s.Product.Name.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Product.Name desc":
                    product_vendors = product_vendors.OrderByDescending(s => s.Product.Name);
                    break;
                default:
                    product_vendors = product_vendors.OrderBy(s => s.Created);
                    break;
            }


            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(product_vendors.ToPagedList(pageNumber, pageSize));

        }

        //
        // GET: /ProductVendor/5

        public ViewResult IndexProduct(long id)
        {
            var product_vendor = db.ProductVendor.Include("Vendor").Include("Product");

            var product_vendors = from s in product_vendor
                                  select s;

            product_vendors = product_vendors.Where(s => s.Product.ID == id);
            product_vendors = product_vendors.OrderBy(s => s.Created);

            ViewBag.ProductID = id;

            return View(product_vendors);
        }

        //
        // GET: /ProductVendor/Details/5

        public ViewResult Details(long id)
        {
            ProductVendor product_vendor = db.ProductVendor.Single(k => k.ID == id);
            return View(product_vendor);
        }

        //
        // GET: /ProductVendor/Create

        public ActionResult Create()
        {
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name");

            List<Product> productList = db.Product.OrderBy(m => m.Name).ToList();

            //ViewBag.ProductID = new SelectList(db.Product, "ID", "Name");
            ViewBag.ProductID = new SelectList(productList, "ID", "Name");

            return View();
        }

        //
        // GET: /ProductVendor/CreateProduct/5
        public ActionResult CreateProduct(long id)
        {
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name");

            List<Product> productList = db.Product.OrderBy(m => m.Name).ToList();

            //ViewBag.ProductID = new SelectList(db.Product, "ID", "Name");
            ViewBag.ProductID = new SelectList(productList, "ID", "Name", id);

            ViewBag.FromProductID = id;

            return View();
        } 

        //
        // POST: /ProductVendor/Create

        [HttpPost]
        public ActionResult Create(ProductVendor product_vendor)
        {
            if (ModelState.IsValid)
            {
                product_vendor.Created = DateTime.Now;
                product_vendor.Modified = DateTime.Now;
                db.ProductVendor.AddObject(product_vendor);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name", product_vendor.VendorID);
            ViewBag.ProductID = new SelectList(db.Product, "ID", "Name", product_vendor.ProductID);
            return View(product_vendor);
        }
        
        //
        // GET: /ProductVendor/Edit/5
 
        public ActionResult Edit(long id)
        {
            ProductVendor product_vendor = db.ProductVendor.Single(k => k.ID == id);
            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name", product_vendor.VendorID);


            List<Product> productList = db.Product.OrderBy(m => m.Name).ToList();

            //ViewBag.ProductID = new SelectList(db.Product, "ID", "Name");
            ViewBag.ProductID = new SelectList(productList, "ID", "Name", product_vendor.ProductID);

            //ViewBag.ProductID = new SelectList(db.Product, "ID", "Name", product_vendor.ProductID);
            return View(product_vendor);
        }

        //
        // POST: /ProductVendor/Edit/5

        [HttpPost]
        public ActionResult Edit(ProductVendor product_vendor)
        {
            if (ModelState.IsValid)
            {
                product_vendor.Modified = DateTime.Now;
                db.ProductVendor.Attach(product_vendor);
                db.ObjectStateManager.ChangeObjectState(product_vendor, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VendorID = new SelectList(db.Vendor, "ID", "Name", product_vendor.VendorID);
            ViewBag.ProductID = new SelectList(db.Product, "ID", "Name", product_vendor.ProductID);
            return View(product_vendor);
        }

        //
        // GET: /ProductVendor/Delete/5
 
        public ActionResult Delete(long id)
        {
            ProductVendor product_vendor = db.ProductVendor.Single(k => k.ID == id);
            return View(product_vendor);
        }

        //
        // POST: /ProductVendor/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            ProductVendor product_vendor = db.ProductVendor.Single(k => k.ID == id);
            db.ProductVendor.DeleteObject(product_vendor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}