﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Models;
using ControlPanel.Repository;
using System.Web.Configuration;

namespace ControlPanel.Controllers
{ 
    public class StaticPagesController  : EController<StaticPagesRepository>
    {
        private likekitEntities db = new likekitEntities();

        //
        // GET: /StaticPages/

        public ViewResult Index()
        {
            return View(db.StaticPage.ToList());
        }


        //
        // POST: /StaticPages/
        [HttpPost]
        public ViewResult Index(HttpPostedFileBase mediakit_file)
        {
            if (mediakit_file != null)
            {
                string FilesFolder = WebConfigurationManager.AppSettings["PathImages"].ToString();
                string fileName = "Mediakit.zip";
                var path = System.IO.Path.Combine(FilesFolder, fileName);
                mediakit_file.SaveAs(path);
            }


            return View(db.StaticPage.ToList());
        }


        //
        // GET: /StaticPages/Details/5

        public ViewResult Details(long id)
        {
            StaticPage staticpage = db.StaticPage.Single(s => s.ID == id);
            return View(staticpage);
        }

        //
        // GET: /StaticPages/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /StaticPages/Create

        [HttpPost]
        public ActionResult Create(StaticPage staticpage)
        {
            if (ModelState.IsValid)
            {
                db.StaticPage.AddObject(staticpage);
                db.SaveChanges();
                Repository.updateSEO(staticpage);

                return RedirectToAction("Index");  
            }

            return View(staticpage);
        }
        
        //
        // GET: /StaticPages/Edit/5
        [ValidateInput(false)] 
        public ActionResult Edit(long id)
        {
            StaticPage staticpage = db.StaticPage.Single(s => s.ID == id);

            //Carreguem les dades de SEO
            Repository.LoadSEO(staticpage);

            return View(staticpage);
        }

        //
        // POST: /StaticPages/Edit/5

        [HttpPost]
        [ValidateInput(false)] 
        public ActionResult Edit(StaticPage staticpage)
        {
            if (ModelState.IsValid)
            {
                staticpage.Modified = System.DateTime.Now;
                db.StaticPage.Attach(staticpage);
                db.ObjectStateManager.ChangeObjectState(staticpage, EntityState.Modified);
                db.SaveChanges();

                //Modifiquem les dades de SEO
                Repository.updateSEO(staticpage);

                return RedirectToAction("Index");
            }
            return View(staticpage);
        }

        //
        // GET: /StaticPages/Delete/5
 
        public ActionResult Delete(long id)
        {
            StaticPage staticpage = db.StaticPage.Single(s => s.ID == id);
            return View(staticpage);
        }

        //
        // POST: /StaticPages/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            StaticPage staticpage = db.StaticPage.Single(s => s.ID == id);
            db.StaticPage.DeleteObject(staticpage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}