﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endepro.Common.Database;
using System.Configuration;
using System.Data;
using System.Collections;

namespace Trenders
{
    /// <summary>
    /// Objecte amb la informació dels Usuaris
    /// </summary>
    public class MailUser
    {
        public Int64 ID = 0;
        public String UserName = String.Empty;
        public Int16 iUserType = 0;
    }


    class Program
    {
        static void Main(string[] args)
        {
            DatabaseBase _database = null;
            IDbCommand command = null;
            IDataReader reader = null;
            try
            {
                _database = new DatabaseMySQL(ConfigurationManager.AppSettings["DBHost"],
                                                  ConfigurationManager.AppSettings["DBUserName"],
                                                  ConfigurationManager.AppSettings["DBPassword"],
                                                  ConfigurationManager.AppSettings["DBDatabaseName"]);
                _database.Open();

                command = _database.CreateCommand("select count(*) as Total from User");

                reader = command.ExecuteReader();

                reader.Read();

                Int64 totalUsers = _database.GetReaderValueInt64(reader, "Total");

                Double percentageUsers = totalUsers * 0.05;  //5%

                if (percentageUsers < 1.0)
                    percentageUsers = 1.0;

                totalUsers = (Int64)Math.Ceiling(percentageUsers);
                
                reader.Close();
                reader.Dispose();
                command.Dispose();

                String TemplateBody = String.Empty;
                String TemplateSubject = String.Empty;

                //Obtenim la plantilla de correu pels trenders
                String SQL_template = String.Format("select ID, Subject, Body, Type, LanguageID, Created, Modified from MailTemplate where Type=10 AND LanguageID=1");
                command = _database.CreateCommand(SQL_template);
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    TemplateSubject = _database.GetReaderValueString(reader, "Subject");
                    TemplateBody = _database.GetReaderValueString(reader, "Body");
                }

                reader.Close();
                reader.Dispose();
                command.Dispose();

                String SQL = String.Format("select a.ID, a.UserType, a.UserName, " +
                                        "(select count(b.ID) from Kit b where b.UserID=a.ID and b.ParentKitID is NULL)+ " +
                                        "(select count(c.ID) from Kit c where c.ParentKitID IN (select kt.ID from Kit kt where kt.UserID=a.ID))+ " +
                                        "(select count(d.ID) from UserLike d, Kit kt where d.KitID = kt.ID AND kt.UserID=a.ID)+ " +
                                        "(select count(e.ID) from KitComment e where e.KitID IN (select kt.ID from Kit kt where kt.UserID=a.ID) and e.Visible=1) as Total " +
                                        "from User a where a.ID <> 22 " +
                                        "order by total desc limit {0}", totalUsers);

                command = _database.CreateCommand(SQL);

                reader = command.ExecuteReader();

                ArrayList users = new ArrayList();

                while (reader.Read())
                {
                    MailUser obj = new MailUser();

                    obj.ID = _database.GetReaderValueInt64(reader, "ID");
                    obj.UserName = _database.GetReaderValueString(reader, "Username");
                    obj.iUserType = _database.GetReaderValueInt16(reader, "UserType");

                    users.Add(obj);
                }

                reader.Close();
                reader.Dispose();
                command.Dispose();

                SQL = String.Format("update User set UserType=0");
                command = _database.CreateCommand(SQL);
                command.ExecuteNonQuery();
                command.Dispose();

                //for (int i = 0; i < users.Count; i++)
                foreach(MailUser user in users) 
                {
                    SQL = String.Format("update User set UserType=1 where ID={0}",user.ID);
                    command = _database.CreateCommand(SQL);
                    command.ExecuteNonQuery();
                    command.Dispose();

                    //Si no era trender generem una entrada per la cúa de correus
                    if (user.iUserType == 0)
                    {
                        String SQL_mail = String.Format("INSERT INTO Mail (Sender, Subject, Body, Status, Created, Modified) " +
                            "values ('{0}','{1}','{2}',{3},'{4}','{5}')",
                            _database.ParseStringToSQL(user.UserName),
                            _database.ParseStringToSQL(TemplateSubject),
                            _database.ParseStringToSQL(TemplateBody),
                            0,
                            _database.ParseDateTimeToSQL(System.DateTime.Now),
                            _database.ParseDateTimeToSQL(System.DateTime.Now)
                            );

                        command = _database.CreateCommand(SQL_mail);
                        command.ExecuteNonQuery();
                        command.Dispose();
                    }
                }

                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (reader!=null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (command != null)
                    command.Dispose();

                if (_database!=null)
                    _database.Close();
            }
        }
    }
}
