--	Accessos a la home
--	Usuaris que es donen d'alta

	select count(ID) FROM User;

--	Temps d'estada a cada fitxa de recomanaci� (a estudiar)

--	N�mero de recomanacions consultades
	select count(ID) FROM AccessLog where OperationType = 0 and ObjectType=1;
--	N�mero de recomanacions consultades per categoria
	-- 1 Llibres
		select count(al.ID) FROM AccessLog al, Kit kt, Product p where al.OperationType = 0 and al.ObjectType=1 and kt.ID = al.RefID and kt.ProductID = p.ID and p.ProductCategoryID = 1;
	-- 2 Series
		select count(al.ID) FROM AccessLog al, Kit kt, Product p where al.OperationType = 0 and al.ObjectType=1 and kt.ID = al.RefID and kt.ProductID = p.ID and p.ProductCategoryID = 2;
	-- 3 Cine
		select count(al.ID) FROM AccessLog al, Kit kt, Product p where al.OperationType = 0 and al.ObjectType=1 and kt.ID = al.RefID and kt.ProductID = p.ID and p.ProductCategoryID = 3;
	-- 4 M�sica
		select count(al.ID) FROM AccessLog al, Kit kt, Product p where al.OperationType = 0 and al.ObjectType=1 and kt.ID = al.RefID and kt.ProductID = p.ID and p.ProductCategoryID = 4;

--	N�mero de recomanacions que s'inicien i els que s'acaben
	--iniciat
	select count(ID) FROM AccessLog where OperationType = 2;
	--iniciat i acabat
	SELECT count( ID ) FROM AccessLog WHERE OperationType =2 AND Published IS NOT NULL;

--	N�mero de recomanacions que s'han iniciat i parat per que no han trobat el producte
	SELECT count( ID ) FROM AccessLog WHERE OperationType =2 AND Published IS NULL;

--	N�mero d'enlla�os a prove�dors clickats

--	N�mero d'enlla�os a prove�dors clickats per categoria

--	N�mero de comentaris
	SELECT count( ID ) FROM KitComment;
	
--	N�mero de rekits
	SELECT count( ID ) FROM Kit where (ParentKitID IS NOT NULL) AND (ParentKitID > 0);

--	N�mero de rekits per categor�a
	-- 1 Llibres
		SELECT count( kt.ID ) FROM Kit kt, Product p where (kt.ParentKitID IS NOT NULL) AND (kt.ParentKitID > 0) and kt.ProductID = p.ID and p.ProductCategoryID = 1;
	-- 2 Series
		SELECT count( kt.ID ) FROM Kit kt, Product p where (kt.ParentKitID IS NOT NULL) AND (kt.ParentKitID > 0) and kt.ProductID = p.ID and p.ProductCategoryID = 2;
	-- 3 Cine
		SELECT count( kt.ID ) FROM Kit kt, Product p where (kt.ParentKitID IS NOT NULL) AND (kt.ParentKitID > 0) and kt.ProductID = p.ID and p.ProductCategoryID = 3;
	-- 4 M�sica	
		SELECT count( kt.ID ) FROM Kit kt, Product p where (kt.ParentKitID IS NOT NULL) AND (kt.ParentKitID > 0) and kt.ProductID = p.ID and p.ProductCategoryID = 4;
		
		
		

--	N�mero de likes
	SELECT count( ID ) FROM UserLike;

--	N�mero de likes per categor�a

--	N�mero de trenders totals
	select count(ID) FROM User where UserType = 1;

--	N�mero de trenders per categoria de productes
