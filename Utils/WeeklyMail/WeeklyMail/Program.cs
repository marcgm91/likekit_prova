﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endepro.Common.Database;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;

namespace WeeklyMail
{
    /// <summary>
    /// Objecte amb la informació dels Usuaris
    /// </summary>
    public class MailUser
    {
        public Int64 ID = 0;
        public String UserName = String.Empty;
        public Int16 iUserType = 0;
        public Int16 TotalKits = 0;
        public Int16 TotalReKits = 0;
        public Int16 TotalLikes = 0;
        public Int16 TotalComments = 0;
        public Int16 TotalFollowers = 0;
        public Int16 Total = 0;
        public Int64 LanguageID = 1;
    }

    class Program
    {
        static void Main(string[] args)
        {
            DatabaseBase _database = null;
            IDbCommand command = null;
            IDataReader reader = null;
            try
            {
                _database = new DatabaseMySQL(ConfigurationManager.AppSettings["DBHost"],
                                                  ConfigurationManager.AppSettings["DBUserName"],
                                                  ConfigurationManager.AppSettings["DBPassword"],
                                                  ConfigurationManager.AppSettings["DBDatabaseName"]);
                _database.Open();

                //Calcular la data inici (fa una setmana)
                DateTime dt_week = DateTime.Now;
                dt_week = dt_week.AddDays(-7); // gives the one week before date

                //Hem de recòrrer tots els usuaris
                //1.Calcular kits fets durant la setmana
                //2.Likes rebuts durant la setmana
                //3.Rekits rebuts durant la setmana
                //4.Comentaris rebuts durant la setmana
                //5.Nous followers durant la setmana
                //6.Recompte de punts setmanals
                //7.Punts totals
                String SQL = String.Format("select a.ID, a.UserType, a.UserName, a.LanguageID," +
                                                        "(select count(b.ID) from Kit b where b.UserID=a.ID and b.ParentKitID is NULL and b.Created >='{0}') as TotalKits, " +
                                                        "(select count(c.ID) from Kit c where c.ParentKitID IN (select kt.ID from Kit kt where kt.UserID=a.ID) and c.Created >='{0}') as TotalRekits, " +
                                                        "(select count(d.ID) from UserLike d, Kit kt where d.KitID = kt.ID AND kt.UserID=a.ID and d.Created >='{0}') as TotalLikes, " +
                                                        "(select count(e.ID) from KitComment e where e.KitID IN (select kt.ID from Kit kt where kt.UserID=a.ID) and e.Visible=1 and e.Created >='{0}') as TotalComments ," +
                                                        "(select count(ur.ID) as count from UserRelation ur where  ur.FollowID=a.ID  and ur.Created >='{0}') as TotalFollowers, " +
                                                        "((select count(b.ID) from Kit b where b.UserID=a.ID and b.ParentKitID is NULL)+ " +
                                                        "(select count(c.ID) from Kit c where c.ParentKitID IN (select kt.ID from Kit kt where kt.UserID=a.ID))+ " +
                                                        "(select count(d.ID) from UserLike d, Kit kt where d.KitID = kt.ID AND kt.UserID=a.ID)+ " +
                                                        "(select count(e.ID) from KitComment e where e.KitID IN (select kt.ID from Kit kt where kt.UserID=a.ID) and e.Visible=1)) as Total " +
                                                        "from User a ", _database.ParseDateTimeToSQL(dt_week));
                                                        //"from User a where (ID=1) OR (ID=381) LIMIT 2", _database.ParseDateTimeToSQL(dt_week));
                                                        //"from User a where (ID=1) OR (ID=381) OR (ID=22) LIMIT 3", _database.ParseDateTimeToSQL(dt_week));
                                                        //"from User a where (ID=1) LIMIT 1", _database.ParseDateTimeToSQL(dt_week));

                command = _database.CreateCommand(SQL);

                reader = command.ExecuteReader();

                ArrayList users = new ArrayList();

                while (reader.Read())
                {
                    MailUser obj = new MailUser();

                    obj.ID = _database.GetReaderValueInt64(reader, "ID");
                    obj.UserName = _database.GetReaderValueString(reader, "Username");
                    obj.iUserType = _database.GetReaderValueInt16(reader, "UserType");
                    obj.TotalKits = _database.GetReaderValueInt16(reader, "TotalKits");
                    obj.TotalReKits = _database.GetReaderValueInt16(reader, "TotalReKits");
                    obj.TotalLikes = _database.GetReaderValueInt16(reader, "TotalLikes");
                    obj.TotalComments = _database.GetReaderValueInt16(reader, "TotalComments");
                    obj.TotalFollowers = _database.GetReaderValueInt16(reader, "TotalFollowers");
                    obj.Total = _database.GetReaderValueInt16(reader, "Total");
                    obj.LanguageID = _database.GetReaderValueInt64(reader, "LanguageID");

                    users.Add(obj);
                }

                reader.Close();
                reader.Dispose();
                command.Dispose();
               
                //Plantilla català
                String TemplateBodyCatala = String.Empty;
                String TemplateSubjectCatala = String.Empty;

                //Obtenim la plantilla de correu pels trenders
                String SQL_template = String.Format("select ID, Subject, Body, Type, LanguageID, Created, Modified from MailTemplate where Type=11 AND LanguageID=2");
                command = _database.CreateCommand(SQL_template);
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    TemplateSubjectCatala = _database.GetReaderValueString(reader, "Subject");
                    TemplateBodyCatala = _database.GetReaderValueString(reader, "Body");
                }

                reader.Close();
                reader.Dispose();
                command.Dispose();

                //Plantilla castellà
                String TemplateBodyCastella = String.Empty;
                String TemplateSubjectCastella = String.Empty;

                //Obtenim la plantilla de correu pels trenders
                SQL_template = String.Format("select ID, Subject, Body, Type, LanguageID, Created, Modified from MailTemplate where Type=11 AND LanguageID=1");
                command = _database.CreateCommand(SQL_template);
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    TemplateSubjectCastella = _database.GetReaderValueString(reader, "Subject");
                    TemplateBodyCastella = _database.GetReaderValueString(reader, "Body");
                }

                reader.Close();
                reader.Dispose();
                command.Dispose();

                //for (int i = 0; i < users.Count; i++)
                foreach(MailUser user in users) 
                {
                    String TemplateSubject = String.Empty;
                    String TemplateBody = String.Empty;

                    if(user.LanguageID == 1){
                        TemplateSubject = TemplateSubjectCastella;
                        TemplateBody = TemplateBodyCastella;
                        //TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("Hola, este es el resumen de tu actividad semanal a Likekit"));
                    }
                    else{
                        TemplateSubject = TemplateSubjectCatala;
                        TemplateBody = TemplateBodyCatala;                    

                        //TemplateBody = TemplateBody.Replace("[FRASE_1]", String.Format("Hola, aquest és el resum de la teva activitat setmanal a Likekit"));
                    }

                    TemplateBody = TemplateBody.Replace("[NUM_KITS]", String.Format("{0}",user.TotalKits));
                    TemplateBody = TemplateBody.Replace("[NUM_LIKES]", String.Format("{0}",user.TotalLikes));
                    TemplateBody = TemplateBody.Replace("[NUM_REKITS]", String.Format("{0}",user.TotalReKits));
                    TemplateBody = TemplateBody.Replace("[NUM_COMMENTS]", String.Format("{0}",user.TotalComments));
                    TemplateBody = TemplateBody.Replace("[NUM_FOLLOWERS]", String.Format("{0}",user.TotalFollowers));

                    Int16 TotalSetmanal = 0;
                    TotalSetmanal = (Int16)((Int16)user.TotalKits + (Int16)user.TotalReKits + (Int16)user.TotalLikes + (Int16)user.TotalComments + (Int16)user.TotalFollowers);


                    if(user.LanguageID == 1){
                        
                        if(TotalSetmanal == 0)
                            TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("Opsss! No has sumado ningún punto esta semana. <br/>Anímate con alguna recomendación. Las estamos esperando."));    
                        else if(user.iUserType == 1)
                            TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("FELICIDADES!! Has conseguido ser Trender. Disfrútalo."));    
                        else
                            TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("Bien, bien, bien! Sigue recomendando. Poco a poco te acercas a los Trenders."));    
                    }
                    else
                    {
                        if(TotalSetmanal == 0)
                            TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("Ooops! No has sumat cap punt aquesta setmana. <br/>Anima't amb alguna recomanació. Les estem esperant."));
                        else if (user.iUserType == 1)
                            TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("FELICITATS!! Has aconseguit ser Trender. Gaudeix."));    
                        else
                            TemplateBody = TemplateBody.Replace("[FRASE_PERSONALITZADA]", String.Format("Bé, bé, bé! Segueix recomanant. Mica en mica t'acostaràs als Trenders."));                        
                    }

                    TemplateBody = TemplateBody.Replace("[PUNTS_SETMANALS]", String.Format("{0}",TotalSetmanal));
                    TemplateBody = TemplateBody.Replace("[PUNTS_TOTALS]", String.Format("{0}",user.Total));

                    //Hauriem de buscar els quatre últims kits
                    String sql_kits = "select distinct(kt.ID), kt.UserID, kt.ProductID, kt.Comment, kt.Valoration, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, usr.Gender, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, " +
                    " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                    " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                    " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes ," +
                    " EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(),usr.Birthday))))+0 AS Age " +
                    " from Kit kt left join User usr on kt.UserID = usr.ID " +
                    " left join Product pr on kt.ProductID = pr.ID ";

                    //String sql_users = String.Format(" (kt.UserID IN (SELECT DISTINCT(ID) FROM User where UserType=1)) ");
                    String sql_users = String.Format(" (kt.UserID IN (SELECT DISTINCT(FollowID) FROM UserRelation where UserID = {0})) ", user.ID);
                    sql_users = " AND (" + sql_users + ") ";
                    String sql_order_by = String.Format(" order by kt.Modified DESC");

                    sql_kits += " where 1=1 ";
                    sql_kits += String.Format("{0} {1} LIMIT 4", sql_users, sql_order_by);

                    command = _database.CreateCommand(sql_kits);
                    reader = command.ExecuteReader();

                    int i = 0;

                    while (reader.Read())
                    {
                        i++;
                        Int64 ID = _database.GetReaderValueInt64(reader, "ID");
                        Int64 UserID = _database.GetReaderValueInt64(reader, "UserID");
                        Int64 ProductID = _database.GetReaderValueInt64(reader, "ProductID");
                        String UserName = _database.GetReaderValueString(reader, "UserName");
                        String FirstName = _database.GetReaderValueString(reader, "FirstName");
                        String LastName = _database.GetReaderValueString(reader, "LastName");
                        String UserPicture = _database.GetReaderValueString(reader, "UserPicture");
                        String ProductName = _database.GetReaderValueString(reader, "ProductName");
                        String ProductImage = _database.GetReaderValueString(reader, "ProductImage");
                        Int16 NumKits = _database.GetReaderValueInt16(reader, "num_kits");
                        Int16 NumComments = _database.GetReaderValueInt16(reader, "num_comments");
                        Int16 NumLikes = _database.GetReaderValueInt16(reader, "num_likes");
                        Int64 ProductCategoryID = _database.GetReaderValueInt64(reader, "ProductCategoryID");
                        Int16 UserType = _database.GetReaderValueInt16(reader, "UserType");
                        String Comment = _database.GetReaderValueString(reader, "Comment");
                        Int16 Valoration = _database.GetReaderValueInt16(reader, "Valoration");

                        UserPicture = "/Content/files/" + UserPicture;

                        String CategoryImage = String.Empty;
                        int category_image_width = 20;
                        switch (ProductCategoryID)
                        {
                            case 1:
                                CategoryImage = "kit-book.png";
                                //category_image_width = 15;
                                category_image_width = 13;
                                break;
                            case 2:
                                CategoryImage = "kit-serie.png";
                                //category_image_width = 18;
                                category_image_width = 16;
                                break;
                            case 3:
                                CategoryImage = "kit-cinema.png";
                                //category_image_width = 19;
                                category_image_width = 17;
                                break;
                            case 4:
                                CategoryImage = "kit-music.png";
                                //category_image_width = 20;
                                category_image_width = 18;
                                break;
                        }

                        String CompleteName = FirstName + " " + LastName;

                        String kitUrl = String.Format("{0}kit/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], GenerateUrlFriendly(ProductName), ID);

                        String kit_html = "<table width=\"166\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"margin-left:8px;margin-top:0px;\" bgcolor=\"#FFFFFF\" border=\"0\">" +
										"<tr>" +
											"<td width=\"156\" height=\"231\" colspan=\"3\" align=\"center\" style=\"background-color: #FFFFFF;\">" +
                                                //"<div style=\"width:156px;height:231px;overflow:hidden;\"><a href=\"" + kitUrl + "\" target=\"_blank\"><img src=\"http://www.likekit.com/Content/files/" + ProductImage + "\" width=\"156\" style=\"display: block;\" alt=\"likekit\" /></a></div>" +
                                                "<div style=\"width:156px;height:231px;overflow:hidden;\"><a href=\"" + kitUrl + "\" target=\"_blank\"><img src=\"http://www.likekit.com/timeline/GetImageThumbnail?image=" + ProductImage + "&width=156&height=231\" style=\"display: block;\" alt=\"likekit\" /></a></div>" +
											"</td>" +
										"</tr>" +
										"<tr style=\"background-color: #E7E6E6;\">" +
											"<td width=\"20\" height=\"20\" style=\"margin-left:5px;margin-top:2px;\">" +
												"<img style=\"vertical-align: middle;\" src=\"http://www.likekit.com/" + UserPicture + "\" width=\"20\" alt=\"likekit\" />" +
											"</td>" +
											"<td width=\"105\" style=\"text-align:left;font-family:Arial;font-weight:bold;font-size:9px;color:#000000;padding-left:10px;\">" +
                                            CompleteName +
											"</td>" +
											"<td width=\"21\" height=\"24\" style=\"margin-right:5px;text-align:right;\">" +
                                                "<img style=\"vertical-align: middle;\" src=\"http://www.likekit.com/Content/images/" + CategoryImage + "\" width=\"" + category_image_width + "\" alt=\"likekit\" />" +
											"</td>" +
										"</tr>" +
									"</table>";

                        switch (i)
                        { 
                            case 1:
                                TemplateBody = TemplateBody.Replace("[KIT_1]", kit_html);
                                break;
                            case 2:
                                TemplateBody = TemplateBody.Replace("[KIT_2]", kit_html);
                                break;
                            case 3:
                                TemplateBody = TemplateBody.Replace("[KIT_3]", kit_html);
                                break;
                            case 4:
                                TemplateBody = TemplateBody.Replace("[KIT_4]", kit_html);
                                break;
                        }
                    }

                    reader.Close();
                    reader.Dispose();
                    command.Dispose();

                    //TemplateBody = TemplateBody.Replace("[KIT_URL]", strURL);

                    //user.UserName = "albert.marimon@gmail.com;amarimon@endepro.com";

                    String SQL_mail = String.Format("INSERT INTO Mail (Sender, Subject, Body, Status, Created, Modified) " +
                        "values ('{0}','{1}','{2}',{3},'{4}','{5}')",
                        _database.ParseStringToSQL(user.UserName),
                        _database.ParseStringToSQL(TemplateSubject),
                        _database.ParseStringToSQL(TemplateBody),
                        0,
                        _database.ParseDateTimeToSQL(System.DateTime.Now),
                        _database.ParseDateTimeToSQL(System.DateTime.Now)
                        );

                    command = _database.CreateCommand(SQL_mail);
                    command.ExecuteNonQuery();
                    command.Dispose();
                }

                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (reader!=null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (command != null)
                    command.Dispose();

                if (_database!=null)
                    _database.Close();
            }
        }

        public static String GenerateUrlFriendly(String phrase)
        {
            /*String str = phrase.ToLower();

            // invalid chars, make into spaces
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces/hyphens into one space       
            str = Regex.Replace(str, @"[\s-]+", " ").Trim();
            // cut and trim it
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();
            // hyphens
            str = Regex.Replace(str, @"\s", "-");

            return str;*/

            char[] allChars;

            //cadena de caracters per ser substituits
            String sCharsToReplace = "áéíóúüñç_àèòï";
            //cadena de caracters per substituir, en el mateix ordre
            String sCharsForReplace = "aeiouunc-aeoi";
            String seoURL = String.Empty;
            int charPos = 0;

            // decodifica qualsevol entitat HTML abans de començar.
            //strTitle = Server.HtmlDecode(phrase);
            phrase = phrase.ToLower();
            phrase = phrase.Trim();

            // substitueix tots els caracters transparents.
            phrase = Regex.Replace(phrase, @"\s+", "-");

            // El seguent bucle substitueix tots els caracters
            // trobats a sCharsToReplace pels de sCharsForReplace 
            allChars = phrase.ToCharArray();
            for (int i = 0; i < allChars.Length; i++)
            {
                charPos = sCharsToReplace.IndexOf(allChars[i]);
                if (charPos != -1)
                { allChars[i] = sCharsForReplace[charPos]; }
                seoURL += allChars[i];
            }

            // Treu qualsevol caracter no substituit que no
            // estigui entre a y z, que no sigui número o que no sigui un guio (-)

            seoURL = new Regex("[^a-z0-9-]").Replace(seoURL, "");
            // treu qualsevol guio solitari al final o principi
            // de la cadena resultant
            seoURL = seoURL.Trim('-');

            return seoURL;
        }
    }
}
