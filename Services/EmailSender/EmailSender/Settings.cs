﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endepro.Common.Xml;
using Endepro.Common;
using System.Reflection;

namespace EmailSender
{
    public class Settings : SettingsBase
    {
        /// <summary>
        /// Nodo con el servidor SMTP para realizar los envíos de e-mails.
        /// </summary>
        private const String _smtpServerNodeName = "smtp-server";
        /// <summary>
        /// Nodo con el puerto del servidor SMTP.
        /// </summary>
        private const String _smtpPortNodeName = "smtp-port";
        /// <summary>
        /// Nodo con el usuario del servidor SMTP.
        /// </summary>
        private const String _smtpUserNodeName = "smtp-user";
        /// <summary>
        /// Nodo con la contraseña del servidor SMTP.
        /// </summary>
        private const String _smtpPasswordNodeName = "smtp-password";

        private const String _mailFromNodeName = "mail-from";
        private const String _mailDisplayNameNodeName = "mail-displayname";

        private const String _rsaPrivateKeyNodeName = "rsa-privatekey";
        private const String _rsaDomainNodeName = "rsa-domain";
        private const String _rsaSelectorNodeName = "rsa-selector";

        public String PathCombine(String path1, String path2)
        {
            String result = System.IO.Path.Combine(path1, path2);
            if (System.IO.Path.DirectorySeparatorChar == '/')
                return result.Replace('\\', '/');
            else if (System.IO.Path.DirectorySeparatorChar == '\\')
                return result.Replace('/', '\\');
            else
                return result;
        }
        
        /// <summary>
        /// Constructor.
        /// </summary>
        public Settings() : //base("C:\\src\\likekit\\trunk\\Services\\EmailSender\\EmailSender\\bin\\Debug\\settings.xml")
            base(Common.GetAssemblyPath(Assembly.GetExecutingAssembly()) + "settings.xml")
        {
        }

        /// <summary>c
        /// Crear un archivo de configuración por defecto.
        /// </summary>
        public override void CreateEmptyFile()
        {
            SmtpServer = "localhost";
            SmtpPort = 25;
            SmtpUser = String.Empty;
            SmtpPassword = String.Empty;
            MailFrom = String.Empty;
            MailDisplayName = String.Empty;
            RSAPrivateKey = String.Empty;
            RSADomainName = String.Empty;
            RSASelector = String.Empty;

            base.CreateEmptyFile();
        }

        public String RSASelector
        {
            get
            {
                return _settings.GetString(_rsaSelectorNodeName);
            }
            set
            {
                _settings.PutString(_rsaSelectorNodeName, value);
            }
        }

        public String RSADomainName
        {
            get
            {
                return _settings.GetString(_rsaDomainNodeName);
            }
            set
            {
                _settings.PutString(_rsaDomainNodeName, value);
            }
        }

        public String RSAPrivateKey
        {
            get
            {
                return _settings.GetString(_rsaPrivateKeyNodeName);
            }
            set
            {
                _settings.PutString(_rsaPrivateKeyNodeName, value);
            }
        }

        public String MailDisplayName
        {
            get
            {
                return _settings.GetString(_mailDisplayNameNodeName);
            }
            set
            {
                _settings.PutString(_mailDisplayNameNodeName, value);
            }
        }

        public String MailFrom
        {
            get
            {
                return _settings.GetString(_mailFromNodeName);
            }
            set
            {
                _settings.PutString(_mailFromNodeName, value);
            }
        }


        /// <summary>
        /// Servidor SMTP para realizar los envíos de e-mails.
        /// </summary>
        public String SmtpServer
        {
            get
            {
                return _settings.GetString(_smtpServerNodeName);
            }
            set
            {
                _settings.PutString(_smtpServerNodeName, value);
            }
        }

        /// <summary>
        /// Puerto del servidor SMTP.
        /// </summary>
        public Int32 SmtpPort
        {
            get
            {
                return _settings.GetInt(_smtpPortNodeName, 25);
            }
            set
            {
                _settings.PutInt(_smtpPortNodeName, value);
            }
        }

        /// <summary>
        /// Usuario del servidor SMTP.
        /// </summary>
        public String SmtpUser
        {
            get
            {
                return _settings.GetString(_smtpUserNodeName);
            }
            set
            {
                _settings.PutString(_smtpUserNodeName, value);
            }
        }

        /// <summary>
        /// Contraseña del servidor SMTP.
        /// </summary>
        public String SmtpPassword
        {
            get
            {
                return _settings.GetString(_smtpPasswordNodeName);
            }
            set
            {
                _settings.PutString(_smtpPasswordNodeName, value);
            }
        }
    }
}
