using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Runtime.InteropServices;

namespace EmailSender
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public static String ServiceName = "EmailSender";

        public ProjectInstaller()
        {
            InitializeComponent();

            uxServiceInstaller.DisplayName = ServiceName;
            uxServiceInstaller.ServiceName = ServiceName;

            try
            {
                uxServiceController.ServiceName = ServiceName;
                uxServiceController.Start();
            }
            catch (Exception)
            {
            }
        }
    }
}