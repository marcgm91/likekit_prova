﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Endepro.Common.Database;
using System.Data;
using System.Collections;
using System.Net.Mail;
using System.Net;
using DKIM;

namespace EmailSender
{
    /// <summary>
    /// Objecte amb la informació dels correus
    /// </summary>
    public class MailStruct
    {
        public Int16 Status = 0;
        public Int64 ID = 0;
        public String Sender = String.Empty;
        public String Subject = String.Empty;
        public String Body = String.Empty;
    }


    public class SendThread
    {
        private Thread _thread = null;
        private DatabaseMySQL _database = null;
        private int _diaAnterior = 0;
        
        public SendThread()
        {
        }

        public void Start()
        {
            if (_thread == null)
            {
                _thread = new Thread(new ThreadStart(SyncThreadStart));
                _thread.Start();
            }
        }

        public void Stop()
        {
            if (_thread != null)
            {
                _thread.Abort();
            }
        }

        public static void SendMail(String Subject, String Body, String To, String Cc = "", String Bcc = "")
        {
            To = To.Replace(';', ',').Replace(" ", String.Empty);
            Cc = Cc.Replace(';', ',').Replace(" ", String.Empty);
            Bcc = Bcc.Replace(';', ',').Replace(" ", String.Empty);
            if (To == String.Empty && Cc == String.Empty && Bcc == String.Empty)
                return;

            if (Program.Settings.SmtpServer == String.Empty)
                return;

            MailMessage mail = new MailMessage();
            try
            {
                mail.From = new MailAddress(Program.Settings.MailFrom, Program.Settings.MailDisplayName);
                mail.Sender = mail.From;
                if (To != String.Empty)
                    mail.To.Add(To);
                if (Cc != String.Empty)
                    mail.CC.Add(Cc);
                if (Bcc != String.Empty)
                    mail.Bcc.Add(Bcc);
                mail.Subject = Subject;
                mail.Body = Body;

                mail.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient(Program.Settings.SmtpServer, Program.Settings.SmtpPort > 0 ? Program.Settings.SmtpPort : 25);
                smtpClient.EnableSsl = true;

                if (Program.Settings.SmtpUser != String.Empty)
                {
                    smtpClient.Credentials = new NetworkCredential(Program.Settings.SmtpUser, Program.Settings.SmtpPassword);
                }

                //RSA
                var privateKey = PrivateKeySigner.Create(Program.Settings.RSAPrivateKey);

                var domainKeySigner = new DomainKeySigner(privateKey, Program.Settings.RSADomainName, Program.Settings.RSASelector, new string[] { "From", "To", "Subject" });
                mail.DomainKeySign(domainKeySigner);

                var dkimSigner = new DkimSigner(privateKey, Program.Settings.RSADomainName, Program.Settings.RSASelector, new string[] { "From", "To", "Subject" });
                mail.DkimSign(dkimSigner);
                //RSA

                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mail.Dispose();
            }
        }


        private void SyncThreadStart()
        {
            try
            {
#if DEBUG
                Thread.Sleep(10000);
#endif
                _database = new DatabaseMySQL(Program.Settings.DatabaseHost,
                    Program.Settings.DatabaseUser,
                    Program.Settings.DatabasePassword,
                    Program.Settings.DatabaseName);

                _database.Open();

                while (true)
                {
                    int iSegons = 0;
                    IDbCommand command = null;
                    IDataReader reader = null;

                    Thread.Sleep(10000);   //10 segons
                    ArrayList _arrayID = new ArrayList();

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //Enviar correus del Pool General
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    try
                    {
                        String sql = String.Format("SELECT ID,Sender,Subject,Body FROM Mail where Status=0");

                        command = _database.CreateCommand(sql);
                        reader = command.ExecuteReader();

                        ArrayList _mails = new ArrayList();

                        while (reader.Read())
                        {
                            MailStruct mail = new MailStruct();

                            Int16 Status = 0;
                            Int64 ID = _database.GetReaderValueInt64(reader, "ID");
                            String Sender = _database.GetReaderValueString(reader, "Sender");
                            String Subject = _database.GetReaderValueString(reader, "Subject");
                            String Body = _database.GetReaderValueString(reader, "Body");

                            mail.Status = 0;
                            mail.ID = ID;
                            mail.Sender = Sender;
                            mail.Subject = Subject;
                            mail.Body = Body;
                               
                            _mails.Add(mail);
                        }

                        reader.Close();
                        reader.Dispose();
                        command.Dispose();

                        foreach (MailStruct obj in _mails)
                        {
                            Int16 Status = 0;
                            try
                            {
                                SendMail(obj.Subject, obj.Body, obj.Sender);
                            }
                            catch (Exception)
                            {
                                Status = 2; //Error
                            }
                            finally
                            {
                                if (Status == 0)
                                    Status = 1;  //OK

                                _database.ExecuteCommand("update Mail set Status=" + Status + " where ID=" + obj.ID);
                            }
                        }

                        iSegons++;

                        if (iSegons == 20)
                        {
                            iSegons = 0;
                            Thread.Sleep(5000);   //5 segons
                        }
                        else
                            Thread.Sleep(1000);   //1 segon
                        
                    }
                    catch (Exception ex)
                    {
                        //Log.InsertLog(_database, Log.LogType.Error, 0, ex);
                    }
                    finally
                    {
                        if (reader != null)
                        {
                            reader.Close();
                            reader.Dispose();
                        }
                        if (command != null)
                            command.Dispose();

                        //Crida la funció que enviarà un correu a nosaltres mateixos.
                        enviarEmailANosaltres();
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    //Log.InsertLog(_database, Log.LogType.Error, 0, ex);
                }
                catch
                { }
            }
            finally
            {
                _database.Close();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Provem si el "emailSender" funciona correctament enviant un e-mail cada cert temps a nosaltres mateixos.
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void enviarEmailANosaltres()
        {
            int diaActual = (int) DateTime.Today.Day;

            if (diaActual != _diaAnterior)
            {
                //Enviar email.
                try
                {
                    SendMail("Comprovacio diaria de EmailSender", "El email Sender funciona correctament", "marcgm91@gmail.com");
                    SendMail("Comprovacio diaria de EmailSender", "El email Sender funciona correctament", "prodriguez@likekit.com");

                    _diaAnterior = diaActual;
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
