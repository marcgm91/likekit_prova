﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace EmailSender
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new EmailSender() 
			};
            ServiceBase.Run(ServicesToRun);
        }


        #region Settings
        private static Settings _settings;

        /// <summary>
        /// Acceso a los parámetros de configuración.
        /// </summary>
        public static Settings Settings
        {
            get
            {
                if (_settings == null)
                    _settings = new Settings();

                return _settings;
            }
        }
        #endregion
    }
}
