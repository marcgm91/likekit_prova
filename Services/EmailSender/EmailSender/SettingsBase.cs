﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endepro.Common.Xml;

namespace EmailSender
{
    public class SettingsBase
    {
        /// <summary>
        /// Nombre del nodo principal.
        /// </summary>
        private const String _parentNodeName = "configuration";
        /// <summary>
        /// Nodo con el nombre del servidor de la Base de Datos.
        /// </summary>
        private const String _databaseHostNodeName = "database-host";
        /// <summary>
        /// Nodo con el nombre de la Base de Datos.
        /// </summary>
        private const String _databaseNameNodeName = "database-name";
        /// <summary>
        /// Nodo con el nombre de usuario de acceso a la Base de Datos.
        /// </summary>
        private const String _databaseUserNodeName = "database-user";
        /// <summary>
        /// Nodo con la contraseña del usuario de acceso a la Base de Datos.
        /// </summary>
        private const String _databasePasswordNodeName = "database-password";

        /// <summary>
        /// Objeto usado para cargar los parámetros de configuración.
        /// </summary>
        protected SettingsParser _settings;

        /// <summary>
        /// Constructor.
        /// </summary>
        public SettingsBase(String filename)
        {
            _settings = new SettingsParser(filename, null);
            _settings.ReadFile(_parentNodeName);
            if (IsEmpty())
                CreateEmptyFile();
        }

        /// <summary>
        /// Guardar los cambios realizados en la configuración.
        /// </summary>
        public virtual void Save()
        {
            _settings.WriteFile(_parentNodeName);
        }

        /// <summary>
        /// Crear un archivo de configuración por defecto.
        /// </summary>
        public virtual void CreateEmptyFile()
        {
            DatabaseHost = "localhost";
            DatabaseName = "database";
            DatabaseUser = "root";
            DatabasePassword = "";
            Save();
        }

        /// <summary>
        /// Comprueba si un parámetro está vacío.
        /// </summary>
        /// <param name="str">Valor del parámetro a comprobar.</param>
        /// <returns>Si está vacío.</returns>
        protected virtual bool IsEmptySetting(String str)
        {
            return str == null || str == String.Empty;
        }

        /// <summary>
        /// Comprueba si el archivo de configuración está vacío.
        /// </summary>
        /// <returns>Si está vacío.</returns>
        public virtual bool IsEmpty()
        {
            return IsEmptySetting(DatabaseHost) && IsEmptySetting(DatabaseName) &&
                   IsEmptySetting(DatabaseUser) && IsEmptySetting(DatabasePassword);
        }

        /// <summary>
        /// Database Host.
        /// </summary>
        public String DatabaseHost
        {
            get
            {
                return _settings.GetString(_databaseHostNodeName);
            }
            set
            {
                _settings.PutString(_databaseHostNodeName, value);
            }
        }

        /// <summary>
        /// Database Name.
        /// </summary>
        public String DatabaseName
        {
            get
            {
                return _settings.GetString(_databaseNameNodeName);
            }
            set
            {
                _settings.PutString(_databaseNameNodeName, value);
            }
        }

        /// <summary>
        /// Database User.
        /// </summary>
        public String DatabaseUser
        {
            get
            {
                return _settings.GetString(_databaseUserNodeName);
            }
            set
            {
                _settings.PutString(_databaseUserNodeName, value);
            }
        }

        /// <summary>
        /// Database Password.
        /// </summary>
        public String DatabasePassword
        {
            get
            {
                return _settings.GetString(_databasePasswordNodeName);
            }
            set
            {
                _settings.PutString(_databasePasswordNodeName, value);
            }
        }

    }
}
