﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace EmailSender
{
    public partial class EmailSender : ServiceBase
    {
        private SendThread _sender = null;

        public EmailSender()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _sender = new SendThread();
            _sender.Start();            
        }

        protected override void OnStop()
        {
            if (_sender!=null)
                _sender.Stop();
        }
    }
}
