﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class UserRelation
    {
        public Int64 ID { get; set; }
        
        public User User { get; set; }
        public User Follow { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}