﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum ObjectType
    {
        Welcome = 0,
        PasswordRecovery = 1,
        UserFollowing = 2,
        KitComment = 3,
        UserLike = 4,
        UserReKit = 5,
        UserRecommendation = 6,
        UserBeta = 7,
        BetaAccess = 8,
        LikeKitInvitation = 9,
        BecomeTrender = 10
    };

    public class MailTemplate
    {
        public Int64 ID { get; set; }

        public String Subject { get; set; }
        public String Body { get; set; }
        public ObjectType Type { get; set; }
        public Language Language { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}