﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum Type
    {
        Timeline = 0,
        InsertKit = 1,
        EditKit = 2,
        DeleteKit = 3,
        Register = 4,
        EditProfile = 5,
        BetaSendMail = 6,
        Like = 7,
        ReKit = 8,
        KitComment = 9,
        Follow = 10
    };

    public class Log
    {
        public Int64 ID { get; set; }
        
        public Int64 RefID { get; set; }
        public String Description { get; set; }
        public Type Type { get; set; }
        public DateTime Created { get; set; }
    }
}