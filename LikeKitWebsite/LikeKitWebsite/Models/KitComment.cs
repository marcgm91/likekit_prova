﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class KitComment
    {
        public Int64 ID { get; set; }
        
        public Kit Kit { get; set; }
        public User User { get; set; }
        public String Comment { get; set; }
        public Double Valoration { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Visible { get; set; }
    }
}