﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum InviteType
    {
        Gmail = 0,
        Yahoo = 1,
        Msn = 2,
        Facebook = 3
    };

    public class Invite
    {
        public bool Show { get; set; }
        public InviteType Type { get; set; }
        public String HtmlOutput { get; set; }
    }
}