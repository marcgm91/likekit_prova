﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    /// <summary>
    /// Objecte amb la informació dels Usuaris
    /// </summary>
    public class BetaUserResult
    {
        public bool Success = true;
        public String Message = String.Empty;
    }

    public class BetaUser
    {
        public Int64 ID { get; set; }

        public String Mail { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}