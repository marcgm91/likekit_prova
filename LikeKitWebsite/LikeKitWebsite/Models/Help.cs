﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class Help
    {
        public Int64 ID { get; set; }
        
        public String Title { get; set; }
        public String Content { get; set; }
        public short Order { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}