﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class ProductCategory
    {
        public Int64 ID { get; set; }
        
        public String Name { get; set; }
        public String Permalink { get; set; }
        public ProductCategory ParentProductCategory { get; set; }        
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public String HtmlHeaderUserImage { get; set; }
        public String HtmlHeaderTitle { get; set; }
        public String HtmlHeaderCategoryName { get; set; }
        public String HtmlHeaderUserName { get; set; }
        public String HtmlOutput { get; set; }
    }
}