﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class HomePage
    {
        public String Timeline { get; set; }
        public Int64 Counter { get; set; }
        public String TitolComodi { get; set; }
        public String DescripcioComodi { get; set; }
        public String MetaTitle { get; set; }
        public String Metadescription { get; set; }
        public String MetaKeywords { get; set; }
        public String IconePagComodi { get; set; }
    }
}