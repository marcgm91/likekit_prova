﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace LikeKitWebsite.Models
{

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
        [Required]
        [Display(Name = "User name")]
        //public string UserName { get; set; }


        //ALBERT: Es fa així per tenir un default value al formulari
        private string _userName = "Tu dirección e-mail";
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        //public string Password { get; set; }

        //ALBERT: Es fa així per tenir un default value al formulari
        private string _password = "Contraseña";
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        [Display(Name = "Recuérdame")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Tu dirección e-mail")]
        //public string Email { get; set; }

        //ALBERT: Es fa així per tenir un default value al formulari
        private string _email = "Tu dirección e-mail";
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Repete Contraseña")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Nombre")]
        //public string FirstName { get; set; }

        //ALBERT: Es fa així per tenir un default value al formulari
        private string _firstName = "Nombre";
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        [Required]
        [Display(Name = "Apellidos")]
        //public string LastName { get; set; }

        //ALBERT: Es fa així per tenir un default value al formulari
        private string _lastName = "Apellidos";
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        public String RegisterCategoryProducts { get; set; }
        public String RegisterFollowings { get; set; }

        public int TwitterRegister { get; set; }
        public int FacebookRegister { get; set; }

        public decimal TwitterUserId { get; set; }
        public string TwitterAccessKey { get; set; }
        public string TwitterAccessSecret { get; set; }

        public string FacebookUserId { get; set; }
        public string FacebookAccessToken { get; set; }

        public String Picsquare { get; set; }
        public String Gender { get; set; }
    }
}
