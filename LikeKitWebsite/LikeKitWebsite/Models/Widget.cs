﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace LikeKitWebsite.Models
{
    public class Widget
    {
        public Int64 UserID { get; set; }
        public String UserPicture { get; set; }
        public String UserFirstName { get; set; }
        public String UserLastName { get; set; }
        public Int64 UserNumFollowers { get; set; }
        public String HtmlLastKits { get; set; }
    }
}