﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum NotifyType
    {
        None = 0,
        ReKit = 1,
        Comment = 2,
        Follow = 3,
        Like = 4,
        CommentedComment = 5
    };

    public class Notify
    {
        public Int64 ID { get; set; }

        public User ToUser { get; set; }
        public User FromUser { get; set; }
        public Int64 RefID { get; set; }
        public NotifyType NotifyType { get; set; }
        public Int16 Readed { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
