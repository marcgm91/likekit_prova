﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class Product
    {
        public Int64 ID { get; set; }
        
        public String Name { get; set; }
        public String Description { get; set; }
        public String Author { get; set; }
        public String Permalink { get; set; }
        public short Year { get; set; }
        public String Image { get; set; }
        public ProductCategory ProductCategory { get; set; }        
        public Language Language { get; set; }        
        public String EAN { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public String LanguageName { get; set; }
        public String PartnerUrl { get; set; }
        public String Observations { get; set; }
        public String ASIN { get; set; }
        public short Active { get; set; }
    }
}