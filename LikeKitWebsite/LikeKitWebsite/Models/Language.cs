﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class Language
    {
        public Int64 ID { get; set; }
        
        public String Code { get; set; }
        public String Description { get; set; }
    }
}