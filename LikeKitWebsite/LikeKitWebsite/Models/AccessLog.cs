﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum AccesLogObjectType
    {
        Home = 0,
        Kit = 1,
        Profile = 2, 
        Product = 3,
        Login = 4
    };

    public enum OperationType
    {
        View = 0,
        Edit = 1,
        OpenKitForm = 2,
        SearchProduct = 3,
        SearchProductWS = 4,
        PublishKit = 5,
        ProductNotFound = 6,
        LoQuiero = 7,
        Login = 8
    };

    public enum DeviceType
    {
        Web = 0,
        Mobile = 1
    };

    public class AccessLog
    {
        public Int64 ID { get; set; }

        public Int64 RefID { get; set; }
        public User User { get; set; }
        public AccesLogObjectType ObjectType { get; set; }
        public OperationType OperationType { get; set; }
        public DeviceType DeviceType { get; set; }
        public String IP { get; set; }
        public String SessionID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public String Observations { get; set; }
        public DateTime Published { get; set; }
    }
}