﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class ProductVendor
    {
        public Int64 ID { get; set; }
        
        public Vendor Vendor { get; set; }
        public Product Product { get; set; }
        public String Url { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}