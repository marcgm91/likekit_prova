﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class Vendor
    {
        public Int64 ID { get; set; }
        
        public String Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}