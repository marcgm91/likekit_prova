﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum Status
    {
        Pending = 0,
        Sent = 1,
        Error = 2
    };

    public class Mail
    {
        public Int64 ID { get; set; }
        
        public String Sender { get; set; }
        public String Subject { get; set; }
        public String Body { get; set; }
        public Status Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}