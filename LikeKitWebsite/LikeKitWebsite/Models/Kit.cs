﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace LikeKitWebsite.Models
{
    public class Kit
    {
        public Int64 ID { get; set; }
        
        public User User { get; set; }
        public Product Product { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public String Comment { get; set; }

        public Double Valoration { get; set; }
        public Kit ParentKit { get; set; }
        public bool Visible { get; set; }
        public Int16 NumKits { get; set; }
        public Int16 NumLikes { get; set; }
        public Int16 NumComments { get; set; }
        public Int16 NumProductKits { get; set; }
        public String TimeDifference { get; set; }
        public ArrayList Comments { get; set; }
        public ArrayList OtherKits { get; set; }
        public ArrayList Tags { get; set; }
        public String HtmlTags { get; set; }
        public String HtmlComments { get; set; }
        public String HtmlOtherKits { get; set; }
        public bool isLike { get; set; }
        public bool canFacebookPublish { get; set; }
        public bool canTwitterPublish { get; set; }
        public String TwitterText { get; set; }
        public String FacebookText { get; set; }
        public String MailText { get; set; }

        public Double MidValoration { get; set; }
        public Double NumKitsMidValoration { get; set; }
        public String StringTags { get; set; }
        public String CategoryName { get; set; }
        public String HomeKitUserUrlFriendly { get; set; }
        public String LoquieroButtonHTML { get; set; }
        public String LoquieroHTML { get; set; }
        public String SchemaType { get; set; }
    }
}