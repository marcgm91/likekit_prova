﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public class UserStatus
    {
        public Int64 ID { get; set; }
        
        public User User { get; set; }
        public Product Product { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public String HtmlHeaderUserImage { get; set; }
        public String HtmlHeaderTitle { get; set; }
        public String HtmlOutput { get; set; }
    }
}