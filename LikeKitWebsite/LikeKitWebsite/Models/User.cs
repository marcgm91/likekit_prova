﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LikeKitWebsite.Models
{
    public enum UserGender
    {
        None = 0,
        Hombre = 1,
        Mujer = 2
    };

    public enum UserType
    {
        Normal = 0,
        Trender = 1
    };

    public enum RegisterType
    {
        Normal = 0,
        Twitter = 1,
        Facebook = 2
    };

    /*
     * MarcGM: Els permisos es componen de un número que és la suma dels números de sota que vols obtenir.
     * P.ex: si vols tindré els permisos "TwitterPublish" i "FacebookPublish" es suma "2" + "4" que donaría "6".
    */
    public enum UserPerms { 
        NoNotificacions = 1,
        TwitterPublish = 2,
        FacebookPublish = 4,
        KitComment = 8,
        KitLike = 16,
        ReKit = 32, 
        Follow = 64,
        RecieveRecommendations = 128,
        LikekitNews = 256
    }

    public class User : IComparable
    {
        public Int64 ID { get; set; }

        public String UserID { get; set; }
        public int UserLevel { get; set; }
        
        public String UserName { get; set; }
        public String Password { get; set; }

        public String FirstName { get; set; }
        public String LastName { get; set; }

        public String Picture { get; set; }
        public String About { get; set; }
        public String Web { get; set; }
        public String Address { get; set; }

        public DateTime Birthday { get; set; }
        public String BirthdayString { get; set; }
        public UserGender Gender { get; set; }

        public UserType UserType { get; set; }

        public String FacebookAccessToken { get; set; }
        public String FacebookID { get; set; }
        public DateTime FacebookExpires { get; set; }
        
        public String TwitterAccessToken { get; set; }
        public String TwitterID { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public int ProfilePercentage { get; set; }
        public int ProfilePercentageWidth { get; set; }

        public String ProfileCategoryKits { get; set; }

        public Int64 NumFollowers { get; set; }
        public Int64 NumFollowings { get; set; }
        public Int64 NumKits { get; set; }
        public Int64 NumLikes { get; set; }

        public Int64 NumReKits { get; set; }
        public Int64 NumComments { get; set; }

        public Int64 NumLikesToUser { get; set; }
        public Int64 NumReKitsToUser { get; set; }
        public Int64 NumCommentsToUser { get; set; }

        public Int64 NumTrenderPoints { get; set; }
        public Int64 NumUserPoints { get; set; }
    
        public String ProfileFollowers { get; set; }
        public String ProfileFollowings { get; set; }
        public String ProfileNowWith { get; set; }
        public bool isFollowing { get; set; }

        public String FirstNameUpper { get; set; }
        public String OriginalPicture { get; set; }

        public RegisterType RegisterType { get; set; }
        public String SocialName { get; set; }
        public String HtmlPicture { get; set; }
        public String HtmlWeb { get; set; }

        public String ProfileUsersLikeUser { get; set; }

        public int iUserType { get; set; }
        public Int64 Perms { get; set; }

        public bool Perm1 { get; set; }
        public bool Perm2 { get; set; }
        public bool Perm3 { get; set; }
        public bool Perm4 { get; set; }
        public bool Perm5 { get; set; }
        public bool Perm6 { get; set; }
        public bool Perm7 { get; set; }
        public bool Perm8 { get; set; }
        public bool Perm9 { get; set; }

        public String Perm1Css { get; set; }
        public String Perm2Css { get; set; }
        public String Perm3Css { get; set; }
        public String Perm4Css { get; set; }
        public String Perm5Css { get; set; }
        public String Perm6Css { get; set; }
        public String Perm7Css { get; set; }
        public String Perm8Css { get; set; }
        public String Perm9Css { get; set; }

        public String UserToken { get; set; }
        public String WidgetURL { get; set; }

        public Language Language { get; set; }
        public String GenderString { get; set; }
        public Vendor Vendor { get; set; }

        public int CompareTo(object obj)
        {
            User Compare = (User)obj;
            int result = this.FirstName.CompareTo(Compare.FirstName);
            if (result == 0)
                result = this.FirstName.CompareTo(Compare.FirstName);
            return result;
        }
    }
}