# --------------------------------------------------------
# Host:                         server04.endepro.lan
# Server version:               5.0.51a-24+lenny4
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3916
# Date/time:                    2012-04-25 14:56:51
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table likekit.AccessLog
CREATE TABLE IF NOT EXISTS `AccessLog` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `RefID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `ObjectType` smallint(6) NOT NULL default '0',
  `OperationType` smallint(6) NOT NULL default '0',
  `DeviceType` smallint(6) NOT NULL default '0',
  `Ip` varchar(50) collate utf8_unicode_ci NOT NULL,
  `SessionID` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `AccessLog_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.City
CREATE TABLE IF NOT EXISTS `City` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Name` varchar(180) collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `CountryID` bigint(20) NOT NULL,
  `PostalCode` varchar(25) collate utf8_unicode_ci NOT NULL,
  `State` varchar(200) collate utf8_unicode_ci default NULL,
  `Province` varchar(200) collate utf8_unicode_ci default NULL,
  `Latitude` float default NULL,
  `Longitude` float default NULL,
  PRIMARY KEY  (`ID`),
  KEY `Country` (`CountryID`),
  CONSTRAINT `City_ibfk_1` FOREIGN KEY (`CountryID`) REFERENCES `Country` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Country
CREATE TABLE IF NOT EXISTS `Country` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `Code` varchar(2) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Help
CREATE TABLE IF NOT EXISTS `Help` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Title` varchar(255) collate utf8_unicode_ci NOT NULL,
  `Content` text collate utf8_unicode_ci NOT NULL,
  `Order` int(4) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Kit
CREATE TABLE IF NOT EXISTS `Kit` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `ProductID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `Valoration` float NOT NULL default '0',
  `Comment` varchar(200) collate utf8_unicode_ci default NULL,
  `ParentKitID` bigint(20) default NULL,
  `Visible` smallint(6) NOT NULL default '1',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `Kit_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `Kit_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.KitComment
CREATE TABLE IF NOT EXISTS `KitComment` (
  `ID` bigint(20) NOT NULL auto_increment,
  `KitID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `Comment` varchar(200) collate utf8_unicode_ci NOT NULL,
  `Visible` smallint(6) NOT NULL default '1',
  PRIMARY KEY  (`ID`),
  KEY `KitID` (`KitID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `KitComment_ibfk_1` FOREIGN KEY (`KitID`) REFERENCES `Kit` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `KitComment_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.KitTag
CREATE TABLE IF NOT EXISTS `KitTag` (
  `ID` bigint(20) NOT NULL auto_increment,
  `KitID` bigint(20) NOT NULL,
  `Name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `KitID` (`KitID`),
  CONSTRAINT `KitTag_ibfk_1` FOREIGN KEY (`KitID`) REFERENCES `Kit` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Language
CREATE TABLE IF NOT EXISTS `Language` (
  `ID` bigint(11) NOT NULL auto_increment,
  `Code` varchar(10) collate utf8_unicode_ci NOT NULL,
  `Description` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `code` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Log
CREATE TABLE IF NOT EXISTS `Log` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Type` smallint(6) NOT NULL,
  `RefID` bigint(20) NOT NULL,
  `Description` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Mail
CREATE TABLE IF NOT EXISTS `Mail` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Sender` varchar(100) collate utf8_unicode_ci NOT NULL,
  `Subject` varchar(255) collate utf8_unicode_ci NOT NULL,
  `Body` text collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `Status` smallint(6) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.MailTemplate
CREATE TABLE IF NOT EXISTS `MailTemplate` (
  `ID` bigint(20) NOT NULL,
  `Type` smallint(6) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Body` text NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `LanguageID` bigint(20) NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `Mailtemplate_ibfk_1` (`LanguageID`),
  CONSTRAINT `Mailtemplate_ibfk_1` FOREIGN KEY (`LanguageID`) REFERENCES `Language` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Data exporting was unselected.


# Dumping structure for table likekit.Product
CREATE TABLE IF NOT EXISTS `Product` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `Image` varchar(40) character set latin1 NOT NULL,
  `Description` text collate utf8_unicode_ci NOT NULL,
  `Author` varchar(255) collate utf8_unicode_ci NOT NULL,
  `Permalink` varchar(100) collate utf8_unicode_ci NOT NULL,
  `Year` int(4) NOT NULL,
  `ProductCategoryID` bigint(20) default NULL,
  `LanguageID` bigint(20) default NULL,
  `EAN` varchar(100) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`ID`),
  KEY `ProductCategoryID` (`ProductCategoryID`),
  KEY `LanguageID` (`LanguageID`),
  CONSTRAINT `Product_ibfk_1` FOREIGN KEY (`ProductCategoryID`) REFERENCES `ProductCategory` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `Product_ibfk_2` FOREIGN KEY (`LanguageID`) REFERENCES `Language` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.ProductCategory
CREATE TABLE IF NOT EXISTS `ProductCategory` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Permalink` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ProductCategoryID` bigint(20) default NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `ProductCategoryID` (`ProductCategoryID`),
  CONSTRAINT `ProductCategory_ibfk_1` FOREIGN KEY (`ProductCategoryID`) REFERENCES `ProductCategory` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.ProductCategoryTrender
CREATE TABLE IF NOT EXISTS `ProductCategoryTrender` (
  `ID` bigint(20) NOT NULL auto_increment,
  `ProductCategoryID` bigint(20) NOT NULL,
  `UserID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `ProductCategoryID` (`ProductCategoryID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `ProductCategoryTrender_ibfk_1` FOREIGN KEY (`ProductCategoryID`) REFERENCES `ProductCategory` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ProductCategoryTrender_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.ProductVendor
CREATE TABLE IF NOT EXISTS `ProductVendor` (
  `ID` bigint(20) NOT NULL auto_increment,
  `VendorID` bigint(20) NOT NULL,
  `ProductID` bigint(20) NOT NULL,
  `Url` varchar(255) collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `VendorID` (`VendorID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `ProductVendor_ibfk_1` FOREIGN KEY (`VendorID`) REFERENCES `Vendor` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `ProductVendor_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.User
CREATE TABLE IF NOT EXISTS `User` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Created` datetime NOT NULL,
  `Modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `Username` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Password` varchar(100) collate utf8_unicode_ci NOT NULL,
  `UserID` varchar(50) collate utf8_unicode_ci NOT NULL,
  `UserLevel` smallint(1) NOT NULL default '1',
  `Surname` varchar(100) collate utf8_unicode_ci NOT NULL,
  `Picture` varchar(50) collate utf8_unicode_ci default NULL,
  `LastLogin` datetime default NULL,
  `Active` smallint(1) NOT NULL default '0',
  `Activated` datetime default NULL,
  `About` varchar(255) collate utf8_unicode_ci NOT NULL,
  `CityID` bigint(20) default NULL,
  `CountryID` bigint(20) default NULL,
  `Web` varchar(200) collate utf8_unicode_ci NOT NULL,
  `Address` varchar(255) collate utf8_unicode_ci NOT NULL,
  `FBID` varchar(100) collate utf8_unicode_ci default NULL,
  `Picsquare` varchar(250) collate utf8_unicode_ci default NULL COMMENT 'URL Facebook Image',
  `Gender` smallint(6) NOT NULL default '0' COMMENT '0=no specified,1=male, 2=female',
  `Birthday` datetime NOT NULL default '0000-00-00 00:00:00',
  `UserType` smallint(1) NOT NULL default '0' COMMENT 'Normal=0,Trender=1',
  `FacebookAccessToken` varchar(255) collate utf8_unicode_ci default NULL,
  `TwitterAccessToken` varchar(255) collate utf8_unicode_ci default NULL,
  `TwitterID` varchar(100) collate utf8_unicode_ci default NULL,
  `FacebookMobile` smallint(6) default '0',
  `TwitterMobile` smallint(6) default '0',
  PRIMARY KEY  (`ID`),
  KEY `username` (`Username`),
  KEY `CityID` (`CityID`),
  KEY `CountryID` (`CountryID`),
  CONSTRAINT `User_ibfk_1` FOREIGN KEY (`CityID`) REFERENCES `City` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `User_ibfk_2` FOREIGN KEY (`CountryID`) REFERENCES `Country` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.UserRelation
CREATE TABLE IF NOT EXISTS `UserRelation` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `FollowID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `FollowID` (`FollowID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `UserRelation_ibfk_1` FOREIGN KEY (`FollowID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `UserRelation_ibfk_2` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.UserStatus
CREATE TABLE IF NOT EXISTS `UserStatus` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `ProductID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `UserStatus_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `UserStatus_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.UserToFollow
CREATE TABLE IF NOT EXISTS `UserToFollow` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `UserToFollow_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.Vendor
CREATE TABLE IF NOT EXISTS `Vendor` (
  `ID` bigint(20) NOT NULL auto_increment,
  `Name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Data exporting was unselected.


# Dumping structure for table likekit.WishList
CREATE TABLE IF NOT EXISTS `WishList` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `ProductID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `WishList_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `WishList_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Dumping structure for table likekit.UserLike
CREATE TABLE IF NOT EXISTS `UserLike` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `ProductID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `UserLike_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `UserLike_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Data exporting was unselected.
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;



ALTER TABLE `Product` ADD `PartnerUrl` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `Product` CHANGE `Image` `Image` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

ALTER TABLE `UserLike` DROP INDEX `ProductID`;
ALTER TABLE `UserLike` ADD `KitID` bigint(20) NOT NULL ;

ALTER TABLE `UserLike`
  ADD CONSTRAINT `UserLike_ibfk_3` FOREIGN KEY (`KitID`) REFERENCES `Kit` (`ID`) ON UPDATE CASCADE;


# Dumping structure for table likekit.UserLike //REGISTER
CREATE TABLE IF NOT EXISTS `UserLikeProduct` (
  `ID` bigint(20) NOT NULL auto_increment,
  `UserID` bigint(20) NOT NULL,
  `ProductID` bigint(20) NOT NULL,
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `UserID` (`UserID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `UserLikeProduct_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `UserLikeProduct_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `User` ADD `OriginalPicture` VARCHAR( 100 ) NOT NULL ;
ALTER TABLE `User` ADD `RegisterType` TINYINT( 1 ) NOT NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `Notify` (
  `ID` bigint(20) NOT NULL auto_increment,
  `ToUserID` bigint(20) NOT NULL,
  `FromUserID` bigint(20) NOT NULL,
  `RefID` bigint(20) NOT NULL,
  `NotifyType` smallint(1) NOT NULL default '0' COMMENT 'None=0,Rekit=1,Comment=2,Follow=3,Like=4',  
  `Readed` smallint(1) NOT NULL default '0' COMMENT 'No=0,Yes=1',  
  `Created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`),
  KEY `ToUserID` (`ToUserID`),
  KEY `FromUserID` (`FromUserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `Notify`
  ADD CONSTRAINT `Notify_ibfk_1` FOREIGN KEY (`ToUserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `Notify_ibfk_2` FOREIGN KEY (`FromUserID`) REFERENCES `User` (`ID`) ON UPDATE CASCADE;
  
ALTER TABLE `Product` ADD `ASIN` VARCHAR( 255 ) NOT NULL ;

ALTER TABLE `Product` ADD `Active` smallint(1) NOT NULL default '1'; 
RENAME TABLE `likekit`.`Help` TO `likekit`.`StaticPage` ;
ALTER TABLE `AccessLog` ADD `Observations` VARCHAR( 255 ) NOT NULL ;
ALTER TABLE `Product` ADD `Observations` VARCHAR( 255 ) NULL ;
ALTER TABLE `AccessLog` ADD `Published` TIMESTAMP NULL ;
ALTER TABLE `User` ADD `UserToken` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
UPDATE `User` set UserToken = UserID;
ALTER TABLE `AccessLog` CHANGE `UserID` `UserID` BIGINT( 20 ) NULL;
ALTER TABLE `User` ADD `LanguageID` BIGINT( 20 ) NOT NULL default 1;

CREATE TABLE IF NOT EXISTS `GeoIPCities` (
  `locId` bigint(20) NOT NULL auto_increment,
  `country` varchar(2) NOT NULL,
  `region` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postalCode` varchar(6) NOT NULL,
  `latitude` decimal NOT NULL,
  `longitude` decimal NOT NULL,
  `metroCode` smallint(1) NOT NULL,  
  `areaCode` char(3) NOT NULL,
   PRIMARY KEY  (`locId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `GeoIPBlocks` (
	`Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`startIpNum` BIGINT(20) NOT NULL,
	`endIpNum` BIGINT(20) NOT NULL,
	`locId` BIGINT(20) NOT NULL,
	PRIMARY KEY (`Id`)
)
COLLATE='utf8_unicode_ci' ENGINE=InnoDB;


ALTER TABLE `StaticPage` ADD `SeoTitle` VARCHAR( 255 ) NOT NULL default '';
ALTER TABLE `StaticPage` ADD `SeoDescription` VARCHAR( 255 ) NOT NULL default '';
ALTER TABLE `StaticPage` ADD `SeoKeywords` VARCHAR( 255 ) NOT NULL default '';


ALTER TABLE `Vendor` ADD `Description` text collate utf8_unicode_ci NOT NULL default '';
ALTER TABLE `Vendor` ADD `Logo` VARCHAR( 100 ) NOT NULL default '';
ALTER TABLE `Vendor` ADD `Url` VARCHAR( 255 ) NOT NULL default '';
ALTER TABLE  `User` ADD  `VendorID` BIGINT NULL ;
