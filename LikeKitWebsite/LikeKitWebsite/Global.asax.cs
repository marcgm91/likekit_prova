﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LikeKitWebsite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              "Kits",
              "kits", // URL pattern
               new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
             "KitsCategoryTags",
             "kits/{Category}/{search}", // URL pattern
              new { controller = "Home", action = "HomeCategory", Category = "", search = "" }
           );


            routes.MapRoute(
              "KitsCategory",
              "kits/{Category}", // URL pattern
               new { controller = "Home", action = "HomeCategory", Category = "" }
            );

            routes.MapRoute(
              "Kit",
              "kit/{name}/{id}", // URL pattern
               new { controller = "Home", action = "Item" },
               new { id = @"\d+"}
            );

            routes.MapRoute(
              "KitTimeline",
              "kit/timeline/{name}/{id}", // URL pattern
               new { controller = "Timeline", action = "Item" },
               new { id = @"\d+" }
            );

            routes.MapRoute(
              "Ayuda",
              "ayuda", // URL pattern
               new { controller = "Home", action = "About", id=1 }
            );

            routes.MapRoute(
              "Prensa",
              "prensa", // URL pattern
               new { controller = "Home", action = "About", id = 2 }
            );

            routes.MapRoute(
              "Legal",
              "legal", // URL pattern
               new { controller = "Home", action = "About", id = 4 }
            );

            routes.MapRoute(
              "PerfilLikes",
              "perfil/{name}/{id}/likes", // URL pattern
               new { controller = "Timeline", action = "Likes" },
               new { id = @"\d+" }//, name = UrlParameter.Optional }
            );

            routes.MapRoute(
              "PublicPerfil",
              "perfilp/{name}/{id}", // URL pattern
               new { controller = "Home", action = "Profile" },
               new { id = @"\d+" }//, name = UrlParameter.Optional }
            );

            routes.MapRoute(
              "Perfil",
              "perfil/{name}/{id}", // URL pattern
               new { controller = "Timeline", action = "Profile" },
               new { id = @"\d+" }//, name = UrlParameter.Optional }
            );

            routes.MapRoute(
              "SitemapsGeneral",
              "sitemaps_general", // URL pattern
               new { controller = "Home", action = "Sitemap", id = 1 }
            );

            routes.MapRoute(
              "SitemapsKits",
              "sitemaps_kits", // URL pattern
               new { controller = "Home", action = "Sitemap", id = 2 }
            );

            routes.MapRoute(
              "SitemapsProfiles",
              "sitemaps_profiles", // URL pattern
               new { controller = "Home", action = "Sitemap", id = 3 }
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_Error(object sender, EventArgs e)
        {

            Exception exception = Server.GetLastError();
            Response.Clear();

            HttpException httpException = exception as HttpException;

            if (httpException != null)
            {
                string action;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found 
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error 
                        action = "HttpError500";
                        break;
                    default:
                        action = "General";
                        break;
                }

                // clear error on server 
                Server.ClearError();

                //Response.Redirect(String.Format("~/Error/{0}/?message={1}", action, exception.Message));
                Response.Redirect(String.Format("~/Error/{0}/", action));
            }
        }

    }
}