﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Collections;
using System.Configuration;

namespace LikeKitWebsite.Repository
{
    public class ProductsRepository : BaseRepository
    {
        public ProductsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Product GetProduct(IDataReader reader)
        {
            if (reader == null)
                return null;

            reader.Read();

            Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
            ProductCategory _productCategory = null;

            if (ProductCategoryID > 0)
            {
                _productCategory = new ProductCategory
                {
                    ID = ProductCategoryID
                };
            }

            Int64 LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");
            Language _language = null;

            if (LanguageID > 0)
            {
                _language = new Language
                {
                    ID = LanguageID
                };
            }

            Product product = new Product
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Name = Database.GetReaderValueString(reader, "Name"),
                Description = Database.GetReaderValueString(reader, "Description"),
                Author = Database.GetReaderValueString(reader, "Author"),
                Permalink = Database.GetReaderValueString(reader, "Permalink"),
                Year = (short)Database.GetReaderValueNumeric(reader, "Year"),
                Image = Database.GetReaderValueString(reader, "Image"),
                ProductCategory = _productCategory,
                Language = _language,
                EAN = Database.GetReaderValueString(reader, "EAN"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                PartnerUrl = Database.GetReaderValueString(reader, "PartnerUrl"),
                ASIN = Database.GetReaderValueString(reader, "ASIN"),
                Observations = Database.GetReaderValueString(reader, "Observations"),
                Active = (short)Database.GetReaderValueNumeric(reader, "Active")
            };

            return product;
        }

        public Product GetProduct(Int64 id)
        {
            String sql = String.Format("select * from Product where ID={0}", id);

            Product product = new Product();

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            
            try
            {
                product = GetProduct(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return product;
        }

        public void CreateProduct(Product Product)
        {
            Int64 ProductCategoryID = 0;

            if (Product.ProductCategory != null)
                ProductCategoryID = Product.ProductCategory.ID;

            Int64 LanguageID = 0;

            if (Product.Language != null)
                LanguageID = Product.Language.ID;


            String SQL = String.Format("INSERT INTO Product (Name, Description, Author, Permalink, Year, Image, ProductCategoryID, LanguageID, EAN, Created, Modified,PartnerUrl,ASIN,Observations,Active) " +
                "values ('{0}','{1}','{2}','{3}',{4},'{5}',{6},{7},'{8}','{9}','{10}','{11}','{12}','{13}',{14})",
                Database.ParseStringToSQL(Product.Name),
                Database.ParseStringToSQL(Product.Description),
                Database.ParseStringToSQL(Product.Author),
                Database.ParseStringToSQL(Product.Permalink),
                Product.Year,
                Database.ParseStringToSQL(Product.Image),
                ProductCategoryID,
                LanguageID,
                Database.ParseStringToSQL(Product.EAN),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseStringToSQL(Product.PartnerUrl),
                Database.ParseStringToSQL(Product.ASIN),
                Database.ParseStringToSQL(Product.Observations),
                Product.Active
                );

            Database.ExecuteCommand(SQL);
        }

        public Int64 CreateWSProduct(String amazon_asin, String amazon_title, String amazon_author, String amazon_year, String amazon_category, String amazon_image, String amazon_url, String amazon_description, String amazon_ean, String amazon_observations)
        {
            Int64 ProductCategoryID = 0;

            if (amazon_category == "Libros")
                ProductCategoryID = 1;
            else if (amazon_category == "Series")
                ProductCategoryID = 2;
            else if (amazon_category == "Cine")
                ProductCategoryID = 3;
            else if (amazon_category == "Música")
                ProductCategoryID = 4;

            Int64 LanguageID = 1;

            //Ara pugem la imatge
            string imageName = String.Format("{0}", amazon_asin);
            try
            {
                //Tractem la imatge
                System.Drawing.Image image = Globals.DownloadImageFromUrl(amazon_image);
                //string imageName = Path.GetFileName(picsquare);

                string fileName = System.IO.Path.Combine(ConfigurationManager.AppSettings["PathImages"], imageName);
                image.Save(fileName);
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, amazon_image);
                Globals.SaveLog(Database, ex_msg, Models.Type.Register, 1);
            }

            if ((amazon_year == String.Empty) || (amazon_year == ""))
                amazon_year = "0";

            String SQL = String.Format("INSERT INTO Product (Name, Description, Author, Permalink, Year, Image, ProductCategoryID, LanguageID, EAN, Created, Modified,PartnerUrl, ASIN, Observations) " +
                "values ('{0}','{1}','{2}','{3}',{4},'{5}',{6},{7},'{8}','{9}','{10}','{11}','{12}','{13}')",
                Database.ParseStringToSQL(amazon_title),
                Database.ParseStringToSQL(amazon_description),
                Database.ParseStringToSQL(amazon_author),
                Database.ParseStringToSQL(String.Empty),
                amazon_year,
                Database.ParseStringToSQL(imageName),
                ProductCategoryID,
                LanguageID,
                Database.ParseStringToSQL(amazon_ean),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseStringToSQL(amazon_url),
                Database.ParseStringToSQL(amazon_asin),
                Database.ParseStringToSQL(amazon_observations)
                );

            Database.ExecuteCommand(SQL);


            Int64 ID = Database.GetLastAutoIncrement();

            return ID;
        }

        public void UpdateProduct(Product Product)
        {
            Int64 ProductCategoryID = 0;

            if (Product.ProductCategory != null)
                ProductCategoryID = Product.ProductCategory.ID;

            Int64 LanguageID = 0;

            if (Product.Language != null)
                LanguageID = Product.Language.ID;

            String SQL = String.Format("UPDATE Product SET Name='{0}'," +
                                       "Description='{1}',Author='{2}',Permalink='{3}',Year={4},Image='{5}',ProductCategoryID={6},LanguageID={7}, " +
                                       "EAN='{8}',Created='{9}',Modified='{10}',PartnerUrl='{11}',ASIN='{12}',Observations='{13}',Active={14}" +
                                       " WHERE ID={15}",
                                        Database.ParseStringToSQL(Product.Name),
                                        Database.ParseStringToSQL(Product.Description),
                                        Database.ParseStringToSQL(Product.Author),
                                        Database.ParseStringToSQL(Product.Permalink),
                                        Product.Year,
                                        Database.ParseStringToSQL(Product.Image),
                                        ProductCategoryID,
                                        LanguageID,
                                        Database.ParseStringToSQL(Product.EAN),
                                        Database.ParseDateTimeToSQL(Product.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        Database.ParseStringToSQL(Product.PartnerUrl),
                                        Database.ParseStringToSQL(Product.ASIN),
                                        Database.ParseStringToSQL(Product.Observations),
                                        Product.Active,
                                        Product.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteProduct(Product Product)
        {
            String SQL = String.Format("DELETE FROM Product " +
                                       " WHERE ID={0}",
                                        Product.ID);

            Database.ExecuteCommand(SQL);
        }

        public ArrayList getProductsByCategoryID(Int64 CategoryID, int limit)
        {
            String sql = String.Format("select * from Product where ProductCategoryID = {0} Order by ID ASC LIMIT 0,{1}", CategoryID, limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                JsonProducts obj;

                while (reader.Read())
                {
                    obj = new JsonProducts();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.Name = Database.GetReaderValueString(reader, "Name");

                    List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return List;
        }

        public String GetRegisterCategoryProducts()
        {
            String sHtml = String.Empty;

            sHtml += GetRegisterCategoryProductsByCategory(1);
            sHtml += GetRegisterCategoryProductsByCategory(3);
            sHtml += GetRegisterCategoryProductsByCategory(2);
            sHtml += GetRegisterCategoryProductsByCategory(4);

            return sHtml;
        }

        public String GetRegisterCategoryProductsByCategory(Int64 CategoryID)
        {
            String sHtml = String.Empty;

            String sql = "select pr.ID, pr.Name as ProductName,pr.Image as ProductImage, pr.Author as AuthorName" +
                " from Product pr " +
                " where 1=1 AND pr.ProductCategoryID = " + CategoryID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String css_id = String.Empty;
            String category_title = String.Empty;
            String margin_left = String.Empty;

            switch (CategoryID)
            {
                case 1:
                    css_id = "books";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIBROS;
                    break;
                case 2:
                    css_id = "series";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SERIES;
                    margin_left = " left20";
                    break;
                case 3:
                    css_id = "cinema";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CINE;
                    margin_left = " left20";
                    break;
                case 4:
                    css_id = "music";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUSICA;
                    break;
            }

            Int16 count = 0;
            String sProductsHtml = String.Empty;

            try
            {
                while (reader.Read())
                {
                    count++;

                    if (count < 7)
                    {
                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        String Name = Database.GetReaderValueString(reader, "ProductName");
                        String Author = Database.GetReaderValueString(reader, "AuthorName");
                        String ProductImage = Database.GetReaderValueString(reader, "ProductImage");

                        sProductsHtml += "<div class=\"register-product\">";
                        sProductsHtml += "<div class=\"register-product-image noselect\" onclick=\"AddRegisterProduct(" + ID + ");\"><img src=\"/Content/files/" + ProductImage + "\" width=\"97px\" /><div class=\"register-product-image-selection\"></div></div>";
                        sProductsHtml += "<div class=\"register-product-text\"><span class=\"register-product-text-title\">" + Name + "</span><br />" + Author + "</div>";
                        sProductsHtml += "</div>";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            if (count > 0)
            {
                sHtml += "<div class=\"register-category\">";
                sHtml += "<div class=\"register-category-title\">" + category_title + "</div>";
                sHtml += sProductsHtml;
                sHtml += "</div>";

                if(CategoryID != 4)
                    sHtml += "<div class=\"register-category-separator\"></div>";
            }

            return sHtml;
        }

        public String KitProductDataHtml(Int64 ProductID, Int64 KitID = 0, Int64 ReKitID = 0)
        {
            String sHtml = String.Empty;

            //String sTags = "Agregar etiquetas";
            String sTags = String.Empty;
            if (KitID > 0)
            { 
                ArrayList tags = new ArrayList();
                KitsRepository kRP = new KitsRepository(Database);
                tags = kRP.GetKitTags(KitID);

                int i = 0;
                sTags = String.Empty;
                foreach(String item in tags)
                {
                    if (i > 0)
                        sTags += ",";
                    sTags += item;
                    i++;
                }
            }

            Int64 id_temp = 0;
            if (KitID > 0)
                id_temp = KitID;
            else if (ReKitID > 0)
                id_temp = ReKitID;

            Double temp_valoration = 3;

            if (id_temp > 0)
            { 
                KitsRepository kRP = new KitsRepository(Database);
                Kit kt_tmp = kRP.GetKitData(id_temp);

                temp_valoration = kt_tmp.Valoration;
            }


            //Recommended Tags
            ArrayList _recommended_tags = GetRecommendedProductTags(ProductID);

            String sql = "select pr.ID, pr.Name as ProductName,pr.Image as ProductImage, pr.Author as AuthorName, pr.Description, pr.Year, pr.ProductCategoryID " +
                " from Product pr " +
                " where 1=1 AND pr.ID = " + ProductID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                while (reader.Read())
                {
                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    String Name = Database.GetReaderValueString(reader, "ProductName");
                    String Author = Database.GetReaderValueString(reader, "AuthorName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                    Int16 Year = Database.GetReaderValueInt16(reader, "Year");
                    String Description = Database.GetReaderValueString(reader, "Description");
                    Int64 Categoria = Database.GetReaderValueInt64(reader, "ProductCategoryID");

                    sHtml += "<div id=\"timeline-kit-product-image\"><img src=\"/Content/files/" + ProductImage + "\" width=\"220px\" /></div>\n" +
                        "<input type=\"hidden\" id=\"kit_product_id\" name=\"kit_product_id\" value=\"" + ID + "\" />\n" +
                        "<input type=\"hidden\" id=\"kit_recommended_tags\" name=\"kit_recommended_tags\" value=\"\" />\n" +
                        "<input type=\"hidden\" id=\"kit_id\" name=\"kit_id\" value=\"" + KitID + "\" />\n" +
                        "<input type=\"hidden\" id=\"ref_kit\" name=\"ref_kit\" value=\"" + ReKitID + "\" />\n" +
                        "<input type=\"hidden\" id=\"kit_valoration\" name=\"kit_valoration\" value=\"" + temp_valoration + "\" />\n" +
                        "<div id=\"timeline-kit-product-info\">\n" +
                            "<div id=\"timeline-kit-product-title\">" + Name + "</div>\n" +
                            "<div id=\"timeline-kit-product-author\">" + Author + "</div>\n" +
                            "<div id=\"timeline-kit-product-year\">" + Year + "</div>\n" +
                            "<div id=\"timeline-kit-product-moreinfo\">\n" +
                                "<div id=\"timeline-kit-product-moreinfo-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MAS_INFORMACION + "</div>\n" +
                                "<div id=\"timeline-kit-product-moreinfo-container\">\n" +
                                    "<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ANO + ".</span> " + Year + "</div>\n";
                                    //"<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">Metrage.</span> 125 mins.</div>\n";

                                /*if (Categoria != 4)
                                    sHtml += "<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">Idioma.</span> Español</div>\n";*/
                                if((Description != String.Empty) && (Description != ""))
                                    sHtml += "<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_DESCRIPCION + ".</span> " + Description + "</div>\n";
                                sHtml += "</div>\n" +
                            "</div>\n" +
                            "<div id=\"timeline-kit-add-tags\"><input type=\"text\" name=\"timeline-kit-add-tags-input\" id=\"timeline-kit-add-tags-input\" value=\"" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_AGREGAR_ETIQUETAS + "\" /></div>\n" +
                            "<div id=\"timeline-kit-add-tags-container\">\n" +
                                "<textarea id=\"kit_form_tags\" name=\"kit_form_tags\">" + sTags + "</textarea>\n" +
                                "<script type=\"text/javascript\">\n" +
                                "$('#timeline-kit-add-tags-container textarea').BetterGrow({\n" +
                                    "on_enter:	function() {\n" +
                                                    "//$('#area3_flag').css('visibility', 'visible');\n" +
                                                "}\n" +
                                "});\n" +
                                "</script>\n" +
                            "</div>\n" +
                            "<div id=\"timeline-kit-recommended-tags\">\n" +
                                "<div id=\"timeline-kit-recommended-tags-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ETIQUETAS_RECOMENDADAS + "</div>\n" +
                                "<div id=\"timeline-kit-recommended-tags-container\">\n";

                                foreach (String item in _recommended_tags)
                                {
                                    sHtml += "<div class=\"timeline-kit-recommended-tags-text\">" + item + "</div>\n";
                                }
                                    sHtml += "</div>\n" +
                                    "</div>\n" +
                                "</div>\n";


                    //temp_valoration

                    sHtml += "<div id=\"timeline-kit-product-valoration-container\">\n" +
                            "<div class=\"timeline-kit-product-valoration-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_VALORACION + "</div>\n";

                    String sCss1 = "";
                    if (temp_valoration == 1)
                        sCss1 = " timeline-kit-product-valoration-1-selected";

                    sHtml += "<div id=\"timeline-kit-product-valoration-1\" class=\"timeline-kit-product-valoration-option " + sCss1 + "\" onclick=\"SetValoration(1);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_NADA_RECOMENDABLE + "</div>\n" +
                            "</div>\n";

                    String sCss2 = "";
                    if (temp_valoration == 2)
                        sCss2 = " timeline-kit-product-valoration-2-selected";

                    sHtml += "<div id=\"timeline-kit-product-valoration-2\" class=\"timeline-kit-product-valoration-option " + sCss2 + "\" onclick=\"SetValoration(2);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_POCO_RECOMENDABLE + "</div>\n" +
                            "</div>\n";

                    String sCss3 = "";
                    if (temp_valoration == 3)
                        sCss3 = " timeline-kit-product-valoration-3-selected";

                    sHtml += "<div id=\"timeline-kit-product-valoration-3\" class=\"timeline-kit-product-valoration-option " + sCss3 + "\" onclick=\"SetValoration(3);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_RECOMENDABLE + "</div>\n" +
                            "</div>\n";

                    String sCss4 = "";
                    if (temp_valoration == 4)
                        sCss4 = " timeline-kit-product-valoration-4-selected";

                    sHtml += "<div id=\"timeline-kit-product-valoration-4\" class=\"timeline-kit-product-valoration-option " + sCss4 + "\" onclick=\"SetValoration(4);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUY_RECOMENDABLE + "</div>\n" +
                            "</div>\n";

                    String sCss5 = "";
                    if (temp_valoration == 5)
                        sCss5 = " timeline-kit-product-valoration-5-selected";

                    sHtml += "<div id=\"timeline-kit-product-valoration-5\" class=\"timeline-kit-product-valoration-option " + sCss5 + "\" onclick=\"SetValoration(5);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IMPRESCINDIBLE + "</div>\n" +
                            "</div>\n";


                    sHtml += "</div>\n";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return sHtml;
        }

        public String KitWSProductDataHtml(String ASIN, String Title, String Author, String Year, String Category, String LargeImage, String URL, String DescriptionAMZ, String EAN, String Observations)
        {
            String sHtml = String.Empty;
            String sTags = String.Empty;

            //Recommended Tags
            //ArrayList _recommended_tags = GetRecommendedProductTags(ProductID);

            String Description = String.Empty;

            try
            {
                sHtml += "<div id=\"timeline-kit-product-image\"><img src=\"" + LargeImage + "\" width=\"220px\" /></div>\n" +
                    "<input type=\"hidden\" id=\"amazon_product\" name=\"amazon_product\" value=\"1\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_asin\" name=\"amazon_asin\" value=\"" + ASIN + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_title\" name=\"amazon_title\" value=\"" + Title + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_author\" name=\"amazon_author\" value=\"" + Author + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_year\" name=\"amazon_year\" value=\"" + Year + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_category\" name=\"amazon_category\" value=\"" + Category + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_image\" name=\"amazon_image\" value=\"" + LargeImage + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_url\" name=\"amazon_url\" value=\"" + URL + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_description\" name=\"amazon_description\" value=\"" + DescriptionAMZ + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_ean\" name=\"amazon_ean\" value=\"" + EAN + "\" />\n" +
                    "<input type=\"hidden\" id=\"amazon_observations\" name=\"amazon_observations\" value=\"" + Observations + "\" />\n" +

                    "<input type=\"hidden\" id=\"kit_recommended_tags\" name=\"kit_recommended_tags\" value=\"\" />\n" +
                    "<input type=\"hidden\" id=\"kit_valoration\" name=\"kit_valoration\" value=\"3\" />\n" +
                    "<div id=\"timeline-kit-product-info\">\n" +
                        "<div id=\"timeline-kit-product-title\">" + Title + "</div>\n" +
                        "<div id=\"timeline-kit-product-author\">" + Author + "</div>\n" +
                        "<div id=\"timeline-kit-product-year\">" + Year + "</div>\n" +
                        "<div id=\"timeline-kit-product-moreinfo\">\n" +
                            "<div id=\"timeline-kit-product-moreinfo-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MAS_INFORMACION + "</div>\n" +
                            "<div id=\"timeline-kit-product-moreinfo-container\">\n" +
                                "<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ANO + ".</span> " + Year + "</div>\n";
                                //"<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">Metrage.</span> 125 mins.</div>\n";

                    /*if (Category != "Musica")
                        sHtml += "<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">Idioma.</span> Español</div>\n";*/
                    if ((Description != String.Empty) && (Description != ""))
                        sHtml += "<div class=\"timeline-kit-product-moreinfo-text\"><span class=\"black\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_DESCRIPCION + ".</span> " + Description + "</div>\n";
                    sHtml += "</div>\n" +
                "</div>\n" +
                "<div id=\"timeline-kit-add-tags\"><input type=\"text\" name=\"timeline-kit-add-tags-input\" id=\"timeline-kit-add-tags-input\" value=\"" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_AGREGAR_ETIQUETAS + "\" /></div>\n" +
                "<div id=\"timeline-kit-add-tags-container\">\n" +
                    "<textarea id=\"kit_form_tags\" name=\"kit_form_tags\">" + sTags + "</textarea>\n" +
                    "<script type=\"text/javascript\">\n" +
                    "$('#timeline-kit-add-tags-container textarea').BetterGrow({\n" +
                        "on_enter:	function() {\n" +
                                        "//$('#area3_flag').css('visibility', 'visible');\n" +
                                    "}\n" +
                    "});\n" +
                    "</script>\n" +
                "</div>\n" +
                "<div id=\"timeline-kit-recommended-tags\">\n" +
                    "<div id=\"timeline-kit-recommended-tags-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ETIQUETAS_RECOMENDADAS + "</div>\n" +
                    "<div id=\"timeline-kit-recommended-tags-container\">\n";

                    /*foreach (String item in _recommended_tags)
                    {
                        sHtml += "<div class=\"timeline-kit-recommended-tags-text\">" + item + "</div>\n";
                    }*/
                    sHtml += "</div>\n" +
                    "</div>\n" +
                "</div>\n";

                    sHtml += "<div id=\"timeline-kit-product-valoration-container\">\n" +
                            "<div class=\"timeline-kit-product-valoration-title\">Valoración</div>\n" +
                            "<div id=\"timeline-kit-product-valoration-1\" class=\"timeline-kit-product-valoration-option\" onclick=\"SetValoration(1);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_NADA_RECOMENDABLE + "</div>\n" +
                            "</div>\n" +
                            "<div id=\"timeline-kit-product-valoration-2\" class=\"timeline-kit-product-valoration-option\" onclick=\"SetValoration(2);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_POCO_RECOMENDABLE + "</div>\n" +
                            "</div>\n" +
                            "<div id=\"timeline-kit-product-valoration-3\" class=\"timeline-kit-product-valoration-option timeline-kit-product-valoration-3-selected\" onclick=\"SetValoration(3);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_RECOMENDABLE + "</div>\n" +
                            "</div>\n" +
                            "<div id=\"timeline-kit-product-valoration-4\" class=\"timeline-kit-product-valoration-option\" onclick=\"SetValoration(4);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUY_RECOMENDABLE + "</div>\n" +
                            "</div>\n" +
                            "<div id=\"timeline-kit-product-valoration-5\" class=\"timeline-kit-product-valoration-option\" onclick=\"SetValoration(5);\">\n" +
                                "<div class=\"timeline-kit-product-valoration-option-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IMPRESCINDIBLE + "</div>\n" +
                            "</div>\n" +
                        "</div>\n";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }

        public ArrayList GetRecommendedProductTags(Int64 ProductID)
        {
            String sHtml = String.Empty;

            String sql = String.Format("SELECT kt.name, count(*) as count FROM KitTag kt, Kit k WHERE k.ID = kt.KitID and k.ProductID = {0} Group by kt.Name order by count desc limit 5",ProductID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList tags = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    String Name = Database.GetReaderValueString(reader, "Name");
                    tags.Add(Name);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Si no hi ha tags, hauriem d'agafar el nom de la categoria
            if (tags.Count == 0)
            {
                String sql_categories = String.Format("SELECT pc.Name from Product p, ProductCategory pc where p.ProductCategoryID=pc.ID AND p.ID ={0}", ProductID);

                IDbCommand command_categories = Database.CreateCommand(sql_categories);
                IDataReader reader_categories = command_categories.ExecuteReader();

                try
                {
                    while (reader_categories.Read())
                    {
                        String Name = Database.GetReaderValueString(reader_categories, "Name");
                        tags.Add(Name);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader_categories.Close();
                    reader_categories.Dispose();
                    command_categories.Dispose();
                }            
            }

            return tags;
        }


        public Int64 WSProductExists(String ASIN)
        {
            if ((ASIN == String.Empty) || (ASIN == ""))
                return 0;

            String sql = "select ID from Product where ASIN='" + ASIN + "'";
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");

                    return ID;
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return 0;
        }
    }
}