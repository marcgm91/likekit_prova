﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class MailsRepository : BaseRepository
    {
        public MailsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Mail GetMail(IDataReader reader)
        {
            if (reader == null)
                return null;

            Mail mail = new Mail
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Sender = Database.GetReaderValueString(reader, "Sender"),
                Subject = Database.GetReaderValueString(reader, "Subject"),
                Body = Database.GetReaderValueString(reader, "Body"),
                Status = (Status)Database.GetReaderValueInt(reader, "Status"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return mail;
        }

        public void CreateMail(Mail Mail)
        {
            String SQL = String.Format("INSERT INTO Mail (Sender, Subject, Body, Status, Created, Modified) " +
                "values ('{0}','{1}','{2}',{3},'{4}','{5}')",
                Database.ParseStringToSQL(Mail.Sender),
                Database.ParseStringToSQL(Mail.Subject),
                Database.ParseStringToSQL(Mail.Body),
                (short)Mail.Status,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateMail(Mail Mail)
        {
            String SQL = String.Format("UPDATE Mail SET Sender='{0}'," +
                                       "Subject='{1}', Body='{2}', Status={3}, Created='{4}', Modified='{5}' " +
                                       " WHERE ID={6}",
                                       Database.ParseStringToSQL(Mail.Sender),
                                       Database.ParseStringToSQL(Mail.Subject),
                                       Database.ParseStringToSQL(Mail.Body),
                                       (short)Mail.Status,
                                       Database.ParseDateTimeToSQL(Mail.Created),
                                       Database.ParseDateTimeToSQL(System.DateTime.Now),
                                       Mail.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteMail(Mail Mail)
        {
            String SQL = String.Format("DELETE FROM Mail " +
                                       " WHERE ID={0}",
                                        Mail.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}