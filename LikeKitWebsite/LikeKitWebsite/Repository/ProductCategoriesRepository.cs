﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class ProductCategoriesRepository : BaseRepository
    {
        public ProductCategoriesRepository(DatabaseBase database)
        {
            Database = database;
        }

        private ProductCategory GetProductCategory(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 ParentProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
            ProductCategory _parentProductCategory = null;

            if (ParentProductCategoryID > 0)
            {
                _parentProductCategory = new ProductCategory
                {
                    ID = ParentProductCategoryID
                };
            }

            ProductCategory productCategory = new ProductCategory
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Name = Database.GetReaderValueString(reader, "Name"),
                ParentProductCategory = _parentProductCategory,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return productCategory;
        }

        public void CreateProductCategory(ProductCategory ProductCategory)
        {
            Int64 ParentProductCategoryID = 0;

            if (ProductCategory.ParentProductCategory != null)
                ParentProductCategoryID = ProductCategory.ParentProductCategory.ID;

            String SQL = String.Format("INSERT INTO ProductCategory (Name, ProductCategoryID, Created, Modified) " +
                "values ('{0}',{1},'{2}','{3}')",
                Database.ParseStringToSQL(ProductCategory.Name),
                ParentProductCategoryID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateProductCategory(ProductCategory ProductCategory)
        {
            Int64 ParentProductCategoryID = 0;

            if (ProductCategory.ParentProductCategory != null)
                ParentProductCategoryID = ProductCategory.ParentProductCategory.ID;

            String SQL = String.Format("UPDATE ProductCategory SET Name='{0}'," +
                                       "ProductCategoryID={1},Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                        Database.ParseStringToSQL(ProductCategory.Name),
                                        ParentProductCategoryID,
                                        Database.ParseDateTimeToSQL(ProductCategory.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        ProductCategory.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteProductCategory(ProductCategory ProductCategory)
        {
            String SQL = String.Format("DELETE FROM ProductCategory " +
                                       " WHERE ID={0}",
                                        ProductCategory.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}