﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class ProductCategoryTendersRepository : BaseRepository
    {
        public ProductCategoryTendersRepository(DatabaseBase database)
        {
            Database = database;
        }

        private ProductCategoryTender GetProductCategoryTender(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
            ProductCategory _productCategory = null;

            if (ProductCategoryID > 0)
            {
                _productCategory = new ProductCategory
                {
                    ID = ProductCategoryID
                };
            }

            ProductCategoryTender productCategoryTender = new ProductCategoryTender
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                ProductCategory = _productCategory,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return productCategoryTender;
        }

        public void CreateProductCategoryTender(ProductCategoryTender ProductCategoryTender)
        {
            Int64 UserID = 0;

            if (ProductCategoryTender.User != null)
                UserID = ProductCategoryTender.User.ID;

            Int64 ProductCategoryID = 0;

            if (ProductCategoryTender.ProductCategory != null)
                ProductCategoryID = ProductCategoryTender.ProductCategory.ID;

            String SQL = String.Format("INSERT INTO ProductCategoryTender (UserID, ProductCategoryID, Created, Modified) " +
                "values ({0},{1},'{2}','{3}')",
                UserID,
                ProductCategoryID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateProductCategoryTender(ProductCategoryTender ProductCategoryTender)
        {
            Int64 UserID = 0;

            if (ProductCategoryTender.User != null)
                UserID = ProductCategoryTender.User.ID;

            Int64 ProductCategoryID = 0;

            if (ProductCategoryTender.ProductCategory != null)
                ProductCategoryID = ProductCategoryTender.ProductCategory.ID;

            String SQL = String.Format("UPDATE ProductCategoryTender SET UserID={0}," +
                                       "ProductCategoryID={1},Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                        UserID,
                                        ProductCategoryID,
                                        Database.ParseDateTimeToSQL(ProductCategoryTender.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        ProductCategoryTender.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteProductCategoryTender(ProductCategoryTender ProductCategoryTender)
        {
            String SQL = String.Format("DELETE FROM ProductCategoryTender " +
                                       " WHERE ID={0}",
                                        ProductCategoryTender.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}