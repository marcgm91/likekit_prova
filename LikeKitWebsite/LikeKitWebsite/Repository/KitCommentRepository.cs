﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;

namespace LikeKitWebsite.Repository
{
    public class KitCommentRepository : BaseRepository
    {
        private KitComment GetKitComment(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 KitID = Database.GetReaderValueInt64(reader, "KitID");
            Kit _kit = null;

            if (KitID > 0)
            {
                _kit = new Kit
                {
                    ID = KitID
                };
            }

            KitComment kitComment = new KitComment
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Kit = _kit,
                Comment = Database.GetReaderValueString(reader, "Comment"),
                Valoration = Database.GetReaderValueDouble(reader, "Valoration"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                Visible = Database.GetReaderValueInt16(reader, "Visible") == 1
            };

            return kitComment;
        }

        public void CreateKit(KitComment kitComment)
        {
            Int64 KitID = 0;

            if (kitComment.Kit != null)
                KitID = kitComment.Kit.ID;

            String SQL = String.Format("INSERT INTO KitComment (KitID, UserID, Created, Modified, Valoration, Comment, Visible) " +
                "values ({0},{1},'{2}','{3}','{4}','{5}',{6})",
                kitComment.User.ID,
                kitComment.Kit.ID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDoubleToSQL(kitComment.Valoration),
                Database.ParseStringToSQL(kitComment.Comment),
                kitComment.Visible ? 1 : 0
                );

            Database.ExecuteCommand(SQL);
        }
    }
}