﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class LanguagesRepository : BaseRepository
    {
        public LanguagesRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Language GetLanguage(IDataReader reader)
        {
            if (reader == null)
                return null;

            Language language = new Language
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Code = Database.GetReaderValueString(reader, "Code"),
                Description = Database.GetReaderValueString(reader, "Description")
            };

            return language;
        }

        public void CreateLanguage(Language Language)
        {
            String SQL = String.Format("INSERT INTO Language (Code, Description) " +
                "values ('{0}','{1}')",
                Database.ParseStringToSQL(Language.Code),
                Database.ParseStringToSQL(Language.Description)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateLanguage(Language Language)
        {
            String SQL = String.Format("UPDATE Language SET Code='{0}'," +
                                       "Description='{1}' " +
                                       " WHERE ID={2}",
                                       Database.ParseStringToSQL(Language.Code),
                                       Database.ParseStringToSQL(Language.Description),
                                       Language.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteLanguage(Language Language)
        {
            String SQL = String.Format("DELETE FROM Language " +
                                       " WHERE ID={0}",
                                        Language.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}