﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class AccessLogsRepository : BaseRepository
    {
        public AccessLogsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private AccessLog GetAccessLog(IDataReader reader)
        {
            if (reader == null)
                return null;

            AccessLog accesslog = null;

            if (reader.Read())
            {
                Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                User _user = null;

                if (UserID > 0)
                {
                    _user = new User
                    {
                        ID = UserID
                    };
                }

                accesslog = new AccessLog
                {
                    ID = Database.GetReaderValueInt64(reader, "ID"),
                    RefID = Database.GetReaderValueInt64(reader, "RefID"),
                    User = _user,
                    ObjectType = (AccesLogObjectType)Database.GetReaderValueInt16(reader, "ObjectType"),
                    OperationType = (OperationType)Database.GetReaderValueInt16(reader, "OperationType"),
                    DeviceType = (DeviceType)Database.GetReaderValueInt16(reader, "DeviceType"),
                    IP = Database.GetReaderValueString(reader, "IP"),
                    SessionID = Database.GetReaderValueString(reader, "SessionID"),
                    Created = Database.GetReaderValueDateTime(reader, "Created"),
                    Observations = Database.GetReaderValueString(reader, "Observations"),
                    Published = Database.GetReaderValueDateTime(reader, "Published"),
                };
            }

            return accesslog;
        }

        public AccessLog GetAccessLog(Int64 id)
        {
            String sql = String.Format("select ID, Created, RefID, UserID, ObjectType, OperationType, DeviceType, Ip, SessionID, Observations, Published from AccessLog where ID={0}", id);

            AccessLog accesslog = new AccessLog();

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                accesslog = GetAccessLog(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return accesslog;
        }

        public void CreateAccessLog(AccessLog AccessLog)
        {
            Int64 UserID = 0;

            if (AccessLog.User != null)
                UserID = AccessLog.User.ID;

            String SQL = String.Format("INSERT INTO AccessLog (RefID, UserID, ObjectType, OperationType, DeviceType, IP, SessionID, Created, Observations) " +
                "values ({0},{1},{2},{3},{4},'{5}','{6}','{7}','{8}')",
                AccessLog.RefID,
                Database.ParseIDToSQLNull(UserID),
                (Int16)AccessLog.ObjectType,
                (Int16)AccessLog.OperationType,
                (Int16)AccessLog.DeviceType,
                Database.ParseStringToSQL(AccessLog.IP),
                Database.ParseStringToSQL(AccessLog.SessionID),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseStringToSQL(AccessLog.Observations)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateAccessLog(AccessLog AccessLog)
        {
            Int64 UserID = 0;

            if (AccessLog.User != null)
                UserID = AccessLog.User.ID;

            String SQL = String.Format("UPDATE AccessLog SET RefID={0}," +
                                       "UserID={1},ObjectType={2},OperationType={3},DeviceType={4},IP='{5}',SessionID='{6}', " +
                                       "Created='{7}',Observations='{8}', Published='{9}' " +
                                       " WHERE ID={10}",
                                        AccessLog.RefID,
                                        UserID,
                                        (Int16)AccessLog.ObjectType,
                                        (Int16)AccessLog.OperationType,
                                        (Int16)AccessLog.DeviceType,
                                        Database.ParseStringToSQL(AccessLog.IP),
                                        Database.ParseStringToSQL(AccessLog.SessionID),
                                        Database.ParseDateTimeToSQL(AccessLog.Created),
                                        Database.ParseStringToSQL(AccessLog.Observations),
                                        Database.ParseDateTimeToSQL(AccessLog.Published),
                                        AccessLog.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteAccessLog(AccessLog AccessLog)
        {
            String SQL = String.Format("DELETE FROM AccessLog " +
                                       " WHERE ID={0}",
                                        AccessLog.ID);

            Database.ExecuteCommand(SQL);
        }

        public AccessLog GetLastAccessLogFromUser(Int64 UserID, OperationType Operation)
        {
            String sql = String.Format("select ID, Created, RefID, UserID, ObjectType, OperationType, DeviceType, Ip, SessionID, Observations, Published from AccessLog where UserID={0} AND OperationType={1} Order by Created DESC Limit 1", UserID, (Int16)Operation);

            AccessLog accesslog = new AccessLog();

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                accesslog = GetAccessLog(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return accesslog;
        }
    }
}