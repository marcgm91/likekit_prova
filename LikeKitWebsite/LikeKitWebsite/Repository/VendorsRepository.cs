﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class VendorsRepository : BaseRepository
    {
        public VendorsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Vendor GetVendor(IDataReader reader)
        {
            if (reader == null)
                return null;

            Vendor vendor = new Vendor
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Name = Database.GetReaderValueString(reader, "Name"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return vendor;
        }

        public void CreateVendor(Vendor Vendor)
        {
            String SQL = String.Format("INSERT INTO Vendor (Name, Created, Modified) " +
                "values ('{0}','{1}','{2}')",
                Database.ParseStringToSQL(Vendor.Name),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateVendor(Vendor Vendor)
        {
            String SQL = String.Format("UPDATE Vendor SET Name='{0}'," +
                                       "Created='{1}',Modified='{2}' " +
                                       " WHERE ID={3}",
                                        Database.ParseStringToSQL(Vendor.Name),
                                        Database.ParseDateTimeToSQL(Vendor.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        Vendor.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteVendor(Vendor Vendor)
        {
            String SQL = String.Format("DELETE FROM Vendor " +
                                       " WHERE ID={0}",
                                        Vendor.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}