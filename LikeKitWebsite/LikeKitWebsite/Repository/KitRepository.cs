﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;

namespace LikeKitWebsite.Repository
{
    public class KitRepository : BaseRepository
    {
        private Kit GetKit(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
            Product _product = null;

            if(ProductID > 0)
            {
                _product = new Product { 
                    ID = ProductID
                };
            }

            Int64 ParentKitID = Database.GetReaderValueInt64(reader, "ParentKitID");
            Kit _parentkit = null;

            if (ParentKitID > 0)
            {
                _parentkit = new Kit { 
                    ID = ParentKitID
                };
            }

            Kit kit = new Kit
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Product = _product,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                Comment = Database.GetReaderValueString(reader, "Comment"),
                Valoration = Database.GetReaderValueDouble(reader, "Valoration"),
                ParentKit = _parentkit,
                Visible = Database.GetReaderValueInt16(reader, "Visible") == 1
            };

            return kit;
        }

        public void CreateKit(Kit kit)
        {
            Int64 ProductID = 0;
            Int64 ParentKitID = 0;

            if (kit.Product != null)
                ProductID = kit.Product.ID;

            if (kit.ParentKit != null)
                ParentKitID = kit.ParentKit.ID;

            String SQL = String.Format("INSERT INTO Kit (UserID, ProductID, Created, Modified, Valoration, Comment, ParentKitID, Visible) " +
                "values ({0},{1},'{2}','{3}','{4}','{5}',{6},{7})",
                kit.User.ID,
                Database.ParseIDToSQLNull(ProductID),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDoubleToSQL(kit.Valoration),
                Database.ParseStringToSQL(kit.Comment),
                Database.ParseIDToSQLNull(ParentKitID),
                kit.Visible ? 1 : 0
                );
            
            Database.ExecuteCommand(SQL);
        }
    }
}