﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Collections;
using System.Configuration;
using System.IO;

namespace LikeKitWebsite.Repository
{
    public class UserRelationsRepository : BaseRepository
    {
        public UserRelationsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private UserRelation GetUserRelation(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 FollowID = Database.GetReaderValueInt64(reader, "FollowID");
            User _follow = null;

            if (FollowID > 0)
            {
                _follow = new User
                {
                    ID = FollowID
                };
            }

            UserRelation userRelation = new UserRelation
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Follow = _follow,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return userRelation;
        }

        public Int64 CreateUserRelation(UserRelation UserRelation)
        {
            Int64 UserID = 0;

            if (UserRelation.User != null)
                UserID = UserRelation.User.ID;

            Int64 FollowID = 0;

            if (UserRelation.Follow != null)
                FollowID = UserRelation.Follow.ID;

            String SQL = String.Format("INSERT INTO UserRelation (UserID, FollowID, Created, Modified) " +
                "values ({0},{1},'{2}','{3}')",
                UserID,
                FollowID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);

            UserRelation.ID = Database.GetLastAutoIncrement();

            return UserRelation.ID;
        }

        public void UpdateUserRelation(UserRelation UserRelation)
        {
            Int64 UserID = 0;

            if (UserRelation.User != null)
                UserID = UserRelation.User.ID;

            Int64 FollowID = 0;

            if (UserRelation.Follow != null)
                FollowID = UserRelation.Follow.ID;

            String SQL = String.Format("UPDATE UserRelation SET UserID={0}," +
                                       "FollowID={1},Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                        UserID,
                                        FollowID,
                                        Database.ParseDateTimeToSQL(UserRelation.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        UserRelation.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteUserRelation(UserRelation UserRelation)
        {
            String SQL = String.Format("DELETE FROM UserRelation " +
                                       " WHERE ID={0}",
                                        UserRelation.ID);

            Database.ExecuteCommand(SQL);
        }

        public ArrayList GetUserRelations(Int64 UserID, int RelationType, int limit)
        {
            String sql_where = String.Empty;

            switch (RelationType)
            { 
                case 1: //Followings
                        sql_where = String.Format(" AND UserID={0}", UserID);
                    break;
                case 2: //Followers
                        sql_where = String.Format(" AND FollowID={0}", UserID);
                    break;
                default:
                        sql_where = String.Format(" AND UserID={0}", UserID);
                    break;
            }

            String sql = String.Format("select * from UserRelation where 1 {0} Order by ID ASC LIMIT 0,{1}", sql_where, limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList users = new ArrayList();

            try
            {
                JsonUser obj;

                while (reader.Read())
                {
                    obj = new JsonUser();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueString(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.Picture = Database.GetReaderValueString(reader, "Picture");
                    obj.About = Database.GetReaderValueString(reader, "About");
                    obj.Web = Database.GetReaderValueString(reader, "Web");

                    users.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return users;
        }


        public Int64 GetNumUserRelations(Int64 UserID, int RelationType)
        {
            Int64 NumRelations = 0;
            String sql_where = String.Empty;

            switch (RelationType)
            {
                case 1: //Followings
                    sql_where = String.Format(" AND UserID={0}", UserID);
                    break;
                case 2: //Followers
                    sql_where = String.Format(" AND FollowID={0}", UserID);
                    break;
                default:
                    sql_where = String.Format(" AND UserID={0}", UserID);
                    break;
            }

            String sql = String.Format("select count(ID) as count from UserRelation where 1 {0}", sql_where);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                if (reader.Read())
                {
                    NumRelations = Database.GetReaderValueInt64(reader, "count");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return NumRelations;
        }

        public bool isFollower(Int64 UserID, Int64 FollowID)
        {
            Int64 Count = 0;
            String sql = String.Format("select count(ID) as count from UserRelation where UserID={0} AND FollowID={1}", UserID, FollowID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            bool isFollower = false;

            try
            {
                if (reader.Read())
                {
                    Count = Database.GetReaderValueInt64(reader, "count");
                    if(Count > 0)
                        isFollower=true;
                    else
                        isFollower = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return isFollower;
        }

        public String GetHtmlProfileRelations(Int64 UserID, int RelationType, Int64 SessionUserID)
        {
            String sql_where = String.Empty;
            String sHtml = String.Empty;
            String sHtmlClose = String.Empty;
            String sButtonClass = String.Empty;

            switch (RelationType)
            {
                case 1: //Followings
                    sql_where = String.Format(" AND UserRelation.UserID={0}", UserID);
                    sql_where += String.Format(" AND User.ID = UserRelation.FollowID ");
                    sHtmlClose = "<div id=\"profile-following-close\" class=\"profile-following-follower-close\"></div>";
                    sButtonClass = "followings";
                    break;
                case 2: //Followers
                    sql_where = String.Format(" AND UserRelation.FollowID={0}", UserID);
                    sql_where += String.Format(" AND User.ID = UserRelation.UserID ");
                    sHtmlClose = "<div id=\"profile-follower-close\" class=\"profile-following-follower-close\"></div>";
                    sButtonClass = "followers";
                    break;
                default:
                    sql_where = String.Format(" AND UserRelation.UserID={0}", UserID);
                    sql_where += String.Format(" AND User.ID = UserRelation.FollowID ");
                    sHtmlClose = "<div id=\"profile-following-close\" class=\"profile-following-follower-close\"></div>";
                    sButtonClass = "followings";
                    break;
            }

            String sql = String.Format("select UserRelation.UserID as urUserID, User.ID as urID, User.UserName as UserName, " +
            " User.FirstName as FirstName, User.LastName as LastName, User.Picture as Picture, User.About as About, User.Web as Web," +
            " User.UserType as UserType from UserRelation, User where 1 {0} Order by urID", sql_where);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList users = new ArrayList();
            try
            {
                JsonUser obj;

                while (reader.Read())
                {
                    obj = new JsonUser();
                    obj.ID = Database.GetReaderValueInt64(reader, "urID");
                    obj.UserID = Database.GetReaderValueString(reader, "urUserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.Picture = Database.GetReaderValueString(reader, "Picture");
                    obj.About = Database.GetReaderValueString(reader, "About");
                    obj.Web = Database.GetReaderValueString(reader, "Web");
                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    users.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Recorrem els usuaris
            foreach(JsonUser usr_obj in users)
            {
                if (usr_obj.Picture != "")
                {
                    String img_path = ConfigurationManager.AppSettings["PathImages"];
                    String sFile = Globals.PathCombine(img_path, usr_obj.Picture);

                    if (!File.Exists(sFile))
                        usr_obj.Picture = "";
                }

                if (usr_obj.Picture == "")
                    usr_obj.Picture = "/Content/images/default_user.png";
                else
                    usr_obj.Picture = "/Content/files/" + usr_obj.Picture;

                sHtml += "<div class=\"profile-following-follower-entry\">\n" +
                    "<div class=\"profile-following-follower-entry-photo\" onclick=\"Profile(" + usr_obj.ID + ");\"><img src=\"" + usr_obj.Picture + "\" width=\"78\" height=\"78\" />";

                if(usr_obj.iUserType == 1)
                    sHtml += "<div class=\"user-trender-medium\"><img src=\"/Content/images/trender.png\" width=\"78\" height=\"78\" /></div>";

                sHtml += "</div>\n" +
                    "<div class=\"profile-following-follower-entry-name\">" + usr_obj.FirstName + " " + usr_obj.LastName + "</div>\n" +
                    "<div class=\"profile-following-follower-entry-about\">" + Globals.WordCut(usr_obj.About, 60, new char[] { ' ' }) + "</div>\n";

                if (SessionUserID != usr_obj.ID)
                {
                    //Hem de comprovar si el seguim o no
                    if (isFollower(SessionUserID, usr_obj.ID))
                        sHtml += "<div id=\"profile-" + sButtonClass + "-btn-" + usr_obj.ID + "\" class=\"profile-following-btn left14\" onclick=\"ProfileUnFollow(" + usr_obj.ID + ",'" + sButtonClass + "');\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SIGUIENDO_MIN + "</div>\n";
                    else
                        sHtml += "<div id=\"profile-" + sButtonClass + "-btn-" + usr_obj.ID + "\" class=\"profile-follow-btn left14\" onclick=\"ProfileFollow(" + usr_obj.ID + ",'" + sButtonClass + "');\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SEGUIR + "</div>\n";
                }

                sHtml += "</div>\n";
            }

            sHtml += sHtmlClose;

            return sHtml;
        }

        public void UnFollow(Int64 UserID, Int64 FollowID)
        {
            String SQL = String.Format("DELETE FROM UserRelation " +
                                       " WHERE UserID={0} AND FollowID={1}",
                                        UserID, FollowID);

            Database.ExecuteCommand(SQL);
        }

        public Int64 getRelationID(Int64 UserID, Int64 FollowID)
        {
            Int64 ID = 0;
            String sql = String.Format("SELECT ID FROM UserRelation " +
                                       " WHERE UserID={0} AND FollowID={1}",
                                        UserID, FollowID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    ID = Database.GetReaderValueInt64(reader, "ID");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return ID;
        }

        public String GetHtmlHomeProfileRelations(Int64 UserID, int RelationType, Int64 SessionUserID)
        {
            String sql_where = String.Empty;
            String sHtml = String.Empty;
            String sHtmlClose = String.Empty;
            String sButtonClass = String.Empty;

            switch (RelationType)
            {
                case 1: //Followings
                    sql_where = String.Format(" AND UserRelation.UserID={0}", UserID);
                    sql_where += String.Format(" AND User.ID = UserRelation.FollowID ");
                    sHtmlClose = "<div id=\"profile-following-close\" class=\"profile-following-follower-close\"></div>";
                    sButtonClass = "followings";
                    break;
                case 2: //Followers
                    sql_where = String.Format(" AND UserRelation.FollowID={0}", UserID);
                    sql_where += String.Format(" AND User.ID = UserRelation.UserID ");
                    sHtmlClose = "<div id=\"profile-follower-close\" class=\"profile-following-follower-close\"></div>";
                    sButtonClass = "followers";
                    break;
                default:
                    sql_where = String.Format(" AND UserRelation.UserID={0}", UserID);
                    sql_where += String.Format(" AND User.ID = UserRelation.FollowID ");
                    sHtmlClose = "<div id=\"profile-following-close\" class=\"profile-following-follower-close\"></div>";
                    sButtonClass = "followings";
                    break;
            }

            String sql = String.Format("select UserRelation.UserID as urUserID, User.ID as urID, User.UserName as UserName, " +
            " User.FirstName as FirstName, User.LastName as LastName, User.Picture as Picture, User.About as About, User.Web as Web," +
            " User.UserType as UserType from UserRelation, User where 1 {0} Order by urID", sql_where);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList users = new ArrayList();
            try
            {
                JsonUser obj;

                while (reader.Read())
                {
                    obj = new JsonUser();
                    obj.ID = Database.GetReaderValueInt64(reader, "urID");
                    obj.UserID = Database.GetReaderValueString(reader, "urUserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.Picture = Database.GetReaderValueString(reader, "Picture");
                    obj.About = Database.GetReaderValueString(reader, "About");
                    obj.Web = Database.GetReaderValueString(reader, "Web");
                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    users.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Recorrem els usuaris
            foreach (JsonUser usr_obj in users)
            {
                if (usr_obj.Picture != "")
                {
                    String img_path = ConfigurationManager.AppSettings["PathImages"];
                    String sFile = Globals.PathCombine(img_path, usr_obj.Picture);

                    if (!File.Exists(sFile))
                        usr_obj.Picture = "";
                }

                if (usr_obj.Picture == "")
                    usr_obj.Picture = "/Content/images/default_user.png";
                else
                    usr_obj.Picture = "/Content/files/" + usr_obj.Picture;

                String user_url_friendly = String.Format("{0}perfilp/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(usr_obj.FirstName + " " + usr_obj.LastName), usr_obj.ID);

                sHtml += "<div class=\"profile-following-follower-entry\">\n" +
                    "<div class=\"profile-following-follower-entry-photo\" onclick=\"parent.location.href='" + user_url_friendly + "';\"><img src=\"" + usr_obj.Picture + "\" width=\"78\" height=\"78\" />";

                if (usr_obj.iUserType == 1)
                    sHtml += "<div class=\"user-trender-medium\"><img src=\"/Content/images/trender.png\" width=\"78\" height=\"78\" /></div>";

                sHtml += "</div>\n" +
                    "<div class=\"profile-following-follower-entry-name\">" + usr_obj.FirstName + " " + usr_obj.LastName + "</div>\n" +
                    "<div class=\"profile-following-follower-entry-about\">" + Globals.WordCut(usr_obj.About, 60, new char[] { ' ' }) + "</div>\n";

                if (SessionUserID != usr_obj.ID)
                {
                    //Hem de comprovar si el seguim o no
                    if (isFollower(SessionUserID, usr_obj.ID))
                        sHtml += "<div id=\"profile-" + sButtonClass + "-btn-" + usr_obj.ID + "\" class=\"profile-following-btn left14\" onclick=\"mostrarEmergent();\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SIGUIENDO_MIN + "</div>\n";
                    else
                        sHtml += "<div id=\"profile-" + sButtonClass + "-btn-" + usr_obj.ID + "\" class=\"profile-follow-btn left14\" onclick=\"mostrarEmergent();\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SEGUIR + "</div>\n";
                }

                sHtml += "</div>\n";
            }

            sHtml += sHtmlClose;

            return sHtml;
        }
    }
}