﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Collections;
using System.Configuration;
using System.IO;

namespace LikeKitWebsite.Repository
{
    /// <summary>
    /// Objecte amb la informació de les valoracions mitges
    /// </summary>
    public class JsonKitValoration
    {
        public Double MidValoration = 0;
        public Double NumKitsMidValoration = 0;
    }

    public class KitsRepository : BaseRepository
    {
        public KitsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Kit GetKit(IDataReader reader, ArrayList kitComments, String htmlOtherKits, String htmlComments)
        {
            if (reader == null)
                return null;

            reader.Read();

            Int64 KitID = Database.GetReaderValueInt64(reader, "ID");
            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID,
                    UserName = Database.GetReaderValueString(reader, "UserName"),
                    FirstName = Database.GetReaderValueString(reader, "FirstName"),
                    LastName = Database.GetReaderValueString(reader, "LastName"),
                    Picture = Database.GetReaderValueString(reader, "UserPicture"),
                    UserType = (UserType)Database.GetReaderValueInt16(reader, "UserType")
                };
            }

            _user.iUserType = 0;
            if (_user.UserType == UserType.Trender)
                _user.iUserType = 1;
            else
                _user.iUserType = 0;

            if (_user.Picture != "")
            {
                String img_path = ConfigurationManager.AppSettings["PathImages"];
                String sFile = Globals.PathCombine(img_path, _user.Picture);

                if (!File.Exists(sFile))
                    _user.Picture = "";
            }

            if (_user.Picture == "")
                _user.Picture = "/Content/images/default_user.png";
            else
                _user.Picture = "/Content/files/" + _user.Picture;

            _user.HtmlPicture = _user.Picture;

            Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
            Product _product = null;

            if(ProductID > 0)
            {
                Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");

                ProductCategory _product_category = null;
                _product_category = new ProductCategory { ID = ProductCategoryID };


                _product = new Product { 
                    ID = ProductID,
                    Name = Database.GetReaderValueString(reader, "ProductName"),
                    Image = Database.GetReaderValueString(reader, "ProductImage"),
                    Author = Database.GetReaderValueString(reader, "ProductAuthor"),
                    Description = Database.GetReaderValueString(reader, "ProductDescription"),
                    LanguageName = Database.GetReaderValueString(reader, "LanguageName"),
                    Year = Database.GetReaderValueInt16(reader, "Year"),
                    PartnerUrl = Database.GetReaderValueString(reader, "PartnerUrl"),
                    ProductCategory = _product_category
                };
            }

            if (_product.Image != "")
            {
                String img_path = ConfigurationManager.AppSettings["PathImages"];
                String sFile = Globals.PathCombine(img_path, _product.Image);

                if (!File.Exists(sFile))
                    _product.Image = "";
            }

            if (_product.Image == "")
                _product.Image = "/Content/images/default_product.png";
            else
                _product.Image = "/Content/files/" + _product.Image;

            Int64 ParentKitID = Database.GetReaderValueInt64(reader, "ParentKitID");
            Kit _parentkit = null;

            if (ParentKitID > 0)
            {
                _parentkit = new Kit { 
                    ID = ParentKitID
                };
            }


            String strTimeDifference = Globals.GetTimeDifference(Database.GetReaderValueDateTime(reader, "Created"));

            Kit kit = new Kit
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Product = _product,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                Comment = Database.GetReaderValueString(reader, "Comment"),
                Valoration = Database.GetReaderValueDouble(reader, "Valoration"),
                ParentKit = _parentkit,
                Visible = Database.GetReaderValueInt16(reader, "Visible") == 1,
                NumKits = Database.GetReaderValueInt16(reader, "num_kits"),
                NumComments = Database.GetReaderValueInt16(reader, "num_comments"),
                NumLikes = Database.GetReaderValueInt16(reader, "num_likes"),
                NumProductKits = Database.GetReaderValueInt16(reader, "num_product_kits"),
                TimeDifference = strTimeDifference,
                Comments = kitComments,
                HtmlOtherKits = htmlOtherKits,
                HtmlComments = htmlComments
            };

            //kit.Comment = LikeKitWebsite.Globals.HtmlEncodingLinks(kit.Comment);


            return kit;
        }

        public Kit GetKit(Int64 id, Int64 SessionUserID)
        {
            ArrayList kitComments = new ArrayList();
            kitComments = GetNumKitComments(id);

            String htmlComments = String.Empty;
            if(SessionUserID > 0)
                htmlComments = GetHtmlKitComments(id, SessionUserID);

            /*ArrayList otherKits = new ArrayList();
            otherKits = GetOtherKits(id);*/
            String htmlOtherKits = String.Empty;
            htmlOtherKits = GetHtmlOtherKits(id);

            //String sql = String.Format("select ID, UserID, ProductID, Created, Modified, Valoration, Comment, ParentKitID, Visible from Kit where ID={0}", id);
            String sql = "select kt.ID, kt.UserID, kt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, pr.Name as ProductName,pr.Image as ProductImage, kt.Created, kt.Modified, kt.Valoration, pr.Year, " +
               " kt.Comment, kt.ParentKitID, kt.Visible, pr.Author as ProductAuthor, pr.Description as ProductDescription, pr.PartnerUrl, lang.Description as LanguageName, pr.ProductCategoryID, " +
               " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
               " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
               " (Select count(ID) from Kit ktp where ktp.ProductID = kt.ProductID) as num_product_kits, " +
               " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes " +
               " from Kit kt left join User usr on kt.UserID = usr.ID " +
               " left join Product pr on kt.ProductID = pr.ID " +
               " left join Language lang on pr.LanguageID = lang.ID " +
               " where kt.ID = " + id;

            Kit kit = new Kit();

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                kit = GetKit(reader, kitComments, htmlOtherKits, htmlComments);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return kit; 
        }

        public Kit GetKitData(Int64 id)
        {
            String sql = "select kt.ID, kt.UserID, kt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, pr.Name as ProductName,pr.Image as ProductImage, kt.Created, kt.Modified, kt.Valoration, pr.Year, " +
               " kt.Comment, kt.ParentKitID, kt.Visible, pr.Author as ProductAuthor, pr.Description as ProductDescription, lang.Description as LanguageName, " +
               " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
               " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
               " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes " +
               " from Kit kt left join User usr on kt.UserID = usr.ID " +
               " left join Product pr on kt.ProductID = pr.ID " +
               " left join Language lang on pr.LanguageID = lang.ID " +
               " where kt.ID = " + id;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Kit kit = null;

            try
            {
                JsonKitComments obj;

                while (reader.Read())
                {
                    if (reader == null)
                        return null;

                    reader.Read();

                    Int64 KitID = Database.GetReaderValueInt64(reader, "ID");
                    Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                    User _user = null;

                    if (UserID > 0)
                    {
                        _user = new User
                        {
                            ID = UserID,
                            UserName = Database.GetReaderValueString(reader, "UserName"),
                            FirstName = Database.GetReaderValueString(reader, "FirstName"),
                            LastName = Database.GetReaderValueString(reader, "LastName"),
                            Picture = Database.GetReaderValueString(reader, "UserPicture")
                        };
                    }

                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    Product _product = null;

                    if (ProductID > 0)
                    {
                        _product = new Product
                        {
                            ID = ProductID,
                            Name = Database.GetReaderValueString(reader, "ProductName"),
                            Image = Database.GetReaderValueString(reader, "ProductImage"),
                            Author = Database.GetReaderValueString(reader, "ProductAuthor"),
                            Description = Database.GetReaderValueString(reader, "ProductDescription"),
                            LanguageName = Database.GetReaderValueString(reader, "LanguageName"),
                            Year = Database.GetReaderValueInt16(reader, "Year")
                        };
                    }

                    Int64 ParentKitID = Database.GetReaderValueInt64(reader, "ParentKitID");
                    Kit _parentkit = null;

                    if (ParentKitID > 0)
                    {
                        _parentkit = new Kit
                        {
                            ID = ParentKitID
                        };
                    }


                    String strTimeDifference = Globals.GetTimeDifference(Database.GetReaderValueDateTime(reader, "Created"));

                    kit = new Kit
                    {
                        ID = Database.GetReaderValueInt64(reader, "ID"),
                        User = _user,
                        Product = _product,
                        Created = Database.GetReaderValueDateTime(reader, "Created"),
                        Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                        Comment = Database.GetReaderValueString(reader, "Comment"),
                        Valoration = Database.GetReaderValueDouble(reader, "Valoration"),
                        ParentKit = _parentkit,
                        Visible = Database.GetReaderValueInt16(reader, "Visible") == 1,
                        NumKits = Database.GetReaderValueInt16(reader, "num_kits"),
                        NumComments = Database.GetReaderValueInt16(reader, "num_comments"),
                        NumLikes = Database.GetReaderValueInt16(reader, "num_likes"),
                        TimeDifference = strTimeDifference
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return kit;
        }

        public Int64 CreateKit(Kit kit)
        {
            Int64 ProductID = 0;
            Int64 ParentKitID = 0;

            if (kit.Product != null)
                ProductID = kit.Product.ID;

            if (kit.ParentKit != null)
                ParentKitID = kit.ParentKit.ID;

            String SQL = String.Format("INSERT INTO Kit (UserID, ProductID, Created, Modified, Valoration, Comment, ParentKitID, Visible) " +
                "values ({0},{1},'{2}','{3}','{4}','{5}',{6},{7})",
                kit.User.ID,
                Database.ParseIDToSQLNull(ProductID),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDoubleToSQL(kit.Valoration),
                Database.ParseStringToSQL(kit.Comment),
                Database.ParseIDToSQLNull(ParentKitID),
                kit.Visible ? 1 : 0
                );
            
            Database.ExecuteCommand(SQL);

            kit.ID = Database.GetLastAutoIncrement();

            return kit.ID;
        }

        public void UpdateKit(Kit Kit)
        {
            Int64 ProductID = 0;

            if (Kit.Product != null)
                ProductID = Kit.Product.ID;

            Int64 UserID = 0;

            if (Kit.User != null)
                UserID = Kit.User.ID;

            Int64 ParentKitID = 0;

            if (Kit.ParentKit != null)
                ParentKitID = Kit.ParentKit.ID;

            String SQL = String.Format("UPDATE Kit SET UserID={0}," +
                                       "ProductID={1},Valoration='{2}',Comment='{3}',ParentKitID={4},Visible={5}, " +
                                       "Created='{6}',Modified='{7}' " +
                                       " WHERE ID={8}",
                                       UserID,
                                       ProductID,
                                       Kit.Valoration,
                                       Database.ParseStringToSQL(Kit.Comment),
                                       ParentKitID,
                                       Kit.Visible ? 1 : 0,
                                       Database.ParseDateTimeToSQL(Kit.Created),
                                       Database.ParseDateTimeToSQL(System.DateTime.Now),
                                       Kit.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteKit(Kit kit)
        {
            //Esborrem els kits Comments
            String SQL = String.Format("DELETE FROM KitComment " +
                                       " WHERE KitID={0}",
                                       kit.ID);

            Database.ExecuteCommand(SQL);

            //Ara hauriem d'esborrar els Kit tags, per evitar errors
            SQL = String.Format("DELETE FROM KitTag WHERE KitID ={0}", kit.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els UserLike
            SQL = String.Format("DELETE FROM UserLike WHERE KitID ={0}", kit.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem el kit
            SQL = String.Format("DELETE FROM Kit " +
                                       " WHERE ID={0}",
                                        kit.ID);

            Database.ExecuteCommand(SQL);
        }


        //Search Kits, timeline
        //Qui : Tots, Trenders, Following
        //Categoria : Tots, Categoria ID
        //Altres : Tots, Els més recomanats, Els més comentats
        //Sexe : Tots, home, dona
        //Franges d'edat : Tots, 20-30, 30-40, 40-50, 50-60, > 60
        public ArrayList SearchKits(int UserType, Int64[] CategoryID, int OrderType, int Gender, int Ages)
        {
            String sql = String.Format("select k.ID, k.UserID as UserID, u.Name as FirstName, k.ProductID as ProductID, p.Name as ProductName, k.Created, k.Modified, k.Comment  from Kit k, User u, Product p where k.UserID = u.ID AND k.ProductID = p.ID");

            String sql_where = String.Empty;
            String sql_from = String.Empty;

            switch(UserType)
            {
                case 1: //Trenders
                    sql_where = String.Format(" AND u.UserType=1");
                    break;
                case 2:
                    sql_where = String.Format(" AND u.ID IN (SELECT ur.FollowID from UserRelation ur where ur.UserID = k.UserID)");
                    break;
            }

            if (CategoryID.Length > 0)
            {
                sql_where += String.Format(" AND p.ProductCategoryID IN ({0})", Globals.ConvertArrayToValues(CategoryID));
            }

            switch (OrderType)
            {
                case 1: //Most recommended
                    sql_where = String.Format(" (Select count(ID) from Kit where Kit.ProductID = k.ProductID) as Count");
                    break;
                case 2: //Most commented
                    sql_where = String.Format(" (Select count(ID) from KitComment where KitComment.KitID = k.ID) as Count");
                    break;
            }

            switch (Gender)
            {
                case 1: //Male
                    sql_where = String.Format(" AND u.Gender=1");
                    break;
                case 2: //Female
                    sql_where = String.Format(" AND u.Gender=2");
                    break;
            }

            //Age
            int from_age = 0;
            int to_age = 0;

            if (Ages > 0)
            {
                switch (Ages)
                {
                    case 1: //20-30
                        from_age = 20;
                        to_age = 30;
                        break;
                    case 2: //30-40
                        from_age = 30;
                        to_age = 40;
                        break;
                    case 3: //40-50
                        from_age = 40;
                        to_age = 50;
                        break;
                    case 4: //50-60
                        from_age = 50;
                        to_age = 60;
                        break;
                    case 5: // > 60
                        from_age = 60;
                        to_age = 100;
                        break;
                }

                //data min data actual + from age
                System.DateTime from_date = System.DateTime.Now;
                System.DateTime to_date = System.DateTime.Now;
                from_date = from_date.AddYears(from_age);
                to_date = to_date.AddYears(to_age);

                sql_where += String.Format(" AND (u.Birthday >= '{0}' AND u.Birthday <= '{1}')", Database.ParseDateTimeToSQL(from_date), Database.ParseDateTimeToSQL(to_date));
            }

            sql += sql_where;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                JsonKits obj;

                while (reader.Read())
                {
                    obj = new JsonKits();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    obj.ProductName = Database.GetReaderValueString(reader, "ProductName");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");

                    List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return List;            
        }

        public Int64 GetNumRekits(Int16 KitID)
        {
            if (KitID <= 0)
                return 0;

            Int64 nRekits = 0;

            String sql = String.Format("select count(ID) as count from Kit where KitID={0}",KitID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nRekits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return nRekits;
        }

        public Int64 GetNumLikes(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nLikes = 0;

            String sql = String.Format("select count(ID) as count from UserLike where UserID={0}", UserID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                if (reader.Read())
                {
                    nLikes = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return nLikes;
        }

        public ArrayList GetNumKitComments(Int64 KitID)
        {
            String sql = String.Format("select kcom.ID, kcom.KitID, kcom.UserID as UserID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, kcom.Created, kcom.Modified, kcom.Comment from KitComment kcom, User usr where kcom.UserID = usr.ID AND kcom.KitID = {0} AND kcom.Visible=1", KitID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                JsonKitComments obj;

                while (reader.Read())
                {
                    obj = new JsonKitComments();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");
                    obj.TimeDifference = Globals.GetTimeDifference(obj.Created);

                    List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return List;
        }

        public String GetHtmlKitComments(Int64 KitID, Int64 SessionUserID)
        {
            String Html = String.Empty;
            String sql = String.Format("select kcom.ID, kcom.KitID, kcom.UserID as UserID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, kcom.Created, kcom.Modified, kcom.Comment from KitComment kcom, User usr where kcom.UserID = usr.ID AND kcom.KitID = {0} AND kcom.Visible=1", KitID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            String Column1 = String.Empty;
            String Column2 = String.Empty;
            String Column3 = String.Empty;

            try
            {
                JsonKitComments obj;

                int i = 0;

                while (reader.Read())
                {
                    i++;
                    obj = new JsonKitComments();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");
                    obj.TimeDifference = Globals.GetTimeDifference(obj.Created);
                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    List.Add(obj);

                    String temp = String.Empty;

                    if (obj.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, obj.UserPicture);

                        if (!File.Exists(sFile))
                            obj.UserPicture = "";
                    }

                    if (obj.UserPicture == "")
                        obj.UserPicture = "/Content/images/default_user.png";
                    else
                        obj.UserPicture = "/Content/files/" + obj.UserPicture;

                    temp = "<div class=\"kit-view-container-comments-entry\">" +
                    "<div class=\"kit-comment-user-image\" onclick=\"Profile(" + obj.UserID + ");\"><img src=\"" + obj.UserPicture + "\" width=\"28px\"/>";
                    
                    if(obj.iUserType == 1)
                        temp += "<div class=\"user-trender-small-kit-view\"><img src=\"/Content/images/trender.png\" width=\"28\" height=\"28\" /></div>";
                    
                    //Codifiquem els links dels comentaris
                    //obj.Comment = LikeKitWebsite.Globals.HtmlEncodingLinks(obj.Comment);
                    obj.Comment = LikeKitWebsite.Globals.HtmlEncodingLinksShort(obj.Comment, LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ENLACE_MIN);

                    temp += "</div>" +
                    "<div class=\"kit-comment-user-published\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_PUBLICADO + " " + obj.TimeDifference + "</div>" +
                    "<div class=\"kit-comment-user-name\" onclick=\"Profile(" + obj.UserID + ");\">" + obj.FirstName + " " + obj.LastName + "</div>" +
                    "<div class=\"kit-comment-text\">" + obj.Comment + "</div>";
                    
                    if (SessionUserID == obj.UserID)                        
                    {
                        temp += "<div class=\"kit-comment-delete\" onclick=\"KitCommentDelete(" + obj.ID + "," + KitID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_BORRAR_TU_COMENTARIO + "</div>";
                    }
                    
                    temp += "</div>";

                    if (i == 1)
                        Column1 += temp;
                    else if (i == 2)
                        Column2 += temp;
                    else if (i == 3)
                    {
                        Column3 += temp;
                        i = 0;
                    }
                }

                Html = String.Format("<div class=\"kit-comments-column\">{0}</div><div class=\"kit-comments-column\">{1}</div><div class=\"kit-comments-column\">{2}</div>",Column1, Column2, Column3);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return Html;
        }


        public ArrayList GetOtherKits(Int64 KitID)
        {
            //Load kit data, to get ProductID
            Kit _kit = GetKitData(KitID);
            Int64 ProductID = _kit.Product.ID;
            Int64 KitUserID = _kit.User.ID;

            String sql = String.Format("select Kit.ID, Kit.UserID, User.UserName as UserName, User.Picture as UserPicture, Kit.Created, Kit.Modified, Kit.Comment from Kit, User where Kit.UserID = User.ID AND Kit.UserID <> {0} AND ProductID = {1}", KitUserID, ProductID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                JsonKits obj;

                while (reader.Read())
                {
                    obj = new JsonKits();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");

                    List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return List;
        }



        public String GetHtmlOtherKits(Int64 KitID)
        {
            String sHtml = String.Empty;
            //Load kit data, to get ProductID
            Kit _kit = GetKitData(KitID);
            Int64 ProductID = _kit.Product.ID;
            Int64 KitUserID = _kit.User.ID;

            String sql = String.Format("select Kit.ID, Kit.UserID, User.UserName as UserName, User.Picture as UserPicture, User.UserType as UserType, Kit.Created, Kit.Modified, Kit.Comment from Kit, User where Kit.UserID = User.ID AND Kit.UserID <> {0} AND ProductID = {1}", KitUserID, ProductID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            //ArrayList List = new ArrayList();

            try
            {
                JsonKits obj;

                while (reader.Read())
                {
                    obj = new JsonKits();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");
                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    if (obj.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, obj.UserPicture);

                        if (!File.Exists(sFile))
                            obj.UserPicture = "";
                    }

                    if (obj.UserPicture == "")
                        obj.UserPicture = "/Content/images/default_user.png";
                    else
                        obj.UserPicture = "/Content/files/" + obj.UserPicture;


                    sHtml += "<div class=\"kit-view-container-users-entry\">" +
                             "<img src=\"" + obj.UserPicture + "\" width=\"87px\" />";

                    if (obj.iUserType == 1)
                        sHtml += "<div class=\"user-trender-medium\"><img src=\"/Content/images/trender.png\" width=\"78\" height=\"78\" /></div>";

                    sHtml += "<div class=\"kit-view-container-users-entry-info\">" +
                        "<div class=\"kit-view-container-users-entry-info-profile\" onclick=\"Profile(" + obj.UserID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_PERFIL + "</div>" +
                        "<div class=\"kit-view-container-users-entry-info-follow\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SEGUIR + "</div>" +
                        "<div class=\"kit-view-container-users-entry-info-kit\" onclick=\"Kit(" + obj.ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_VER_KIT + "</div>" +
                    "</div>" +
                "</div>";

                    //List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //return List;
            return sHtml;
        }

        public Int64 GetUserNumKits(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nRekits = 0;

            String sql = String.Format("select count(ID) as count from Kit where UserID={0}", UserID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nRekits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return nRekits;
        }

        public Int64 GetUserNumReKits(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nRekits = 0;

            String sql = String.Format("select count(ID) as count from Kit where UserID={0} AND ParentKitID > 0", UserID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nRekits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return nRekits;
        }

        public Int64 GetNumUserComments(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nRekits = 0;

            String sql = String.Format("select count(ID) as count from KitComment where UserID={0} AND Visible=1", UserID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nRekits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return nRekits;
        }

        public String GetHtmlKitTags(Int64 KitID)
        {
            String sHtml = String.Empty;

            String sql = "select KitTag.Name " +
                " from KitTag  " +
                " where 1=1 AND KitTag.KitID = " + KitID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                while (reader.Read())
                {
                    String Name = Database.GetReaderValueString(reader, "Name");
                    sHtml += "<div class=\"kit-view-menu-content-text\">" + Name + "</div>\n";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return sHtml;
        }

        public String GetStringKitTags(Int64 KitID)
        {
            String sHtml = String.Empty;

            String sql = "select KitTag.Name " +
                " from KitTag  " +
                " where 1=1 AND KitTag.KitID = " + KitID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int16 count = 0;

            try
            {
                while (reader.Read())
                {
                    String Name = Database.GetReaderValueString(reader, "Name");

                    if (count > 0)
                        sHtml += ",";

                    sHtml += Name;
                    count++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return sHtml;
        }

        public ArrayList GetKitTags(Int64 KitID)
        {
            String sHtml = String.Empty;

            String sql = "select KitTag.Name " +
                " from KitTag  " +
                " where 1=1 AND KitTag.KitID = " + KitID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList tags = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    String Name = Database.GetReaderValueString(reader, "Name");
                    tags.Add(Name);
                    //sHtml += "<div class=\"kit-view-menu-content-text\">" + Name + "</div>\n";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return tags;
        }

        public void DeleteTags(Int64 KitID)
        {
            String SQL = String.Format("DELETE FROM KitTag " +
                                       " WHERE KitID={0}",
                                        KitID);

            Database.ExecuteCommand(SQL);
        }

        public JsonKitValoration GetKitMidValoration(Int64 ProductID)
        { 
            //JsonKitValoration

            String sql = "select Valoration from Kit where ProductID = " + ProductID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            JsonKitValoration obj = new JsonKitValoration();

            Double count = 0;
            Double ValorationSum = 0;

            try
            {
                while (reader.Read())
                {
                    count++;
                    ValorationSum = ValorationSum + Database.GetReaderValueDouble(reader, "Valoration");
                }

                obj.NumKitsMidValoration = count;

                if(obj.NumKitsMidValoration > 0)
                {
                    obj.MidValoration = Math.Round((ValorationSum / count),0);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return obj;
        }

        public Int64 GetNumTrenderPoints()
        {
            String sql = String.Format("Select ");
            sql += String.Format("(select count(ID) from Kit where UserID=Usr.ID) as num_kits,");

            //sql += String.Format("(select count(ID) from UserLike where UserID=Usr.ID) as num_likes,");
            sql += String.Format("(select count(ul.ID) as count from UserLike ul, Kit kt where ul.KitID = kt.ID AND kt.UserID=Usr.ID) as num_likes,");
            
            //sql += String.Format("(select count(ID) from Kit where UserID=Usr.ID AND ParentKitID > 0) as num_rekits,");
            sql += String.Format("(select count(ID) as count from Kit where ParentKitID IN (select kt.ID from Kit kt where UserID=Usr.ID)) as num_rekits,");

            //sql += String.Format("(select count(ID) as count from KitComment where UserID=Usr.ID) as num_comments ");
            sql += String.Format("(select count(ID) as count from KitComment where KitID IN (select kt.ID from Kit kt where UserID=Usr.ID) AND Visible=1) as num_comments ");
            
            sql += String.Format(" from User Usr where UserType = 1");

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            JsonKitValoration obj = new JsonKitValoration();

            Int64 TrenderPoints = 0;

            try
            {
                while (reader.Read())
                {
                    Int64 TotalTemp = 0;
                    Int64 NumKits = Database.GetReaderValueInt64(reader, "num_kits");
                    Int64 NumLikes = Database.GetReaderValueInt64(reader, "num_likes");
                    Int64 NumRekits = Database.GetReaderValueInt64(reader, "num_rekits");
                    Int64 NumComments = Database.GetReaderValueInt64(reader, "num_comments");

                    TotalTemp = NumKits + NumLikes + NumRekits + NumComments;

                    if ((TotalTemp > 0) && ((TotalTemp < TrenderPoints) || (TrenderPoints == 0)))
                    {
                        TrenderPoints = TotalTemp;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return TrenderPoints;
        }

        public Int64 GetNumLikesToUser(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nLikes = 0;

            String sql = String.Format("select count(ul.ID) as count from UserLike ul, Kit kt where ul.KitID = kt.ID AND kt.UserID={0}", UserID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                if (reader.Read())
                {
                    nLikes = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return nLikes;
        }


        public Int64 GetUserNumReKitsToUser(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nRekits = 0;

            String sql = String.Format("select count(ID) as count from Kit where ParentKitID IN (select kt.ID from Kit kt where UserID={0})", UserID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nRekits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return nRekits;
        }

        public Int64 GetNumUserCommentsToUser(Int64 UserID)
        {
            if (UserID <= 0)
                return 0;

            Int64 nRekits = 0;

            String sql = String.Format("select count(ID) as count from KitComment where KitID IN (select kt.ID from Kit kt where UserID={0}) AND Visible=1", UserID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nRekits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }


            return nRekits;
        }


        public String GetHomeHtmlOtherKits(Int64 KitID)
        {
            String sHtml = String.Empty;
            //Load kit data, to get ProductID
            Kit _kit = GetKitData(KitID);
            Int64 ProductID = _kit.Product.ID;
            Int64 KitUserID = _kit.User.ID;

            String sql = String.Format("select Kit.ID, Kit.UserID, User.UserName as UserName, User.Picture as UserPicture, User.UserType as UserType, User.FirstName, User.LastName, Kit.Created, Kit.Modified, Kit.Comment from Kit, User where Kit.UserID = User.ID AND Kit.UserID <> {0} AND ProductID = {1}", KitUserID, ProductID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            //ArrayList List = new ArrayList();

            try
            {
                JsonKits obj;

                while (reader.Read())
                {
                    obj = new JsonKits();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");
                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    if (obj.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, obj.UserPicture);

                        if (!File.Exists(sFile))
                            obj.UserPicture = "";
                    }

                    if (obj.UserPicture == "")
                        obj.UserPicture = "/Content/images/default_user.png";
                    else
                        obj.UserPicture = "/Content/files/" + obj.UserPicture;


                    sHtml += "<div class=\"kit-view-container-users-entry\">" +
                             "<img src=\"" + obj.UserPicture + "\" width=\"87px\" />";

                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");

                    if (obj.iUserType == 1)
                        sHtml += "<div class=\"user-trender-medium\"><img src=\"/Content/images/trender.png\" width=\"78\" height=\"78\" /></div>";

                    String user_url_friendly = String.Format("{0}perfilp/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(FirstName + " " + LastName), obj.UserID);

                    sHtml += "<div class=\"kit-view-container-users-entry-info\">" +
                        "<div class=\"kit-view-container-users-entry-info-profile\" onclick=\"parent.location.href='" + user_url_friendly + "';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_PERFIL + "</div>" +
                        "<div class=\"kit-view-container-users-entry-info-follow\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SEGUIR + "</div>" +
                        "<div class=\"kit-view-container-users-entry-info-kit\" onclick=\"parent.location.href= SUBDOMAIN_PATH + '/Home';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_VER_KIT + "</div>" +
                    "</div>" +
                "</div>";

                    //List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //return List;
            return sHtml;
        }




        public String GetLoquieroHTML(Int64 ProductID, Int64 UserID)
        {
            String sHtml = String.Empty;
            String sHtmlContainer = String.Empty;

            TimeLineRepository TLRep = new TimeLineRepository(Database);
            
            String sql = String.Empty;

            if(UserID > 0)
            {
                User kitUser = TLRep.GetUser(UserID);


                //Primer mirem si té Vendor associat
                if ((kitUser.Vendor != null) && (kitUser.Vendor.ID != null) && (kitUser.Vendor.ID > 0))
                {
                    sql = String.Format("Select pv.Url, v.Name, v.Logo from ProductVendor pv left join Vendor v on v.ID = pv.VendorID where pv.ProductID = {0} and pv.VendorID={1}", ProductID, kitUser.Vendor.ID);
                }
                else
                    sql = String.Format("Select pv.Url, v.Name, v.Logo from ProductVendor pv left join Vendor v on v.ID = pv.VendorID where pv.ProductID = {0} ", ProductID);
            }
            else
                sql = String.Format("Select pv.Url, v.Name, v.Logo from ProductVendor pv left join Vendor v on v.ID = pv.VendorID where pv.ProductID = {0} ", ProductID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int16 count = 0;

            try
            {
                while (reader.Read())
                {
                    String Url = Database.GetReaderValueString(reader, "Url");
                    String Name = Database.GetReaderValueString(reader, "Name");
                    String Logo = Database.GetReaderValueString(reader, "Logo");

                    String image = String.Empty;

                    if (Logo != "")
                        image = "<img src=\"/Content/files/" + Logo + "\" width=\"98px\" alt=\"" + Name + "\"/>";

                    sHtmlContainer += "<div class=\"kit-user-loquiero-vendor\" onclick=\"var wnd=window.open('" + Url + "');\"><div class=\"kit-user-loquiero-vendor-img\">" + image + "</div><div class=\"kit-user-loquiero-vendor-name\">" + Name + "</div></div>";

                    count++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Posem la url d'amazon per defecte
            if (count == 0)
            {
                ProductsRepository pRP = new ProductsRepository(Database);
                Product product = pRP.GetProduct(ProductID);
                if (product.PartnerUrl != "")
                {
                    String image = "<img src=\"/Content/files/amazon.png\" width=\"98px\" alt=\"Amazon\"/>";

                    sHtmlContainer += "<div class=\"kit-user-loquiero-vendor\" onclick=\"var wnd=window.open('" + product.PartnerUrl + "');\"><div class=\"kit-user-loquiero-vendor-img\">" + image + "</div><div class=\"kit-user-loquiero-vendor-name\">Amazon</div></div>";
                    count = 1;
                }
            }


            if (count > 0)
            {
                sHtml += "<div id=\"kit-user-loquiero-container\">";
                sHtml += "<div id=\"kit-user-loquiero-container-top\"></div>";
                sHtml += "<div id=\"kit-user-loquiero-container-center\">";
                sHtml += "<div id=\"kit-user-loquiero-container-close\"></div>";

                sHtml += sHtmlContainer;

                sHtml += "</div>";
                sHtml += "<div id=\"kit-user-loquiero-container-bottom\"></div>";
                sHtml += "</div>";
                //sHtml += "<div id=\"kit-user-loquiero\"><a href=\"#\" target=\"_blank\" onclick=\"LoQuieroDialog(" + ProductID + "));\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LO_QUIERO + "!</a></div>";
            }

            return sHtml;
        }
    }
}