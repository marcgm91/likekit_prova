﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class UserToFollowsRepository : BaseRepository
    {
        public UserToFollowsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private UserToFollow GetUserToFollow(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            UserToFollow userToFollow = new UserToFollow
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return userToFollow;
        }

        public void CreateUserToFollow(UserToFollow UserToFollow)
        {
            Int64 UserID = 0;

            if (UserToFollow.User != null)
                UserID = UserToFollow.User.ID;

            String SQL = String.Format("INSERT INTO UserToFollow (UserID, Created, Modified) " +
                "values ({0},'{1}','{2}')",
                UserID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateUserToFollow(UserToFollow UserToFollow)
        {
            Int64 UserID = 0;

            if (UserToFollow.User != null)
                UserID = UserToFollow.User.ID;

            String SQL = String.Format("UPDATE UserToFollow SET UserID={0}," +
                                       "Created='{1}',Modified='{2}' " +
                                       " WHERE ID={3}",
                                        UserID,
                                        Database.ParseDateTimeToSQL(UserToFollow.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        UserToFollow.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteUserToFollow(UserToFollow UserToFollow)
        {
            String SQL = String.Format("DELETE FROM UserToFollow " +
                                       " WHERE ID={0}",
                                        UserToFollow.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}