﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using EndeproLib.Endepro.Common;
using System.Collections;
using System.IO;
using System.Configuration;
using Twitterizer;

namespace LikeKitWebsite.Repository
{
    /// <summary>
    /// Objecte amb la informació dels Usuaris
    /// </summary>
    public class JsonProducts
    {
        public Int64 ID = 0;
        public String Name = String.Empty;
    }

    /// <summary>
    /// Objecte amb la informació dels kits
    /// </summary>
    public class JsonKits
    {
        public Int64 ID = 0;
        public Int64 UserID = 0;
        public String UserName = String.Empty;
        public Int64 ProductID = 0;
        public String ProductName = String.Empty;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
        public DateTime Created;
        public DateTime Modified;
        public String Comment = String.Empty;
        public String UserPicture = String.Empty;
        public String ProductImage = String.Empty;
        public Int16 NumKits = 0;
        public Int16 NumComments = 0;
        public Int16 NumLikes = 0;
        public Int64 ProductCategoryID = 0;
        public Int16 iUserType = 0;
        public Int16 Valoration = 0;
    }

    /// <summary>
    /// Objecte amb la informació dels kitComments
    /// </summary>
    public class JsonKitComments
    {
        public Int64 ID = 0;
        public Int64 UserID = 0;
        public String UserName = String.Empty;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
        public String UserPicture = String.Empty;
        public DateTime Created;
        public DateTime Modified;
        public String Comment = String.Empty;
        public String TimeDifference = String.Empty;
        public Int16 iUserType = 0;
    }

    /// <summary>
    /// Objecte amb la informació dels usersLikeUser
    /// </summary>
    public class JsonUsersLikeUser
    {
        public Int64 ID = 0;
        public Int64 UserID = 0;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
        public String UserPicture = String.Empty;
        public String UserLocation = String.Empty;
        public Int16 iUserType = 0;
    }

    public class TimeLineRepository : BaseRepository
    {

        public TimeLineRepository()
        {
        }

        public TimeLineRepository(DatabaseBase database)
        {
            Database = database;
        }

        public Product GetProduct(Int64 id)
        {
            ProductsRepository _productsRp = new ProductsRepository(Database);
            return _productsRp.GetProduct(id);
        }

        public void CreateKit(Kit kit)
        {
            KitsRepository _kitsRp = new KitsRepository(Database);
            _kitsRp.CreateKit(kit);
        }

        public Kit GetKit(Int64 id, Int64 SessionUserID = 0)
        {
            KitsRepository _kitsRp = new KitsRepository(Database);
            return _kitsRp.GetKit(id, SessionUserID);
        }

        public User GetUser(Int64 id)
        {
            UsersRepository _usersRp = new UsersRepository(Database);
            return _usersRp.GetUser(id);
        }

        public object SearchProducts(string search, int limit)
        {
            String sql = String.Format("select * from Product where Name like '%{0}%' Order by Name ASC LIMIT 0,{1}", Database.ParseStringToSQL(search), limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                JsonProducts obj;

                while (reader.Read())
                {
                    obj = new JsonProducts();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.Name = Database.GetReaderValueString(reader, "Name");

                    List.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return List;
        }

        public String SearchProductsKitHtml(string search, String categories, int limit)
        {
            if ((categories != String.Empty) && (categories[0] == ','))
            {
                categories = categories.Remove(0, 1);
            }

            String sql_categories = String.Empty;
            if (categories != String.Empty)
                sql_categories = String.Format(" AND ProductCategory.ID IN({0})", categories);

            String sql = String.Format("select Product.ID, Product.Name, Product.Author, Product.Year, ProductCategory.Name as CategoryName, Product.Observations, Product.ProductCategoryID from Product, ProductCategory where ProductCategory.ID = Product.ProductCategoryID AND (Product.Name like '%{0}%' OR Product.Author like '%{0}%') AND Product.Active=1 {1} Order by Product.Name ASC LIMIT 0,{2}", Database.ParseStringToSQL(search), sql_categories, limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String sHtml = String.Empty;

            int count = 0;

            try
            {
                JsonProducts obj;

                while (reader.Read())
                {
                    count ++;
                    obj = new JsonProducts();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.Name = Database.GetReaderValueString(reader, "Name");
                    String author = Database.GetReaderValueString(reader, "Author");
                    Int16 nYear = Database.GetReaderValueInt16(reader, "Year");
                    String Observations = Database.GetReaderValueString(reader, "Observations");
                    Int64 CategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");

                    String year = String.Empty;
                    if (nYear > 0)
                        year = String.Format("{0}.",nYear);

                    if((author != "") && (author != String.Empty))
                        author = String.Format("{0}.",author);

                    String category = Database.GetReaderValueString(reader, "CategoryName");

                    String sFormat = String.Empty;

                    //Mirem la categoria, si és música afegim el format
                    if ((CategoryID == 4) && (Observations != String.Empty))
                    {
                        sFormat = String.Format(" ({0})", Observations);
                    }

                    sHtml += "<div class=\"timeline-kit-search-result\" onclick=\"KitLoadProduct(" + obj.ID + ");\">\n" +
                              "<div class=\"timeline-kit-search-result-title\">" + obj.Name + ". " + author + " " + year + sFormat + "</div>\n" +
                              "<div class=\"timeline-kit-search-result-category\">" + category + "</div>\n" +
                              "</div>\n";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            { 
                /*sHtml = "<div class=\"timeline-kit-search-result-noresults-title\">Disculpa, no hemos encontrado nada que corresponda.</div>" +
                        "<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";*/
            }

            return sHtml;
        }

        public String WSSearchProductsKitHtml(string search, String categories, int limit)
        {
            String sHtml = String.Empty;

            String sAssociateTag = "likekit08-21";
            String sAssociateTagUK = "likekit04-21";
            String sAWSAccessKeyID = "AKIAIBDCTHDECKPMLEFQ";
            String sAWSSecretAccessKey = "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";

            List<AmazonResponse> books =  new List<AmazonResponse>();
            List<AmazonResponse> series = new List<AmazonResponse>();
            List<AmazonResponse> films = new List<AmazonResponse>();
            List<AmazonResponse> music = new List<AmazonResponse>();
            List<AmazonResponse> musicEs = new List<AmazonResponse>();

            string[] tokensizedStrsCategories;
            tokensizedStrsCategories = categories.Split(new char[] { ',' });

            int lenTags = tokensizedStrsCategories.Length;

            if ((categories == String.Empty) || (categories == ""))
            {
                books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
                series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
                films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
                music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
                musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);
            }
            else
            {
                for (int i = 0; i < lenTags; i++)
                {
                    string tmp = tokensizedStrsCategories[i];

                    if (tmp != "")
                    {
                        Int16 category = Int16.Parse(tmp);

                        switch (category)
                        {
                            case 1:
                                books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
                                break;
                            case 2:
                                series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
                                break;
                            case 3:
                                films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
                                break;
                            case 4:
                                music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
                                musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);
                                break;
                        }
                    }
                }
            }

            //Products reorder
            List<AmazonResponse> products = new List<AmazonResponse>();
            List<AmazonResponse> products_hidden = new List<AmazonResponse>();

            int count_tmp = 0;
            int count_products_hidden = 0;
            for (int count_products = 0; (count_products < limit) && (count_tmp < limit) ; count_products++)
            {
                if (books.Count > count_products)
                {
                    products.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products){
                    products.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products.Add(musicEs[count_products]);
                    count_tmp++;
                }

                count_products_hidden = count_products;
            }

            //Omplim la segona pàgina
            for (int count_products = count_products_hidden; (count_products < (limit + limit)) && (count_tmp < (limit + limit)); count_products++)
            {
                if (books.Count > count_products)
                {
                    products_hidden.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products_hidden.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products_hidden.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products_hidden.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products_hidden.Add(musicEs[count_products]);
                    count_tmp++;
                }
            }

            int count = 0;

            foreach (AmazonResponse product in products)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    { 
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                String sFormat = String.Empty;

                //Mirem la categoria, si és música afegim el format
                if ((product.Category == "Música") && (product.Observations != String.Empty))
                {
                    sFormat = String.Format(" ({0})", product.Observations);
                }

                if (product.Description != null)
                {
                    product.Description = Globals.ParseAmazonDescription(product.Description);
                }

                sHtml += "<div class=\"timeline-kit-search-result\" onclick=\"KitLoadWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "','" + product.Description + "','" + product.EAN + "','" + product.Observations + "');\">\n" +
                          "<div class=\"timeline-kit-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + sFormat + "</div>\n" +
                          "<div class=\"timeline-kit-search-result-category\">" + product.Category + "</div>\n" +
                          "</div>\n";            
            }


            if (products_hidden.Count > 0)
            {
                sHtml += "<div class=\"timeline-kit-search-result\">\n" +
                            "<div id=\"timeline-kit-search-result-more-results\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MAS_RESULTADOS + "</div></div>\n" +
                            "</div>\n";
            }

            sHtml += "<div id=\"timeline-kit-search-result-more-results-container\">\n";

            foreach (AmazonResponse product in products_hidden)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                String sFormat = String.Empty;

                //Mirem la categoria, si és música afegim el format
                if ((product.Category == "Música") && (product.Observations != String.Empty))
                {
                    sFormat = String.Format(" ({0})", product.Observations);
                }

                if (product.Description != null)
                {
                    product.Description = Globals.ParseAmazonDescription(product.Description);
                }

                sHtml += "<div class=\"timeline-kit-search-result\" onclick=\"KitLoadWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "','" + product.Description + "','" + product.EAN + "','" + product.Observations + "');\">\n" +
                          "<div class=\"timeline-kit-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + sFormat + "</div>\n" +
                          "<div class=\"timeline-kit-search-result-category\">" + product.Category + "</div>\n" +
                          "</div>\n";
            }

            sHtml += "</div>\n";

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            {
                sHtml = "<div class=\"timeline-kit-search-result-noresults-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_SEARCH_NORESULT + "</div>";
                //"<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";
            }

            return sHtml;
        }

        public String SearchProductsNowWithHtml(string search, String categories, int limit)
        {
            String sql_categories = String.Empty;

            if ((categories != String.Empty) && (categories[0] == ','))
            { 
                categories = categories.Remove(0, 1);
            }

            if (categories != String.Empty)
                sql_categories = String.Format(" AND ProductCategory.ID IN({0})", categories);

            String sql = String.Format("select Product.ID, Product.Name, Product.Author, Product.Year, ProductCategory.Name as CategoryName, Product.Observations, Product.ProductCategoryID from Product, ProductCategory where ProductCategory.ID = Product.ProductCategoryID AND (Product.Name like '%{0}%' OR Product.Author like '%{0}%') AND Product.Active=1 {1} Order by Product.Name ASC LIMIT 0,{2}", Database.ParseStringToSQL(search), sql_categories, limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String sHtml = String.Empty;

            int count = 0;

            try
            {
                JsonProducts obj;

                while (reader.Read())
                {
                    count++;
                    obj = new JsonProducts();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.Name = Database.GetReaderValueString(reader, "Name");
                    String author = Database.GetReaderValueString(reader, "Author");
                    Int16 nYear = Database.GetReaderValueInt16(reader, "Year");
                    String Observations = Database.GetReaderValueString(reader, "Observations");
                    Int64 CategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");	

                    String year = String.Empty;
                    if (nYear > 0)
                        year = String.Format("{0}.", nYear);

                    if ((author != "") && (author != String.Empty))
                        author = String.Format("{0}.", author);

                    String category = Database.GetReaderValueString(reader, "CategoryName");

                    String sFormat = String.Empty;

                    //Mirem la categoria, si és música afegim el format
                    if ((CategoryID == 4) && (Observations != String.Empty))
                    {
                        sFormat = String.Format(" ({0})", Observations);
                    }		

                    sHtml += "<div class=\"timeline-now-with-search-result\" onclick=\"NowWithAddProduct(" + obj.ID + ");\">\n" +
                              "<div class=\"timeline-now-with-search-result-title\">" + obj.Name + ". " + author + " " + year + sFormat + "</div>\n" +
                              "<div class=\"timeline-now-with-search-result-category\">" + category + "</div>\n" +
                              "</div>\n";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            {
                /*sHtml = "<div class=\"timeline-kit-search-result-noresults-title\">Disculpa, no hemos encontrado nada que corresponda.</div>" +
                        "<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones..</div>";*/
            }

            return sHtml;
        }

        public String SearchProductsWishListHtml(string search, String categories, int limit)
        {
            if ((categories != String.Empty) && (categories[0] == ','))
            {
                categories = categories.Remove(0, 1);
            }

            String sql_categories = String.Empty;
            if (categories != String.Empty)
                sql_categories = String.Format(" AND ProductCategory.ID IN({0})", categories);

            String sql = String.Format("select Product.ID, Product.Name, Product.Author, Product.Year, ProductCategory.Name as CategoryName, Product.Observations, Product.ProductCategoryID from Product, ProductCategory where ProductCategory.ID = Product.ProductCategoryID AND (Product.Name like '%{0}%' OR Product.Author like '%{0}%') AND Product.Active=1 {1} Order by Product.Name ASC LIMIT 0,{2}", Database.ParseStringToSQL(search), sql_categories, limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String sHtml = String.Empty;

            int count = 0;

            try
            {
                JsonProducts obj;

                while (reader.Read())
                {
                    count++;
                    obj = new JsonProducts();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.Name = Database.GetReaderValueString(reader, "Name");
                    String author = Database.GetReaderValueString(reader, "Author");
                    Int16 nYear = Database.GetReaderValueInt16(reader, "Year");
                    String Observations = Database.GetReaderValueString(reader, "Observations");
                    Int64 CategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");

                    String year = String.Empty;
                    if (nYear > 0)
                        year = String.Format("{0}.", nYear);

                    if ((author != "") && (author != String.Empty))
                        author = String.Format("{0}.", author);

                    String category = Database.GetReaderValueString(reader, "CategoryName");

                    String sFormat = String.Empty;

                    //Mirem la categoria, si és música afegim el format
                    if ((CategoryID == 4) && (Observations != String.Empty))
                    {
                        sFormat = String.Format(" ({0})", Observations);
                    }

                    sHtml += "<div class=\"timeline-wishlist-search-result\" onclick=\"WishListAddProduct(" + obj.ID + ");\">\n" +
                              "<div class=\"timeline-wishlist-search-result-title\">" + obj.Name + ". " + author + " " + year + sFormat + "</div>\n" +
                              "<div class=\"timeline-wishlist-search-result-category\">" + category + "</div>\n" +
                              "</div>\n";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            {
                /*sHtml = "<div class=\"timeline-kit-search-result-noresults-title\">Disculpa, no hemos encontrado nada que corresponda.</div>" +
                        "<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";*/
            }

            return sHtml;
        }


        public ArrayList GetKitOtherUsers(Int64 KitID, Int64 SessionUserID)
        {
            //Load kit data, to get ProductID
            Kit _kit = GetKit(KitID, SessionUserID);

            Int64 ProductID = _kit.Product.ID;

            String sql = String.Format("select UserID from Kit where ProductID = {0}", ProductID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                UsersRepository _usersRp = new UsersRepository(Database);

                while (reader.Read())
                {
                    Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");

                    if (UserID > 0)
                    {
                        User obj = _usersRp.GetUser(UserID);
                        List.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return List;
        }

        public void AddToWishList(Int64 ProductID, Int64 UserID)
        {
            WishList wl = new WishList();
            wl.Product = new Product();
            wl.Product.ID = ProductID;
            wl.User = new User();
            wl.User.ID = UserID;

            WishListsRepository _wishRP = new WishListsRepository(Database);
            _wishRP.CreateWishList(wl);
        }

        public String GetTimeline(DateTime date, Int64 SessionUserID, String search = "", String filter = "", String tags = "", Int16 Limit = 15, Int16 Start = 0)
        {
            String sHtml = String.Empty;

            String sql = "select distinct(kt.ID), kt.UserID, kt.ProductID, kt.Comment, kt.Valoration, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, usr.Gender, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, " +
                " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes ," +
                " EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(),usr.Birthday))))+0 AS Age " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID ";

            bool do_filter = true;


            if ((search != "") || (tags != ""))
                sql += " left join KitTag ktag on kt.ID = ktag.KitID ";

            sql += " where 1=1 ";

            if (search != "")
            {
                sql += String.Format(" AND (pr.Name like '%{0}%' OR pr.Author like '%{0}%' OR ktag.Name like '%{0}%')", search);
                do_filter = false;
            }

            //Mirem els tags
            string[] tokensizedStrsTags;
            tokensizedStrsTags = tags.Split(new char[] { ',' });

            int lenTags = tokensizedStrsTags.Length;

            String sql_tags = String.Empty;
            String sql_search_tags = String.Empty;

            for (int i = 0; i < lenTags; i++)
            {
                string tmp = tokensizedStrsTags[i];

                if (tmp != "")
                {
                    if (sql_tags != "")
                        sql_tags += " OR ";

                    if (sql_search_tags != "")
                        sql_search_tags += " OR ";
                    
                    sql_tags += String.Format(" (ktag.Name like '%{0}%')", tmp.Trim());
                    sql_search_tags += String.Format(" (pr.Name like '%{0}%')", tmp.Trim());


                }
            }

            if ((sql_tags != "") && (sql_search_tags != ""))
            {
                sql += " AND ((" + sql_tags + ") OR (" + sql_search_tags + ")) ";
            }
            else if (sql_tags != "")
            {
                sql += " AND (" + sql_tags + ") ";
            }
            else if (sql_search_tags != "")
            {
                sql += " AND (" + sql_search_tags + ") ";
            }

            if (date == null)
                date = DateTime.Now;

            sql += String.Format(" AND (kt.Created <= '{0}')", Database.ParseDateTimeToSQL(date));

            if (filter == null)
                filter = "";

            String sql_users = String.Empty;
            String sql_categories = String.Empty;
            String sql_relation = String.Empty;
            String sql_age = String.Empty;
            String sql_genere = String.Empty;
            String sql_order_by = String.Empty;

            string[] tokensizedStrs;
            tokensizedStrs = filter.Split(new char[] { ',' });
            int len = tokensizedStrs.Length;

            bool todos = false;
            bool trenders = false;

            String sql_age_function = " EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(),usr.Birthday))))+0 ";

            //De moment no treiem el filtre si fem una cerca textual
            if (do_filter)
            {
                for (int i = 0; i < len; i++)
                {
                    string tmp = tokensizedStrs[i];

                    if (tmp == "true")
                    {
                        switch (i)
                        {
                            case 0: //TRENDERS
                                sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(ID) FROM User where UserType=1)) ");
                                trenders = true;
                                break;
                            case 1: //FOLLOWINGS
                                if (sql_users != "")
                                    sql_users += " OR ";
                                sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(FollowID) FROM UserRelation where UserID = {0})) ", SessionUserID);
                                break;
                            case 2: //TODOS
                                sql_users = String.Empty;
                                todos = true;
                                break;
                            case 3: //LIBROS
                                sql_categories += String.Format("(pr.ProductCategoryID = {0})", 1);
                                break;
                            case 4: //CINE
                                if (sql_categories != "")
                                    sql_categories += " OR ";
                                sql_categories += String.Format(" (pr.ProductCategoryID = {0})", 3);
                                break;
                            case 5: //SERIES
                                if (sql_categories != "")
                                    sql_categories += " OR ";
                                sql_categories += String.Format(" (pr.ProductCategoryID = {0})", 2);
                                break;
                            case 6: //MÚSICA
                                if (sql_categories != "")
                                    sql_categories += " OR ";
                                sql_categories += String.Format(" (pr.ProductCategoryID = {0})", 4);
                                break;
                            case 7: //TODOS
                                sql_categories = String.Empty;
                                break;
                            case 8: //MÁS COMENTADO
                                /*sql_relation += String.Format(" AND num_comments > 0 ");*/
                                sql_order_by = String.Format(" order by num_comments DESC");
                                break;
                            case 9: //MÁS RECOMENDADO
                                //sql_relation += String.Format(" AND kt.UserID IN ()like '%{0}%'", search);
                                sql_order_by = String.Format(" order by num_kits DESC");
                                break;
                            case 10: //TODOS
                                sql_relation = String.Empty;
                                break;
                            case 11: //16-25
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=16) AND ({0} <=25))", sql_age_function);
                                break;
                            case 12: //26-35
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=26) AND ({0} <=35))", sql_age_function);
                                break;
                            case 13: //36-45
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=36) AND ({0} <=45))", sql_age_function);
                                break;
                            case 14: //46-55
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=46) AND ({0} <=55))", sql_age_function);
                                break;
                            case 15: //56-65
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=56) AND ({0} <=65))", sql_age_function);
                                break;
                            case 16: //>65
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" ({0} > 65)", sql_age_function);
                                break;

                            case 17: //>Man
                                if (sql_genere != "")
                                    sql_genere += " OR ";
                                sql_genere += String.Format(" (usr.Gender = 1)");
                                break;
                            case 18: //>Woman
                                if (sql_genere != "")
                                    sql_genere += " OR ";
                                sql_genere += String.Format(" (usr.Gender = 2)");
                                break;
                        }
                    }
                }

                if (sql_users != "")
                {
                    if(!trenders)
                        sql_users = " AND (" + sql_users + " OR kt.UserID = " + SessionUserID + ") ";
                    else
                        sql_users = " AND (" + sql_users + ") ";
                }
                else if (!todos)
                {
                    sql_users = " AND (";
                    sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(ID) FROM User where UserType=1)) ");
                    sql_users += " OR ";
                    sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(FollowID) FROM UserRelation where UserID = {0})) ", SessionUserID);
                    sql_users += " OR kt.UserID = " + SessionUserID + ") ";
                }

                if (sql_categories != "")
                    sql_categories = " AND (" + sql_categories + ") ";

                if (sql_order_by == "")
                    sql_order_by = String.Format(" order by kt.Modified DESC");

                if (sql_age != "")
                    sql_age = " AND (" + sql_age + ") ";

                if (sql_genere != "")
                    sql_genere = " AND (" + sql_genere + ") ";
            }

            String sql_limit = "";

            sql_limit = String.Format(" LIMIT {0}, {1}", Start, Limit);

            sql += String.Format("{0} {1} {2} {3} {4} {5} {6}", sql_users, sql_categories, sql_relation, sql_age, sql_genere, sql_order_by, sql_limit);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList objs = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    JsonKits obj = new JsonKits();

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    String UserName = Database.GetReaderValueString(reader, "UserName");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    String ProductName = Database.GetReaderValueString(reader, "ProductName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                    Int16 NumKits = Database.GetReaderValueInt16(reader, "num_kits");
                    Int16 NumComments = Database.GetReaderValueInt16(reader, "num_comments");
                    Int16 NumLikes = Database.GetReaderValueInt16(reader, "num_likes");
                    Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
                    Int16 UserType = Database.GetReaderValueInt16(reader, "UserType");
                    String Comment = Database.GetReaderValueString(reader, "Comment");
                    Int16 Valoration = Database.GetReaderValueInt16(reader, "Valoration");

                    if (UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, UserPicture);

                        if (!File.Exists(sFile))
                            UserPicture = "";
                    }

                    if (UserPicture == "")
                        UserPicture = "/Content/images/default_user.png";
                    else
                        UserPicture = "/Content/files/" + UserPicture;

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = "/Content/images/default_product.png";
                    else
                        ProductImage = "/Content/files/" + ProductImage;

                    obj.ID = ID;
                    obj.UserID = UserID;
                    obj.ProductID = ProductID;
                    obj.UserName = UserName;
                    obj.FirstName = FirstName;
                    obj.LastName = LastName;
                    obj.UserPicture = UserPicture;
                    obj.ProductName = ProductName;
                    obj.ProductImage = ProductImage;
                    obj.NumKits = NumKits;
                    obj.NumComments = NumComments;
                    obj.NumLikes = NumLikes;
                    obj.ProductCategoryID = ProductCategoryID;
                    obj.iUserType = UserType;
                    obj.Comment = Comment;
                    obj.Valoration = Valoration;

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            int num_kits = 0;

            try
            {
                foreach (JsonKits kit in objs)
                {
                    num_kits++;
                    String display_name = String.Format("{0} {1}", kit.FirstName, kit.LastName);

                    //Mirem la llargada del productName.
                    kit.ProductName = Globals.WordCut(kit.ProductName, 40, new char[] { ' ' });


                    String item_scope = String.Empty;
                    String sCssCategory = String.Empty;

                    switch (kit.ProductCategoryID)
                    {
                        case 1:
                            sCssCategory = "book-category-image";
                            item_scope = " itemscope itemtype=\"http://schema.org/Book\"";
                            break;
                        case 2:
                            sCssCategory = "book-serie-image";
                            item_scope = " itemscope itemtype=\"http://schema.org/TVSeries\"";
                            break;
                        case 3:
                            sCssCategory = "book-cinema-image";
                            item_scope = " itemscope itemtype=\"http://schema.org/Movie\"";
                            break;
                        case 4:
                            sCssCategory = "book-music-image";
                            item_scope = " itemscope itemtype=\"http://schema.org/MusicAlbum\"";
                            break;
                    }

                    String url_friendly = String.Format("{0}kit/timeline/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(kit.ProductName), kit.ID);

                    sHtml += "<div class=\"timeline-entry\" " + item_scope + ">\n" +
                        "<div class=\"timeline-entry-image\">\n" +
                            "<img src=\"" + kit.ProductImage + "\" width=\"220px\" alt=\"" + kit.ProductName + "\" title=\"" + kit.ProductName + "\" />\n" +
                            //"<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\" onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "';\">\n" +
                            "<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\" onclick=\"parent.location.href='" + url_friendly + "';\">\n" +
                                "<div class=\"timeline-entry-info-title\">" + kit.ProductName + "</div>\n" +
                                "<div class=\"timeline-entry-info-kit-link-container\">\n" +
                                    "<div class=\"timeline-entry-info-kit-link-container-border\">\n" +
                                    //"<div class=\"timeline-entry-info-kit-link\" onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_KIT + "</div>\n";
                                    "<div class=\"timeline-entry-info-kit-link\" onclick=\"parent.location.href='" + url_friendly + "';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_KIT + "</div>\n";

                    if (SessionUserID == kit.UserID)
                        sHtml += "<div class=\"timeline-entry-info-kit-link\" onclick=\"EditKit(event," + kit.ID + ");return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_EDITA_TU_KIT + "</div>\n";
                    sHtml += "</div>\n";
                    sHtml += "</div>\n";

                    //Mirem que l'usuari que ha creat un kit, no pugui fer un like del seu propi kit
                    if (SessionUserID != kit.UserID)
                    {
                        //Mirem si l'usuari ja ha fet un like o no
                        UserLikesRepository ulRP = new UserLikesRepository(Database);
                        Int64 ul = ulRP.Exists(SessionUserID, kit.ID);
                        if (ul > 0)
                            sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-dislike-link\" onclick=\"DisLike(event," + kit.ID + "," + ul + ");return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_DISLIKE + "</div>\n";
                        else
                            sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-like-link\" onclick=\"Like(event," + kit.ID + ");return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIKE + "</div>\n";
                    }

                    /*String sCssCategory = String.Empty;

                    switch (kit.ProductCategoryID)
                    {
                        case 1:
                            sCssCategory = "book-category-image";
                            break;
                        case 2:
                            sCssCategory = "book-serie-image";
                            break;
                        case 3:
                            sCssCategory = "book-cinema-image";
                            break;
                        case 4:
                            sCssCategory = "book-music-image";
                            break;
                    }*/

                    sHtml += "<div class=\"timeline-entry-info-comment-link\" onclick=\"KitComment(event," + kit.ID + "); return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTAR + "</div>\n";

                    if (SessionUserID != kit.UserID)
                        sHtml += "<div class=\"timeline-entry-info-rekit-link\" onclick=\"ReKit(event," + kit.ID + ");return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_REKIT + "</div>\n";

                    sHtml += "</div>\n" +
                        "</div>\n" +
                        "<div class=\"timeline-entry-info-user\">\n" +
                            "<div class=\"timeline-entry-info-user-image\" onclick=\"Profile(" + kit.UserID + ");return false;\"><img src=\"" + kit.UserPicture + "\" width=\"28px\" alt=\"" + display_name + "\" title=\"" + display_name + "\" />";

                    if (kit.iUserType == 1)
                        sHtml += "<div class=\"user-trender-small-timeline\"><img src=\"/Content/images/trender.png\" width=\"28\" height=\"28\" /></div>";

                    sHtml += "</div>\n" +
                            "<div class=\"timeline-entry-info-username\" onclick=\"Profile(" + kit.UserID + ");return false;\">" + display_name + "</div>\n" +
                            "<div class=\"timeline-entry-info-category-image " + sCssCategory + "\"></div>\n" +
                        "</div>\n" +
                        //"<div class=\"timeline-entry-info-user-comment\" itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\" onclick=\"Kit(" + kit.ID + ");\">\n";
                        "<div class=\"timeline-entry-info-user-comment\" itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\" onclick=\"Kit('" + url_friendly + "');\">\n";

                    //Mirem si hi ha enllaços
                    kit.Comment = LikeKitWebsite.Globals.HtmlEncodingLinksShortTimeline(kit.Comment, LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ENLACE_MIN);

                    sHtml += "<div class=\"timeline-entry-info-user-comment-text\">" + kit.Comment + "</div>\n";
                    sHtml += "<div class=\"timeline-entry-info-user-comment-img\"><img src=\"/Content/images/kit-view-valoration-" + kit.Valoration + ".png\" /></div>\n";

                    sHtml += "</div>\n" +
                        "<div class=\"timeline-entry-options\">\n" +
                            "<div class=\"timeline-entry-options-likes\"><span id=\"timeline-entry-options-number-" + kit.ID + "\" class=\"timeline-entry-options-number\">" + kit.NumLikes + "</span> Likes</div>\n" +
                            //"<div class=\"timeline-entry-options-comments\"  onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "#kit-view-container-comments';\"><span class=\"timeline-entry-options-number\">" + kit.NumComments + "</span> " + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTARIOS + "</div>\n" +
                            "<div class=\"timeline-entry-options-comments\"  onclick=\"parent.location.href='" + url_friendly + "#kit-view-container-comments';\"><span class=\"timeline-entry-options-number\">" + kit.NumComments + "</span> " + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTARIOS + "</div>\n" +
                            "<div class=\"timeline-entry-options-kits\"><span class=\"timeline-entry-options-number\">" + kit.NumKits + "</span> ReKits</div>\n" +
                        "</div>\n" +
                        "<div class=\"timeline-entry-footer\"></div>\n" +
                    "</div>\n";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            /*if (Start > 0)
            {
                sHtml = "<div id=\"timeline-kits-container\">" + sHtml;
            }
            else
            {
                sHtml += "</div>"; //Important, tanquem el div aquí.
            }*/

            if (num_kits > 0)
            {
                //Beta version
                //String url = String.Format("/beta/Timeline/TimelineSearchPager/?date={0}&start={1}&limit={2}&Search={3}&filter={4}", HttpUtility.UrlEncode(date.ToString()), (Start + Limit), Limit, HttpUtility.UrlEncode(search), HttpUtility.UrlEncode(filter));
                String url = String.Format("/Timeline/TimelineSearchPager/?date={0}&start={1}&limit={2}&Search={3}&filter={4}", HttpUtility.UrlEncode(date.ToString()), (Start + Limit), Limit, HttpUtility.UrlEncode(search), HttpUtility.UrlEncode(filter));
                sHtml += String.Format("<nav id=\"page-nav\"><a href=\"{0}\"></a></nav>", url);
            }

            //Afegim la data
            sHtml += "<div id=\"timeline-date\">" + date.ToString() + "</div>";

            /*if (Start > 0)
            {
                sHtml += "</div>";
            }*/

            return sHtml;
        }

        public String GetProfileCategoryKits(Int64 UserID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;

            sHtml += GetProfileCategoryKitsByCategory(UserID, 1, SessionUserID);
            sHtml += GetProfileCategoryKitsByCategory(UserID, 3, SessionUserID);
            sHtml += GetProfileCategoryKitsByCategory(UserID, 2, SessionUserID);
            sHtml += GetProfileCategoryKitsByCategory(UserID, 4, SessionUserID);

            return sHtml;
        }

        public String GetProfileCategoryKitsByCategory(Int64 UserID, Int64 CategoryID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;

            String sql = "select kt.ID, kt.UserID, kt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, pr.Name as ProductName,pr.Image as ProductImage " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID " +
                " where 1=1 AND kt.UserID = " + UserID + " AND pr.ProductCategoryID = " + CategoryID + " order by kt.Valoration Desc, kt.Created Desc";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String css_id = String.Empty;
            String category_title = String.Empty;
            String margin_left = String.Empty;

            switch(CategoryID)
            {
                case 1:
                    css_id = "books";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIBROS;
                    break;
                case 2:
                    css_id = "series";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SERIES;
                    margin_left = " left22";
                    break;
                case 3:
                    css_id = "cinema";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CINE;
                    margin_left = " left22";
                    break;
                case 4:
                    css_id = "music";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUSICA;
                    break;
            }

            sHtml += "<div id=\"profile-kits-" + css_id + "\" class=\"profile-kits-section" + margin_left + "\">" +
            "<div class=\"profile-kits-section-images\">";

            Int16 count = 0;

            try
            {
                while (reader.Read())
                {
                    count++;

                    if (count < 7)
                    {
                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                        String ProductName = Database.GetReaderValueString(reader, "ProductName");

                        if (ProductImage != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, ProductImage);

                            if (!File.Exists(sFile))
                                ProductImage = "";
                        }

                        if (ProductImage == "")
                            ProductImage = "/Content/images/default_product.png";
                        else
                            ProductImage = "/Content/files/" + ProductImage;

                        String sCss = "";

                        if((count == 1) || (count == 3))
                            sCss = "profile-kits-section-entry-right-bottom";
                        else if((count == 2) || (count == 4))
                            sCss = "profile-kits-section-entry-left-bottom";
                        else if(count == 5)
                            sCss = "profile-kits-section-entry-right";
                        else if(count == 6)
                            sCss = "profile-kits-section-entry-left";

                        String url_friendly = String.Format("{0}kit/timeline/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(ProductName), ID);

                        //sHtml += "<div class=\"profile-kits-section-entry " + sCss + "\" onclick=\"Kit(" + ID + ");\"><img src=\"" + ProductImage + "\" width=\"110px\"/></div>";
                        sHtml += "<div class=\"profile-kits-section-entry " + sCss + "\" onclick=\"Kit('" + url_friendly + "');\"><img src=\"" + ProductImage + "\" width=\"110px\"/></div>";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            int add_categories = 6 -count;
            int position = 0;

            if (count < 6)
            {
                for (int j = 0; j < add_categories; j++)
                {
                    position = count + j + 1;

                    String sCss = String.Empty;

                    if ((position == 1) || (position == 3))
                        sCss = "profile-kits-section-entry-right-bottom";
                    else if ((position == 2) || (position == 4))
                        sCss = "profile-kits-section-entry-left-bottom";
                    else if (position == 5)
                        sCss = "profile-kits-section-entry-right";
                    else if (position == 6)
                        sCss = "profile-kits-section-entry-left";

                    if (UserID != SessionUserID){
                        sHtml += "<div class=\"profile-kits-section-entry-empty-no-add " + sCss + "\"></div>";
                    }
                    else{
                        sHtml += "<div class=\"profile-kits-section-entry-empty " + sCss + "\"></div>";
                    }

                    
                }
            }

            sHtml += "</div>" +
                "<div class=\"profile-kits-section-footer\" onclick=\"parent.location.href= SUBDOMAIN_PATH + '/Timeline/CategoryKits/" + UserID + "/?Category=" + CategoryID + "';\">" +
                    category_title + " <span class=\"kits-number\"> " + count + "</span> <span class=\"kits-number-view\">(" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_VER_TODOS + ")</span>" +
                "</div>" +
            "</div>";

            /*if (count > 0)
                return sHtml;
            else
                return String.Empty;*/
            return sHtml;
        }

        public void NowWithAddProduct(Int64 ProductID, Int64 UserID)
        {

            UserStatus us = new UserStatus();
            us.Product = new Product();
            us.Product.ID = ProductID;
            us.User = new User();
            us.User.ID = UserID;

            UserStatusRepository _usRP = new UserStatusRepository(Database);
            _usRP.CreateUserStatus(us);
        }

        public String GetHomeTimeline(DateTime date, short language, String search = "", Int16 Limit = 15, Int16 Start = 0, Int16 type = 0)
        {
            String sHtml = String.Empty;

            //Màxim 30 resultats
            if (Start < 30)
            {
                String sql = "select kt.ID, kt.UserID, kt.ProductID, kt.Comment, kt.Valoration, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, pr.EAN," +
                    " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                    " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                    " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes " +
                    " from Kit kt left join User usr on kt.UserID = usr.ID " +
                    " left join Product pr on kt.ProductID = pr.ID " +
                    " where 1=1 ";

                //Ara fem els selects en funció del type
                if (type > 0)
                {
                    switch (type)
                    {

                        case 1: //Cine
                            sql += String.Format(" AND pr.ProductCategoryID = 3");
                            break;
                        case 2: //Libros
                            sql += String.Format(" AND pr.ProductCategoryID = 1");
                            break;
                        case 3: //Música
                            sql += String.Format(" AND pr.ProductCategoryID = 4");
                            break;
                        case 4: //Series
                            sql += String.Format(" AND pr.ProductCategoryID = 2");
                            break;
                        case 5: //Trenders
                            sql += String.Format(" AND (kt.UserID IN (SELECT DISTINCT(ID) FROM User where UserType=1)) ");
                            break;
                        case 6: //Cartelera (17-6-2013).
                            //Faig servir el camp "EAN"(que no es fà servir en les películes). "1" = si estan en cartellera. Camp buït si no ho estan.
                            sql += String.Format(" AND pr.ProductCategoryID = 3 AND pr.EAN = '1' OR (kt.ID IN (SELECT DISTINCT(KitID) FROM Kittag WHERE Name = '#cartellera' OR Name = '#cartelera'))");
                            break;
                    }
                }

                if (language > 0)
                    sql += String.Format(" AND (usr.LanguageID={0})", language);

                if (date == null)
                    date = DateTime.Now;

                if (search != "")
                    sql += String.Format(" AND (pr.Name like '%{0}%' OR pr.Author like '%{0}%')", search);

                String sql_users = String.Empty;
                String sql_categories = String.Empty;
                String sql_relation = String.Empty;
                String sql_order_by = String.Empty;

                sql += String.Format(" AND (kt.Created <= '{0}')", Database.ParseDateTimeToSQL(date));

                sql_order_by = String.Format(" order by kt.Modified DESC");

                String sql_limit = "";

                sql_limit = String.Format(" LIMIT {0}, {1}", Start, Limit);

                sql += String.Format("{0} {1} {2} {3} {4}", sql_users, sql_categories, sql_relation, sql_order_by, sql_limit);

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();

                ArrayList objs = new ArrayList();

                try
                {
                    while (reader.Read())
                    {
                        JsonKits obj = new JsonKits();

                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                        Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                        String UserName = Database.GetReaderValueString(reader, "UserName");
                        String FirstName = Database.GetReaderValueString(reader, "FirstName");
                        String LastName = Database.GetReaderValueString(reader, "LastName");
                        String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                        String ProductName = Database.GetReaderValueString(reader, "ProductName");
                        String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                        Int16 NumKits = Database.GetReaderValueInt16(reader, "num_kits");
                        Int16 NumComments = Database.GetReaderValueInt16(reader, "num_comments");
                        Int16 NumLikes = Database.GetReaderValueInt16(reader, "num_likes");
                        Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
                        String Comment = Database.GetReaderValueString(reader, "Comment");
                        Int16 Valoration = Database.GetReaderValueInt16(reader, "Valoration");

                        if (UserPicture != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, UserPicture);

                            if (!File.Exists(sFile))
                                UserPicture = "";
                        }

                        if (UserPicture == "")
                            UserPicture = "/Content/images/default_user.png";
                        else
                            UserPicture = "/Content/files/" + UserPicture;

                        if (ProductImage != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, ProductImage);

                            if (!File.Exists(sFile))
                                ProductImage = "";
                        }

                        if (ProductImage == "")
                            ProductImage = "/Content/images/default_product.png";
                        else
                            ProductImage = "/Content/files/" + ProductImage;


                        obj.ID = ID;
                        obj.UserID = UserID;
                        obj.ProductID = ProductID;
                        obj.UserName = UserName;
                        obj.FirstName = FirstName;
                        obj.LastName = LastName;
                        obj.UserPicture = UserPicture;
                        obj.ProductName = ProductName;
                        obj.ProductImage = ProductImage;
                        obj.NumKits = NumKits;
                        obj.NumComments = NumComments;
                        obj.NumLikes = NumLikes;
                        obj.ProductCategoryID = ProductCategoryID;
                        obj.Comment = Comment;
                        obj.Valoration = Valoration;

                        objs.Add(obj);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();
                }

                int num_kits = 0;
                try
                {
                    foreach (JsonKits kit in objs)
                    {
                        num_kits++;
                        String display_name = String.Format("{0} {1}", kit.FirstName, kit.LastName);

                        String item_scope = String.Empty;
                        String sCssCategory = String.Empty;

                        switch (kit.ProductCategoryID)
                        {
                            case 1:
                                sCssCategory = "book-category-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/Book\"";
                                break;
                            case 2:
                                sCssCategory = "book-serie-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/TVSeries\"";
                                break;
                            case 3:
                                sCssCategory = "book-cinema-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/Movie\"";
                                break;
                            case 4:
                                sCssCategory = "book-music-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/MusicAlbum\"";
                                break;
                        }

                        String url_friendly = String.Format("{0}kit/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(kit.ProductName), kit.ID);
                        String user_url_friendly = String.Format("{0}perfilp/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(display_name), kit.UserID);

                        sHtml += "<div class=\"timeline-entry\" " + item_scope + ">\n" +
                            "<div class=\"timeline-entry-image\">\n" +
                                "<img itemprop=\"image\" src=\"" + kit.ProductImage + "\" width=\"220px\" alt=\"" + kit.ProductName + "\" title=\"" + kit.ProductName + "\" />\n" +
                            //"<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\" onclick=\"parent.location.href='/Home/Item/" + kit.ID + "';\">\n" +
                                "<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\" onclick=\"parent.location.href='" + url_friendly + "';\">\n" +
                                    "<div class=\"timeline-entry-info-title\" itemprop=\"name\">" + kit.ProductName + "</div>\n" +
                                        "<div class=\"timeline-entry-info-kit-link-container\">\n" +
                                            "<div class=\"timeline-entry-info-kit-link-container-border\">\n" +
                            //"<div class=\"timeline-entry-info-kit-link\" onclick=\"parent.location.href='/Home/Item/" + kit.ID + "';\">Ir al kit</div>\n";
                                                "<div class=\"timeline-entry-info-kit-link\"><a href=\"" + url_friendly + "\">Ir al kit</a></div>\n";

                        sHtml += "</div>\n";
                        sHtml += "</div>\n";

                        sHtml += "</div>\n" +
                            "</div>\n" +
                            "<div class=\"timeline-entry-info-review\" itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\">" +
                            "<div class=\"timeline-entry-info-user\">\n" +
                                "<div class=\"timeline-entry-info-user-image\"><a href=\"" + user_url_friendly + "\"><img src=\"" + kit.UserPicture + "\" width=\"28px\" alt=\"" + display_name + "\" title=\"" + display_name + "\" /></a></div>\n" +
                                "<div class=\"timeline-entry-info-username\" itemprop=\"author\"><a href=\"" + user_url_friendly + "\">" + display_name + "</a></div>\n" +
                                "<div class=\"timeline-entry-info-category-image " + sCssCategory + "\"></div>\n" +
                            "</div>\n" +

                            //"<div class=\"timeline-entry-info-user-comment\" itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\" onclick=\"HomeKit(" + kit.ID + ");\">\n";
                            "<div class=\"timeline-entry-info-user-comment\" onclick=\"HomeKit('" + url_friendly + "');\">\n";


                        //Mirem si hi ha enllaços
                        kit.Comment = LikeKitWebsite.Globals.HtmlEncodingLinksShortTimeline(kit.Comment, LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ENLACE_MIN);

                        sHtml += "<div class=\"timeline-entry-info-user-comment-text\" itemprop=\"name\">" + kit.Comment + "</div>\n";
                        sHtml += "<div class=\"timeline-entry-info-user-comment-img\" itemprop=\"reviewRating\" itemscope itemtype=\"http://schema.org/Rating\"><meta itemprop=\"ratingValue\" content=\"" + kit.Valoration + "\" /><meta itemprop=\"bestRating\" content=\"5\" /><img src=\"/Content/images/kit-view-valoration-" + kit.Valoration + ".png\" /></div>\n";

                        sHtml += "</div>\n";
                        sHtml += "</div>\n" + //fi review
                        "<div class=\"timeline-entry-options\">\n" +
                            "<div class=\"timeline-entry-options-likes\"><span class=\"timeline-entry-options-number\">" + kit.NumLikes + "</span> Likes</div>\n" +
                            "<div class=\"timeline-entry-options-comments\"><span class=\"timeline-entry-options-number\">" + kit.NumComments + "</span> " + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTARIOS + "</div>\n" +
                            "<div class=\"timeline-entry-options-kits\"><span class=\"timeline-entry-options-number\">" + kit.NumKits + "</span> ReKits</div>\n" +
                        "</div>\n" +
                        "<div class=\"timeline-entry-footer\"></div>\n" +
                    "</div>\n" +
                    "<input type=\"hidden\" id=\"type\" name=\"type\" value=\"" + type + "\" />";
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                /*if (Start > 0)
                {
                    sHtml = "<div id=\"timeline-kits-container\">" + sHtml;
                }
                else
                {
                    sHtml += "</div>"; //Important, tanquem el div aquí.
                }*/

                if (num_kits > 0)
                {
                    //Beta version
                    //String url = String.Format("/beta/Home/HomeTimelinePager/?date={0}&start={1}&limit={2}&search={3}&type={4}", HttpUtility.UrlEncode(date.ToString()), (Start + Limit), Limit, HttpUtility.UrlEncode(search), type);
                    String url = String.Format("/Home/HomeTimelinePager/?date={0}&start={1}&limit={2}&search={3}&type={4}", HttpUtility.UrlEncode(date.ToString()), (Start + Limit), Limit, HttpUtility.UrlEncode(search), type);
                    sHtml += String.Format("<nav id=\"page-nav\"><a href=\"{0}\"></a></nav>", url);
                }
            }
            /*if (Start > 0)
            {
                sHtml += "</div>";
            }*/

            return sHtml;
        }

        public Int64 GetHomeTimelineCounter(DateTime date, short language, String search = "")
        {
            String sHtml = String.Empty;

            String sql = "select count(kt.ID) as count " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                //" left join Product pr on kt.ProductID = pr.ID " +
                " where 1=1 ";

            /*if (language > 0)
                sql += String.Format(" AND (usr.LanguageID={0})", language);*/

            if (date == null)
                date = DateTime.Now;

            /*if (search != "")
                sql += String.Format(" AND (pr.Name like '%{0}%' OR pr.Author like '%{0}%')", search);*/

            String sql_users = String.Empty;
            String sql_categories = String.Empty;
            String sql_relation = String.Empty;
            String sql_order_by = String.Empty;

            sql += String.Format(" AND (kt.Created <= '{0}')", Database.ParseDateTimeToSQL(date));

            sql_order_by = String.Format(" order by kt.Modified DESC");

            sql += String.Format("{0} {1} {2} {3}", sql_users, sql_categories, sql_relation, sql_order_by);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int64 num_kits = 0;

            try
            {
                if (reader.Read())
                {
                    num_kits = Database.GetReaderValueInt64(reader, "count");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return num_kits;
        }

        public void CreateNotify(Int64 toUserID, Int64 fromUserID, Int64 refID, NotifyType notifyType)
        {
            String SQL = String.Format("INSERT INTO Notify (ToUserID, FromUserID, RefID, NotifyType, Readed, Created, Modified) " +
                "values ({0},{1},{2},{3},{4},'{5}','{6}')",
                toUserID,
                fromUserID,
                refID,
                (Int16)notifyType,
                0,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateReadUserNotifies(Int64 UserID)
        {
            String SQL = String.Format("UPDATE Notify SET Readed=1, Modified='{0}'" +
                                       " WHERE ToUserID={1}",
                                       Database.ParseDateTimeToSQL(DateTime.Now),
                                       UserID);

            Database.ExecuteCommand(SQL);
        }


        public String GetUsersLikeUserHtml(Int64 UserID, Int64 SessionUserID)
        {

            //String sql = String.Format("select count(ktusrs.ID) as count, us.ID, us.UserID, us.FirstName, us.LastName, us.Picture, us.UserType from Kit ktusrs left join User us on ktusrs.UserID = us.ID where ktusrs.ProductID IN(select ktu.ProductID from Kit ktu where ktu.UserID = {0}) AND ktusrs.UserID <> {0} GROUP BY ktusrs.UserID order by count DESC LIMIT 3", UserID);
            String sql = String.Format("select count(kitothers.ID) as count, us.ID, us.UserID, us.FirstName, us.LastName, us.Picture, us.UserType from  Kit ktusrs, Kit kitothers left join User us on kitothers.UserID = us.ID where ktusrs.ProductID = kitothers.ProductID AND ktusrs.Valoration = kitothers.Valoration AND ktusrs.UserID = {0} AND kitothers.UserID <> {0} GROUP BY kitothers.UserID order by count DESC LIMIT 3;", UserID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String sHtml = String.Empty;

            int count = 0;

            ArrayList objs = new ArrayList();

            try
            {
                JsonUsersLikeUser obj;

                while (reader.Read())
                {
                    obj = new JsonUsersLikeUser();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = obj.ID;//Database.GetReaderValueInt64(reader, "UserID");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "Picture");

                    if (obj.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, obj.UserPicture);

                        if (!File.Exists(sFile))
                            obj.UserPicture = "";
                    }

                    if (obj.UserPicture == "")
                        obj.UserPicture = "/Content/images/default_user.png";
                    else
                        obj.UserPicture = "/Content/files/" + obj.UserPicture;


                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {


                foreach (JsonUsersLikeUser user in objs)
                {
                    count++;

                    String display_name = String.Format("{0} {1}", user.FirstName, user.LastName);
                    String sCss = String.Empty;

                    if(count > 1)
                        sCss = "left22";

                    ArrayList images = GetFirstKitUserImages(user.ID,3);

                    String image_1 = String.Empty;
                    String image_2 = String.Empty;
                    String image_3 = String.Empty;

                    int count_images = 0;

                    foreach (String image in images)
                    {
                        count_images++;

                        switch (count_images)
                        { 
                            case 1:
                                image_1 = image;
                                break;
                            case 2:
                                image_2 = image;
                                break;
                            case 3:
                                image_3 = image;
                                break;
                        }
                    }

                    if (image_1 == String.Empty)
                        image_1 = "/Content/images/default_user.png";

                    if (image_2 == String.Empty)
                        image_2 = "/Content/images/default_user.png";

                    if (image_3 == String.Empty)
                        image_3 = "/Content/images/default_user.png";

                    sHtml += "<div class=\"profile-users-section " + sCss + "\">" +
                            "<div class=\"profile-users-images\">" +
                            "<div class=\"profile-users-bigentry\"><img src=\"" + image_1 + "\" width=\"220px\"/></div>" +
                            "<div class=\"profile-users-entry\"><img src=\"" + image_2 + "\" width=\"110px\"/></div>" +
                            "<div class=\"profile-users-entry\"><img src=\"" + image_3 + "\" width=\"110px\"/></div>" +
                        "</div>" +
                        "<div class=\"profile-users-footer\">" +
                            "<div class=\"profile-users-image\" onclick=\"Profile(" + user.ID + ");\"><img src=\"" + user.UserPicture +"\" width=\"28px\"/>";

                    if (user.iUserType == 1)
                        sHtml += "<div class=\"user-trender-small\"><img src=\"/Content/images/trender.png\" width=\"28\" height=\"28\" /></div>";
                            
                            
                     sHtml += "</div>" +
                            "<div class=\"profile-users-name\" onclick=\"Profile(" + user.ID + ");\">" + display_name + "</div>" +
                        "</div>" +
                    "</div>";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }

        public ArrayList GetFirstKitUserImages(Int64 UserID, Int16 ImageNumber)
        {

            String sql = String.Format("select p.Image from Kit k, Product p where  k.ProductID = p.ID AND k.UserID = {0}  LIMIT {1};", UserID, ImageNumber);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList images = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    String Image = String.Empty;
                    Image = Database.GetReaderValueString(reader, "Image");

                    if (Image != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, Image);

                        if (!File.Exists(sFile))
                            Image = "";
                    }

                    if (Image == "")
                        Image = "/Content/images/default_product.png";
                    else
                        Image = "/Content/files/" + Image;

                    images.Add(Image);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return images;
        }


        //DeleteRelatedNotify(KitCommentID, NotifyType.Comment)
        public void DeleteRelatedNotify(Int64 ID, NotifyType type)
        {
            String sql = String.Empty;

            sql = String.Format("DELETE FROM Notify WHERE RefID={0} AND NotifyType = {1}",ID, (int)type);

            Database.ExecuteCommand(sql);
        }


        public String GetHtmlCategoryKits(Int64 UserID, Int64 SessionUserID, Int16 CategoryID)
        {
            String sHtml = String.Empty;

            String sql = "select distinct(kt.ID), kt.UserID, kt.ProductID, kt.Comment, kt.Valoration, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, " +
                " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID ";

            sql += " where 1=1 ";

            String sql_users = String.Empty;
            String sql_categories = String.Empty;
            String sql_relation = String.Empty;
            String sql_order_by = String.Empty;

            sql_categories += String.Format(" AND (pr.ProductCategoryID = {0}) ", CategoryID);
            sql_users = " AND (kt.UserID = " + UserID + ") ";

            if (sql_order_by == "")
                sql_order_by = String.Format(" order by kt.Valoration DESC, pr.Name, kt.Modified DESC");

            sql += String.Format("{0} {1} {2} {3}", sql_users, sql_categories, sql_relation, sql_order_by);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            //ArrayList objs = new ArrayList();

            int count = 0;
            
            Dictionary<string, ArrayList> category_kits = new Dictionary<string, ArrayList>();

            try
            {
                while (reader.Read())
                {
                    JsonKits obj = new JsonKits();

                    count++;

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    Int64 KitUserID = Database.GetReaderValueInt64(reader, "UserID");
                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    String UserName = Database.GetReaderValueString(reader, "UserName");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    String ProductName = Database.GetReaderValueString(reader, "ProductName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                    Int16 NumKits = Database.GetReaderValueInt16(reader, "num_kits");
                    Int16 NumComments = Database.GetReaderValueInt16(reader, "num_comments");
                    Int16 NumLikes = Database.GetReaderValueInt16(reader, "num_likes");
                    Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
                    Int16 UserType = Database.GetReaderValueInt16(reader, "UserType");
                    Double Valoration = Database.GetReaderValueDouble(reader, "Valoration");
                    String Comment = Database.GetReaderValueString(reader, "Comment");

                    if (UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, UserPicture);

                        if (!File.Exists(sFile))
                            UserPicture = "";
                    }

                    if (UserPicture == "")
                        UserPicture = "/Content/images/default_user.png";
                    else
                        UserPicture = "/Content/files/" + UserPicture;

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = "/Content/images/default_product.png";
                    else
                        ProductImage = "/Content/files/" + ProductImage;

                    obj.ID = ID;
                    obj.UserID = KitUserID;
                    obj.ProductID = ProductID;
                    obj.UserName = UserName;
                    obj.FirstName = FirstName;
                    obj.LastName = LastName;
                    obj.UserPicture = UserPicture;
                    obj.ProductName = ProductName;
                    obj.ProductImage = ProductImage;
                    obj.NumKits = NumKits;
                    obj.NumComments = NumComments;
                    obj.NumLikes = NumLikes;
                    obj.ProductCategoryID = ProductCategoryID;
                    obj.iUserType = UserType;
                    obj.Comment = Comment;
                    obj.Valoration = (Int16)Valoration;

                    //Mirem la primera lletra del Producte
                    //String key = obj.ProductName[0].ToString().ToUpper();
                    //Ara ho fem diferent, Ho guardem en funció de la Valoració
                    String key = String.Empty;

                    

                    if (Valoration == 1)
                        key = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_NADA_RECOMENDABLE;
                    else if(Valoration == 2)
                        key = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_POCO_RECOMENDABLE;
                    else if (Valoration == 3)
                        key = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_RECOMENDABLES;
                    else if (Valoration == 4)
                        key = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUY_RECOMENDABLES;
                    else if (Valoration == 5)
                        key = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IMPRESCINDIBLES;

                    if (category_kits.ContainsKey(key))
                    {
                        ArrayList tmp = category_kits[key];
                        tmp.Add(obj);
                        category_kits[key] = tmp;
                    }
                    else
                    {
                        ArrayList tmp = new ArrayList();
                        tmp.Add(obj);
                        category_kits[key] = tmp;                    
                    }

                    //objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {
                sHtml += "<div id=\"category-kits-main-header\">\n";
                sHtml += "<div id=\"category-kits-main-header-count\">" + count + " Kits </div>\n";
                sHtml += "<div id=\"category-kits-main-header-5\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_NADA_RECOMENDABLE + "</div>\n";
                sHtml += "<div id=\"category-kits-main-header-4\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_POCO_RECOMENDABLE  + "</div>\n";
                sHtml += "<div id=\"category-kits-main-header-3\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_RECOMENDABLES  + "</div>\n";
                sHtml += "<div id=\"category-kits-main-header-2\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUY_RECOMENDABLES + "</div>\n";
                sHtml += "<div id=\"category-kits-main-header-1\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IMPRESCINDIBLES + "</div>\n";
                sHtml += "</div>\n";

                sHtml += "<div id=\"category-kits-container\">\n";

                int count_letters = 0;
                foreach (var pair in category_kits)
                {
                    //count_letters++;

                    String letter = pair.Key;
                    ArrayList list = pair.Value;

                    if (letter == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_NADA_RECOMENDABLE)
                        count_letters = 5;
                    else if (letter == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_POCO_RECOMENDABLE)
                        count_letters = 4;
                    else if (letter == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_RECOMENDABLES)
                        count_letters = 3;
                    else if (letter == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUY_RECOMENDABLES)
                        count_letters = 2;
                    else if (letter == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IMPRESCINDIBLES)
                        count_letters = 1;

                    sHtml += "<div id=\"category-kits-header-" + count_letters + "\" class=\"category-kits-header\" onclick=\"ShowCategoryKits(" + count_letters + ");\">" + letter + " (" + list.Count + ") </div>\n";
                    sHtml += "<div id=\"category-kits-container-" + count_letters + "\" class=\"category-kits-container\">\n";
                
                    foreach (JsonKits kit in list)
                    {

                        String display_name = String.Format("{0} {1}", kit.FirstName, kit.LastName);

                        String item_scope = String.Empty;
                        String sCssCategory = String.Empty;

                        switch (kit.ProductCategoryID)
                        {
                            case 1:
                                sCssCategory = "book-category-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/Book\"";
                                break;
                            case 2:
                                sCssCategory = "book-serie-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/TVSeries\"";
                                break;
                            case 3:
                                sCssCategory = "book-cinema-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/Movie\"";
                                break;
                            case 4:
                                sCssCategory = "book-music-image";
                                item_scope = " itemscope itemtype=\"http://schema.org/MusicAlbum\"";
                                break;
                        }

                        String url_friendly = String.Format("{0}kit/timeline/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(kit.ProductName), kit.ID);

                        sHtml += "<div id=\"timeline-entry-id-" + kit.ID + "\" class=\"timeline-entry\" " + item_scope + ">\n" +
                            "<div class=\"timeline-entry-image\">\n" +
                                "<img src=\"" + kit.ProductImage + "\" width=\"220px\" alt=\"" + kit.ProductName + "\" title=\"" + kit.ProductName + "\" />\n" +
                                //"<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\" onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "';\">\n" +
                                "<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\" onclick=\"parent.location.href='" + url_friendly + "';\">\n" +
                                    "<div class=\"timeline-entry-info-title\">" + kit.ProductName + "</div>\n" +
                                    "<div class=\"timeline-entry-info-kit-link-container\">\n" +
                                        "<div class=\"timeline-entry-info-kit-link-container-border\">\n" +
                                        //"<div class=\"timeline-entry-info-kit-link\" onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_KIT + "</div>\n";
                                        "<div class=\"timeline-entry-info-kit-link\" onclick=\"parent.location.href='" + url_friendly + "';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_KIT + "</div>\n";

                        if (SessionUserID == kit.UserID)
                        {
                            sHtml += "<div class=\"timeline-entry-info-kit-link\" onclick=\"EditKit(event," + kit.ID + ");return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_EDITA_TU_KIT + "</div>\n";
                            sHtml += "<div class=\"timeline-entry-info-kit-link\" onclick=\"DeleteKit(event," + kit.ID + "," + count_letters + ");return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ELIMINAR_KIT + "</div>\n";
                        }
                        sHtml += "</div>\n";
                        sHtml += "</div>\n";

                        if (SessionUserID != kit.UserID)
                        {
                            //Mirem si l'usuari ja ha fet un like o no
                            UserLikesRepository ulRP = new UserLikesRepository(Database);
                            Int64 ul = ulRP.Exists(SessionUserID, kit.ID);
                            if (ul > 0)
                                sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-dislike-link\" onclick=\"DisLike(event," + kit.ID + "," + ul + ");return false;\">Dislike</div>\n";
                            else
                                sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-like-link\" onclick=\"Like(event," + kit.ID + ");return false;\">Like</div>\n";
                        }
                        
                        /*String sCssCategory = String.Empty;

                        switch (kit.ProductCategoryID)
                        {
                            case 1:
                                sCssCategory = "book-category-image";
                                break;
                            case 2:
                                sCssCategory = "book-serie-image";
                                break;
                            case 3:
                                sCssCategory = "book-cinema-image";
                                break;
                            case 4:
                                sCssCategory = "book-music-image";
                                break;
                        }*/

                        if (SessionUserID != kit.UserID)
                        {
                            sHtml += "<div class=\"timeline-entry-info-comment-link\" onclick=\"KitComment(event," + kit.ID + "); return false;\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTAR + "</div>\n";
                        }
                        
                        if (SessionUserID != kit.UserID)
                            sHtml += "<div class=\"timeline-entry-info-rekit-link\" onclick=\"ReKit(event," + kit.ID + ");return false;\">ReKit</div>\n";

                        sHtml += "</div>\n" +
                            "</div>\n" +
                            "<div class=\"timeline-entry-info-user\">\n" +
                                "<div class=\"timeline-entry-info-user-image\" onclick=\"Profile(" + kit.UserID + ");return false;\"><img src=\"" + kit.UserPicture + "\" width=\"28px\" alt=\"" + display_name + "\" title=\"" + display_name + "\" />";

                        if (kit.iUserType == 1)
                            sHtml += "<div class=\"user-trender-small-timeline\"><img src=\"/Content/images/trender.png\" width=\"28\" height=\"28\" /></div>";

                        sHtml += "</div>\n" +
                                "<div class=\"timeline-entry-info-username\" onclick=\"Profile(" + kit.UserID + ");return false;\">" + display_name + "</div>\n" +
                                "<div class=\"timeline-entry-info-category-image " + sCssCategory + "\"></div>\n" +
                            "</div>\n" +
                            //"<div class=\"timeline-entry-info-user-comment\" itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\" onclick=\"Kit(" + kit.ID + ");\">\n";
                            "<div class=\"timeline-entry-info-user-comment\" itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\" onclick=\"Kit('" + url_friendly + "');\">\n";

                        sHtml += "<div class=\"timeline-entry-info-user-comment-text\">" + Globals.WordCut(kit.Comment, 200, new char[] { ' ' }) + "</div>\n";
                        sHtml += "<div class=\"timeline-entry-info-user-comment-img\"><img src=\"/Content/images/kit-view-valoration-" + kit.Valoration + ".png\" /></div>\n";

                        sHtml += "</div>\n" +

                            "<div class=\"timeline-entry-options\">\n" +
                                "<div class=\"timeline-entry-options-likes\"><span class=\"timeline-entry-options-number\">" + kit.NumLikes + "</span> Likes</div>\n" +
                                //"<div class=\"timeline-entry-options-comments\"  onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "#kit-view-container-comments';\"><span class=\"timeline-entry-options-number\">" + kit.NumComments + "</span> Comentarios</div>\n" +
                                "<div class=\"timeline-entry-options-comments\"  onclick=\"parent.location.href='" + url_friendly + "#kit-view-container-comments';\"><span class=\"timeline-entry-options-number\">" + kit.NumComments + "</span> Comentarios</div>\n" +
                                "<div class=\"timeline-entry-options-kits\"><span class=\"timeline-entry-options-number\">" + kit.NumKits + "</span> ReKits</div>\n" +
                            "</div>\n" +
                            "<div class=\"timeline-entry-footer\"></div>\n" +
                        "</div>\n";
                    }

                    sHtml += "</div>\n";
                }
                sHtml += "</div>\n";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }

        public void TwitterPublish(User user, String Message)
        {
            Globals.SaveLog(Database, String.Format("user =: {0}, Message := {1}", user.UserID, Message), Models.Type.Timeline, 1); //xivato 1

            Globals.SaveLog(Database, String.Format("TwitterAccesToken =: {0}", user.TwitterAccessToken), Models.Type.Timeline, 1); //xivato 2
            if (user.TwitterAccessToken == String.Empty)
                return;

            string[] aSplit = user.TwitterAccessToken.Split('|'); //¿?¿?¿Mirar porque cortan el "Twitter token por donde esta la barra(|)¿?¿?¿
            Globals.SaveLog(Database, String.Format("TwitterAccesToken =: {0}", aSplit), Models.Type.Timeline, 1); //xivato 3

            string TwitterConsumerKey = "q8P43ZQD4tu4Qi1DQznA";
            string TwitterConsumerSecret = "Z5qSrOu0NqUlDki6mvdiBkKf8KMomduaxfnu6Z4MvU";

            try
            {
                OAuthTokens tokens = new OAuthTokens();
                tokens.AccessToken = aSplit[0];
                Globals.SaveLog(Database, String.Format("Xiv__tokens.AccessToken:>>> {0}", aSplit[0]), Models.Type.Timeline, 1);
                tokens.AccessTokenSecret = aSplit[1];
                Globals.SaveLog(Database, String.Format("Xiv__tokens.AccessTokenSecret:>>> {0}", aSplit[1]), Models.Type.Timeline, 1);
                tokens.ConsumerKey = TwitterConsumerKey; //FET
                tokens.ConsumerSecret = TwitterConsumerSecret; //FET

                if (Message.Length > 140)
                    Message = Message.Substring(0, 140);
                Globals.SaveLog(Database, String.Format("Message =: {0}", Message), Models.Type.Timeline, 1); //xivato 4

                TwitterResponse<TwitterStatus> tweetResponse = TwitterStatus.Update(tokens, Message); //CREC QUE EL ERROR ESTÁ EN AQUESTA LÍNIA (s'ha de mirar que retorna "TwitterStatus.Update(tokens, Message)").

                //XIVATO>>>
                if (tweetResponse == null)
                {
                    Globals.SaveLog(Database, String.Format("tweetResponse ES NULL"), Models.Type.Timeline, 1); //xivato 4.1
                }
                else
                {
                    Globals.SaveLog(Database, String.Format("tweetResponse NO ES NULL"), Models.Type.Timeline, 1); //xivato 4.2
                    Globals.SaveLog(Database, String.Format("tweetResponse(valor) =: {0}", tweetResponse), Models.Type.Timeline, 1); //xivato 4.3
                }
                //<<<Xivato

                //Globals.SaveLog(Database, String.Format("TwitterStatus =: {0}", tweetResponse.PropertyName), Models.Type.Timeline, 1); //xivato 5
                Globals.SaveLog(Database, String.Format("tweetResponse.Result =: {0}", tweetResponse.Result), Models.Type.Timeline, 1); //xivato 6
                Globals.SaveLog(Database, String.Format("RequestResult.Success =: {0}", RequestResult.Success), Models.Type.Timeline, 1); //xivato 7
                if (tweetResponse.Result != RequestResult.Success)
                {
                    String response = tweetResponse.Result.ToString();
                    Globals.SaveLog(Database, String.Format("response =: {0}", response), Models.Type.Timeline, 1); //xivato 7
                    Globals.SaveLog(Database, String.Format("Twitter status update failed with Error: -->{0}<--  <<<.", tweetResponse.ErrorMessage), Models.Type.Timeline, 1); //xivato 7.1
                }
                else
                {
                    Globals.SaveLog(Database, String.Format("Twitter status successfully posted."), Models.Type.Timeline, 1); //xivato 8
                }
            }
            catch (Exception ex)
            {
                Globals.SaveLog(Database, String.Format("ERROR_ex:>>> {0}",ex), Models.Type.Timeline, 1); //xivato error
            }
        }

        public void FacebookPublish(User user, String Message,String Link, String Name, String Caption, String Picture, String Description)
        {
            if (user.FacebookAccessToken == String.Empty)
                return;

            Facebook.FacebookClient fbClient = new Facebook.FacebookClient(user.FacebookAccessToken);

            Dictionary<string, object> postMessage = new Dictionary<string, object>();
            postMessage["message"] = Message;
            postMessage["link"] = Link;
            postMessage["name"] = Name;
            postMessage["caption"] = Caption;
            postMessage["picture"] = Picture;
            postMessage["description"] = Description;

            fbClient.Post("me/feed", postMessage);
        }

        public void FacebookInvite(User user, String Message, String Link, String Name, String Caption, String Picture, String Description, String UserFacebookID)
        {
            if (user.FacebookAccessToken == String.Empty)
                return;

            Facebook.FacebookClient fbClient = new Facebook.FacebookClient(user.FacebookAccessToken);

            Dictionary<string, object> postMessage = new Dictionary<string, object>();
            postMessage["message"] = Message;
            postMessage["link"] = Link;
            postMessage["name"] = Name;
            postMessage["caption"] = Caption;
            postMessage["picture"] = Picture;
            postMessage["description"] = Description;

            String fbPostType = String.Format("{0}/feed", UserFacebookID);

            fbClient.Post(fbPostType, postMessage);
        }

        public String WSSearchProductsNowWithHtml(string search, String categories, int limit)
        {
            String sHtml = String.Empty;

            String sAssociateTag = "likekit08-21";
            String sAssociateTagUK = "likekit04-21";
            String sAWSAccessKeyID = "AKIAIBDCTHDECKPMLEFQ";
            String sAWSSecretAccessKey = "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";


            List<AmazonResponse> books = new List<AmazonResponse>();
            List<AmazonResponse> series = new List<AmazonResponse>();
            List<AmazonResponse> films = new List<AmazonResponse>();
            List<AmazonResponse> music = new List<AmazonResponse>();
            List<AmazonResponse> musicEs = new List<AmazonResponse>();

            string[] tokensizedStrsCategories;
            tokensizedStrsCategories = categories.Split(new char[] { ',' });

            int lenTags = tokensizedStrsCategories.Length;

            if ((categories == String.Empty) || (categories == ""))
            {
                books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
                series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
                films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
                music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
                musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);
            }
            else
            {
                for (int i = 0; i < lenTags; i++)
                {
                    string tmp = tokensizedStrsCategories[i];

                    if (tmp != "")
                    {
                        Int16 category = Int16.Parse(tmp);

                        switch (category)
                        {
                            case 1:
                                books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
                                break;
                            case 2:
                                series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
                                break;
                            case 3:
                                films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
                                break;
                            case 4:
                                music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
                                musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);
                                break;
                        }
                    }
                }
            }

            /*List<AmazonResponse> books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
            List<AmazonResponse> series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
            List<AmazonResponse> films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
            List<AmazonResponse> music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
            List<AmazonResponse> musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);*/

            //Products reorder
            List<AmazonResponse> products = new List<AmazonResponse>();
            List<AmazonResponse> products_hidden = new List<AmazonResponse>();

            int count_tmp = 0;
            int count_products_hidden = 0;
            for (int count_products = 0; (count_products < limit) && (count_tmp < limit); count_products++)
            {
                if (books.Count > count_products)
                {
                    products.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products.Add(musicEs[count_products]);
                    count_tmp++;
                }

                count_products_hidden = count_products;
            }

            //Omplim la segona pàgina
            for (int count_products = count_products_hidden; (count_products < (limit + limit)) && (count_tmp < (limit + limit)); count_products++)
            {
                if (books.Count > count_products)
                {
                    products_hidden.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products_hidden.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products_hidden.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products_hidden.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products_hidden.Add(musicEs[count_products]);
                    count_tmp++;
                }
            }

            int count = 0;

            foreach (AmazonResponse product in products)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                String sFormat = String.Empty;

                //Mirem la categoria, si és música afegim el format
                if ((product.Category == "Música") && (product.Observations != String.Empty))
                {
                    sFormat = String.Format(" ({0})", product.Observations);
                }

                if (product.Description != null)
                {
                    product.Description = Globals.ParseAmazonDescription(product.Description);
                }

                sHtml += "<div class=\"timeline-now-with-search-result\" onclick=\"NowWithAddWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "','" + product.Description + "','" + product.EAN + "','" + product.Observations + "');\">\n" +
                          "<div class=\"timeline-now-with-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + sFormat + "</div>\n" +
                          "<div class=\"timeline-now-with-search-result-category\">" + product.Category + "</div>\n" +
                          "</div>\n";
            }

            if (products_hidden.Count > 0)
            {
                sHtml += "<div class=\"timeline-kit-search-result\">\n" +
                            "<div id=\"timeline-kit-search-result-more-results\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MAS_RESULTADOS + "</div></div>\n" +
                            "</div>\n";
            }

            sHtml += "<div id=\"timeline-kit-search-result-more-results-container\">\n";

            foreach (AmazonResponse product in products_hidden)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                String sFormat = String.Empty;

                //Mirem la categoria, si és música afegim el format
                if ((product.Category == "Música") && (product.Observations != String.Empty))
                {
                    sFormat = String.Format(" ({0})", product.Observations);
                }

                if (product.Description != null)
                {
                    product.Description = Globals.ParseAmazonDescription(product.Description);
                }

                sHtml += "<div class=\"timeline-now-with-search-result\" onclick=\"NowWithAddWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "','" + product.Description + "','" + product.EAN + "','" + product.Observations + "');\">\n" +
                          "<div class=\"timeline-now-with-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + sFormat + "</div>\n" +
                          "<div class=\"timeline-now-with-search-result-category\">" + product.Category + "</div>\n" +
                          "</div>\n";
            }

            sHtml += "</div>\n";

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            {
                sHtml = "<div class=\"timeline-kit-search-result-noresults-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_SEARCH_NORESULT + "</div>";
                //"<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";
            }

            return sHtml;
        }

        public String WSSearchProductsWishListHtml(string search, String categories, int limit)
        {
            String sHtml = String.Empty;

            String sAssociateTag = "likekit08-21";
            String sAssociateTagUK = "likekit04-21";
            String sAWSAccessKeyID = "AKIAIBDCTHDECKPMLEFQ";
            String sAWSSecretAccessKey = "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";

            List<AmazonResponse> books = new List<AmazonResponse>();
            List<AmazonResponse> series = new List<AmazonResponse>();
            List<AmazonResponse> films = new List<AmazonResponse>();
            List<AmazonResponse> music = new List<AmazonResponse>();
            List<AmazonResponse> musicEs = new List<AmazonResponse>();

            string[] tokensizedStrsCategories;
            tokensizedStrsCategories = categories.Split(new char[] { ',' });

            int lenTags = tokensizedStrsCategories.Length;

            if ((categories == String.Empty) || (categories == ""))
            {
                books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
                series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
                films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
                music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
                musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);
            }
            else
            {
                for (int i = 0; i < lenTags; i++)
                {
                    string tmp = tokensizedStrsCategories[i];

                    if (tmp != "")
                    {
                        Int16 category = Int16.Parse(tmp);

                        switch (category)
                        {
                            case 1:
                                books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
                                break;
                            case 2:
                                series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
                                break;
                            case 3:
                                films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
                                break;
                            case 4:
                                music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
                                musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);
                                break;
                        }
                    }
                }
            }

            /*List<AmazonResponse> books = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Books, SearchCountry.ES);
            List<AmazonResponse> series = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Series, SearchCountry.ES);
            List<AmazonResponse> films = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTag, search, SearchType.Films, SearchCountry.ES);
            List<AmazonResponse> music = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.Music, SearchCountry.UK);
            List<AmazonResponse> musicEs = Amazon.SearchBooks(sAWSAccessKeyID, sAWSSecretAccessKey, sAssociateTagUK, search, SearchType.MusicEs, SearchCountry.ES);*/

            //Products reorder
            List<AmazonResponse> products = new List<AmazonResponse>();
            List<AmazonResponse> products_hidden = new List<AmazonResponse>();

            int count_tmp = 0;
            int count_products_hidden = 0;
            for (int count_products = 0; (count_products < limit) && (count_tmp < limit); count_products++)
            {
                if (books.Count > count_products)
                {
                    products.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products.Add(musicEs[count_products]);
                    count_tmp++;
                }

                count_products_hidden = count_products;
            }

            //Omplim la segona pàgina
            for (int count_products = count_products_hidden; (count_products < (limit + limit)) && (count_tmp < (limit + limit)); count_products++)
            {
                if (books.Count > count_products)
                {
                    products_hidden.Add(books[count_products]);
                    count_tmp++;
                }

                if (series.Count > count_products)
                {
                    products_hidden.Add(series[count_products]);
                    count_tmp++;
                }

                if (films.Count > count_products)
                {
                    products_hidden.Add(films[count_products]);
                    count_tmp++;
                }

                if (music.Count > count_products)
                {
                    products_hidden.Add(music[count_products]);
                    count_tmp++;
                }

                if (musicEs.Count > count_products)
                {
                    products_hidden.Add(musicEs[count_products]);
                    count_tmp++;
                }
            }

            int count = 0;

            foreach (AmazonResponse product in products)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                String sFormat = String.Empty;

                //Mirem la categoria, si és música afegim el format
                if ((product.Category == "Música") && (product.Observations != String.Empty))
                {
                    sFormat = String.Format(" ({0})", product.Observations);
                }

                if (product.Description != null)
                {
                    product.Description = Globals.ParseAmazonDescription(product.Description);
                }

                sHtml += "<div class=\"timeline-wishlist-search-result\" onclick=\"WishListAddWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "','" + product.Description + "','" + product.EAN + "','" + product.Observations + "');\">\n" +
                          "<div class=\"timeline-wishlist-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + sFormat + "</div>\n" +
                          "<div class=\"timeline-wishlist-search-result-category\">" + product.Category + "</div>\n" +
                          "</div>\n";
            }


            if (products_hidden.Count > 0)
            {
                sHtml += "<div class=\"timeline-kit-search-result\">\n" +
                            "<div id=\"timeline-kit-search-result-more-results\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MAS_RESULTADOS + "</div></div>\n" +
                            "</div>\n";
            }

            sHtml += "<div id=\"timeline-kit-search-result-more-results-container\">\n";

            foreach (AmazonResponse product in products_hidden)
            {
                count++;

                String Year = String.Empty;

                //Mirem el Year
                if ((product.Year != null) && (product.Year != "") && (product.Year != "null"))
                {
                    try
                    {
                        DateTime date = Convert.ToDateTime(product.Year);
                        Year = date.Year.ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                String ProductTitle = String.Empty;
                String ProductAuthor = String.Empty;

                if (product.Title != null)
                    ProductTitle = product.Title.Replace("'", "\\'");
                if (product.Author != null)
                    ProductAuthor = product.Author.Replace("'", "\\'");

                String sFormat = String.Empty;

                //Mirem la categoria, si és música afegim el format
                if ((product.Category == "Música") && (product.Observations != String.Empty))
                {
                    sFormat = String.Format(" ({0})", product.Observations);
                }

                if (product.Description != null)
                {
                    product.Description = Globals.ParseAmazonDescription(product.Description);
                }

                sHtml += "<div class=\"timeline-wishlist-search-result\" onclick=\"WishListAddWSProduct('" + product.ASIN + "','" + ProductTitle + "','" + ProductAuthor + "','" + Year + "','" + product.Category + "','" + product.LargeImage + "','" + product.URL + "','" + product.Description + "','" + product.EAN + "','" + product.Observations + "');\">\n" +
                          "<div class=\"timeline-wishlist-search-result-title\">" + product.Title + ". " + product.Author + " " + Year + sFormat + "</div>\n" +
                          "<div class=\"timeline-wishlist-search-result-category\">" + product.Category + "</div>\n" +
                          "</div>\n";
            }

            sHtml += "</div>\n";

            //Mostrem missatge de que no hem trobat res
            if (count == 0)
            {
                sHtml = "<div class=\"timeline-kit-search-result-noresults-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_SEARCH_NORESULT + "</div>";
                //"<div class=\"timeline-kit-search-result-noresults-text\">Tomamos nota de tu búsqueda para seguir mejorando Likekit. Puedes probar buscando de otra forma o animarte con nuevas recomendaciones.</div>";
            }

            return sHtml;
        }

        public String SearchPeopleHtml(string search, int limit, Int64 SessionUserID)
        {
            String sql = String.Format("select User.ID, User.FirstName, User.LastName, User.Picture, User.Address from User where (User.FirstName like '%{0}%' OR User.LastName like '%{0}%') Order by User.FirstName, User.LastName ASC LIMIT 0,{1}", Database.ParseStringToSQL(search), limit);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String sHtml = String.Empty;

            int count = 0;
            ArrayList objs = new ArrayList();

            try
            {
                JsonUsersLikeUser obj;

                while (reader.Read())
                {
                    count++;
                    obj = new JsonUsersLikeUser();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.UserLocation = Database.GetReaderValueString(reader, "Address");
                    obj.UserPicture = Database.GetReaderValueString(reader, "Picture");

                    if (obj.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, obj.UserPicture);

                        if (!File.Exists(sFile))
                            obj.UserPicture = "";
                    }

                    if (obj.UserPicture == "")
                        obj.UserPicture = "/Content/images/default_user.png";
                    else
                        obj.UserPicture = "/Content/files/" + obj.UserPicture;

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {

                int count_results = 0;
                bool odd = false;

                foreach (JsonUsersLikeUser user in objs)
                {
                    String sCss = "";
                    String sCssType = "search-people-result-entry-odd";
                    
                    if (count_results == 0)
                        sCss = "search-people-result-border-top";

                    if (odd == false)
                    {
                        sCssType = "";
                        odd = true;
                    }
                    else
                    {
                        sCssType = "search-people-result-entry-odd";
                        odd = false;
                    }
                    
                    String sDisplayName = String.Format("{0} {1}", user.FirstName, user.LastName);

                    sHtml += "<div class=\"search-people-result-entry " + sCss + sCssType + "\">" +
                        "<div class=\"search-people-result-entry-image\" onclick=\"parent.location.href='/Timeline/Profile/" + user.ID + "';\">" +
                        "<img src=\"" + user.UserPicture + "\" width=\"28px\" alt=\"" + sDisplayName + "\" title=\"" + sDisplayName + "\" />\n" +
                        "</div>" +
                        "<div class=\"search-people-result-entry-data\">" +
                        "<div class=\"search-people-result-entry-name\">" + sDisplayName + "</div>" +
                        "<div class=\"search-people-result-entry-location\">" + user.UserLocation + "</div>" +
                        "</div>";

                        if (SessionUserID != user.ID)
                        {
                            UserRelationsRepository urRP = new UserRelationsRepository(Database);

                            //Hem de comprovar si el seguim o no
                            if (urRP.isFollower(SessionUserID, user.ID))
                                sHtml += "<div id=\"search-people-user-btn-" + user.ID + "\" class=\"search-people-result-entry-following search-people-result-entry-btn-on\" onclick=\"SearchPeopleUnFollow(" + user.ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SIGUIENDO_MIN + "</div>\n";
                            else
                                sHtml += "<div id=\"search-people-user-btn-" + user.ID + "\" class=\"search-people-result-entry-follow search-people-result-entry-btn\" onclick=\"SearchPeopleFollow(" + user.ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SEGUIR + "</div>\n";
                        }

                    sHtml += "</div>";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }

        public void Invite(String mail, Int64 UserID)
        {
            User user = GetUser(UserID);

            //Carreguem les dades del template
            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);

            short _lang = 1;

            if (user.Language != null)
                _lang = (short)user.Language.ID;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.LikeKitInvitation, _lang);

            String sDisplayName = String.Format("{0} {1}", user.FirstName, user.LastName);
            mail_template.Body = mail_template.Body.Replace("[FRIEND_USERNAME]", sDisplayName);

            String user_profile_url = String.Format("{0}/perfilp/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], user.FirstName, user.ID);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            user_profile_url = user_profile_url.Replace("//", "/");
            user_profile_url = user_profile_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[USER_PROFILE_URL]", user_profile_url);


            String register_image = String.Format("{0}/logo-small.png", ConfigurationManager.AppSettings["URLImages"]);
            
            //Evitem el problema de duplicar les barres, ja que això genera problemes
            register_image = register_image.Replace("//", "/");
            register_image = register_image.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[REGISTER_IMAGE_URL]", register_image);

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);
            String url_register = String.Format("{0}/Account/Register", ConfigurationManager.AppSettings["MainURL"]);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            url_register = url_register.Replace("//", "/");
            url_register = url_register.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[REGISTER_URL]", url_register);

            bool mail_sent = true;
            //Enviem un correu
            try
            {
                Globals.SendMail(Database, mail_template.Subject, mail_template.Body, mail);
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Database, ex_msg, Models.Type.Register, 1);
                mail_sent = false;
            }
        }


        public void LikeMail(Int64 ToUserID, Int64 FromUserID, Int64 KitID)
        {
            User toUser = GetUser(ToUserID);
            User fromUser = GetUser(FromUserID);

            //Carreguem les dades del template
            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);

            short _lang = 1;

            if (toUser.Language != null)
                _lang = (short)toUser.Language.ID;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.UserLike, _lang);

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

            String sDisplayName = String.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);
            mail_template.Body = mail_template.Body.Replace("[LIKE_USERNAME]", sDisplayName);
            
            String user_profile_url = String.Format("{0}/Timeline/Profile/{1}", ConfigurationManager.AppSettings["MainURL"], fromUser.ID);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            user_profile_url = user_profile_url.Replace("//", "/");
            user_profile_url = user_profile_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[USER_PROFILE_URL]", user_profile_url);

            String sProductName = String.Empty;

            Kit kit = GetKit(KitID);

            Product product = GetProduct(kit.Product.ID);

            sProductName = product.Name;

            mail_template.Body = mail_template.Body.Replace("[PRODUCT_NAME]", sProductName);

            String kit_url = String.Format("{0}kit/timeline/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(sProductName), kit.ID);

            //String kit_url = String.Format("{0}/Home/Item/{1}", ConfigurationManager.AppSettings["MainURL"], KitID);
            //String kit_url = String.Format("{0}{1}", ConfigurationManager.AppSettings["MainURL"], url_friendly);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            kit_url = kit_url.Replace("//", "/");
            kit_url = kit_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[KIT_URL]", kit_url);

            //Enviem un correu
            try
            {
                Globals.SendMail(Database, mail_template.Subject, mail_template.Body, toUser.UserName);
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Database, ex_msg, Models.Type.Like, 1);
            }
        }

        public void KitCommentMail(Int64 ToUserID, Int64 FromUserID, Int64 KitID)
        {
            User toUser = GetUser(ToUserID);
            User fromUser = GetUser(FromUserID);

            //Carreguem les dades del template
            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);

            short _lang = 1;

            if (toUser.Language != null)
                _lang = (short)toUser.Language.ID;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.KitComment, _lang);

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

            String sDisplayName = String.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);
            mail_template.Body = mail_template.Body.Replace("[COMENT_USERNAME]", sDisplayName);
            
            String user_profile_url = String.Format("{0}/Timeline/Profile/{1}", ConfigurationManager.AppSettings["MainURL"], fromUser.ID);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            user_profile_url = user_profile_url.Replace("//", "/");
            user_profile_url = user_profile_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[USER_PROFILE_URL]", user_profile_url);

            String sProductName = String.Empty;

            Kit kit = GetKit(KitID);

            Product product = GetProduct(kit.Product.ID);

            sProductName = product.Name;

            mail_template.Body = mail_template.Body.Replace("[PRODUCT_NAME]", sProductName);

            //String kit_url = String.Format("{0}/Timeline/Item/{1}", ConfigurationManager.AppSettings["MainURL"], KitID);
            String kit_url = String.Format("{0}/kit/timeline/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(sProductName), KitID);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            kit_url = kit_url.Replace("//", "/");
            kit_url = kit_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[KIT_URL]", kit_url);

            //Enviem un correu
            try
            {
                Globals.SendMail(Database, mail_template.Subject, mail_template.Body, toUser.UserName);
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Database, ex_msg, Models.Type.Like, 1);
            }
        }

        public void FollowMail(Int64 ToUserID, Int64 FromUserID)
        {
            User toUser = GetUser(ToUserID);
            User fromUser = GetUser(FromUserID);

            //Carreguem les dades del template
            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);

            short _lang = 1;

            if (toUser.Language != null)
                _lang = (short)toUser.Language.ID;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.UserFollowing, _lang);

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

            String sDisplayName = String.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);
            mail_template.Body = mail_template.Body.Replace("[FOLLOWER_USERNAME]", sDisplayName);

            String user_profile_url = String.Format("{0}/Timeline/Profile/{1}", ConfigurationManager.AppSettings["MainURL"], fromUser.ID);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            user_profile_url = user_profile_url.Replace("//", "/");
            user_profile_url = user_profile_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[USER_PROFILE_URL]", user_profile_url);

            //Enviem un correu
            try
            {
                Globals.SendMail(Database, mail_template.Subject, mail_template.Body, toUser.UserName);
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Database, ex_msg, Models.Type.Like, 1);
            }
        }

        public void ReKitMail(Int64 ToUserID, Int64 FromUserID, Int64 KitID)
        {
            User toUser = GetUser(ToUserID);
            User fromUser = GetUser(FromUserID);

            //Carreguem les dades del template
            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);

            short _lang = 1;

            if (toUser.Language != null)
                _lang = (short)toUser.Language.ID;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.UserReKit, _lang);

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

            String sDisplayName = String.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);
            mail_template.Body = mail_template.Body.Replace("[REKIT_USERNAME]", sDisplayName);

            String user_profile_url = String.Format("{0}/Timeline/Profile/{1}", ConfigurationManager.AppSettings["MainURL"], fromUser.ID);

            String sProductName = String.Empty;

            Kit kit = GetKit(KitID);

            Product product = GetProduct(kit.Product.ID);

            sProductName = product.Name;

            mail_template.Body = mail_template.Body.Replace("[PRODUCT_NAME]", sProductName);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            user_profile_url = user_profile_url.Replace("//", "/");
            user_profile_url = user_profile_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[USER_PROFILE_URL]", user_profile_url);

            String kit_url = String.Format("{0}kit/timeline/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(sProductName), kit.ID);

            //String kit_url = String.Format("{0}/Home/Item/{1}", ConfigurationManager.AppSettings["MainURL"], KitID);
            //String kit_url = String.Format("{0}{1}", ConfigurationManager.AppSettings["MainURL"], url_friendly);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            kit_url = kit_url.Replace("//", "/");
            kit_url = kit_url.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[REKIT_URL]", kit_url);

            //Enviem un correu
            try
            {
                Globals.SendMail(Database, mail_template.Subject, mail_template.Body, toUser.UserName);
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Database, ex_msg, Models.Type.Like, 1);
            }
        }

        public String getAdvancedFilterRecommendedTagsHtml()
        {
            String sHtml = String.Empty;

            String sql = String.Format("SELECT kt.name, count(*) as count FROM KitTag kt Group by kt.Name order by count desc limit 6");

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                int count = 0;
                while (reader.Read())
                {
                    count++;
                    String Name = Database.GetReaderValueString(reader, "Name");

                    Name = Name.ToLower();
                    //Posem la primera lletra a majúscula
                    Name = char.ToUpper(Name[0]) + Name.Substring(1); 

                    sHtml += "<div class=\"timeline-advanced-filter-column-fitler-option\" id=\"timeline-advanced-filter-recommeded-tag-" + count + "\" onclick=\"AdvFilterRecommendedTag(" + count + ");\">" +
                        "<div class=\"timeline-advanced-filter-column-fitler-option-round\"></div>" +
                        "<div class=\"timeline-advanced-filter-column-fitler-option-text\">" + Name + "</div>" +
                    "</div>";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return sHtml;
        }


        public void CommentedCommentsNotifies(Int64 KitID, Int64 UserID)
        {
            String sql = String.Format("SELECT Distinct(UserID) FROM KitComment WHERE KitID= {0}", KitID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList List = new ArrayList();

            try
            {
                int i = 0;

                while (reader.Read())
                {
                    i++;
                    Int64 UserCommentID = Database.GetReaderValueInt64(reader, "UserID");
                    List.Add(UserCommentID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            foreach (Int64 user_id in List)
            {
                //Creem la notificació
                if (user_id != UserID)
                {
                    CreateNotify(user_id, UserID, KitID, NotifyType.CommentedComment);
                }
            }
        }

        public String GetTimelineCounter(String date, Int64 SessionUserID, String search = "", String filter = "", String tags = "", Int16 Limit = 15, Int16 Start = 0)
        {
            String sHtml = String.Empty;

            String sql = "select distinct(kt.ID), kt.UserID, kt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, usr.Gender, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, " +
                " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes ," +
                " EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(),usr.Birthday))))+0 AS Age " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID ";

            bool do_filter = true;


            if ((search != "") || (tags != ""))
                sql += " left join KitTag ktag on kt.ID = ktag.KitID ";

            sql += " where 1=1 ";

            if (search != "")
            {
                sql += String.Format(" AND (pr.Name like '%{0}%' OR pr.Author like '%{0}%' OR ktag.Name like '%{0}%')", search);
                do_filter = false;
            }

            //Mirem els tags
            string[] tokensizedStrsTags;
            tokensizedStrsTags = tags.Split(new char[] { ',' });

            int lenTags = tokensizedStrsTags.Length;

            String sql_tags = String.Empty;

            for (int i = 0; i < lenTags; i++)
            {
                string tmp = tokensizedStrsTags[i];

                if (tmp != "")
                {
                    if (sql_tags != "")
                        sql_tags += " OR ";

                    sql_tags += String.Format(" (ktag.Name like '%{0}%')", tmp.Trim());
                }
            }

            if (sql_tags != "")
                sql += " AND (" + sql_tags + ") ";

            DateTime date_sql = DateTime.Now;

            if (date == String.Empty)
                date_sql = DateTime.Now;
            else
                date_sql = DateTime.Parse(date);

            sql += String.Format(" AND (kt.Created > '{0}')", Database.ParseDateTimeToSQL(date_sql));

            if (filter == null)
                filter = "";

            String sql_users = String.Empty;
            String sql_categories = String.Empty;
            String sql_relation = String.Empty;
            String sql_age = String.Empty;
            String sql_genere = String.Empty;
            String sql_order_by = String.Empty;

            string[] tokensizedStrs;
            tokensizedStrs = filter.Split(new char[] { ',' });
            int len = tokensizedStrs.Length;

            bool todos = false;
            bool trenders = false;

            String sql_age_function = " EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(),usr.Birthday))))+0 ";

            //De moment no treiem el filtre si fem una cerca textual
            if (do_filter)
            {
                for (int i = 0; i < len; i++)
                {
                    string tmp = tokensizedStrs[i];

                    if (tmp == "true")
                    {
                        switch (i)
                        {
                            case 0: //TRENDERS
                                sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(ID) FROM User where UserType=1)) ");
                                trenders = true;
                                break;
                            case 1: //FOLLOWINGS
                                if (sql_users != "")
                                    sql_users += " OR ";
                                sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(FollowID) FROM UserRelation where UserID = {0})) ", SessionUserID);
                                break;
                            case 2: //TODOS
                                sql_users = String.Empty;
                                todos = true;
                                break;
                            case 3: //LIBROS
                                sql_categories += String.Format("(pr.ProductCategoryID = {0})", 1);
                                break;
                            case 4: //CINE
                                if (sql_categories != "")
                                    sql_categories += " OR ";
                                sql_categories += String.Format(" (pr.ProductCategoryID = {0})", 3);
                                break;
                            case 5: //SERIES
                                if (sql_categories != "")
                                    sql_categories += " OR ";
                                sql_categories += String.Format(" (pr.ProductCategoryID = {0})", 2);
                                break;
                            case 6: //MÚSICA
                                if (sql_categories != "")
                                    sql_categories += " OR ";
                                sql_categories += String.Format(" (pr.ProductCategoryID = {0})", 4);
                                break;
                            case 7: //TODOS
                                sql_categories = String.Empty;
                                break;
                            case 8: //MÁS COMENTADO
                                /*sql_relation += String.Format(" AND num_comments > 0 ");*/
                                sql_order_by = String.Format(" order by num_comments DESC");
                                break;
                            case 9: //MÁS RECOMENDADO
                                //sql_relation += String.Format(" AND kt.UserID IN ()like '%{0}%'", search);
                                sql_order_by = String.Format(" order by num_kits DESC");
                                break;
                            case 10: //TODOS
                                sql_relation = String.Empty;
                                break;
                            case 11: //16-25
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=16) AND ({0} <=25))", sql_age_function);
                                break;
                            case 12: //26-35
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=26) AND ({0} <=35))", sql_age_function);
                                break;
                            case 13: //36-45
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=36) AND ({0} <=45))", sql_age_function);
                                break;
                            case 14: //46-55
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=46) AND ({0} <=55))", sql_age_function);
                                break;
                            case 15: //56-65
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" (({0} >=56) AND ({0} <=65))", sql_age_function);
                                break;
                            case 16: //>65
                                if (sql_age != "")
                                    sql_age += " OR ";
                                sql_age += String.Format(" ({0} > 65)", sql_age_function);
                                break;

                            case 17: //>Man
                                if (sql_genere != "")
                                    sql_genere += " OR ";
                                sql_genere += String.Format(" (usr.Gender = 1)");
                                break;
                            case 18: //>Woman
                                if (sql_genere != "")
                                    sql_genere += " OR ";
                                sql_genere += String.Format(" (usr.Gender = 2)");
                                break;
                        }

                    }
                }

                if (sql_users != "")
                {
                    if (!trenders)
                        sql_users = " AND (" + sql_users + " OR kt.UserID = " + SessionUserID + ") ";
                    else
                        sql_users = " AND (" + sql_users + ") ";
                }
                else if (!todos)
                {
                    sql_users = " AND (";
                    sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(UserID) FROM ProductCategoryTrender)) ");
                    sql_users += " OR ";
                    sql_users += String.Format(" (kt.UserID IN (SELECT DISTINCT(FollowID) FROM UserRelation where UserID = {0})) ", SessionUserID);
                    sql_users += " OR kt.UserID = " + SessionUserID + ") ";
                }

                if (sql_categories != "")
                    sql_categories = " AND (" + sql_categories + ") ";

                if (sql_order_by == "")
                    sql_order_by = String.Format(" order by kt.Modified DESC");

                if (sql_age != "")
                    sql_age = " AND (" + sql_age + ") ";

                if (sql_genere != "")
                    sql_genere = " AND (" + sql_genere + ") ";
            }

            sql += String.Format("{0} {1} {2} {3} {4} {5}", sql_users, sql_categories, sql_relation, sql_age, sql_genere, sql_order_by);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            int num_kits = 0;

            try
            {
                while (reader.Read())
                {
                    num_kits++;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return num_kits.ToString();
        }


        public String WidgetLastKits(Int64 WidgetUserID)
        {
            String sHtml = String.Empty;

            String sql = "select distinct(kt.ID), kt.UserID, kt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, usr.Gender, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, " +
                " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID ";

            sql += " where 1=1 AND kt.UserID=" + WidgetUserID + " LIMIT 4";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList objs = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    JsonKits obj = new JsonKits();

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    String UserName = Database.GetReaderValueString(reader, "UserName");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    String ProductName = Database.GetReaderValueString(reader, "ProductName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                    Int16 NumKits = Database.GetReaderValueInt16(reader, "num_kits");
                    Int16 NumComments = Database.GetReaderValueInt16(reader, "num_comments");
                    Int16 NumLikes = Database.GetReaderValueInt16(reader, "num_likes");
                    Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
                    Int16 UserType = Database.GetReaderValueInt16(reader, "UserType");

                    if (UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, UserPicture);

                        if (!File.Exists(sFile))
                            UserPicture = "";
                    }

                    if (UserPicture == "")
                        UserPicture = "/Content/images/default_user.png";
                    else
                        UserPicture = "/Content/files/" + UserPicture;

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = "/Content/images/default_product.png";
                    else
                        ProductImage = "/Content/files/" + ProductImage;

                    obj.ID = ID;
                    obj.UserID = UserID;
                    obj.ProductID = ProductID;
                    obj.UserName = UserName;
                    obj.FirstName = FirstName;
                    obj.LastName = LastName;
                    obj.UserPicture = UserPicture;
                    obj.ProductName = ProductName;
                    obj.ProductImage = ProductImage;
                    obj.NumKits = NumKits;
                    obj.NumComments = NumComments;
                    obj.NumLikes = NumLikes;
                    obj.ProductCategoryID = ProductCategoryID;
                    obj.iUserType = UserType;

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {
                foreach (JsonKits kit in objs)
                {
                    sHtml += "<div class=\"widget-kit-entry\">\n" +
                        "<div class=\"widget-kit-entry-image\">\n" +
                            "<img src=\"" + kit.ProductImage + "\" width=\"141px\" alt=\"" + kit.ProductName + "\" title=\"" + kit.ProductName + "\" />\n" +
                            "<div class=\"widget-kit-entry-title\">" + Globals.WordCut(kit.ProductName, 20, new char[] { ' ' }) + "</div>\n" +

                            "<div class=\"widget-kit-entry-options\">\n" +
                                "<div class=\"widget-kit-entry-options-likes\"><span class=\"widget-kit-entry-options-number\">" + kit.NumLikes + "</span></div>\n" +
                                "<div class=\"widget-kit-entry-options-comments\"><span class=\"widget-kit-entry-options-number\">" + kit.NumComments + "</span></div>\n" +
                                "<div class=\"widget-kit-entry-options-kits\"><span class=\"widget-kit-entry-options-number\">" + kit.NumKits + "</span></div>\n" +
                            "</div>\n" +
                        "</div>\n";

                    sHtml += "</div>\n";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }

        public String GetIPCulture(String address)
        {
            int A = 0;
            int B = 0;
            int C = 0;
            int D = 0;

            string[] temp = address.Split('.');

            A = int.Parse(temp[0]);
            B = int.Parse(temp[1]);
            C = int.Parse(temp[2]);
            D = int.Parse(temp[3]);

            Int32 iAddress = Convert.ToInt32((A * Math.Pow(256, 3)) + (B * Math.Pow(256, 2)) + (C * 256) + D);

            String sql = String.Format("SELECT region FROM GeoIPBlocks blocks LEFT JOIN GeoIPCities cities on  cities.locId = blocks.locId where {0} between blocks.startIpNum and blocks.endIpNum limit 1", iAddress); 

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String culture = String.Empty;

            try
            {
                int i = 0;

                while (reader.Read())
                {
                    culture = Database.GetReaderValueString(reader, "region");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            if (culture == "56")
                return "ca";
            else
                return "es";

        }

        public String GenerateSitemap(Int64 type)
        {
            String sXml = String.Empty;

            /*sXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " +
                        "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " +
                        "xmlns:video=\"http://www.google.com/schemas/sitemap-video/1.1\">\n";*/

            sXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
            

            if (type == 1)
            {
                sXml += "<url>\n" +
                    "<loc>" + ConfigurationManager.AppSettings["MainURL"] + "</loc>\n" +
                  "</url>\n" +
                   "<url>\n" +
                    "<loc>" + ConfigurationManager.AppSettings["MainURL"] + "kits</loc>\n" +
                  "</url>\n" +
                   "<url>\n" +
                    "<loc>" + ConfigurationManager.AppSettings["MainURL"] + "ayuda</loc>\n" +
                  "</url>\n" +
                  "<url>\n" +
                    "<loc>" + ConfigurationManager.AppSettings["MainURL"] + "prensa</loc>\n" +
                  "</url>\n" +
                  "<url>\n" +
                    "<loc>" + ConfigurationManager.AppSettings["MainURL"] + "legal</loc>\n" +
                  "</url>\n";
                
            }
            else if (type == 2)
            { 
                String sql = "select distinct(kt.ID), kt.UserID, kt.ProductID, kt.Comment, kt.Valoration, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, usr.UserType as UserType, usr.Gender, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID " +
                    " from Kit kt left join User usr on kt.UserID = usr.ID " +
                    " left join Product pr on kt.ProductID = pr.ID ";

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();
            
                try
                {
                    while (reader.Read())
                    {
                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                        Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                        String UserName = Database.GetReaderValueString(reader, "UserName");
                        String FirstName = Database.GetReaderValueString(reader, "FirstName");
                        String LastName = Database.GetReaderValueString(reader, "LastName");
                        String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                        String ProductName = Database.GetReaderValueString(reader, "ProductName");
                        String ProductImage = Database.GetReaderValueString(reader, "ProductImage");

                        sXml += String.Format("<url>\n" +
                                    "<loc>{0}kit/{1}/{2}</loc>\n" +
                                "</url>\n", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(ProductName), ID);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();
                }
            }
            else if (type == 3)
            {
                String sql = String.Format("select User.ID, User.UserName, " +
                " User.FirstName as FirstName, User.LastName as LastName, User.Picture as Picture, User.About as About, User.Web as Web," +
                " User.UserType as UserType from User");

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        String UserName = Database.GetReaderValueString(reader, "UserName");
                        String FirstName = Database.GetReaderValueString(reader, "FirstName");
                        String LastName = Database.GetReaderValueString(reader, "LastName");
                        String UserPicture = Database.GetReaderValueString(reader, "Picture");

                        sXml += String.Format("<url>\n" +
                                    "<loc>{0}perfilp/{1}/{2}</loc>\n" +
                                "</url>\n", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(FirstName + " " + LastName), ID);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();
                }            
            }

            sXml += "</urlset>";

            return sXml;
        }



        public String GetHomeProfileCategoryKits(Int64 UserID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;

            sHtml += GetHomeProfileCategoryKitsByCategory(UserID, 1, SessionUserID);
            sHtml += GetHomeProfileCategoryKitsByCategory(UserID, 3, SessionUserID);
            sHtml += GetHomeProfileCategoryKitsByCategory(UserID, 2, SessionUserID);
            sHtml += GetHomeProfileCategoryKitsByCategory(UserID, 4, SessionUserID);
            return sHtml;
        }

        public String GetHomeProfileCategoryKitsByCategory(Int64 UserID, Int64 CategoryID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;

            String sql = "select kt.ID, kt.UserID, kt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, pr.Name as ProductName,pr.Image as ProductImage " +
                " from Kit kt left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID " +
                " where 1=1 AND kt.UserID = " + UserID + " AND pr.ProductCategoryID = " + CategoryID + " order by kt.Valoration Desc, kt.Created Desc";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String css_id = String.Empty;
            String category_title = String.Empty;
            String margin_left = String.Empty;

            switch (CategoryID)
            {
                case 1:
                    css_id = "books";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIBROS;
                    break;
                case 2:
                    css_id = "series";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SERIES;
                    margin_left = " left22";
                    break;
                case 3:
                    css_id = "cinema";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CINE;
                    margin_left = " left22";
                    break;
                case 4:
                    css_id = "music";
                    category_title = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUSICA;
                    break;
            }

            sHtml += "<div id=\"profile-kits-" + css_id + "\" class=\"profile-kits-section" + margin_left + "\">" +
            "<div class=\"profile-kits-section-images\">";

            Int16 count = 0;

            try
            {
                while (reader.Read())
                {
                    count++;

                    if (count < 7)
                    {
                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                        String ProductName = Database.GetReaderValueString(reader, "ProductName");

                        if (ProductImage != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, ProductImage);

                            if (!File.Exists(sFile))
                                ProductImage = "";
                        }

                        if (ProductImage == "")
                            ProductImage = "/Content/images/default_product.png";
                        else
                            ProductImage = "/Content/files/" + ProductImage;

                        String sCss = "";

                        if ((count == 1) || (count == 3))
                            sCss = "profile-kits-section-entry-right-bottom";
                        else if ((count == 2) || (count == 4))
                            sCss = "profile-kits-section-entry-left-bottom";
                        else if (count == 5)
                            sCss = "profile-kits-section-entry-right";
                        else if (count == 6)
                            sCss = "profile-kits-section-entry-left";

                        String url_friendly = String.Format("{0}kit/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(ProductName), ID);

                        //sHtml += "<div class=\"profile-kits-section-entry " + sCss + "\" onclick=\"Kit(" + ID + ");\"><img src=\"" + ProductImage + "\" width=\"110px\"/></div>";
                        sHtml += "<div class=\"profile-kits-section-entry " + sCss + "\"><a href=\"" + url_friendly + "\"><img src=\"" + ProductImage + "\" width=\"110px\"/></a></div>";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            int add_categories = 6 - count;
            int position = 0;

            if (count < 6)
            {
                for (int j = 0; j < add_categories; j++)
                {
                    position = count + j + 1;

                    String sCss = String.Empty;

                    if ((position == 1) || (position == 3))
                        sCss = "profile-kits-section-entry-right-bottom";
                    else if ((position == 2) || (position == 4))
                        sCss = "profile-kits-section-entry-left-bottom";
                    else if (position == 5)
                        sCss = "profile-kits-section-entry-right";
                    else if (position == 6)
                        sCss = "profile-kits-section-entry-left";

                    if (UserID != SessionUserID)
                    {
                        sHtml += "<div class=\"profile-kits-section-entry-empty-no-add " + sCss + "\"></div>";
                    }
                    else
                    {
                        sHtml += "<div class=\"profile-kits-section-entry-empty " + sCss + "\"></div>";
                    }


                }
            }

            sHtml += "</div>" +
                "<div class=\"profile-kits-section-footer\" onclick=\"mostrarEmergent();\">" +
                    category_title + " <span class=\"kits-number\"> " + count + "</span> <span class=\"kits-number-view\">(" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_VER_TODOS + ")</span>" +
                "</div>" +
            "</div>";

            /*if (count > 0)
                return sHtml;
            else
                return String.Empty;*/
            return sHtml;
        }


        public String GetHomeUsersLikeUserHtml(Int64 UserID, Int64 SessionUserID)
        {

            //String sql = String.Format("select count(ktusrs.ID) as count, us.ID, us.UserID, us.FirstName, us.LastName, us.Picture, us.UserType from Kit ktusrs left join User us on ktusrs.UserID = us.ID where ktusrs.ProductID IN(select ktu.ProductID from Kit ktu where ktu.UserID = {0}) AND ktusrs.UserID <> {0} GROUP BY ktusrs.UserID order by count DESC LIMIT 3", UserID);
            String sql = String.Format("select count(kitothers.ID) as count, us.ID, us.UserID, us.FirstName, us.LastName, us.Picture, us.UserType from  Kit ktusrs, Kit kitothers left join User us on kitothers.UserID = us.ID where ktusrs.ProductID = kitothers.ProductID AND ktusrs.Valoration = kitothers.Valoration AND ktusrs.UserID = {0} AND kitothers.UserID <> {0} GROUP BY kitothers.UserID order by count DESC LIMIT 3;", UserID);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            String sHtml = String.Empty;

            int count = 0;

            ArrayList objs = new ArrayList();

            try
            {
                JsonUsersLikeUser obj;

                while (reader.Read())
                {
                    obj = new JsonUsersLikeUser();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.UserID = obj.ID;//Database.GetReaderValueInt64(reader, "UserID");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "Picture");

                    if (obj.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, obj.UserPicture);

                        if (!File.Exists(sFile))
                            obj.UserPicture = "";
                    }

                    if (obj.UserPicture == "")
                        obj.UserPicture = "/Content/images/default_user.png";
                    else
                        obj.UserPicture = "/Content/files/" + obj.UserPicture;


                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {


                foreach (JsonUsersLikeUser user in objs)
                {
                    count++;

                    String display_name = String.Format("{0} {1}", user.FirstName, user.LastName);
                    String sCss = String.Empty;

                    if (count > 1)
                        sCss = "left22";

                    ArrayList images = GetFirstKitUserImages(user.ID, 3);

                    String image_1 = String.Empty;
                    String image_2 = String.Empty;
                    String image_3 = String.Empty;

                    int count_images = 0;

                    foreach (String image in images)
                    {
                        count_images++;

                        switch (count_images)
                        {
                            case 1:
                                image_1 = image;
                                break;
                            case 2:
                                image_2 = image;
                                break;
                            case 3:
                                image_3 = image;
                                break;
                        }
                    }

                    if (image_1 == String.Empty)
                        image_1 = "/Content/images/default_user.png";

                    if (image_2 == String.Empty)
                        image_2 = "/Content/images/default_user.png";

                    if (image_3 == String.Empty)
                        image_3 = "/Content/images/default_user.png";


                    String user_url_friendly = String.Format("{0}perfilp/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(display_name), user.ID);

                    sHtml += "<div class=\"profile-users-section " + sCss + "\">" +
                            "<div class=\"profile-users-images\">" +
                            "<div class=\"profile-users-bigentry\"><img src=\"" + image_1 + "\" width=\"220px\"/></div>" +
                            "<div class=\"profile-users-entry\"><img src=\"" + image_2 + "\" width=\"110px\"/></div>" +
                            "<div class=\"profile-users-entry\"><img src=\"" + image_3 + "\" width=\"110px\"/></div>" +
                        "</div>" +
                        "<div class=\"profile-users-footer\">" +
                            "<div class=\"profile-users-image\"><a href=\"" + user_url_friendly + "\"><img src=\"" + user.UserPicture + "\" width=\"28px\"/></a>";

                    if (user.iUserType == 1)
                        sHtml += "<div class=\"user-trender-small\"><img src=\"/Content/images/trender.png\" width=\"28\" height=\"28\" /></div>";


                    sHtml += "</div>" +
                           "<div class=\"profile-users-name\"><a href=\"" + user_url_friendly + "\">" + display_name + "</a></div>" +
                       "</div>" +
                   "</div>";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }

        public String GetTitolPaginaComodi(Int16 Type)
        {
            var titol = "";

            switch (Type)
            {
                //Trenders
                case 5:
                    titol = "Trenders";
                break;
                //Cartellera
                case 6:
                titol = @LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CARTELERA_MIN;
                break;
                default:
                    titol = "";
                break;
            }

            return titol;
        }

        public String GetDescripcioComodi(Int16 Type)
        {
            var descripcio = "";

            switch (Type)
            {
                //Trenders
                case 5:
                    descripcio = @LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_DESCRIPCION_TRENDERS; //Descripció traduïdes.
                    break;
                //Cartellera
                case 6:
                    descripcio = @LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_DESCRIPCION_CARTELERA; //Descripció traduïdes.
                    break;
                default:
                    descripcio = "";
                    break;
            }

            return descripcio;
        }
    }
}