﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class HelpsRepository : BaseRepository
    {
        public HelpsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Help GetHelp(IDataReader reader)
        {
            if (reader == null)
                return null;

            Help help = new Help
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Title = Database.GetReaderValueString(reader, "Title"),
                Content = Database.GetReaderValueString(reader, "Content"),
                Order = (short)Database.GetReaderValueNumeric(reader, "Order"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return help;
        }

        public void CreateHelp(Help Help)
        {
            String SQL = String.Format("INSERT INTO Help (Title, Content, Order, Created, Modified) " +
                "values ('{0}','{1}',{2},'{3}','{4}')",
                Database.ParseStringToSQL(Help.Title),
                Database.ParseStringToSQL(Help.Content),
                Help.Order,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateHelp(Help Help)
        {
            String SQL = String.Format("UPDATE Help SET Title='{0}'," +
                                       "Content='{1}',Order={2},Created='{3}',Modified='{4}' " +
                                       " WHERE ID={5}",
                                       Database.ParseStringToSQL(Help.Title),
                                       Database.ParseStringToSQL(Help.Content),
                                       Help.Order,
                                       Database.ParseDateTimeToSQL(Help.Created),
                                       Database.ParseDateTimeToSQL(System.DateTime.Now),
                                       Help.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteHelp(Help Help)
        {
            String SQL = String.Format("DELETE FROM Help " +
                                       " WHERE ID={0}",
                                        Help.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}