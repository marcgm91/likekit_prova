﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Configuration;
using System.IO;

namespace LikeKitWebsite.Repository
{
    public class WishListsRepository : BaseRepository
    {
        public WishListsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private WishList GetWishList(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
            Product _product = null;

            if (ProductID > 0)
            {
                _product = new Product
                {
                    ID = ProductID
                };
            }

            WishList wishList = new WishList
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Product = _product,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return wishList;
        }

        public void CreateWishList(WishList WishList)
        {
            Int64 UserID = 0;

            if (WishList.User != null)
                UserID = WishList.User.ID;

            Int64 ProductID = 0;

            if (WishList.Product != null)
                ProductID = WishList.Product.ID;

            String SQL = String.Format("INSERT INTO WishList (UserID, ProductID, Created, Modified) " +
                "values ({0},{1},'{2}','{3}')",
                UserID,
                ProductID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateWishList(WishList WishList)
        {
            Int64 UserID = 0;

            if (WishList.User != null)
                UserID = WishList.User.ID;

            Int64 ProductID = 0;

            if (WishList.Product != null)
                ProductID = WishList.Product.ID;

            String SQL = String.Format("UPDATE WishList SET UserID={0}," +
                                       "ProductID={1},Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                        UserID,
                                        ProductID,
                                        Database.ParseDateTimeToSQL(WishList.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        WishList.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteWishList(WishList WishList)
        {
            String SQL = String.Format("DELETE FROM WishList " +
                                       " WHERE ID={0}",
                                        WishList.ID);

            Database.ExecuteCommand(SQL);
        }

        public String GetHtmlWishList(Int64 UserID, Int64 SessionUserID, String search = "")
        {
            String sHtml = String.Empty;

            String sql = "select wt.ID, wt.UserID, wt.ProductID, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, pr.Name as ProductName,pr.Image as ProductImage, pr.PartnerUrl " +
                " from WishList wt left join User usr on wt.UserID = usr.ID " +
                " left join Product pr on wt.ProductID = pr.ID " +
                " where 1=1 ";

            if (search != "")
                sql += String.Format(" AND pr.Name like '%{0}%'", search);

            String sql_users = String.Empty;
            String sql_categories = String.Empty;
            String sql_relation = String.Empty;
            String sql_order_by = String.Empty;

            /*if (sql_users != "")
                sql_users = " AND (" + sql_users + " OR wt.UserID = " + SessionUserID + ") ";
            else
            {
                sql_users = " AND (";
                sql_users += String.Format(" (wt.UserID IN (SELECT DISTINCT(UserID) FROM ProductCategoryTrender)) ");
                sql_users += " OR ";
                sql_users += String.Format(" (wt.UserID IN (SELECT DISTINCT(FollowID) FROM UserRelation where UserID = {0})) ", SessionUserID);
                sql_users += " OR wt.UserID = " + SessionUserID + ") ";
            }

            if (sql_categories != "")
                sql_categories = " AND (" + sql_categories + ") ";*/

            sql_users += " AND wt.UserID = " + UserID + " ";

            sql_order_by = " order by wt.Created DESC ";

            sql += String.Format("{0} {1} {2} {3}", sql_users, sql_categories, sql_relation, sql_order_by);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            if (UserID == SessionUserID)
            {
                //Afegim un element buit, per poder afegir nous productes a la wishlist.
                sHtml += "<div class=\"wishlist-entry\">\n" +
                    "<div class=\"wishlist-entry-image\">\n" +
                        "<div class=\"wishlist-entry-add\" onclick=\"AddWishListForm(" + SessionUserID + ");\"></div>" +
                    "</div>\n" +
                "</div>\n";
            }

            try
            {
                while (reader.Read())
                {
                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    //Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    String UserName = Database.GetReaderValueString(reader, "UserName");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    String ProductName = Database.GetReaderValueString(reader, "ProductName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = "/Content/images/default_product.png";
                    else
                        ProductImage = "/Content/files/" + ProductImage;


                    String PartnerUrl = Database.GetReaderValueString(reader, "PartnerUrl");

                    String display_name = String.Format("{0} {1}", FirstName, LastName);

                    sHtml += "<div class=\"wishlist-entry\">\n" +
                        "<div class=\"wishlist-entry-image\">\n" +
                            "<img src=\"" + ProductImage + "\" width=\"220px\" alt=\"" + ProductName + "\" title=\"" + ProductName + "\" />\n" +
                            "<div id=\"wishlist-entry-info-" + ID + "\" class=\"wishlist-entry-info\">\n" +
                                "<div class=\"wishlist-entry-info-title\">" + ProductName + "</div>\n" +
                                "<div class=\"wishlist-entry-info-kit-link-container\">\n" +
                                    "<div class=\"wishlist-entry-info-kit-link\"  onclick=\"ProductKit(" + ProductID + ");\">Kit</div>\n" +
                                "</div>\n";

                    /*if ((PartnerUrl != String.Empty) && (PartnerUrl != ""))
                        sHtml += "<div class=\"wishlist-entry-info-loquiero-link\"><a href=\"" + PartnerUrl + "\" onclick=\"LoQuiero(" + ProductID + ",'" + PartnerUrl + "');\" target=\"_blank\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LO_QUIERO + "</a></div>\n";*/

                    sHtml += "<div class=\"wishlist-entry-info-loquiero-link\" onclick=\"LoadLoQuieroDialog(" + ProductID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LO_QUIERO + "</div>\n";

                    if(UserID == SessionUserID)
                        sHtml += "<div class=\"wishlist-entry-info-delete-link\" onclick=\"DelWishList(" + ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ELIMINAR_DEL_WISHLIST + "</div>\n";

                            sHtml += "</div>\n" +
                        "</div>\n" +
                    "</div>\n";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return sHtml;
        }
    }
}