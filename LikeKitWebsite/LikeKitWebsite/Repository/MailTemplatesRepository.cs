﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class MailTemplatesRepository : BaseRepository
    {
        public MailTemplatesRepository(DatabaseBase database)
        {
            Database = database;
        }

        public MailTemplatesRepository()
        {
        }

        private MailTemplate GetMailTemplate(IDataReader reader)
        {
            if (reader == null)
                return null;
            
            reader.Read();
            
            Int64 LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");
            Language _language = null;

            if (LanguageID > 0)
            {
                _language = new Language
                {
                    ID = LanguageID
                };
            }

            MailTemplate mailTemplate = new MailTemplate
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Subject = Database.GetReaderValueString(reader, "Subject"),
                Body = Database.GetReaderValueString(reader, "Body"),
                Type = (ObjectType)Database.GetReaderValueInt(reader, "Type"),
                Language = _language,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return mailTemplate;
        }

        public MailTemplate GetMailTemplate(Int16 Type, Int16 LanguageID)
        {
            MailTemplate mailTemplate;

            String sql = String.Format("select ID, Subject, Body, Type, LanguageID, Created, Modified from MailTemplate where Type={0} AND LanguageID={1}", Type, LanguageID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            
            try
            {
                reader.Read();

                Int64 _LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");
                Language _language = null;

                if (_LanguageID > 0)
                {
                    _language = new Language
                    {
                        ID = _LanguageID
                    };
                }

                mailTemplate = new MailTemplate
                {
                    ID = Database.GetReaderValueInt64(reader, "ID"),
                    Subject = Database.GetReaderValueString(reader, "Subject"),
                    Body = Database.GetReaderValueString(reader, "Body"),
                    Type = (ObjectType)Database.GetReaderValueInt(reader, "Type"),
                    Language = _language,
                    Created = Database.GetReaderValueDateTime(reader, "Created"),
                    Modified = Database.GetReaderValueDateTime(reader, "Modified")
                };
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return mailTemplate;
        }

        public void CreateMailTemplate(MailTemplate MailTemplate)
        {
            Int64 LanguageID = 0;

            if (MailTemplate.Language != null)
                LanguageID = MailTemplate.Language.ID;

            String SQL = String.Format("INSERT INTO MailTemplate (Subject, Body, Type, LanguageID, Created, Modified) " +
                "values ('{0}','{1}',{2},{3},'{4}','{5}')",
                Database.ParseStringToSQL(MailTemplate.Subject),
                Database.ParseStringToSQL(MailTemplate.Body),
                (short)MailTemplate.Type,
                LanguageID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateMailTemplate(MailTemplate MailTemplate)
        {
            Int64 LanguageID = 0;

            if (MailTemplate.Language != null)
                LanguageID = MailTemplate.Language.ID;

            String SQL = String.Format("UPDATE MailTemplate SET Subject='{0}', Body='{1}', Type={2}, LanguageID={3}, Created='{4}', Modified='{5}' " +
                                       " WHERE ID={6}",
                                    Database.ParseStringToSQL(MailTemplate.Subject),
                                    Database.ParseStringToSQL(MailTemplate.Body),
                                    (short)MailTemplate.Type,
                                    LanguageID,
                                    Database.ParseDateTimeToSQL(System.DateTime.Now),
                                    Database.ParseDateTimeToSQL(System.DateTime.Now),
                                    MailTemplate.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteMailTemplate(MailTemplate MailTemplate)
        {
            String SQL = String.Format("DELETE FROM MailTemplate " +
                                       " WHERE ID={0}",
                                        MailTemplate.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}