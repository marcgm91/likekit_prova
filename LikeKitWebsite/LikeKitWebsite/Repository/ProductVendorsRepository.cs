﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class ProductVendorsRepository : BaseRepository
    {
        public ProductVendorsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private ProductVendor GetProductVendor(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 VendorID = Database.GetReaderValueInt64(reader, "VendorID");
            Vendor _vendor = null;

            if (VendorID > 0)
            {
                _vendor = new Vendor
                {
                    ID = VendorID
                };
            }

            Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
            Product _product = null;

            if (ProductID > 0)
            {
                _product = new Product
                {
                    ID = ProductID
                };
            }

            ProductVendor productVendor = new ProductVendor
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Vendor = _vendor,
                Product = _product,
                Url = Database.GetReaderValueString(reader, "Url"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return productVendor;
        }

        public void CreateProductVendor(ProductVendor ProductVendor)
        {
            Int64 VendorID = 0;

            if (ProductVendor.Vendor != null)
                VendorID = ProductVendor.Vendor.ID;

            Int64 ProductID = 0;

            if (ProductVendor.Product != null)
                ProductID = ProductVendor.Product.ID;

            String SQL = String.Format("INSERT INTO ProductVendor (VendorID, ProductID, Url, Created, Modified) " +
                "values ({0},{1},'{2}','{3}','{4}')",
                VendorID,
                ProductID,
                Database.ParseStringToSQL(ProductVendor.Url),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateProductVendor(ProductVendor ProductVendor)
        {
            Int64 VendorID = 0;

            if (ProductVendor.Vendor != null)
                VendorID = ProductVendor.Vendor.ID;

            Int64 ProductID = 0;

            if (ProductVendor.Product != null)
                ProductID = ProductVendor.Product.ID;

            String SQL = String.Format("UPDATE ProductVendor SET VendorID={0}," +
                                       "ProductID={1}, Url='{2}',Created='{3}',Modified='{4}' " +
                                       " WHERE ID={5}",
                                        VendorID,
                                        ProductID,
                                        Database.ParseStringToSQL(ProductVendor.Url),
                                        Database.ParseDateTimeToSQL(ProductVendor.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        ProductVendor.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteProductVendor(ProductVendor ProductVendor)
        {
            String SQL = String.Format("DELETE FROM ProductVendor " +
                                       " WHERE ID={0}",
                                        ProductVendor.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}