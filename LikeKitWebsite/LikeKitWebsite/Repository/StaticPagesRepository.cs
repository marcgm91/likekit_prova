﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class StaticPagesRepository : BaseRepository
    {
        public StaticPagesRepository(DatabaseBase database)
        {
            Database = database;
        }

        private StaticPage GetStaticPage(IDataReader reader)
        {
            if (reader == null)
                return null;
            
            StaticPage static_page = null;
            
            if (reader.Read())
            {
                static_page = new StaticPage
                {
                    ID = Database.GetReaderValueInt64(reader, "ID"),
                    Title = Database.GetReaderValueString(reader, "Title"),
                    Content = Database.GetReaderValueString(reader, "Content"),
                    Order = (short)Database.GetReaderValueNumeric(reader, "Order"),
                    Created = Database.GetReaderValueDateTime(reader, "Created"),
                    Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                    SeoTitle = Database.GetReaderValueString(reader, "SeoTitle"),
                    SeoDescription = Database.GetReaderValueString(reader, "SeoDescription"),
                    SeoKeywords = Database.GetReaderValueString(reader, "SeoKeywords")
                };
            }

            return static_page;
        }

        public StaticPage GetStaticPage(Int64 id)
        {
            String sql = String.Format("select ID, Title, Content, `Order`, Created, Modified, SeoTitle, SeoDescription, SeoKeywords from StaticPage where ID={0}", id);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            StaticPage static_page = null;

            try
            {
                static_page = GetStaticPage(reader);
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return static_page;
        }

        public void CreateStaticPage(StaticPage StaticPage)
        {
            String SQL = String.Format("INSERT INTO StaticPage (Title, Content, `Order`, Created, Modified) " +
                "values ('{0}','{1}',{2},'{3}','{4}')",
                Database.ParseStringToSQL(StaticPage.Title),
                Database.ParseStringToSQL(StaticPage.Content),
                StaticPage.Order,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateStaticPage(StaticPage StaticPage)
        {
            String SQL = String.Format("UPDATE StaticPage SET Title='{0}'," +
                                       "Content='{1}',`Order`={2},Created='{3}',Modified='{4}' " +
                                       " WHERE ID={5}",
                                       Database.ParseStringToSQL(StaticPage.Title),
                                       Database.ParseStringToSQL(StaticPage.Content),
                                       StaticPage.Order,
                                       Database.ParseDateTimeToSQL(StaticPage.Created),
                                       Database.ParseDateTimeToSQL(System.DateTime.Now),
                                       StaticPage.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteStaticPage(StaticPage StaticPage)
        {
            String SQL = String.Format("DELETE FROM StaticPage " +
                                       " WHERE ID={0}",
                                        StaticPage.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}