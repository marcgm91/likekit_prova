﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Configuration;
using System.IO;
using System.Collections;
using Endepro.Common.Google;
using Endepro.Common.Social;

namespace LikeKitWebsite.Repository
{
    /// <summary>
    /// Objecte amb la informació dels Usuaris
    /// </summary>
    public class JsonUser
    {
        public Int64 ID = 0;
        public String UserID = String.Empty;
        public String UserName = String.Empty;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
        public String Picture = String.Empty;
        public String About = String.Empty;
        public String Web = String.Empty;
        public Int16 iUserType = 0;
    }

    /// <summary>
    /// Objecte amb la informació del Login de l'Usuari
    /// </summary>
    public class JsonLogin
    {
        public bool success = false;
        public Int64 ID = 0;
        public String UserID = String.Empty;
        public String UserName = String.Empty;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
        public String Picture = String.Empty;
        public String About = String.Empty;
        public String Web = String.Empty;
        public String Picsquare = String.Empty;
        public String Password = String.Empty;
        public Int64 NumInteractions = 0;
        public Int64 LanguageID = 0;
    }

    /// <summary>
    /// Objecte amb la informació dels Usuaris
    /// </summary>
    public class JsonUserResult
    {
        public bool Success = true;
        public String Message = String.Empty;
    }

    /// <summary>
    /// Objecte amb la informació dels notifies
    /// </summary>
    public class JsonNotifies
    {
        public Int64 ID = 0;
        public Int64 ToUserID = 0;
        public Int64 FromUserID = 0;
        public String UserName = String.Empty;
        public String FirstName = String.Empty;
        public String LastName = String.Empty;
        public String UserPicture = String.Empty;
        public NotifyType NotifyType = NotifyType.None;
        public Int64 RefID = 0;
        public DateTime Created;
        public DateTime Modified;
        public Int16 iUserType = 0;
    }

    public class UsersRepository : BaseRepository
    {
        public UsersRepository()
        {
        }

        public UsersRepository(DatabaseBase database)
        {
            Database = database;
        }

        private String CreateUserID(String userName)
        {
            return Endepro.Common.Crypto.GetMD5Hash(String.Format("{0}{1}", userName, System.DateTime.UtcNow));
        }

        private User GetUser(IDataReader reader)
        {
            if (reader == null)
                return null;

            User user = null;

            if (reader.Read())
            {

                Int64 LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");
                Language _language = null;

                if (LanguageID > 0)
                {
                    _language = new Language
                    {
                        ID = LanguageID
                    };
                }

                Int64 VendorID = Database.GetReaderValueInt64(reader, "VendorID");
                Vendor _vendor = null;

                if (VendorID > 0)
                {
                    _vendor = new Vendor
                    {
                        ID = VendorID
                    };
                }


                user = new User
                {
                    ID = Database.GetReaderValueInt64(reader, "ID"),
                    UserID = Database.GetReaderValueString(reader, "UserID"),
                    UserName = Database.GetReaderValueString(reader, "UserName"),
                    Password = Database.GetReaderValueString(reader, "Password"),
                    UserLevel = Database.GetReaderValueInt16(reader, "UserLevel"),
                    FirstName = Database.GetReaderValueString(reader, "FirstName"),
                    LastName = Database.GetReaderValueString(reader, "LastName"),
                    Picture = Database.GetReaderValueString(reader, "Picture"),
                    About = Database.GetReaderValueString(reader, "About"),
                    Web = Database.GetReaderValueString(reader, "Web"),
                    Address = Database.GetReaderValueString(reader, "Address"),
                    Gender = (UserGender)Database.GetReaderValueInt16(reader, "Gender"),
                    UserType = (UserType)Database.GetReaderValueInt16(reader, "UserType"),
                    FacebookAccessToken = Database.GetReaderValueString(reader, "FacebookAccessToken"),
                    TwitterAccessToken = Database.GetReaderValueString(reader, "TwitterAccessToken"),
                    FacebookExpires = Database.GetReaderValueDateTime(reader, "FacebookExpires"),
                    FacebookID = Database.GetReaderValueString(reader, "FacebookID"),
                    TwitterID = Database.GetReaderValueString(reader, "TwitterID"),
                    Birthday = Database.GetReaderValueDateTime(reader, "Birthday"),
                    OriginalPicture = Database.GetReaderValueString(reader, "OriginalPicture"),
                    RegisterType = (RegisterType)Database.GetReaderValueInt16(reader, "RegisterType"),
                    Perms = Database.GetReaderValueInt64(reader, "Perms"),
                    UserToken = Database.GetReaderValueString(reader, "UserToken"),
                    Language = _language,
                    Vendor = _vendor
                };

                /*
                //Mirem el register type, ja que fent el cast, falla
                Int16 regType = Database.GetReaderValueInt16(reader, "RegisterType");
                if (regType == 1)
                    user.RegisterType = RegisterType.Twitter;
                else if (regType == 2)
                    user.RegisterType = RegisterType.Facebook;
                */

                if (user.Picture != "")
                {
                    String img_path = ConfigurationManager.AppSettings["PathImages"];
                    String sFile = Globals.PathCombine(img_path, user.Picture);

                    if (!File.Exists(sFile))
                        user.Picture = "";
                }

                if (user.OriginalPicture != "")
                {
                    String img_path = ConfigurationManager.AppSettings["PathImages"];
                    String sFile = Globals.PathCombine(img_path, user.OriginalPicture);

                    if (!File.Exists(sFile))
                        user.OriginalPicture = "";
                }

                user.iUserType = 0;
                if (user.UserType == UserType.Trender)
                    user.iUserType = 1;
                else
                    user.iUserType = 0;

                user.BirthdayString = user.Birthday.ToString("dd/MM/yyyy");
                user.ProfilePercentage = GetUserPercentage(user);
                user.ProfilePercentageWidth = (int)Math.Round(user.ProfilePercentage * 1.4);

                user.HtmlWeb = LikeKitWebsite.Globals.HtmlEncodingLinks(user.Web);

            }

            return user;
        }

        public User GetUser(Int64 id)
        {
            String sql = String.Format("select ID, Created, Username, Password, UserID, UserLevel, FirstName, LastName, Picture, About, Web, Address, Gender, UserType, FacebookAccessToken, FacebookID, FacebookExpires, TwitterAccessToken, TwitterID, Birthday, OriginalPicture, RegisterType, Perms, UserToken, LanguageID, VendorID from User where ID={0}", id);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            User user = null;

            try
            {
                user = GetUser(reader);
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return user;
        }

        public User GetUserByUserID(String UserID)
        {
            String sql = String.Format("select ID, Created, Username, Password, UserID, UserLevel, FirstName, LastName, Picture, About, Web, Address, Gender, UserType, FacebookAccessToken, FacebookID, FacebookExpires, TwitterAccessToken, TwitterID, OriginalPicture, RegisterType, Perms, UserToken, LanguageID, VendorID from User where UserID='{0}'", UserID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            User user = null;

            try
            {
                user = GetUser(reader);
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return user;
        }

        public User GetUserByFacebookID(String UserID)
        {
            String sql = String.Format("select ID, Created, Username, Password, UserID, UserLevel, FirstName, LastName, Picture, About, Web, Address, Gender, UserType, FacebookAccessToken, FacebookID, FacebookExpires, TwitterAccessToken, TwitterID, OriginalPicture, RegisterType, Perms, UserToken, LanguageID, VendorID from User where FacebookID='{0}'", UserID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            User user = null;

            try
            {
                user = GetUser(reader);
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return user;
        }

        public User CreateUser(User user)
        {
            String UserID = CreateUserID(user.UserName);

            String SQL = String.Format("INSERT INTO User (Created, Username, Password, UserID, FirstName, LastName, Picture, About, Web, Address, " +
                "Gender, RegisterType, Perms, UserToken) " +
                "values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},508, '{12}')",
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseStringToSQL(user.UserName),
                Database.ParseStringToSQL(Endepro.Common.Crypto.GetMD5Hash(user.Password)),
                Database.ParseStringToSQL(UserID),
                Database.ParseStringToSQL(user.FirstName),

                //Database.ParseStringToSQL(user.LastName),
                //He cambiat i l'he possat "String.Empty" perquè el camp a la base de dades el deixi en blanc.
                Database.ParseStringToSQL(String.Empty), //"user.LastName".

                Database.ParseStringToSQL(String.Empty),
                Database.ParseStringToSQL(String.Empty),
                Database.ParseStringToSQL(String.Empty),
                Database.ParseStringToSQL(String.Empty),
                0, (int)RegisterType.Normal,
                Database.ParseStringToSQL(UserID));

            Database.ExecuteCommand(SQL);
            user.ID = GetLastAutoIncrement();

            //MarcGM(2-5-2013): Aquestes 2 línies de sota (la consulta aquesta), activa al usuari sense haber de confirmar cap e-mail (camp "Active" de la taula "User" de la BD).
            String SQLActivateUser = String.Format("UPDATE User SET Active = true WHERE ID = user.ID");
            Database.ExecuteCommand(SQLActivateUser);

            //Carreguem les dades del template
            String Subject = String.Empty;
            String Body = String.Empty;
            String To = String.Empty;
            String From = String.Empty;

            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);

            short _lang = 1;

            if (user.Language != null)
                _lang = (short)user.Language.ID;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.Welcome, _lang);

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

            String url_register = String.Format("{0}/Account/Activation/?id={1}", ConfigurationManager.AppSettings["MainURL"], UserID);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            url_register = url_register.Replace("//", "/");
            url_register = url_register.Replace("http:/", "http://");
            mail_template.Body = mail_template.Body.Replace("[REGISTER_URL]", url_register);
            
            //MarcGM (2-5-2013): He comentat lo de "enviar correu" perquè a partir d'ara, l'usuari es registra automàticament (sense confirmar la compta).
            //Enviem un correu
            /*try
            {
                Globals.SendMail(Database, mail_template.Subject, mail_template.Body, user.UserName);
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Database, ex_msg, Models.Type.Register, 1);
            }*/

            return user;
        }

        public void UpdateUser(User user)
        {
            String SQL = String.Format("UPDATE User SET Created='{0}',Username='{1}',Password='{2}',UserID='{3}',FirstName='{4}',LastName='{5}'," +
                                                   "Picture='{6}',About='{7}',Web='{8}',Address='{9}',Gender={10}, " +
                                                   "OriginalPicture='{11}', Birthday='{12}', Perms='{13}', LanguageID={14} " +
                                                   " WHERE ID={15}",
                                                   Database.ParseDateTimeToSQL(user.Created),
                                                   Database.ParseStringToSQL(user.UserName),
                                                   Database.ParseStringToSQL(user.Password),
                                                   Database.ParseStringToSQL(user.UserID),
                                                   Database.ParseStringToSQL(user.FirstName),
                                                   Database.ParseStringToSQL(user.LastName),
                                                   Database.ParseStringToSQL(user.Picture),
                                                   Database.ParseStringToSQL(user.About),
                                                   Database.ParseStringToSQL(user.Web),
                                                   Database.ParseStringToSQL(user.Address),
                                                   (Int16)user.Gender,
                                                   Database.ParseStringToSQL(user.OriginalPicture),
                                                   Database.ParseDateTimeToSQL(user.Birthday),
                                                   user.Perms,
                                                   user.Language.ID,
                                                   user.ID);

            Database.ExecuteCommand(SQL);
        }

        public void UpdateUserPassword(User user)
        {
            String SQL = String.Format("UPDATE User SET Modified='{0}',Password='{1}'" +
                                       " WHERE ID={2}",
                                       Database.ParseDateTimeToSQL(DateTime.Now),
                                       Database.ParseStringToSQL(user.Password),
                                       user.ID);

            Database.ExecuteCommand(SQL);
        }

        public bool ActivateUser(String userid)
        {
            //Primer obtenim l'usuari
            User _user = new User();
            try
            {
                _user = GetUserByUserID(userid);

                //Tornem a generar l'UserID per seguretat
                String newUserID = CreateUserID(_user.UserName);

                if (_user.ID > 0)
                {
                    String SQL = String.Format("UPDATE User SET Active=1, UserID='{0}', Activated='{1}', Modified='{1}' WHERE ID={2}", newUserID, Database.ParseDateTimeToSQL(DateTime.UtcNow), _user.ID);
                    Database.ExecuteCommand(SQL);
                    return true;
                }
                else
                    return false;
            }
            catch {
                return false;
            }
        }

        public void DeleteUser(User user)
        {
            //Esborrem els kits Comments
            String SQL = String.Format("DELETE FROM KitComment " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Ara hauriem d'esborrar els Kit tags, per evitar errors
            SQL = String.Format("DELETE FROM KitTag WHERE KitID IN (SELECT ID FROM Kit WHERE UserID={0})", user.ID);

            Database.ExecuteCommand(SQL);


            //Ara hauriem d'esborrar els Kit comments, fet als kits de l'usuari
            SQL = String.Format("DELETE FROM KitComment WHERE KitID IN (SELECT ID FROM Kit WHERE UserID={0})", user.ID);

            Database.ExecuteCommand(SQL);


            //Ara hauriem d'esborrar els likes, fet als kits de l'usuari
            SQL = String.Format("DELETE FROM UserLike WHERE KitID IN (SELECT ID FROM Kit WHERE UserID={0})", user.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els kits
            SQL = String.Format("DELETE FROM Kit " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem les notificacions 
            SQL = String.Format("DELETE FROM Notify " +
                                       " WHERE ToUserID={0} OR FromUserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem ProductCategoryTrender
            SQL = String.Format("DELETE FROM ProductCategoryTrender " +
                           " WHERE UserID={0}",
                            user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem els Likes
            SQL = String.Format("DELETE FROM UserLike  " +
                                       " WHERE UserID={0}",
                                        user.ID);
            Database.ExecuteCommand(SQL);

            //Esborrem els Likes de producte
            SQL = String.Format("DELETE FROM UserLikeProduct  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els UserRelation
            SQL = String.Format("DELETE FROM UserRelation  " +
                                       " WHERE UserID={0} OR FollowID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);


            //Esborrem els Wishlist
            SQL = String.Format("DELETE FROM WishList  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem els Nowwith
            SQL = String.Format("DELETE FROM UserStatus  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Esborrem els AccessLog
            SQL = String.Format("DELETE FROM AccessLog  " +
                                       " WHERE UserID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);

            //Finalment esborrem l'usuari
            SQL = String.Format("DELETE FROM User " +
                                       " WHERE ID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DisableUser(User user)
        {
            String SQL = String.Format("UPDATE User SET Active=2 " +
                                       " WHERE ID={0}",
                                        user.ID);

            Database.ExecuteCommand(SQL);
        }

#region Facebook
        public User GetFacebookUser(int FacebookID)
        {
            String SQL = String.Format("SELECT * from User where FacebookID='{0}'", FacebookID);

            IDbCommand cmd = null;
            IDataReader reader = null;
            try
            {
                cmd = Database.CreateCommand(SQL);
                reader = cmd.ExecuteReader();

                if (reader.Read())
                    return GetUser(reader);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return null;
        }
#endregion


        public JsonLogin ValidateUser(String username, String password)
        {
            String md5_password = LikeKitWebsite.Globals.GetMD5(password);

            String UserID = String.Empty;
            Int64 ID = 0;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            String sql = String.Format("SELECT UserID,ID, UserName, FirstName, LastName, Picture, About, Web from User where UPPER(Username)='{0}' AND Password='{1}' AND active=1 ",
                                       Database.ParseStringToSQL(username.ToUpper()),
                                       Database.ParseStringToSQL(md5_password));

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    UserID = Database.GetReaderValueString(reader, "UserID");
                    ID = Database.GetReaderValueInt64(reader, "ID");

                    login.success = true;
                    login.UserID = UserID;
                    login.UserName = Database.GetReaderValueString(reader, "UserName");
                    login.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    login.LastName = Database.GetReaderValueString(reader, "LastName");
                    login.Picture = Database.GetReaderValueString(reader, "Picture");
                    login.About = Database.GetReaderValueString(reader, "About");
                    login.Web = Database.GetReaderValueString(reader, "Web");
                }
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, sql);
                Globals.SaveLog(Database, ex_msg, Models.Type.Register, 1);
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return login;
        }

        public JsonLogin GetUserData(String username, String password)
        {
            String md5_password = LikeKitWebsite.Globals.GetMD5(password);

            String UserID = String.Empty;
            Int64 ID = 0;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            String sql = String.Format("SELECT UserID,ID, UserName, FirstName, LastName, Picture, About, Web, LanguageID from User where Username='{0}' AND Password='{1}' AND active=1 ",
                                       Database.ParseStringToSQL(username),
                                       Database.ParseStringToSQL(md5_password));

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    UserID = Database.GetReaderValueString(reader, "UserID");
                    ID = Database.GetReaderValueInt64(reader, "ID");

                    login.success = true;
                    login.ID = ID;
                    login.UserID = UserID;
                    login.UserName = Database.GetReaderValueString(reader, "UserName");
                    login.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    login.LastName = Database.GetReaderValueString(reader, "LastName");
                    login.Picture = Database.GetReaderValueString(reader, "Picture");
                    login.About = Database.GetReaderValueString(reader, "About");
                    login.Web = Database.GetReaderValueString(reader, "Web");
                    login.LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            login.NumInteractions = GetNumUserInteraction(login.ID);

            return login;
        }

        public JsonLogin GetUserDataAutoLogin(String UserID)
        {
            Int64 ID = 0;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            String sql = String.Format("SELECT UserID,ID, UserName, FirstName, LastName, Picture, About, Web, LanguageID from User where UserID='{0}' AND active=1 ",
                                       Database.ParseStringToSQL(UserID));

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    UserID = Database.GetReaderValueString(reader, "UserID");
                    ID = Database.GetReaderValueInt64(reader, "ID");

                    login.success = true;
                    login.ID = ID;
                    login.UserID = UserID;
                    login.UserName = Database.GetReaderValueString(reader, "UserName");
                    login.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    login.LastName = Database.GetReaderValueString(reader, "LastName");
                    login.Picture = Database.GetReaderValueString(reader, "Picture");
                    login.About = Database.GetReaderValueString(reader, "About");
                    login.Web = Database.GetReaderValueString(reader, "Web");
                    login.LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            //Actualitzem l'UserID
            login.UserID = CreateUserID(login.UserID);

            sql = String.Format("update User set UserID='{0}' where ID={1}",
                   Database.ParseStringToSQL(login.UserID), ID);
            
            command = Database.CreateCommand(sql);
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                command.Dispose();
            }

            login.NumInteractions = GetNumUserInteraction(login.ID);

            return login;
        }

        public int GetUserPercentage(User user)
        {
            int percentage = 0;

            if (user != null)
            {
                if ((user.UserName != "") && (user.UserName != String.Empty))
                    percentage++;
                if((user.FirstName != "") && (user.FirstName != String.Empty))
                    percentage++;
                if((user.LastName != "") && (user.LastName != String.Empty))
                    percentage++;
                if ((user.Gender != 0))
                    percentage++;
                if ((user.BirthdayString != "01/01/0001"))
                    percentage++;
                /*if((user.Picture != "") && (user.Picture != String.Empty))
                    percentage++;*/
                if((user.About != "") && (user.About != String.Empty))
                    percentage++;
                if((user.Web != "") && (user.Web != String.Empty))
                    percentage++;
                if((user.Address != "") && (user.Address != String.Empty))
                    percentage++;

                float test = (float)percentage/8;
                float total_percentage = test*100;
                percentage = (int)Math.Round(total_percentage);
            }

            return percentage;
        }


        public JsonLogin LoginUserTwitter(String idTwitter, String userName, String city, String picsquare, String AccessToken)
        {
            String UserID = String.Empty;
            Int64 ID = 0;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            String sql = String.Format("SELECT UserID,ID, UserName, FirstName, LastName, Picture, About, Web, LanguageID from User where TwitterID='{0}' AND active=1 AND RegisterType={1}",
                                       Database.ParseStringToSQL(idTwitter), (int)RegisterType.Twitter);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    UserID = Database.GetReaderValueString(reader, "UserID");
                    ID = Database.GetReaderValueInt64(reader, "ID");

                    login.success = true;
                    login.ID = ID;
                    login.UserID = UserID;
                    login.UserName = Database.GetReaderValueString(reader, "UserName");
                    login.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    login.LastName = Database.GetReaderValueString(reader, "LastName");
                    login.Picture = Database.GetReaderValueString(reader, "Picture");
                    login.About = Database.GetReaderValueString(reader, "About");
                    login.Web = Database.GetReaderValueString(reader, "Web");
                    login.LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");

                    //Fem un update del mail/username i el AccessToken
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();

                    sql = String.Format("update User set Username='{0}@twitter', TwitterAccessToken='{1}', Picsquare='{2}' where ID={3}",
                                       userName, Database.ParseStringToSQL(AccessToken), picsquare, ID);
                    command = Database.CreateCommand(sql);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        command.Dispose();
                    }
                }
                else
                {
                    //No l'hem trobat a user per twitterID, mirem per mail
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();

                    sql = String.Format("SELECT UserID,ID, UserName, FirstName, LastName, Picture, About, Web from User where Username='{0}@twitter' AND active=1 AND RegisterType={1}",
                                       Database.ParseStringToSQL(userName), (int)RegisterType.Twitter);

                    command = Database.CreateCommand(sql);
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        UserID = Database.GetReaderValueString(reader, "UserID");
                        ID = Database.GetReaderValueInt64(reader, "ID");

                        login.success = true;
                        login.ID = ID;
                        login.UserID = UserID;
                        login.UserName = Database.GetReaderValueString(reader, "UserName");
                        login.FirstName = Database.GetReaderValueString(reader, "FirstName");
                        login.LastName = Database.GetReaderValueString(reader, "LastName");
                        login.Picture = Database.GetReaderValueString(reader, "Picture");
                        login.About = Database.GetReaderValueString(reader, "About");
                        login.Web = Database.GetReaderValueString(reader, "Web");

                        //Hem d'actualitzar twitterID
                        reader.Close();
                        reader.Dispose();
                        command.Dispose();

                        /*sql = String.Format("update User set TwitterID='{0}', City='{1}', Picsquare='{2}', TwitterAccessToken='{3}' where ID={4}",
                              Database.ParseStringToSQL(idTwitter),
                              Database.ParseStringToSQL(city),
                              Database.ParseStringToSQL(picsquare),
                              Database.ParseStringToSQL(AccessToken),
                              ID);*/
                        sql = String.Format("update User set TwitterID='{0}', Picsquare='{1}', TwitterAccessToken='{2}' where ID={3}",
                              Database.ParseStringToSQL(idTwitter),
                              Database.ParseStringToSQL(picsquare),
                              Database.ParseStringToSQL(AccessToken),
                              ID);

                        IDbCommand command2 = Database.CreateCommand(sql);
                        try
                        {
                            command2.ExecuteNonQuery();
                        }
                        finally
                        {
                            command2.Dispose();
                        }
                    }
                    else
                    {
                        return login;
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            login.NumInteractions = GetNumUserInteraction(login.ID);
            UserID = RegenerateKey(UserID, userName + "@twitter");

            return login;
        }

        public String RegenerateKey(String userid, String username)
        {
            String NewUserId = Globals.GetMD5(String.Format("{0}{1}", username, DateTime.UtcNow));
            String sql = String.Format("update User set UserID='{0}' where UserID='{1}'",
                                       NewUserId,
                                       userid);
            IDbCommand command2 = Database.CreateCommand(sql);
            try
            {
                command2.ExecuteNonQuery();
            }
            finally
            {
                command2.Dispose();
            }

            return NewUserId;
        }
       
        public JsonLogin RegisterTwitter(String idTwitter, String Username, String Firstname,String Lastname, String Ip, String locale, String city, String picsquare, String AccessToken, String SocialName)
        {
            String md5_password = Globals.GetMD5(idTwitter);
            String UserID = Globals.GetMD5(String.Format("{0}{1}", Username, DateTime.UtcNow));

            Int64 ID = 0;
            Int16 UserLevel = 1;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            String twitter_picture = String.Format("https://api.twitter.com/1/users/profile_image?screen_name={0}&size=original", SocialName);

            string imageName = String.Format("{0}", idTwitter);
            try
            {
                //Tractem la imatge
                System.Drawing.Image image = Globals.DownloadImageFromUrl(twitter_picture);
                //string imageName = Path.GetFileName(picsquare);

                string fileName = System.IO.Path.Combine(ConfigurationManager.AppSettings["PathImages"], imageName);
                image.Save(fileName);
            }
            catch (Exception ex) {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, twitter_picture);
                Globals.SaveLog(Database, ex_msg, Models.Type.Register, 1);
            }


            String sql = String.Format("insert into User(Created, Modified, Username, Password, UserID, UserLevel, FirstName, LastName, LastLogin, Active, Activated, Picsquare, Gender, UserType, TwitterAccessToken,TwitterID, Picture, RegisterType, Perms, UserToken)" +

                                       " values('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}','{8}',{9},'{10}','{11}',{12}, {13}, '{14}', '{15}', '{16}', {17}, 508, '{18}')",
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       Database.ParseStringToSQL(Username),
                                       Database.ParseStringToSQL(md5_password),
                                       Database.ParseStringToSQL(UserID),
                                       UserLevel,
                                       Database.ParseStringToSQL(Firstname),
                                       Database.ParseStringToSQL(Lastname),
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       1,
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       Database.ParseStringToSQL(picsquare),
                                       1,
                                       0,
                                       Database.ParseStringToSQL(AccessToken),
                                       Database.ParseStringToSQL(idTwitter),
                                       Database.ParseStringToSQL(imageName),
                                       (int)RegisterType.Twitter,
                                       Database.ParseStringToSQL(UserID));


            IDbCommand command = Database.CreateCommand(sql);

            try
            {
                command.ExecuteNonQuery();

                ID = GetLastAutoIncrement();

                login.success = true;
                login.ID = ID;
                login.UserID = UserID;
                login.UserName = Username;
                login.FirstName = Firstname;
                login.LastName = Lastname;
                login.Picture = picsquare;
                login.Password = idTwitter;
            }
            finally
            {
                command.Dispose();
            }

            login.NumInteractions = GetNumUserInteraction(login.ID);

            return login;
        }

        public JsonLogin RegisterFacebook(String idFacebook, String Username, String Firstname, String Lastname, String gender, String picsquare, String AccessToken, String SocialName, Int64 LanguageID)
        {
            String md5_password = Globals.GetMD5(idFacebook);
            String UserID = Globals.GetMD5(String.Format("{0}{1}", Username, DateTime.UtcNow));

            Int64 ID = 0;
            Int16 UserLevel = 1;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            Int16 _gender = 1;

            if (gender == "male")
                _gender = 1;
            else if (gender == "female")
                _gender = 2;

            //Tractem la imatge
            string imageName = String.Format("{0}", idFacebook);
            try{
                System.Drawing.Image image = Globals.DownloadImageFromUrl(picsquare);
                //string imageName = Path.GetFileName(picsquare);
                string fileName = System.IO.Path.Combine(ConfigurationManager.AppSettings["PathImages"], imageName);
                image.Save(fileName);
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, picsquare);
                Globals.SaveLog(Database, ex_msg, Models.Type.Register, 1);
            }

            String sql = String.Format("insert into User(Created, Modified, Username, Password, UserID, UserLevel, FirstName, LastName, LastLogin, Active, Activated, Picsquare, Gender, UserType, 	FacebookAccessToken, FacebookID, Picture, RegisterType, Perms, UserToken, LanguageID)" +
                                       " values('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}','{8}',{9},'{10}','{11}',{12}, {13}, '{14}', '{15}','{16}',{17}, 508, '{18}',{19})",
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       Database.ParseStringToSQL(Username),
                                       Database.ParseStringToSQL(md5_password),
                                       Database.ParseStringToSQL(UserID),
                                       UserLevel,
                                       Database.ParseStringToSQL(Firstname),
                                       Database.ParseStringToSQL(Lastname),
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       1,
                                       Database.ParseDateTimeToSQL(DateTime.UtcNow),
                                       Database.ParseStringToSQL(picsquare),
                                       _gender,
                                       0,
                                       Database.ParseStringToSQL(AccessToken),
                                       Database.ParseStringToSQL(idFacebook),
                                       Database.ParseStringToSQL(imageName),
                                       (int)RegisterType.Facebook,
                                       Database.ParseStringToSQL(UserID), 
                                       LanguageID);
            Globals.SaveLog(Database, String.Format("facebook register sql : {0}",sql), Models.Type.Register, 1);
            IDbCommand command = Database.CreateCommand(sql);

            try
            {
                command.ExecuteNonQuery();

                ID = GetLastAutoIncrement();

                login.success = true;
                login.ID = ID;
                login.UserID = UserID;
                login.UserName = Username;
                login.FirstName = Firstname;
                login.LastName = Lastname;
                login.Picture = picsquare;
                login.Password = idFacebook;
            }
            finally
            {
                command.Dispose();
            }

            login.NumInteractions = GetNumUserInteraction(login.ID);

            return login;
        }


        public JsonLogin LoginUserFacebook(String idFacebook, String userName, String AccessToken, String Picsquare)
        {
            String UserID = String.Empty;
            Int64 ID = 0;

            JsonLogin login = new JsonLogin();
            login.success = false;
            login.UserID = String.Empty;

            String sql = String.Format("SELECT UserID,ID, UserName, FirstName, LastName, Picture, About, Web, Picsquare, LanguageID from User where FacebookID='{0}' AND active=1 AND RegisterType={1}",
                                       Database.ParseStringToSQL(idFacebook), (int)RegisterType.Facebook);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    UserID = Database.GetReaderValueString(reader, "UserID");
                    ID = Database.GetReaderValueInt64(reader, "ID");

                    login.success = true;
                    login.ID = ID;
                    login.UserID = UserID;
                    login.UserName = Database.GetReaderValueString(reader, "UserName");
                    login.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    login.LastName = Database.GetReaderValueString(reader, "LastName");
                    login.Picture = Database.GetReaderValueString(reader, "Picture");
                    login.About = Database.GetReaderValueString(reader, "About");
                    login.Web = Database.GetReaderValueString(reader, "Web");
                    login.Picsquare = Database.GetReaderValueString(reader, "Picsquare");
                    login.LanguageID = Database.GetReaderValueInt64(reader, "LanguageID");

                    //Fem un update del mail/username i el AccessToken
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();

                    sql = String.Format("update User set Username='{0}', FacebookAccessToken='{1}', Picsquare='{2}' where ID={3}",
                                       userName, Database.ParseStringToSQL(AccessToken), Picsquare, ID);
                    command = Database.CreateCommand(sql);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        command.Dispose();
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            login.NumInteractions = GetNumUserInteraction(login.ID);

            UserID = RegenerateKey(UserID, userName);

            return login;
        }

        public JsonUserResult CheckMail(String Mail)
        {
            JsonUserResult result = new JsonUserResult();

            BetaUsersRepository rep = new BetaUsersRepository(Database);
            if (!rep.Exists(Mail))
            {
                result.Success = true;
                result.Message = String.Format("Success");
                
            }
            else
            {
                result.Success = false;
                result.Message = String.Format("Error");
            }

            return result;
        }

        public JsonUserResult CheckMailLogin(String Mail)
        {
            JsonUserResult result = new JsonUserResult();

            if (MailExists(Mail))
            {
                result.Success = true;
                result.Message = String.Format("Success");

            }
            else
            {
                result.Success = false;
                result.Message = String.Format("Error");
            }

            return result;
        }


        public String GetRegisterFollowings()
        {
            String sHtml = String.Empty;
            String html_hiddens = String.Empty;

            String sql = "select ID, FirstName, LastName, Picture" +
                            " from User " +
                            " where ID=31";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int16 count = 0;

            sHtml += "<div class=\"register-users\">";

            try
            {
                while (reader.Read())
                {
                    count++;

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String Picture = Database.GetReaderValueString(reader, "Picture");

                    if (Picture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, Picture);

                        if (!File.Exists(sFile))
                            Picture = "";
                    }

                    if (Picture == "")
                        Picture = "/Content/images/default_user.png";
                    else
                        Picture = "/Content/files/" + Picture;

                    String sCss = String.Empty;

                    if (count == 8)
                    {
                        sCss = "margin-right-0";
                        count = 0;
                    }

                    html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                    sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            sql = "select ID, FirstName, LastName, Picture" +
                " from User " +
                " where 1=1 AND Active = 1 AND (ID <> 31) LIMIT 23";

            IDbCommand command_2 = Database.CreateCommand(sql);
            IDataReader reader_2 = command_2.ExecuteReader();

            try
            {
                while (reader_2.Read())
                {
                    count++;

                    Int64 ID = Database.GetReaderValueInt64(reader_2, "ID");
                    String FirstName = Database.GetReaderValueString(reader_2, "FirstName");
                    String LastName = Database.GetReaderValueString(reader_2, "LastName");
                    String Picture = Database.GetReaderValueString(reader_2, "Picture");

                    if (Picture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, Picture);

                        if (!File.Exists(sFile))
                            Picture = "";
                    }

                    if (Picture == "")
                        Picture = "/Content/images/default_user.png";
                    else
                        Picture = "/Content/files/" + Picture;

                    String sCss = String.Empty;

                    if (count == 8)
                    {
                        sCss = "margin-right-0";
                        count = 0;
                    }

                    html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                    sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader_2.Close();
                reader_2.Dispose();
                command_2.Dispose();
            }

            sHtml += "</div>";

            sHtml = html_hiddens + sHtml;

            return sHtml;
        }

        public String GetRegisterFollowingsFacebook(List<InviteContact> contacts, Int64 user_id, Int64 LanguageID)
        {
            ArrayList likekit_users = new ArrayList();

            Globals.SaveLog(Database, String.Format("GetRegisterFollowingsFacebook : 1"), Models.Type.Register, 1);

            String sHtml = String.Empty;
            String html_hiddens = String.Empty;
            String not_in = "";

            sHtml += "<div class=\"register-users\">";

            Int16 contacts_exist = 0;
            Int16 count = 0;
            //Mirem quins contactes existeixen com a usuaris
            foreach (InviteContact contact in contacts)
            {
                String mail = contact.Email;
                String Name = contact.Name;
                User user = null;

                user = GetUserByFacebookID(mail);

                if (user != null)
                {
                    contacts_exist++;
                    count++;
                    //Si existeix, el seguim, i el mostrem

                    /* Això ho farem després
                    UserRelation ur = new UserRelation();
                    ur.User = new User();
                    ur.User.ID = user_id;
                    ur.Follow = new User();
                    ur.Follow.ID = user.ID;

                    UserRelationsRepository _urRP = new UserRelationsRepository(Database);
                    Int64 follow_id = _urRP.CreateUserRelation(ur);

                    TimeLineRepository _tRP = new TimeLineRepository(Database);

                    //Creem la notificació
                    _tRP.CreateNotify(user.ID, user_id, follow_id, NotifyType.Follow);

                    //Mirem si li enviem el correu.
                    //User user = Repository.GetUser(UserID);

                    user.Perm7 = Globals.GetPerms(user.Perms, (int)UserPerms.Follow);

                    if (user.Perm7 == true)
                    {
                        _tRP.FollowMail(user.ID, user_id);
                    }*/

                    //Si no hem superat els 24 usuaris, el mostrem
                    if (contacts_exist < 25)
                    {
                        String Picture = user.Picture;
                        
                        if (Picture == "")
                            Picture = "/Content/images/default_user.png";
                        else
                            Picture = "/Content/files/" + Picture;

                        String sCss = String.Empty;

                        if (count == 8)
                        {
                            sCss = "margin-right-0";
                            count = 0;
                        }

                        html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + user.ID + "\" />\n";
                        html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowingsFacebook\" name=\"RegisterFollowingsFacebook[]\" value=\"" + user.ID + "\" />\n";

                        sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";

                        if (not_in != "")
                            not_in += "," + user.ID;
                        else
                            not_in += user.ID;
                    }
                }
            }

            if (not_in != "")
                not_in = " AND User.ID NOT IN (" + not_in + ")";

            Globals.SaveLog(Database, String.Format("GetRegisterFollowingsFacebook : 2"), Models.Type.Register, 1);

            if (contacts_exist < 24)
            {
                //Ususari de likekit i/o trenders
                String sql = "select ID, FirstName, LastName, Picture" +
                                " from User " +
                                " where ID=31";

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        count++;

                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                        String FirstName = Database.GetReaderValueString(reader, "FirstName");
                        String LastName = Database.GetReaderValueString(reader, "LastName");
                        String Picture = Database.GetReaderValueString(reader, "Picture");

                        if (Picture != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, Picture);

                            if (!File.Exists(sFile))
                                Picture = "";
                        }

                        if (Picture == "")
                            Picture = "/Content/images/default_user.png";
                        else
                            Picture = "/Content/files/" + Picture;

                        String sCss = String.Empty;

                        if (count == 8)
                        {
                            sCss = "margin-right-0";
                            count = 0;
                        }

                        html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                        sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();
                }

                contacts_exist++;

                Globals.SaveLog(Database, String.Format("GetRegisterFollowingsFacebook : 3"), Models.Type.Register, 1);

                //Ususari de likekit i/o trenders
                String sql_trenders = "select ID, FirstName, LastName, Picture" +
                                " from User " +
                                " where ID=31";


                int limit = 24 - contacts_exist;

                sql_trenders = "select ID, FirstName, LastName, Picture" +
                    " from User " +
                    " where 1=1 AND Active = 1 AND UserType=1 AND LanguageID=" + LanguageID + " AND (ID <> 31) " + not_in + "  LIMIT " + limit;

                Globals.SaveLog(Database, String.Format("Sql trenders : {0}", sql_trenders), Models.Type.Register, 1);

                IDbCommand command_trenders = Database.CreateCommand(sql_trenders);
                IDataReader reader_trenders = command_trenders.ExecuteReader();

                try
                {
                    while (reader_trenders.Read())
                    {
                        count++;

                        Int64 ID = Database.GetReaderValueInt64(reader_trenders, "ID");
                        String FirstName = Database.GetReaderValueString(reader_trenders, "FirstName");
                        String LastName = Database.GetReaderValueString(reader_trenders, "LastName");
                        String Picture = Database.GetReaderValueString(reader_trenders, "Picture");

                        if (Picture != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, Picture);

                            if (!File.Exists(sFile))
                                Picture = "";
                        }

                        if (Picture == "")
                            Picture = "/Content/images/default_user.png";
                        else
                            Picture = "/Content/files/" + Picture;

                        String sCss = String.Empty;

                        if (count == 8)
                        {
                            sCss = "margin-right-0";
                            count = 0;
                        }

                        html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowingsTrenders\" name=\"RegisterFollowingsTrenders[]\" value=\"" + ID + "\" />\n";
                        html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                        sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";
                        contacts_exist++;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader_trenders.Close();
                    reader_trenders.Dispose();
                    command_trenders.Dispose();
                }

                if (contacts_exist < 24)
                {
                    limit = 24 - contacts_exist;

                    sql = "select ID, FirstName, LastName, Picture" +
                        " from User " +
                        " where 1=1 AND Active = 1 AND LanguageID=" + LanguageID + " AND UserType=0 AND (ID <> 31) " + not_in + "  LIMIT " + limit;

                    IDbCommand command_2 = Database.CreateCommand(sql);
                    IDataReader reader_2 = command_2.ExecuteReader();

                    try
                    {
                        while (reader_2.Read())
                        {
                            count++;

                            Int64 ID = Database.GetReaderValueInt64(reader_2, "ID");
                            String FirstName = Database.GetReaderValueString(reader_2, "FirstName");
                            String LastName = Database.GetReaderValueString(reader_2, "LastName");
                            String Picture = Database.GetReaderValueString(reader_2, "Picture");

                            if (Picture != "")
                            {
                                String img_path = ConfigurationManager.AppSettings["PathImages"];
                                String sFile = Globals.PathCombine(img_path, Picture);

                                if (!File.Exists(sFile))
                                    Picture = "";
                            }

                            if (Picture == "")
                                Picture = "/Content/images/default_user.png";
                            else
                                Picture = "/Content/files/" + Picture;

                            String sCss = String.Empty;

                            if (count == 8)
                            {
                                sCss = "margin-right-0";
                                count = 0;
                            }

                            html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                            sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";

                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        reader_2.Close();
                        reader_2.Dispose();
                        command_2.Dispose();
                    }
                }
            }

            sHtml += "</div>";

            sHtml = html_hiddens + sHtml;

            return sHtml;
        }

        public String GetRegisterFollowingsFacebookCommaSeparated(List<InviteContact> contacts, Int64 user_id, Int64 LanguageID)
        {
            ArrayList likekit_users = new ArrayList();

            String html_hiddens = String.Empty;

            String FollwingsCommaSeparated = "";

            String not_in = "";

            Int16 contacts_exist = 0;
            Int16 count = 0;
            //Mirem quins contactes existeixen com a usuaris
            foreach (InviteContact contact in contacts)
            {
                String mail = contact.Email;
                String Name = contact.Name;
                User user = null;

                user = GetUserByFacebookID(mail);

                if (user != null)
                {
                    contacts_exist++;
                    count++;

                    //Si no hem superat els 24 usuaris, el mostrem
                    if (contacts_exist < 25)
                    {
                        if (count > 1)
                            FollwingsCommaSeparated += ",";

                        FollwingsCommaSeparated += user.ID;

                        //html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + user.ID + "\" />\n";
                        //html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowingsFacebook\" name=\"RegisterFollowingsFacebook[]\" value=\"" + user.ID + "\" />\n";

                        if (not_in != "")
                            not_in += "," + user.ID;
                        else
                            not_in += user.ID;
                    }
                }
            }

            if (not_in != "")
                not_in = " AND User.ID NOT IN (" + not_in + ")";

            Globals.SaveLog(Database, String.Format("GetRegisterFollowingsFacebook : 2"), Models.Type.Register, 1);

            if (contacts_exist < 24)
            {
                //Ususari de likekit i/o trenders
                String sql = "select ID, FirstName, LastName, Picture" +
                                " from User " +
                                " where ID=31";

                IDbCommand command = Database.CreateCommand(sql);
                IDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        count++;

                        Int64 ID = Database.GetReaderValueInt64(reader, "ID");

                        //html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                        if (count > 1)
                            FollwingsCommaSeparated += ",";

                        FollwingsCommaSeparated += ID;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    command.Dispose();
                }

                contacts_exist++;

                Globals.SaveLog(Database, String.Format("GetRegisterFollowingsFacebook : 3"), Models.Type.Register, 1);

                //Ususari de likekit i/o trenders
                String sql_trenders = "select ID, FirstName, LastName, Picture" +
                                " from User " +
                                " where ID=31";


                int limit = 24 - contacts_exist;

                sql_trenders = "select ID, FirstName, LastName, Picture" +
                    " from User " +
                    " where 1=1 AND Active = 1 AND UserType=1 AND LanguageID=" + LanguageID + " AND (ID <> 31) " + not_in + "  LIMIT " + limit;

                //Globals.SaveLog(Database, String.Format("Sql trenders : {0}", sql_trenders), Models.Type.Register, 1);

                IDbCommand command_trenders = Database.CreateCommand(sql_trenders);
                IDataReader reader_trenders = command_trenders.ExecuteReader();

                try
                {
                    while (reader_trenders.Read())
                    {
                        count++;

                        Int64 ID = Database.GetReaderValueInt64(reader_trenders, "ID");
                        /*String FirstName = Database.GetReaderValueString(reader_trenders, "FirstName");
                        String LastName = Database.GetReaderValueString(reader_trenders, "LastName");
                        String Picture = Database.GetReaderValueString(reader_trenders, "Picture");

                        if (Picture != "")
                        {
                            String img_path = ConfigurationManager.AppSettings["PathImages"];
                            String sFile = Globals.PathCombine(img_path, Picture);

                            if (!File.Exists(sFile))
                                Picture = "";
                        }

                        if (Picture == "")
                            Picture = "/Content/images/default_user.png";
                        else
                            Picture = "/Content/files/" + Picture;

                        String sCss = String.Empty;

                        if (count == 8)
                        {
                            sCss = "margin-right-0";
                            count = 0;
                        }*/

                        /*html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowingsTrenders\" name=\"RegisterFollowingsTrenders[]\" value=\"" + ID + "\" />\n";
                        html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                        sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";*/

                        if (count > 1)
                            FollwingsCommaSeparated += ",";

                        FollwingsCommaSeparated += ID;

                        contacts_exist++;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    reader_trenders.Close();
                    reader_trenders.Dispose();
                    command_trenders.Dispose();
                }

                if (contacts_exist < 24)
                {
                    limit = 24 - contacts_exist;

                    sql = "select ID, FirstName, LastName, Picture" +
                        " from User " +
                        " where 1=1 AND Active = 1 AND LanguageID=" + LanguageID + " AND UserType=0 AND (ID <> 31) " + not_in + "  LIMIT " + limit;

                    IDbCommand command_2 = Database.CreateCommand(sql);
                    IDataReader reader_2 = command_2.ExecuteReader();

                    try
                    {
                        while (reader_2.Read())
                        {
                            count++;

                            Int64 ID = Database.GetReaderValueInt64(reader_2, "ID");
                            /*String FirstName = Database.GetReaderValueString(reader_2, "FirstName");
                            String LastName = Database.GetReaderValueString(reader_2, "LastName");
                            String Picture = Database.GetReaderValueString(reader_2, "Picture");

                            if (Picture != "")
                            {
                                String img_path = ConfigurationManager.AppSettings["PathImages"];
                                String sFile = Globals.PathCombine(img_path, Picture);

                                if (!File.Exists(sFile))
                                    Picture = "";
                            }

                            if (Picture == "")
                                Picture = "/Content/images/default_user.png";
                            else
                                Picture = "/Content/files/" + Picture;

                            String sCss = String.Empty;

                            if (count == 8)
                            {
                                sCss = "margin-right-0";
                                count = 0;
                            }

                            html_hiddens += "<input type=\"hidden\" id=\"RegisterFollowings\" name=\"RegisterFollowings[]\" value=\"" + ID + "\" />\n";

                            sHtml += "<div class=\"register-user-image " + sCss + "\"><img src=\"" + Picture + "\" width=\"97px\" /></div>";*/

                            if (count > 1)
                                FollwingsCommaSeparated += ",";

                            FollwingsCommaSeparated += ID;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        reader_2.Close();
                        reader_2.Dispose();
                        command_2.Dispose();
                    }
                }
            }

            return FollwingsCommaSeparated;
        }

        public User GetUserByMail(String mail)
        {
            String sql = String.Format("select ID, Created, Username, Password, UserID, UserLevel, FirstName, LastName, Picture, About, Web, Address, Gender, UserType, FacebookAccessToken, FacebookID, FacebookExpires, TwitterAccessToken, TwitterID, OriginalPicture, RegisterType, Perms, UserToken, LanguageID, VendorID from User where Username='{0}'", mail);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            User user = null;

            try
            {
                user = GetUser(reader);
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return user;
        }


        public String GetUserInteraction(Int64 UserID)
        {
            String sHtml = String.Empty;

            String sql = "SELECT n.ID, n.ToUserID, n.FromUserID, n.RefID, n.NotifyType, n.Created, n.Modified, u.ID as UserID, u.FirstName, u.LastName, u.Picture, u.UserType FROM Notify n, User u WHERE n.ToUserID = " + UserID + " AND n.FromUserID = u.ID Order by Created DESC";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int16 count = 0;

            sHtml += "<div class=\"interaction-entry-container\">";

            ArrayList objs = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    JsonNotifies obj = new JsonNotifies();

                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.ToUserID = Database.GetReaderValueInt64(reader, "ToUserID");
                    obj.FromUserID = Database.GetReaderValueInt64(reader, "FromUserID");
                    obj.RefID = Database.GetReaderValueInt64(reader, "RefID");
                    obj.FirstName = Database.GetReaderValueString(reader, "FirstName");
                    obj.LastName = Database.GetReaderValueString(reader, "LastName");
                    obj.UserPicture = Database.GetReaderValueString(reader, "Picture");
                    obj.NotifyType = (NotifyType)Database.GetReaderValueInt16(reader, "NotifyType");                    
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Modified = Database.GetReaderValueDateTime(reader, "Modified");
                    obj.iUserType = Database.GetReaderValueInt16(reader, "UserType");

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {
                foreach (JsonNotifies notify in objs)
                {
                    count++;

                    if (notify.UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, notify.UserPicture);

                        if (!File.Exists(sFile))
                            notify.UserPicture = "";
                    }

                    if (notify.UserPicture == "")
                        notify.UserPicture = "/Content/images/default_user.png";
                    else
                        notify.UserPicture = "/Content/files/" + notify.UserPicture;

                    String sCss = "interaction-entry";

                    if (count == 2)
                    {
                        sCss = " interaction-entry-on";
                        count = 0;
                    }

                    String sText = "";

                    String Definition = String.Empty;
                    
                    switch(notify.NotifyType)
                    {
                        case NotifyType.ReKit: //Rekit
                            try
                            {
                                KitsRepository ktRP = new KitsRepository(Database);
                                Kit kit = ktRP.GetKit(notify.RefID, UserID);

                                ProductsRepository pRP = new ProductsRepository(Database);
                                Product product = pRP.GetProduct(kit.Product.ID);

                                String url_friendly = String.Format("/kit/timeline/{0}/{1}", Globals.GenerateUrlFriendly(product.Name), kit.ID);

                                sText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HA_REKITEADO_TU_KIT + " <a href=\"{0}\">{1}</a>", url_friendly, product.Name);
                            }
                            catch { }
                            break;
                        case NotifyType.Comment: //KitComment

                            try
                            {
                                KitCommentsRepository kcRP = new KitCommentsRepository(Database);
                                KitComment kc = kcRP.GetKitComment(notify.RefID);

                                KitsRepository ktRP2 = new KitsRepository(Database);
                                Kit kit2 = ktRP2.GetKit(kc.Kit.ID, UserID);

                                ProductsRepository pRP2 = new ProductsRepository(Database);
                                Product product2 = pRP2.GetProduct(kit2.Product.ID);

                                String url_friendly = String.Format("/kit/timeline/{0}/{1}", Globals.GenerateUrlFriendly(product2.Name), kit2.ID);

                                sText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HA_COMENTADO_TU_KIT + " <a href=\"{0}\">{1}</a>", url_friendly, product2.Name);
                            }
                            catch { }
                            break;
                        case NotifyType.Follow: //UserRelation
                            sText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_AHORA_TE_SIGUE);
                            break;
                        case NotifyType.Like: //Like

                            try
                            {
                                UserLikesRepository ulRP = new UserLikesRepository(Database);
                                UserLike ul = ulRP.GetUserLike(notify.RefID);

                                KitsRepository ktRP3 = new KitsRepository(Database);
                                Kit kit3 = ktRP3.GetKit(ul.Kit.ID, UserID);

                                ProductsRepository pRP3 = new ProductsRepository(Database);
                                Product product3 = pRP3.GetProduct(kit3.Product.ID);

                                String url_friendly = String.Format("/kit/timeline/{0}/{1}", Globals.GenerateUrlFriendly(product3.Name), kit3.ID);

                                sText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HA_HECHO_UN_LIKE_A_TU_KIT + " <a href=\"{0}\">{1}</a>", url_friendly, product3.Name);
                            }
                            catch { }
                            break;
                        case NotifyType.CommentedComment: //CommentedKitComment

                            try
                            {
                                /*KitCommentsRepository kcRP = new KitCommentsRepository(Database);
                                KitComment kc2 = kcRP.GetKitComment(notify.RefID);*/

                                KitsRepository ktRP2 = new KitsRepository(Database);
                                Kit kit4 = ktRP2.GetKit(notify.RefID, UserID);

                                ProductsRepository pRP2 = new ProductsRepository(Database);
                                Product product3 = pRP2.GetProduct(kit4.Product.ID);

                                //Obtenim la informació de l'usuari que ha fet el kit
                                UsersRepository ursRP = new UsersRepository(Database);
                                User kituser = ursRP.GetUser(kit4.User.ID);

                                String KitUserName = String.Format("{0} {1}",kituser.FirstName,kituser.LastName);

                                String url_friendly = String.Format("/kit/timeline/{0}/{1}", Globals.GenerateUrlFriendly(product3.Name), kit4.ID);
                                String url_friendly_profile = String.Format("/perfil/{0}/{1}", Globals.GenerateUrlFriendly(KitUserName), kit4.User.ID);

                                sText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_TAMBIEN_HA_COMENTADO_EL_KIT + " <a href=\"{0}\">{1}</a> de <a href=\"{2}\">{3}</a>", url_friendly, product3.Name, url_friendly_profile, KitUserName);
                            }
                            catch { }
                            break;

                    }

                    String strTimeDifference = Globals.GetTimeDifference(notify.Created);

                    sHtml += "<div class=\"" + sCss + "\">" +
                    "<div class=\"interaction-entry-user-image\" onclick=\"Profile(" + notify.FromUserID + ");\"><img src=\"" + notify.UserPicture + "\" width=\"88px\" />";

                    if(notify.iUserType == 1)
                        sHtml += "<div class=\"user-trender-medium-88\"><img src=\"/Content/images/trender.png\" width=\"88\" height=\"88\" /></div>";

                    sHtml += "</div>" +
                    "<div class=\"interaction-entry-user-text\">" + notify.FirstName + " " + notify.LastName + "<br/>" + sText + "</div>" +
                    "<div class=\"interaction-entry-user-date\">" + strTimeDifference + "</div>" +
                    "</div>";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            sHtml += "</div>";

            return sHtml;
        }

        public Int64 GetNumUserInteraction(Int64 UserID)
        {
            String sHtml = String.Empty;

            /*String sql = "(SELECT kt1.ID, kt1.Created, 1 as Type, u.ID as UserID, u.FirstName, u.LastName, u.Picture, kt2.ID as KitID, p.Name as Definition FROM Kit kt1, Kit kt2, User u, Product p where kt1.ParentKitID = kt2.ID AND kt2.UserID = " + UserID + " AND u.ID = kt1.UserID AND p.ID = kt2.ProductID)" +
            " UNION " +
            " (SELECT kc.ID, kc.Created, 2 as Type, u.ID as UserID, u.FirstName, u.LastName, u.Picture, kt.ID as KitID, p.Name as Definition FROM KitComment kc, Kit kt, User u, Product p where kc.KitID = kt.ID AND kt.UserID = " + UserID + " AND u.ID = kt.UserID  AND p.ID = kt.ProductID) " +
            " UNION " +
            " (SELECT ur.ID, ur.Created, 3 as Type, u.ID as UserID, u.FirstName, u.LastName, u.Picture, 0 as KitID, '' as Definition FROM UserRelation ur, User u where ur.FollowID = " + UserID + " AND u.ID = ur.UserID) " +
            " UNION " +
            " (SELECT ul.ID, ul.Created, 4 as Type, u.ID as UserID, u.FirstName, u.LastName, u.Picture, kt.ID as KitID, p.Name as Definition FROM UserLike ul, Kit kt, User u, Product p where ul.KitID = kt.ID AND kt.UserID = " + UserID + " AND u.ID = ul.UserID AND p.ID = kt.ProductID) Order by Created DESC";*/

            String sql = "SELECT ID FROM Notify WHERE ToUserID = " + UserID + " AND Readed = 0";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int16 count = 0;

            try
            {
                while (reader.Read())
                {
                    count++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return count;
        }

        public bool MailExists(String mail)
        {
            if ((mail == String.Empty) || (mail == ""))
                return false;

            String sql = "select ID from User where Username='" + mail + "'";
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                return reader.Read();
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }
        }

        public void UpdateOriginalImage(Int64 UserID, String ImageName)
        {
            String SQL = String.Format("UPDATE User SET Modified='{0}',OriginalPicture='{1}'" +
                                       " WHERE ID={2}",
                                       Database.ParseDateTimeToSQL(DateTime.Now),
                                       Database.ParseStringToSQL(ImageName),
                                       UserID);

            Database.ExecuteCommand(SQL);
        }

        public void UpdateImage(Int64 UserID, String ImageName)
        {
            String SQL = String.Format("UPDATE User SET Modified='{0}',Picture='{1}'" +
                                       " WHERE ID={2}",
                                       Database.ParseDateTimeToSQL(DateTime.Now),
                                       Database.ParseStringToSQL(ImageName),
                                       UserID);

            Database.ExecuteCommand(SQL);
        }

        public void UpdateUserTwitterConnect(User user)
        {
            String SQL = String.Format("UPDATE User SET Modified='{0}',TwitterAccessToken='{1}',TwitterID='{2}'" +
                                                   " WHERE ID={3}",
                                                   Database.ParseDateTimeToSQL(DateTime.Now),
                                                   Database.ParseStringToSQL(user.TwitterAccessToken),
                                                   Database.ParseStringToSQL(user.TwitterID),
                                                   user.ID);

            Database.ExecuteCommand(SQL);
        }

        public void UpdateUserFacebookConnect(User user)
        {
            String SQL = String.Format("UPDATE User SET Modified='{0}',FacebookAccessToken='{1}',FacebookID='{2}'" +
                                                   " WHERE ID={3}",
                                                   Database.ParseDateTimeToSQL(DateTime.Now),
                                                   Database.ParseStringToSQL(user.FacebookAccessToken),
                                                   Database.ParseStringToSQL(user.FacebookID),
                                                   user.ID);

            Database.ExecuteCommand(SQL);
        }

        public String GetRecommendedSearchPeople()
        {
            String sHtml = String.Empty;

            String sql = String.Format("select a.ID, a.FirstName, a.LastName, a.Picture, " +
                    "(select count(b.ID) from Kit b where b.UserID=a.ID and b.ParentKitID is NULL)+ " +
                    "(select count(c.ID) from Kit c where c.UserID=a.ID and c.ParentKitID is not NULL)+ " +
                    "(select count(d.ID) from UserLike d where d.UserID=a.ID)+ " +
                    "(select count(e.ID) from KitComment e where e.UserID=a.ID) as Total " +
                    "from User a " +
                    "order by total desc limit 8");

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            Int16 count = 0;

            try
            {
                while (reader.Read())
                {
                    count++;

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String Picture = Database.GetReaderValueString(reader, "Picture");

                    if (Picture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, Picture);

                        if (!File.Exists(sFile))
                            Picture = "";
                    }

                    if (Picture == "")
                        Picture = "/Content/images/default_user.png";
                    else
                        Picture = "/Content/files/" + Picture;

                    String sCss = String.Empty;

                    if (count == 8)
                    {
                        sCss = " search-people-recommended-users-entry-short ";
                    }

                    sHtml += "<div class=\"search-people-recommended-users-entry " + sCss + "\" onclick=\"parent.location.href='/Timeline/Profile/" + ID + "';\"><img src=\"" + Picture + "\" width=\"88px\" /></div>";

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return sHtml;
        }

        public String InviteProcessContacts(List<InviteContact> contacts, Int64 SessionUserID, InviteType inviteType)
        {
            String sHtml = String.Empty;

            ArrayList likekit_users = new ArrayList();
            ArrayList no_likekit_users = new ArrayList();

            try
            {
                foreach (InviteContact contact in contacts)
                {
                    String mail = contact.Email;
                    String Name = contact.Name;
                    User user = null;

                    if (inviteType == InviteType.Facebook)
                    {
                        user = GetUserByFacebookID(mail);
                    }
                    else
                        user = GetUserByMail(mail);

                    if (user != null)
                        likekit_users.Add(user);
                    else
                        no_likekit_users.Add(contact);
                }

                int count_results = 0;
                bool odd = false;

                if (likekit_users.Count > 0)
                {
                    sHtml += String.Format("<div class=\"invite-likekit-users-header\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_TIENES_X_CONTACTOS_QUE_ESTAN_EN_LIKEKIT + "</div>", likekit_users.Count);
                }

                likekit_users.Sort();

                //Primer parsegem els usuaris de likekit
                foreach (User user in likekit_users)
                {
                    String sCss = "";
                    String sCssType = "invite-result-entry-odd";

                    if (count_results == 0)
                        sCss = "invite-result-border-top";

                    if (odd == false)
                    {
                        sCssType = "";
                        odd = true;
                    }
                    else
                    {
                        sCssType = "invite-result-entry-odd";
                        odd = false;
                    }

                    String sDisplayName = String.Format("{0} {1}", user.FirstName, user.LastName);

                    if (user.Picture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, user.Picture);

                        if (!File.Exists(sFile))
                            user.Picture = "";
                    }

                    if (user.Picture == "")
                        user.Picture = "/Content/images/default_user.png";
                    else
                        user.Picture = "/Content/files/" + user.Picture;

                    sHtml += "<div class=\"invite-result-entry " + sCss + sCssType + "\">" +
                        "<div class=\"invite-result-entry-image\" onclick=\"parent.location.href='/Timeline/Profile/" + user.ID + "';\">" +
                        "<img src=\"" + user.Picture + "\" width=\"28px\" alt=\"" + sDisplayName + "\" title=\"" + sDisplayName + "\" />\n" +
                        "</div>" +
                        "<div class=\"invite-result-entry-data\">" +
                        "<div class=\"invite-result-entry-name\">" + sDisplayName + "</div>" +
                        "<div class=\"invite-result-entry-location\">" + user.Address + "</div>" +
                        "</div>";

                    if (SessionUserID != user.ID)
                    {
                        UserRelationsRepository urRP = new UserRelationsRepository(Database);

                        //Hem de comprovar si el seguim o no
                        if (urRP.isFollower(SessionUserID, user.ID))
                            sHtml += "<div id=\"invite-user-btn-" + user.ID + "\" class=\"invite-result-entry-following invite-result-entry-btn-on\" onclick=\"InviteUnFollow(" + user.ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SIGUIENDO_MIN + "</div>\n";
                        else
                            sHtml += "<div id=\"invite-user-btn-" + user.ID + "\" class=\"invite-result-entry-follow invite-result-entry-btn\" onclick=\"InviteFollow(" + user.ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SEGUIR + "</div>\n";
                    }

                    sHtml += "</div>";

                }

                if (no_likekit_users.Count > 0)
                {
                    sHtml += String.Format("<div class=\"invite-no-likekit-users-header\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_TIENES_X_CONTACTOS_QUE_NO_ESTAN_EN_LIKEKIT + "</div>", no_likekit_users.Count);
                }


                count_results = 0;
                odd = false;

                no_likekit_users.Sort();

                //Primer parsegem els usuaris de likekit
                foreach (InviteContact contact in no_likekit_users)
                {
                    String sCssType = "invite-nolikekit-result-entry-odd";


                    if (odd == false)
                    {
                        sCssType = "";
                        odd = true;
                    }
                    else
                    {
                        sCssType = "invite-nolikekit-result-entry-odd";
                        odd = false;
                    }

                    String sDisplayName = String.Empty;
                    if (contact.Name != null)
                        sDisplayName = contact.Name;
                    else
                        sDisplayName = contact.Email;

                    sHtml += "<div class=\"invite-nolikekit-result-entry " + sCssType + "\">" +
                        "<div class=\"invite-nolikekit-result-name\">" + sDisplayName + "</div>";

                    if (inviteType == InviteType.Facebook)
                        sHtml += "<div id=\"invite-nolikekit-result-link-" + count_results + "\" class=\"invite-nolikekit-result-link\" onclick=\"SendFacebookInvitation('" + contact.Email + "'," + count_results + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ENVIAR_INVITACION + "</div>";
                    else
                        sHtml += "<div id=\"invite-nolikekit-result-link-" + count_results + "\" class=\"invite-nolikekit-result-link\" onclick=\"SendInvitation('" + contact.Email + "'," + count_results + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ENVIAR_INVITACION + "</div>";
                    sHtml += "</div>";

                    count_results++;
                }

            }
            catch (Exception ex)
            {
                throw ex; 
            }

            return sHtml;
        }

        public User GetUserByUserToken(String UserToken)
        {
            String sql = String.Format("select ID, Created, Username, Password, UserID, UserLevel, FirstName, LastName, Picture, About, Web, Address, Gender, UserType, FacebookAccessToken, FacebookID, FacebookExpires, TwitterAccessToken, TwitterID, OriginalPicture, RegisterType, Perms, UserToken, LanguageID, VendorID from User where UserToken='{0}'", UserToken);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            User user = null;

            try
            {
                user = GetUser(reader);
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return user;
        }

        public static void UpdateLastLogin(DatabaseBase database,Int64 UserID)
        {
            //AccessLog
            AccessLog al = new AccessLog();
            AccessLogsRepository alRP = new AccessLogsRepository(database);

            al.DeviceType = DeviceType.Web;
            al.ObjectType = AccesLogObjectType.Login;
            al.OperationType = OperationType.Login;

            User user = new User
            {
                ID = UserID
            };

            al.User = user;
            al.RefID = 0;
            al.IP = String.Empty;
            al.SessionID = String.Empty;
            al.Observations = String.Empty;

            alRP.CreateAccessLog(al);

            String SQL = String.Format("UPDATE User SET LastLogin='{0}' WHERE ID={1}",
                                       database.ParseDateTimeToSQL(DateTime.Now),
                                       UserID);

            database.ExecuteCommand(SQL);
        }
    }
}