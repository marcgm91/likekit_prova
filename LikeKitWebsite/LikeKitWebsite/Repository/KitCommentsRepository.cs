﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Collections;

namespace LikeKitWebsite.Repository
{
    /// <summary>
    /// Objecte amb la informació dels kitComments
    /// </summary>
    public class JsonKitComment
    {
        public Int64 ID = 0;
        public Int64 KitID = 0;
        public Int64 UserID = 0;
        public String UserName = String.Empty;
        public String Comment = String.Empty;
        public DateTime Created = DateTime.Now;
        public bool Visible = true;
    }

    public class KitCommentsRepository : BaseRepository
    {
        public KitCommentsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private KitComment GetKitComment(IDataReader reader)
        {
            if (reader == null)
                return null;

            reader.Read();

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 KitID = Database.GetReaderValueInt64(reader, "KitID");
            Kit _kit = null;

            if (KitID > 0)
            {
                _kit = new Kit
                {
                    ID = KitID
                };
            }

            KitComment kitComment = new KitComment
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Kit = _kit,
                Comment = Database.GetReaderValueString(reader, "Comment"),
                //Valoration = Database.GetReaderValueDouble(reader, "Valoration"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified"),
                Visible = Database.GetReaderValueInt16(reader, "Visible") == 1
            };

            return kitComment;
        }

        public KitComment GetKitComment(Int64 id)
        {
            String sql = "select ID, KitID, UserID, Created, Modified, Comment, Visible " +
               " from KitComment WHERE ID = " + id;

            KitComment kitComment = new KitComment();

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                kitComment = GetKitComment(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return kitComment;
        }

        public Int64 CreateKitComment(KitComment kitComment)
        {
            Int64 KitID = 0;

            if (kitComment.Kit != null)
                KitID = kitComment.Kit.ID;

            String SQL = String.Format("INSERT INTO KitComment (KitID, UserID, Created, Modified, Comment, Visible) " +
                "values ({0},{1},'{2}','{3}','{4}',{5})",
                kitComment.Kit.ID,
                kitComment.User.ID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseStringToSQL(kitComment.Comment),
                kitComment.Visible ? 1 : 0
                );

            Database.ExecuteCommand(SQL);

            kitComment.ID = Database.GetLastAutoIncrement();

            return kitComment.ID;
        }

        public void UpdateKitComment(KitComment KitComment)
        {
            Int64 KitID = 0;

            if (KitComment.Kit != null)
                KitID = KitComment.Kit.ID;

            Int64 UserID = 0;

            if (KitComment.User != null)
                UserID = KitComment.User.ID;

            String SQL = String.Format("UPDATE KitComment SET KitID={0}," +
                                       "UserID={1},Valoration='{2}',Comment='{3}',Visible={4}, " +
                                       "Created='{5}',Modified='{6}' " +
                                       " WHERE ID={7}",
                                       KitID,
                                       UserID,
                                       KitComment.Valoration,
                                       Database.ParseStringToSQL(KitComment.Comment),
                                       KitComment.Visible ? 1 : 0,
                                       Database.ParseDateTimeToSQL(KitComment.Created),
                                       Database.ParseDateTimeToSQL(System.DateTime.Now),
                                       KitComment.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteKitComment(KitComment KitComment)
        {
            String SQL = String.Format("DELETE FROM KitComment " +
                                       " WHERE ID={0}",
                                        KitComment.ID);

            Database.ExecuteCommand(SQL);
        }

        public ArrayList GetKitComments(Int64 KitID, int limit)
        {
            String sql = String.Format("select KitComment.*, User.UserName as UserName from KitComment, User where KitComment.KitID = {0} AND KitComment.UserID = User.ID Order by ID ASC LIMIT 0,{1}", KitID, limit);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList kitComents = new ArrayList();

            try
            {
                JsonKitComment obj;

                while (reader.Read())
                {
                    obj = new JsonKitComment();
                    obj.ID = Database.GetReaderValueInt64(reader, "ID");
                    obj.KitID = Database.GetReaderValueInt64(reader, "KitID");
                    obj.UserID = Database.GetReaderValueInt64(reader, "UserID");
                    obj.UserName = Database.GetReaderValueString(reader, "UserName");
                    obj.Comment = Database.GetReaderValueString(reader, "Comment");
                    obj.Created = Database.GetReaderValueDateTime(reader, "Created");
                    obj.Visible = Database.GetReaderValueInt(reader, "Visible") == 1;

                    kitComents.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return kitComents;
        }

        public Int64 GetNumKitComments(Int16 KitID)
        {
            if (KitID <= 0)
                return 0;

            Int64 nKitComments = 0;

            String sql = String.Format("select count(ID) as count from KitComment where KitID={0}", KitID);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    nKitComments = Database.GetReaderValueInt64(reader, "count");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return nKitComments;
        }
    }
}