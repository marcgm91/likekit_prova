﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Collections;
using System.Configuration;
using System.IO;

namespace LikeKitWebsite.Repository
{
    public class UserStatusRepository : BaseRepository
    {
        public UserStatusRepository(DatabaseBase database)
        {
            Database = database;
        }

        private UserStatus GetUserStatus(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
            User _user = null;

            if (UserID > 0)
            {
                _user = new User
                {
                    ID = UserID
                };
            }

            Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
            Product _product = null;

            if (ProductID > 0)
            {
                _product = new Product
                {
                    ID = ProductID
                };
            }

            UserStatus userStatus = new UserStatus
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                User = _user,
                Product = _product,
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return userStatus;
        }

        public void CreateUserStatus(UserStatus UserStatus)
        {
            Int64 UserID = 0;

            if (UserStatus.User != null)
                UserID = UserStatus.User.ID;

            Int64 ProductID = 0;

            if (UserStatus.Product != null)
                ProductID = UserStatus.Product.ID;

            String SQL = String.Format("INSERT INTO UserStatus (UserID, ProductID, Created, Modified) " +
                "values ({0},{1},'{2}','{3}')",
                UserID,
                ProductID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateUserStatus(UserStatus UserStatus)
        {
            Int64 UserID = 0;

            if (UserStatus.User != null)
                UserID = UserStatus.User.ID;

            Int64 ProductID = 0;

            if (UserStatus.Product != null)
                ProductID = UserStatus.Product.ID;

            String SQL = String.Format("UPDATE UserStatus SET UserID={0}," +
                                       "ProductID={1},Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                        UserID,
                                        ProductID,
                                        Database.ParseDateTimeToSQL(UserStatus.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        UserStatus.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteUserStatus(UserStatus UserStatus)
        {
            String SQL = String.Format("DELETE FROM UserStatus " +
                                       " WHERE ID={0}",
                                        UserStatus.ID);

            Database.ExecuteCommand(SQL);
        }

        public String GetHtmlProfileNowWith(Int64 UserID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;
            String sHtmlClose = String.Empty;
            String sButtonClass = String.Empty;

            String sql = "select us.ID, us.UserID, us.ProductID, pr.Name as ProductName,pr.Image as ProductImage " +
            " from UserStatus us left join Product pr on us.ProductID = pr.ID " +
            " where 1=1 AND us.UserID = " + UserID;

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            sHtml = "<div class=\"profile-now-with-images\">\n";

            int count = 0;
            try
            {
                while (reader.Read())
                {
                    Int64 UserStatusID = Database.GetReaderValueInt64(reader, "ID");
                    String ProductPicture = Database.GetReaderValueString(reader, "ProductImage");

                    if (ProductPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductPicture);

                        if (!File.Exists(sFile))
                            ProductPicture = "";
                    }

                    if (ProductPicture == "")
                        ProductPicture = "/Content/images/default_product.png";
                    else
                        ProductPicture = "/Content/files/" + ProductPicture;

                    String sCss = String.Empty;

                    if (count > 0)
                        sCss = "profile-now-with-images-entry-left";

                    String image_width = "87px";
                    if(count == 0)
                        image_width = "88px";

                    sHtml += "<div class=\"profile-now-with-images-entry " + sCss + "\">\n" +
                        "<div class=\"profile-now-with-images-entry-img\"><img src=\"" + ProductPicture + "\" width=\"" + image_width + "\"/></div>\n";

                    //Mirem si es pot editar
                    if(UserID == SessionUserID)
                    {
                        sHtml += "<div class=\"profile-now-with-close\" onclick=\"DelNowWith(" + UserStatusID + ");\">\n" +
                        "<img src=\"/Content/images/close-now-with.png\" width=\"18px\" class=\"profile-now-with-close-img\"/>\n" +
                        "</div>\n" +
                        "<div class=\"profile-now-with-tip\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ELIMINA + "</div>\n";
                    }

                    sHtml += "</div>\n";

                    count++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            if ((count < 8) && (UserID == SessionUserID))
            {
                int adds = 8 - count;


                for (int j = 0; j < adds; j++)
                {
                    String sCss = String.Empty;

                    if ((adds < 8) || (j > 0))
                        sCss = "profile-now-with-images-entry-left";

                    sHtml += "<div class=\"profile-now-with-images-entry " + sCss + "\">\n" +
                    "<div class=\"profile-now-with-add\" onclick=\"AddNowWith();\">\n" +
                        "<img src=\"/Content/images/profile-now-with-add.png\" width=\"24px\" class=\"profile-now-with-add-img\"/>\n" +
                    "</div>\n" +
                    "<div class=\"profile-now-with-tip-add\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_NUEVO + "</div>\n" +
                    "</div>\n";
                }
            }

            if ((count < 8) && (UserID != SessionUserID))
            {
                int adds = 8 - count;


                for (int j = 0; j < adds; j++)
                {
                    String sCss = String.Empty;

                    if ((adds < 8) || (j > 0))
                        sCss = "profile-now-with-images-entry-left";

                    sHtml += "<div class=\"profile-now-with-images-entry " + sCss + "\">\n" +
                    "</div>\n";
                }
            }

            sHtml += "</div>";

            return sHtml;
        }

        public String GetNowWith(Int64 UserID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;

            String sql = "select us.ID, us.UserID, us.ProductID, pr.Name as ProductName,pr.Image as ProductImage " +
            " from UserStatus us left join Product pr on us.ProductID = pr.ID " +
            " where 1=1 AND us.UserID = " + UserID +  " order by us.Created DESC";

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList objs = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    JsonKits obj = new JsonKits();

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    String ProductName = Database.GetReaderValueString(reader, "ProductName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = "/Content/images/default_product.png";
                    else
                        ProductImage = "/Content/files/" + ProductImage;

                    obj.ID = ID;
                    obj.UserID = UserID;
                    obj.ProductID = ProductID;
                    obj.ProductName = ProductName;
                    obj.ProductImage = ProductImage;

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {
                int count = 0;
                foreach (JsonKits kit in objs)
                {

                    String display_name = String.Format("{0} {1}", kit.FirstName, kit.LastName);

                    sHtml += "<div class=\"nowwith-entry\">\n" +
                        "<div class=\"nowwith-entry-image\">\n" +
                            "<img src=\"" + kit.ProductImage + "\" width=\"220px\" alt=\"" + kit.ProductName + "\" title=\"" + kit.ProductName + "\" />\n" +
                            "<div id=\"nowwith-entry-info-" + kit.ID + "\" class=\"nowwith-entry-info\">\n" +
                                "<div class=\"nowwith-entry-info-title\">" + kit.ProductName + "</div>\n" +
                                "<div class=\"nowwith-entry-info-kit-link-container\">\n" +
                                    "<div class=\"nowwith-entry-info-kit-link-container-border\">\n" +
                                        "<div class=\"nowwith-entry-info-kit-link\" onclick=\"ProductKit(" + kit.ProductID + ");\">Kit</div>\n" +
                                    "</div>\n" +
                                "</div>\n";
                    
                    if(UserID == SessionUserID)
                        sHtml += "<div class=\"nowwith-entry-info-delete-link\" onclick=\"DelNowWith(" + kit.ID + "," + kit.UserID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_ELIMINAR + "</div>\n";
                    sHtml += "</div>\n";
                    sHtml += "</div>\n" +
                    "</div>\n";
                    count++;
                }

                //Si som l'usuari del now with, i no hi ha 8 elements, hem de posar els elements per afegir
                if ((UserID == SessionUserID) && (count < 8))
                {
                    int num_adds = 8 - count;

                    for (int i = 0; i < num_adds; i++)
                    {
                        sHtml += "<div class=\"nowwith-entry\">\n" +
                            "<div class=\"nowwith-entry-add-content\" onclick=\"AddNowWith(" + SessionUserID + ");\"></div>\n" +
                        "</div>\n";                    
                    
                    }
                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }
    }
}