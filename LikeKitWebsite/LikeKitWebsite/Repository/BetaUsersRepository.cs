﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Configuration;

namespace LikeKitWebsite.Repository
{
    public class BetaUsersRepository : BaseRepository
    {
        public BetaUsersRepository()
        {
        }

        public BetaUsersRepository(DatabaseBase database)
        {
            Database = database;
        }

        private BetaUser GetBetaUser(IDataReader reader)
        {
            if (reader == null)
                return null;

            reader.Read();

            BetaUser betaUser = new BetaUser
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Mail = Database.GetReaderValueString(reader, "Mail"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return betaUser;
        }

        public BetaUser GetBetaUser(Int64 id)
        {
            String sql = String.Format("select * from BetaUser where ID={0}", id);
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            return GetBetaUser(reader);
        }

        public bool Exists(String mail)
        {
            if ((mail == String.Empty) || (mail == ""))
                return false;

            String sql = "select ID from User where Username='" + mail + "'";
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                return reader.Read();
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }
        }

        public BetaUserResult CreateBetaUser(BetaUser BetaUser)
        {
            BetaUserResult result = new BetaUserResult();


            if (!Exists(BetaUser.Mail))
            {
                String SQL = String.Format("INSERT INTO BetaUser (Mail, Created, Modified) " +
                    "values ('{0}','{1}','{2}')",
                    Database.ParseStringToSQL(BetaUser.Mail),
                    Database.ParseDateTimeToSQL(System.DateTime.Now),
                    Database.ParseDateTimeToSQL(System.DateTime.Now)
                    );

                Database.ExecuteCommand(SQL);

                //Carreguem les dades del template
                String Subject = String.Empty;
                String Body = String.Empty;
                String To = String.Empty;
                String From = String.Empty;

                MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Database);
                MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.UserBeta, 1);

                mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

                bool mail_sent = true;
                //Enviem un correu
                try
                {
                    Globals.SendMail(Database, mail_template.Subject, mail_template.Body, BetaUser.Mail);
                }
                catch (Exception ex)
                {
                    //Guardem log
                    String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                    Globals.SaveLog(Database, ex_msg, Models.Type.BetaSendMail, 1);
                    mail_sent = false;

                    result.Success = false;
                    result.Message = String.Format("ErrorMail");
                }

                if (mail_sent)
                {
                    result.Success = true;
                    //result.Message = String.Format("Petición enviada");
                    result.Message = String.Format("Success");
                }
            }
            else
            {
                result.Success = false;
                //result.Message = String.Format("Éste correo ya está inscrito, grácias");
                result.Message = String.Format("Error");
            }

            return result;
        }
    }
}