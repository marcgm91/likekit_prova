﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class KitTagsRepository : BaseRepository
    {
        public KitTagsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private KitTag GetKitTag(IDataReader reader)
        {
            if (reader == null)
                return null;

            Int64 KitID = Database.GetReaderValueInt64(reader, "KitID");
            Kit _kit = null;

            if (KitID > 0)
            {
                _kit = new Kit
                {
                    ID = KitID
                };
            }

            KitTag kitTag = new KitTag
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                Kit = _kit,
                Name = Database.GetReaderValueString(reader, "Name"),
                Created = Database.GetReaderValueDateTime(reader, "Created"),
                Modified = Database.GetReaderValueDateTime(reader, "Modified")
            };

            return kitTag;
        }

        public void CreateKitTag(KitTag KitTag)
        {
            Int64 KitID = 0;

            if (KitTag.Kit != null)
                KitID = KitTag.Kit.ID;

            String SQL = String.Format("INSERT INTO KitTag (KitID, Created, Modified, Name) " +
                "values ({0},'{1}','{2}','{3}')",
                KitTag.Kit.ID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseStringToSQL(KitTag.Name)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateKitTag(KitTag KitTag)
        {
            Int64 KitID = 0;

            if (KitTag.Kit != null)
                KitID = KitTag.Kit.ID;

            String SQL = String.Format("UPDATE KitTag SET KitID={0}," +
                                       "Name='{1}',Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                       KitID,
                                       Database.ParseStringToSQL(KitTag.Name),
                                       Database.ParseDateTimeToSQL(KitTag.Created),
                                       Database.ParseDateTimeToSQL(System.DateTime.Now),
                                       KitTag.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteTag(KitTag KitTag)
        {
            String SQL = String.Format("DELETE FROM KitTag " +
                                       " WHERE ID={0}",
                                        KitTag.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}