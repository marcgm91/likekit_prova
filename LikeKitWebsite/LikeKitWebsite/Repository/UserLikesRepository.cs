﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;
using System.Collections;
using System.Configuration;
using System.IO;

namespace LikeKitWebsite.Repository
{
    public class UserLikesRepository : BaseRepository
    {
        public UserLikesRepository(DatabaseBase database)
        {
            Database = database;
        }

        private UserLike GetUserLike(IDataReader reader)
        {
            if (reader == null)
                return null;

            UserLike UserLike = null;
            
            if (reader.Read())
            {

                Int64 UserID = Database.GetReaderValueInt64(reader, "UserID");
                User _user = null;

                if (UserID > 0)
                {
                    _user = new User
                    {
                        ID = UserID
                    };
                }
   
                Int64 KitID = Database.GetReaderValueInt64(reader, "KitID");
                Kit _kit = null;

                if (KitID > 0)
                {
                    _kit = new Kit
                    {
                        ID = KitID
                    };
                }

                UserLike = new UserLike
                {
                    ID = Database.GetReaderValueInt64(reader, "ID"),
                    User = _user,
                    Kit = _kit,
                    Created = Database.GetReaderValueDateTime(reader, "Created"),
                    Modified = Database.GetReaderValueDateTime(reader, "Modified")
                };
            }
            
            return UserLike;
        }

        public UserLike GetUserLike(Int64 id)
        {
            String sql = "select ID, UserID, KitID, Created, Modified " +
               " from UserLike WHERE ID = " + id;

            UserLike userLike = new UserLike();

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            try
            {
                userLike = GetUserLike(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return userLike;
        }

        public Int64 CreateUserLike(UserLike UserLike)
        {
            Int64 UserID = 0;

            if (UserLike.User != null)
                UserID = UserLike.User.ID;

            Int64 KitID = 0;

            if (UserLike.Kit != null)
                KitID = UserLike.Kit.ID;

            String SQL = String.Format("INSERT INTO UserLike (UserID, KitID, Created, Modified) " +
                "values ({0},{1},'{2}','{3}')",
                UserID,
                KitID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);

            return GetLastAutoIncrement();
        }

        public void CreateUserLikeProduct(UserLikeProduct UserLikeProduct)
        {
            Int64 UserID = 0;

            if (UserLikeProduct.User != null)
                UserID = UserLikeProduct.User.ID;

            Int64 ProductID = 0;

            if (UserLikeProduct.Product != null)
                ProductID = UserLikeProduct.Product.ID;

            String SQL = String.Format("INSERT INTO UserLikeProduct (UserID, ProductID, Created, Modified) " +
                "values ({0},{1},'{2}','{3}')",
                UserID,
                ProductID,
                Database.ParseDateTimeToSQL(System.DateTime.Now),
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateUserLike(UserLike UserLike)
        {
            Int64 UserID = 0;

            if (UserLike.User != null)
                UserID = UserLike.User.ID;

            Int64 KitID = 0;

            if (UserLike.Kit != null)
                KitID = UserLike.Kit.ID;

            String SQL = String.Format("UPDATE UserLike SET UserID={0}," +
                                       "KitID={1},Created='{2}',Modified='{3}' " +
                                       " WHERE ID={4}",
                                        UserID,
                                        KitID,
                                        Database.ParseDateTimeToSQL(UserLike.Created),
                                        Database.ParseDateTimeToSQL(System.DateTime.Now),
                                        UserLike.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteUserLike(UserLike UserLike)
        {
            String SQL = String.Format("DELETE FROM UserLike " +
                                       " WHERE ID={0}",
                                        UserLike.ID);

            Database.ExecuteCommand(SQL);
        }

        public Int64 Exists(Int64 UserID, Int64 KitID)
        {
            if ((UserID < 0) || (KitID < 0))
                return 0;

            String sql = "select ID from UserLike where UserID=" + UserID + " AND KitID=" + KitID;
            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    return Database.GetReaderValueInt64(reader, "ID");
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            return 0;
        }

        public String GetHtmlLikes(Int64 UserID, Int64 SessionUserID)
        {
            String sHtml = String.Empty;

            String sql = "select kt.ID, kt.UserID, kt.ProductID, kt.Comment, kt.Valoration, usr.UserName, usr.FirstName, usr.LastName, usr.Picture as UserPicture, pr.Name as ProductName,pr.Image as ProductImage, pr.ProductCategoryID, " +
                " (Select count(ID) from Kit rekt where rekt.ParentKitID = kt.ID) as num_kits, " +
                " (Select count(ID) from KitComment ktcm where ktcm.KitID = kt.ID) as num_comments, " +
                " (Select count(ID) from UserLike ul where ul.KitID = kt.ID) as num_likes " +
                " from UserLike ul left join Kit kt on ul.KitID = kt.ID left join User usr on kt.UserID = usr.ID " +
                " left join Product pr on kt.ProductID = pr.ID " +
                " where 1=1";


            String sql_users = String.Empty;
            String sql_categories = String.Empty;
            String sql_relation = String.Empty;
            String sql_order_by = String.Empty;


            sql += " AND ul.UserID = " + UserID;

            sql_order_by = String.Format(" order by ul.Modified DESC");

            sql += String.Format("{0} {1} {2} {3}", sql_users, sql_categories, sql_relation, sql_order_by);

            IDbCommand command = Database.CreateCommand(sql);
            IDataReader reader = command.ExecuteReader();

            ArrayList objs = new ArrayList();

            try
            {
                while (reader.Read())
                {
                    JsonKits obj = new JsonKits();

                    Int64 ID = Database.GetReaderValueInt64(reader, "ID");
                    Int64 UserLikeUserID = Database.GetReaderValueInt64(reader, "UserID");
                    Int64 ProductID = Database.GetReaderValueInt64(reader, "ProductID");
                    String UserName = Database.GetReaderValueString(reader, "UserName");
                    String FirstName = Database.GetReaderValueString(reader, "FirstName");
                    String LastName = Database.GetReaderValueString(reader, "LastName");
                    String UserPicture = Database.GetReaderValueString(reader, "UserPicture");
                    String ProductName = Database.GetReaderValueString(reader, "ProductName");
                    String ProductImage = Database.GetReaderValueString(reader, "ProductImage");
                    Int16 NumKits = Database.GetReaderValueInt16(reader, "num_kits");
                    Int16 NumComments = Database.GetReaderValueInt16(reader, "num_comments");
                    Int16 NumLikes = Database.GetReaderValueInt16(reader, "num_likes");
                    Int64 ProductCategoryID = Database.GetReaderValueInt64(reader, "ProductCategoryID");
                    String Comment = Database.GetReaderValueString(reader, "Comment");
                    Int16 Valoration = Database.GetReaderValueInt16(reader, "Valoration");

                    if (UserPicture != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, UserPicture);

                        if (!File.Exists(sFile))
                            UserPicture = "";
                    }

                    if (UserPicture == "")
                        UserPicture = "/Content/images/default_user.png";
                    else
                        UserPicture = "/Content/files/" + UserPicture;

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = "/Content/images/default_product.png";
                    else
                        ProductImage = "/Content/files/" + ProductImage;

                    obj.ID = ID;
                    obj.UserID = UserLikeUserID;
                    obj.ProductID = ProductID;
                    obj.UserName = UserName;
                    obj.FirstName = FirstName;
                    obj.LastName = LastName;
                    obj.UserPicture = UserPicture;
                    obj.ProductName = ProductName;
                    obj.ProductImage = ProductImage;
                    obj.NumKits = NumKits;
                    obj.NumComments = NumComments;
                    obj.NumLikes = NumLikes;
                    obj.ProductCategoryID = ProductCategoryID;
                    obj.Comment = Comment;
                    obj.Valoration = Valoration;

                    objs.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                command.Dispose();
            }

            try
            {
                foreach (JsonKits kit in objs)
                {

                    String display_name = String.Format("{0} {1}", kit.FirstName, kit.LastName);

                    sHtml += "<div class=\"timeline-entry\">\n" +
                        "<div class=\"timeline-entry-image\">\n" +
                            "<img src=\"" + kit.ProductImage + "\" width=\"220px\" alt=\"" + kit.ProductName + "\" title=\"" + kit.ProductName + "\" />\n" +
                            "<div id=\"timeline-entry-info-" + kit.ID + "\" class=\"timeline-entry-info\">\n" +
                                "<div class=\"timeline-entry-info-title\">" + kit.ProductName + "</div>\n" +
                                "<div class=\"timeline-entry-info-kit-link-container\">\n" +
                                    "<div class=\"timeline-entry-info-kit-link-container-border\">\n" +
                                    "<div class=\"timeline-entry-info-kit-link\" onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_IR_AL_KIT + "</div>\n";

                    if (SessionUserID == kit.UserID)
                        sHtml += "<div class=\"timeline-entry-info-kit-link\" onclick=\"EditKit(" + kit.ID + ");\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_EDITA_TU_KIT + "</div>\n";
                    sHtml += "</div>\n";
                    sHtml += "</div>\n";

                    //Mirem si l'usuari ja ha fet un like o no
                    UserLikesRepository ulRP = new UserLikesRepository(Database);
                    Int64 ul = ulRP.Exists(SessionUserID, kit.ID);
                    //if (ul > 0)
                        
                    if(UserID == SessionUserID)
                        sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-dislike-link\" onclick=\"DisLikeReload(" + kit.ID + "," + ul + ");\">Dislike</div>\n";
                    /*else
                        sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-dislike-link\" onclick=\"DisLike(" + kit.ID + "," + ul + ");\">Dislike</div>\n";*/
                    //sHtml += "<div id=\"timeline-kit-like-" + kit.ID + "\" class=\"timeline-entry-info-like-link\" onclick=\"Like(" + kit.ID + ");\">Like</div>\n";

                    String sCssCategory = String.Empty;

                    switch (kit.ProductCategoryID)
                    {
                        case 1:
                            sCssCategory = "book-category-image";
                            break;
                        case 2:
                            sCssCategory = "book-serie-image";
                            break;
                        case 3:
                            sCssCategory = "book-cinema-image";
                            break;
                        case 4:
                            sCssCategory = "book-music-image";
                            break;
                    }


                    sHtml += "<div class=\"timeline-entry-info-comment-link\" onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "#kit-view-container-comment-entry-container';\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTAR + "</div>\n" +
                                "<div class=\"timeline-entry-info-rekit-link\" onclick=\"ReKit(event," + kit.ID + ");\">ReKit</div>\n" +
                            "</div>\n" +
                        "</div>\n" +
                        "<div class=\"timeline-entry-info-user\">\n" +
                            "<div class=\"timeline-entry-info-user-image\" onclick=\"Profile(" + kit.UserID + ");\"><img src=\"" + kit.UserPicture + "\" width=\"28px\" alt=\"" + display_name + "\" title=\"" + display_name + "\" /></div>\n" +
                            "<div class=\"timeline-entry-info-username\" onclick=\"Profile(" + kit.UserID + ");\">" + display_name + "</div>\n" +
                            "<div class=\"timeline-entry-info-category-image " + sCssCategory + "\"></div>\n" +
                        "</div>\n" +

                    "<div class=\"timeline-entry-info-user-comment\" onclick=\"Kit(" + kit.ID + ");\">\n";

                    sHtml += "<div class=\"timeline-entry-info-user-comment-text\">" + Globals.WordCut(kit.Comment, 200, new char[] { ' ' }) + "</div>\n";
                    sHtml += "<div class=\"timeline-entry-info-user-comment-img\"><img src=\"/Content/images/kit-view-valoration-" + kit.Valoration + ".png\" /></div>\n";

                    sHtml += "</div>\n" +
                        "<div class=\"timeline-entry-options\">\n" +
                            "<div class=\"timeline-entry-options-likes\"><span class=\"timeline-entry-options-number\">" + kit.NumLikes + "</span> Likes</div>\n" +
                            "<div class=\"timeline-entry-options-comments\"  onclick=\"parent.location.href='/Timeline/Item/" + kit.ID + "#kit-view-container-comments';\"><span class=\"timeline-entry-options-number\">" + kit.NumComments + "</span> " + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_COMENTARIOS + "</div>\n" +
                            "<div class=\"timeline-entry-options-kits\"><span class=\"timeline-entry-options-number\">" + kit.NumKits + "</span> ReKits</div>\n" +
                        "</div>\n" +
                        "<div class=\"timeline-entry-footer\"></div>\n" +
                    "</div>\n";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sHtml;
        }
    }
}