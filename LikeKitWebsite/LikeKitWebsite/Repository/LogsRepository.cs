﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LikeKitWebsite.Models;
using System.Data;
using Endepro.Common.Database;

namespace LikeKitWebsite.Repository
{
    public class LogsRepository : BaseRepository
    {
        public LogsRepository(DatabaseBase database)
        {
            Database = database;
        }

        private Log GetLog(IDataReader reader)
        {
            if (reader == null)
                return null;

            Log log = new Log
            {
                ID = Database.GetReaderValueInt64(reader, "ID"),
                RefID = Database.GetReaderValueInt64(reader, "RefID"),
                Description = Database.GetReaderValueString(reader, "Description"),
                Type = (LikeKitWebsite.Models.Type)Database.GetReaderValueInt(reader, "Type"),
                Created = Database.GetReaderValueDateTime(reader, "Created")
            };

            return log;
        }

        public void CreateLog(Log Log)
        {
            String SQL = String.Format("INSERT INTO Log (RefID, Description, Type, Created) " +
                "values ({0},'{1}',{2},'{3}')",
                Log.RefID,
                Database.ParseStringToSQL(Log.Description),
                (short)Log.Type,
                Database.ParseDateTimeToSQL(System.DateTime.Now)
                );

            Database.ExecuteCommand(SQL);
        }

        public void UpdateLog(Log Log)
        {
            String SQL = String.Format("UPDATE Log SET RefID={0}," +
                                       "Description='{1}', Type={2}, Created='{3}' " +
                                       " WHERE ID={4}",
                                       Log.RefID,
                                       Database.ParseStringToSQL(Log.Description),
                                       (short)Log.Type,
                                       Database.ParseDateTimeToSQL(Log.Created),
                                       Log.ID);

            Database.ExecuteCommand(SQL);
        }

        public void DeleteLog(Log Log)
        {
            String SQL = String.Format("DELETE FROM Log " +
                                       " WHERE ID={0}",
                                        Log.ID);

            Database.ExecuteCommand(SQL);
        }
    }
}