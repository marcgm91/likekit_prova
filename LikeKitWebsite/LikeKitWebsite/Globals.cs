﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using Endepro.Common.Database;
using System.Configuration;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Net;
using DKIM;
using System.ComponentModel;
using System.Globalization;
using System.Resources;
using System.Reflection;

namespace LikeKitWebsite
{
    public class Globals
    {
        #region Constants Globals
        /// <summary>
        /// Validated user ID session variable name
        /// </summary>
        public const String SessionUserID = "ID";

        /// <summary>
        /// Validated user UserID session variable name
        /// </summary>
        public const String SessionUserUserID = "UserID";
        /// <summary>
        /// Validated user Name session variable name
        /// </summary>
        public const String SessionUserName = "UserName";
        /// <summary>
        /// Validated user Password session variable name
        /// </summary>
        public const String SessionUserPassword = "UserPwd";

        /// <summary>
        /// Last visited url session variable name
        /// </summary>
        public const String SessionLastUrl = "LastUrl";

        /// <summary>
        /// Locale session variable name
        /// </summary>
        public const String SessionLocale = "Locale";

        /// <summary>
        /// Validated user FirstName session variable name
        /// </summary>
        public const String SessionFirstName = "FirstName";

        /// <summary>
        /// Validated user LastName session variable name
        /// </summary>
        public const String SessionLastName = "LastName";

        /// <summary>
        /// Validated user Picture session variable name
        /// </summary>
        public const String SessionPicture = "Picture";

        /// <summary>
        /// Validated user Web session variable name
        /// </summary>
        public const String SessionWeb = "Web";

        /// <summary>
        /// Validated user About session variable name
        /// </summary>
        public const String SessionAbout = "About";

        /// <summary>
        /// Validated user NumInteractions session variable name
        /// </summary>
        public const String SessionNumInteractions = "NumInteractions";



        //RSA Private Key
        public static String RSA_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----" +
            "MIICXQIBAAKBgQDhoSd53ehTEPucMEG4AzpiFOaichGcgG+ISWzSXvRZm3Kq3rVY" +
            "XR9kOdeabN/B7rZJ6EjnAnkoWjtTOA7Qy0XkD4JKBCqj6GHQ8ARLANX1sK2qkhY1" +
            "Y4jZbFO/8LL7t7k3yLp5Rfqaw/1mKxosv+oKLt+UXJu1+vhqR9qPMNWzPQIDAQAB" +
            "AoGBAKNQr69BVWGS4FysfVVBWOgXst0Qbfj26kQeyZr4Siv2HajarS65sPgt+gZ4" +
            "GfpY3C9g6Cv2GVaBAYH6CJdPG1VdiJYQH+M9XVDdy/2SxKYLRmCGp6zhI9Lxidiy" +
            "Ev7haX/3XlelBTG0BdMfEiYqTA0TIGeL3cpNzpXASWa78mQNAkEA8l3hlYjBq+Ud" +
            "H3A7yDsyyixuzEl3W/Qnl2wenV6uppzhgxJm4n3VE2ejBhLqASpwd3tQALbqKcLY" +
            "nX3mPCLcswJBAO5SQSbR1LHcYMa0CxVMb4Xascv7tNAreeCfPQ54Tr+WTeGMH2Mc" +
            "SY+J4My4yXr7VXFQVsraIEg2Ai43jtsaCE8CQFesJD0ucjiXMZi8xbNhzraZqh7V" +
            "+IpX4LaC0vREWtNEbsM2Ned6c0iItc0trF5tlq9x+Pe5My2WITufXlikl8cCQB7v" +
            "aGuckypfO+SvHD0whc1vPOMIkkjvS8u/f9JwBXDTFtrvMGfSFS5frGGGnwiqtUWX" +
            "MxzzwXr+H7xWE+IW9/sCQQCW6XdKDdcQvwea36AZ2S1fL+iUq55013fUchGXbSIe" +
            "vw2/N9XC+c6n4dpOaZpRAoJR+S/7fubz13FhhFIXmXJK" +
            "-----END RSA PRIVATE KEY-----";     

        #endregion

        /// <summary>
        /// Converts Int64 array, to comma separated string values
        /// </summary>
        /// <param name="ids">Array Int64</param>
        /// <returns>String</returns>
        public static String ConvertArrayToValues(Int64[] ids)
        {
            String ret = string.Empty;
            String coma = string.Empty;
            for (int i = 0; i < ids.Length; i++)
            {
                if (i > 0) coma = ",";
                Int64 tmp = ids[i];
                ret += string.Format("{0}{1}", coma, tmp);
            }
            return ret;
        }

        /// <summary>
        /// Converts Strning array, to comma separated string values
        /// </summary>
        /// <param name="ids">Array String</param>
        /// <returns>String</returns>
        public static String ConvertArrayToValues(String[] ids)
        {
            String ret = string.Empty;
            String coma = string.Empty;
            for (int i = 0; i < ids.Length; i++)
            {
                if (i > 0) coma = ",";
                String tmp = ids[i];
                ret += string.Format("{0}'{1}'", coma, tmp);
            }
            return ret;
        }

        /// <summary>
        /// Sends Mail
        /// </summary>
        /// <param name="Subject">DatabaseBase Database</param>
        /// <param name="Subject">String Subject</param>
        /// <param name="Body">String Body</param>
        /// <param name="To">String To</param>
        /// <param name="From">String From</param>
        /// <param name="Cc">String Cc</param>
        /// <param name="Bcc">String Bcc</param>
        /// <returns>nothing</returns>
        public static void SendMail(DatabaseBase Database, String Subject, String Body, String To, String From = "likekit@likekit.com", String Cc = "", String Bcc = "")
        {
            To = To.Replace(';', ',').Replace(" ", String.Empty);
            Cc = Cc.Replace(';', ',').Replace(" ", String.Empty);
            Bcc = Bcc.Replace(';', ',').Replace(" ", String.Empty);
            if (To == String.Empty && Cc == String.Empty && Bcc == String.Empty)
                return;

            if (ConfigurationManager.AppSettings["MailServer"] == String.Empty)
                return;

            if (From == String.Empty)
                From = "likekit@likekit.com";

            MailMessage mail = new MailMessage();
            try
            {
                mail.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"], "LikeKit");
                mail.Sender = mail.From;
                if (To != String.Empty)
                    mail.To.Add(To);
                if (Cc != String.Empty)
                    mail.CC.Add(Cc);
                if (Bcc != String.Empty)
                    mail.Bcc.Add(Bcc);
                mail.Subject = Subject;
                mail.Body = Body;

                mail.IsBodyHtml = true;

                //Guardem els correus en una taula a la BBDD
                LikeKitWebsite.Models.Mail _mail = new LikeKitWebsite.Models.Mail();
                _mail.Sender = To;
                _mail.Subject = Subject;
                _mail.Body = Body;
                _mail.Status = Models.Status.Pending;

                LikeKitWebsite.Repository.MailsRepository _mailsRep = new LikeKitWebsite.Repository.MailsRepository(Database);
                _mailsRep.CreateMail(_mail);


                /*SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MailServer"], Int16.Parse(ConfigurationManager.AppSettings["MailPort"]) > 0 ? Int16.Parse(ConfigurationManager.AppSettings["MailPort"]) : 25);
                smtpClient.EnableSsl = true;

                if (ConfigurationManager.AppSettings["MailUser"] != String.Empty)
                {
                    smtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["MailUser"], ConfigurationManager.AppSettings["MailPassword"]);
                }

                //RSA
                var privateKey = PrivateKeySigner.Create(RSA_PRIVATE_KEY);

                var domainKeySigner = new DomainKeySigner(privateKey, "likekit.com", "likekit", new string[] { "From", "To", "Subject" });
                mail.DomainKeySign(domainKeySigner);

                var dkimSigner = new DkimSigner(privateKey, "likekit.com", "likekit", new string[] { "From", "To", "Subject" });
                mail.DkimSign(dkimSigner);
                //RSA

                //smtpClient.Send(mail);
                //L'enviem de forma asíncrona
                object userState = GetMD5(String.Format("{0}{1}", "Likekit", DateTime.UtcNow)); 

                //wire up the event for when the Async send is completed
                smtpClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);
                smtpClient.SendAsync(mail,userState);
                 */
            }
            catch (Exception ex)
            {
                //Globals.HandleException(database, ex);
                throw ex;
            }
            finally
            {
                //mail.Dispose();
            }
        }

        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            /*

            //Get the Original MailMessage object
            MailMessage mail= (MailMessage)e.UserState;

            //write out the subject
            string subject = mail.Subject;*/

            /*
            if (e.Cancelled)
            {
                Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "UpdateProfile " + test);
                Globals.SaveLog(database, ex_msg, Models.Type.EditProfile, 1);
            }
            if (e.Error != null)
            {
                Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message [{0}] sent.", subject );
            }
            
            */

            if (e.Cancelled)
            {
                Console.WriteLine("Send canceled for mail with error [{0}].", e.Error.ToString());
            }
            if (e.Error != null)
            {
                Console.WriteLine("Error {0} occurred when sending mail ", e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message [{0}] sent.", e.Error.ToString());
            }
        }

        /// Saves log into database
        /// </summary>
        /// <param name="database">DatabaseBase database</param>
        /// <param name="Message">String Message</param>
        /// <param name="Type">Models.Type Type</param>
        /// <param name="RefID">Int64 RefID</param>
        /// <returns>nothing</returns>
        public static void SaveLog(DatabaseBase database, String Message, Models.Type Type, Int64 RefID)
        {
             LikeKitWebsite.Models.Log _log = new  LikeKitWebsite.Models.Log();
            _log.Description = Message;
            _log.Type = Type;
            _log.RefID = RefID;

            LikeKitWebsite.Repository.LogsRepository _logRep = new LikeKitWebsite.Repository.LogsRepository(database);
            _logRep.CreateLog(_log);
        }

        /// <summary>
        /// Get MD5 of a String
        /// </summary>
        /// <param name="input">String to codify.</param>
        public static String GetMD5(String input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            string password = s.ToString();
            return password;
        }

        /// <summary>
        /// Get a string description of time difference
        /// </summary>
        /// <param name="input">DateTime to diff.</param>
        public static String GetTimeDifference(DateTime dt)
        {
            String difference = String.Empty;

            TimeSpan span = DateTime.Now.Subtract(dt);

            if (span.Days > 0)
            {
                //difference = String.Format("{0} días", span.Days);

                //user.Birthday.ToString("dd/MM/yyyy");

                difference = String.Format("{0}", dt.ToString("d MMM"));
            }
            else if (span.Hours > 0)
            {
                if (span.Hours == 1)
                    difference = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HACE_UNA_HORA);
                else
                    difference = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HACE_X_HORAS, span.Hours);
            }
            else if (span.Minutes > 0)
            {
                if (span.Minutes == 1)
                    difference = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HACE_UN_MINUTO);
                else
                    difference = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HACE_X_MINUTOS, span.Minutes);
            }
            else
                difference = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HACE_POCOS_SEGUNDOS);
 
            return difference;
        }

        /// <summary>
        /// Function to download Image from website
        /// </summary>
        /// <param name="imageUrl">URL address to download image</param>
        /// <returns>Image</returns>
        public static System.Drawing.Image DownloadImageFromUrl(string imageUrl)
        {
            System.Drawing.Image image = null;

            try
            {
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                System.IO.Stream stream = webResponse.GetResponseStream();

                image = System.Drawing.Image.FromStream(stream);

                webResponse.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return image;
        }



        public static String HtmlEncoding(String plainText)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in plainText)
                sb.AppendFormat("&#{0};", (int)c);

            return sb.ToString();
        }

        //Transform url text in html link
        public static string ConvertUrlsToLinks(string plainText)
        {
            string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            return r.Replace(plainText, "<a href=\"$1\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
        }

        //With links
        public static String HtmlEncodingLinks(String plainText)
        {
            String url = String.Empty;
            String result = String.Empty;
            String url_final = String.Empty;

            //Busquem url
            MatchCollection mc = Regex.Matches(plainText, @"(www[^ \s]+|http[^ \s]+)([\s]|$)", RegexOptions.IgnoreCase);

            if ((mc.Count > 0) && (mc[0] != null))
            {
                url = mc[0].Value;
                result = plainText.Replace(url, "");
            }
            else
                result = plainText;

            string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            url_final = r.Replace(url, "<a href=\"$1\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");

            result = String.Format("{0} {1}", HtmlEncoding(result), url_final);

            return result;
        }

        //With links
        public static String HtmlEncodingLinksShort(String plainText, String link_name)
        {
            String url = String.Empty;
            String result = String.Empty;
            String url_final = String.Empty;

            //Busquem url
            MatchCollection mc = Regex.Matches(plainText, @"(www[^ \s]+|http[^ \s]+)([\s]|$)", RegexOptions.IgnoreCase);

            if ((mc.Count > 0) && (mc[0] != null))
            {
                url = mc[0].Value;
                result = plainText.Replace(url, "");
            }
            else
                result = plainText;

            string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            url_final = r.Replace(url, "<a href=\"$1\" target=\"&#95;blank\">" + link_name + "</a>").Replace("href=\"www", "href=\"http://www");

            result = String.Format("{0} {1}", HtmlEncoding(result), url_final);

            return result;
        }

        //With links
        public static String HtmlEncodingLinksShortTimeline(String plainText, String link_name)
        {
            String url = String.Empty;
            String result = String.Empty;
            String url_final = String.Empty;

            //Busquem url
            MatchCollection mc = Regex.Matches(plainText, @"(www[^ \s]+|http[^ \s]+)([\s]|$)", RegexOptions.IgnoreCase);

            if ((mc.Count > 0) && (mc[0] != null))
            {
                url = mc[0].Value;
                result = plainText.Replace(url, "");
            }
            else
                result = plainText;

            string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            url_final = r.Replace(url, "<a href=\"$1\" target=\"&#95;blank\">" + link_name + "</a>").Replace("href=\"www", "href=\"http://www");

            result = String.Format("{0} {1}", Globals.WordCut(result, 200, new char[] { ' ' }), url_final);

            return result;
        }

        /// <summary>
        /// Al parecer la función Path.Combine da algún problema en Mono, por lo que hacemos esta adaptación.
        /// </summary>
        /// <param name="path1">La primera ruta de acceso.</param>
        /// <param name="path2">La segunda ruta de acceso.</param>
        /// <returns>Cadena que contiene las rutas de acceso combinadas.</returns>
        public static String PathCombine(String path1, String path2)
        {
            String result = System.IO.Path.Combine(path1, path2);
            if (System.IO.Path.DirectorySeparatorChar == '/')
                return result.Replace('\\', '/');
            else if (System.IO.Path.DirectorySeparatorChar == '\\')
                return result.Replace('/', '\\');
            else
                return result;
        }

        /// <summary>
        /// Obtener el nombre del archivo de la ruta indicada teniendo en cuenta que puede venir de varios sistemas operativos.
        /// </summary>
        /// <param name="path">Ruta de la que extraer el nombre del archivo.</param>
        /// <returns>El nombre de archivo.</returns>
        public static String GetFilename(String path)
        {
            String str = path;
            if (System.IO.Path.DirectorySeparatorChar == '/')
                str = str.Replace('\\', '/');
            else if (System.IO.Path.DirectorySeparatorChar == '\\')
                str = str.Replace('/', '\\');
            return System.IO.Path.GetFileName(str);
        }

        /// <summary>
        /// Combinar una URL.
        /// </summary>
        /// <param name="baseUrl">URL base.</param>
        /// <param name="file">Archivo a combinar con la base.</param>
        /// <returns>URL resultado de la combinación.</returns>
        public static String CombineUrl(string baseUrl, string file)
        {
            if (baseUrl[baseUrl.Length - 1] != '/')
                return baseUrl + "/" + file;
            else
                return baseUrl + file;
        }



        public static void CropImage(String FileName, String DestinationImage, Rectangle cropArea)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(FileName);

            Bitmap bmpImage = new Bitmap(FullsizeImage);
            Bitmap bmpCrop = bmpImage.Clone(cropArea,
            bmpImage.PixelFormat);

            FullsizeImage.Dispose();

            System.Drawing.Image FinalImage = (System.Drawing.Image)(bmpCrop);

            FinalImage.Save(DestinationImage);
        }

        public static void ResizeImageAspectRatio(string OriginalFile, string NewFile, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= NewWidth)
                {
                    NewWidth = FullsizeImage.Width;
                }
            }

            int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                // Resize with height instead
                NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                NewHeight = MaxHeight;
            }

            System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            FullsizeImage.Dispose();

            // Save resized picture
            NewImage.Save(NewFile);
        }

        public static void ImageCropCenter(string OriginalFile, string NewFile, int Width, int Height)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

            System.Drawing.Image NewImage = ImageCropCenter(FullsizeImage, Width, Height);

            NewImage.Save(NewFile);

            FullsizeImage.Dispose();
            NewImage.Dispose();
        }

        private static System.Drawing.Image ImageCropCenter(System.Drawing.Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                destY = (int)((Height - (sourceHeight * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentH;
                destX = (int)((Width - (sourceWidth * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width,
                    Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        private static System.Drawing.Image ImageCropXY(System.Drawing.Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                destY = (int)((Height - (sourceHeight * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentH;
                destX = (int)((Width - (sourceWidth * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width,
                    Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static System.Drawing.Image ImageCropXY(System.Drawing.Image image, int width, int height, int x, int y)
        {
            try
            {
                Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                bmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                Graphics gfx = Graphics.FromImage(bmp);
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfx.DrawImage(image, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
                // Dispose to free up resources
                //image.Dispose();
                //bmp.Dispose();
                gfx.Dispose();

                return (System.Drawing.Image)bmp;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public static System.Drawing.Image FixedSizeImage(System.Drawing.Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height,
                              PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                             imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Red);
            grPhoto.InterpolationMode =
                    InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image FullsizeImage, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            //System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

            // Prevent using images internal thumbnail
            /*FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);*/

            if (OnlyResizeIfWider)
            {
                if (FullsizeImage.Width <= NewWidth)
                {
                    NewWidth = FullsizeImage.Width;
                }
            }

            int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                // Resize with height instead
                NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                NewHeight = MaxHeight;
            }

            System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            FullsizeImage.Dispose();

            // Save resized picture
            //NewImage.Save(NewFile);
            return NewImage;
        }

        public static string WordCut(string text, int cutOffLength, char[] separators)
        {
            if (text.Length > cutOffLength)
            {
                cutOffLength = cutOffLength > text.Length ? text.Length : cutOffLength;

                int separatorIndex = text.Substring(0, cutOffLength).LastIndexOfAny(separators);
                if (separatorIndex > 0)
                    return text.Substring(0, separatorIndex) + "...";
                return text.Substring(0, cutOffLength) + "...";
            }
            else
            {
                return text;
            }
        }

        public static bool GetPerms(long PermsRegister, int NumPerm)
        {
            return ((PermsRegister & NumPerm) != 0);
        }

        /// <summary>
        /// HTML-encodes a string and returns the encoded string.
        /// </summary>
        /// <param name="text">The text string to encode. </param>
        /// <returns>The HTML-encoded text.</returns>
        public static string HtmlEncode(string text)
        {
            if (text == null)
                return null;

            StringBuilder sb = new StringBuilder(text.Length);

            int len = text.Length;
            for (int i = 0; i < len; i++)
            {
                switch (text[i])
                {

                    case '<':
                        sb.Append("&lt;");
                        break;
                    case '>':
                        sb.Append("&gt;");
                        break;
                    case '"':
                        sb.Append("&quot;");
                        break;
                    case '&':
                        sb.Append("&amp;");
                        break;
                    default:
                        if (text[i] > 159)
                        {
                            // decimal numeric entity
                            sb.Append("&#");
                            sb.Append(((int)text[i]).ToString(CultureInfo.InvariantCulture));
                            sb.Append(";");
                        }
                        else
                            sb.Append(text[i]);
                        break;
                }
            }
            return sb.ToString();
        }


        static Regex EncodeLiteralRegex;

        // Format a bunch of literals.
        public static string Format(string format, params object[] items)
        {
            return string.Format(format,
                items.Select(item => EncodeString("" + item)).ToArray());
        }

        // Given a string, return a string suitable for safe
        // use within a Javascript literal inside a <script> block.
        // This approach errs on the side of "ugly" escaping.
        public static string EncodeString(string value)
        {
            if (EncodeLiteralRegex == null)
            {
                // initial accept "space to ~" in ASCII then reject quotes 
                // and some XML chars (this avoids `</script>`, `<![CDATA[..]]>>`, and XML vs HTML issues)
                // "/" is not allowed because it requires an escape in JSON
                var accepted = Enumerable.Range(32, 127 - 32)
                    .Except(new int[] { '"', '\'', '\\', '&', '<', '>', '/' });
                // pattern matches everything but accepted
                EncodeLiteralRegex = new Regex("[^" +
                    string.Join("", accepted.Select(c => @"\x" + c.ToString("x2")).ToArray())
                    + "]");
            }
            return EncodeLiteralRegex.Replace(value ?? "", (match) =>
            {
                var ch = (int)match.Value[0]; // only matches a character at a time
                return ch <= 127
                    ? @"\x" + ch.ToString("x2") // not JSON
                    : @"\u" + ch.ToString("x4");
            });
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static String StripTagsCharArray(String source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new String(array, 0, arrayIndex);
        }

        public static String ParseAmazonDescription(String Description)
        {
            Description = Description.Replace("\n", "");
            Description = Description.Replace("\t", " ");
            Description = Description.Replace("'", "\\'");
            Description = Description.Replace("Product Description", "");
            Description = Description.Replace("Descripción del producto", "");
            Description = Description.Replace("</div>", "");
            Description = Description.Replace("<br /> <br />", "<br />");
            Description = Description.Replace("\"", "");

            Description = StripTagsCharArray(Description);
            Description = Globals.WordCut(Description, 600, new char[] { ' ' });

            //Description = Globals.HtmlEncode(Description);
            //Description = HttpUtility.HtmlEncode(Description);
            //Description = Globals.EncodeString(Description);

            return Description;
        }


        #region LocaleManager
        public static ResourceManager _localeManager = null;

        /// <summary>
        /// ResourceManager to get translated strings.
        /// </summary>
        public static ResourceManager LocaleManager
        {
            get
            {
                if (_localeManager == null)
                {
                    // Get the appropriate resource from the assembly
                    Assembly asm = Assembly.GetExecutingAssembly();
                    _localeManager = new ResourceManager("LikeKitWebsite.Resources.LikeKitWebsiteLocal", asm);
                }

                return _localeManager;
            }
        }

        public static string _(string str)
        {
            try
            {
                string result = LocaleManager.GetString(str);
                if (result == String.Empty)
                    result = str;

                return result;
            }
            catch
            {
                return str;
            }
        }

        public static string _(string str, CultureInfo culture)
        {
            try
            {
                string result = LocaleManager.GetString(str, culture);
                if (result == String.Empty)
                    result = str;
                return result;
            }
            catch
            {
                return str;
            }
        }

        public static string _(string str, string str_culture)
        {
            try
            {
                CultureInfo culture = new CultureInfo(str_culture);
                string result = LocaleManager.GetString(str, culture);
                if (result == String.Empty)
                    result = str;
                return result;
            }
            catch
            {
                CultureInfo culture = new CultureInfo("es");
                string result = LocaleManager.GetString(str, culture);
                if (result == String.Empty)
                    result = str;
                return result;
            }
        }

        public static String GenerateUrlFriendly(String phrase)
        {
            /*String str = phrase.ToLower();

            // invalid chars, make into spaces
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces/hyphens into one space       
            str = Regex.Replace(str, @"[\s-]+", " ").Trim();
            // cut and trim it
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();
            // hyphens
            str = Regex.Replace(str, @"\s", "-");

            return str;*/

            char[] allChars;

            //cadena de caracters per ser substituits
            String sCharsToReplace = "áéíóúüñç_àèòï";
            //cadena de caracters per substituir, en el mateix ordre
            String sCharsForReplace = "aeiouunc-aeoi";
            String seoURL = String.Empty;
            int charPos = 0;

            // decodifica qualsevol entitat HTML abans de començar.
            //strTitle = Server.HtmlDecode(phrase);
            phrase = phrase.ToLower();
            phrase = phrase.Trim();

            // substitueix tots els caracters transparents.
            phrase = Regex.Replace(phrase, @"\s+", "-");

            // El seguent bucle substitueix tots els caracters
            // trobats a sCharsToReplace pels de sCharsForReplace 
            allChars = phrase.ToCharArray();
            for (int i = 0; i < allChars.Length; i++)
            {
                charPos = sCharsToReplace.IndexOf(allChars[i]);
                if (charPos != -1)
                { allChars[i] = sCharsForReplace[charPos]; }
                seoURL += allChars[i];
            }

            // Treu qualsevol caracter no substituit que no
            // estigui entre a y z, que no sigui número o que no sigui un guio (-)

            seoURL = new Regex("[^a-z0-9-]").Replace(seoURL, "");
            // treu qualsevol guio solitari al final o principi
            // de la cadena resultant
            seoURL = seoURL.Trim('-');
            
            return seoURL;
        }

        #endregion
    }
}