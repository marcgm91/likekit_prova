﻿
$(document).ready(function () {

    setTimelineTop();

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    $("#profile-following-menu").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#profile-following-container").toggle();

        //Tanquem followers
        $("#profile-followers-menu").removeClass('menu-left-section-selected');
        $("#profile-followers-container").hide();
    })

    $("#profile-followers-menu").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#profile-followers-container").toggle();

        //Tanquem followings
        $("#profile-following-menu").removeClass('menu-left-section-selected');
        $("#profile-following-container").hide();
    })

    $("#profile-following-close").live('click', function () {
        $("#profile-following-menu").toggleClass('menu-left-section-selected');
        $("#profile-following-container").toggle();
    })

    $("#profile-follower-close").live('click', function () {
        $("#profile-followers-menu").toggleClass('menu-left-section-selected');
        $("#profile-followers-container").toggle();
    })

    //Hover now-with entry
    $(".profile-now-with-images-entry").live('hover', function () {
        $(this).children('.profile-now-with-close:first').toggle();
    });

    $(".profile-now-with-close-img").live('hover', function () {
        $(this).closest('.profile-now-with-images-entry').children('.profile-now-with-tip:first').toggle();
    });

    $(".profile-now-with-add-img").live('hover', function () {
        $(this).closest('.profile-now-with-images-entry').children('.profile-now-with-tip-add:first').toggle();
    })

    //Logout
    $("#logout-profile").live('click', function () {
        parent.location.href = '/Account/LogOff';
    });

    //Now with Search
    $("#now-with-product-search").live('click', function () {

        $('#now-with-product-search').addClass('timeline-search-italic');

        var search = $('#now-with-product-search').val();
        if (search == 'Translate(STR_AHORA_ESTOY_CON_MIN)') {
            $('#now-with-product-search').val('');
        }
    });

    //Now with Search Enter
    $("#now-with-product-search").keypress(function (e) {
        if (e.keyCode == 13) {
            NowWithSearch();
        }
    });

    //Now With close
    $("#timeline-now-with-close").live('click', function () {
        $('#timeline-now-with-modal').css('display', 'none');
    });

});

function setTimelineTop() {
    var height = $('#header').height();
    $('#interactions-container').css("margin-top", height + "px");
}