﻿
$(document).ready(function () {

    //Password recovery
    $("#password-recovery-mail").on('click', function () {

        var mail = $('#password-recovery-mail').val();
        
        if (mail == 'Translate(STR_TU_DIRECCION_EMAIL)') {
            $('#password-recovery-mail').val('');
            $('#password-recovery-mail').addClass('login-italic');
        }
    });


    $("#password-recovery-mail").on('focus', function () {

        var mail = $('#password-recovery-mail').val();

        if (mail == 'Translate(STR_TU_DIRECCION_EMAIL)') {
            $('#password-recovery-mail').val('');
            $('#password-recovery-mail').addClass('login-italic');
        }
    });

    //Register : Mail
    $("#password-recovery-button").live('click', function () {
        SendPasswordRecoveryMail();
    });

    //Mail enter
    $("#password-recovery-mail").keypress(function (e) {
        if (e.keyCode == 13) {
            SendPasswordRecoveryMail();
        }
    });

    $("#password-recovery-close").on('click', function () {
        parent.location.href = '/Account/LogOn';
    });

    $(".login-social-twitter").hover(function () {
        $(this).toggleClass('login-social-twitter-on');
    });

    $(".login-social-facebook").hover(function () {
        $(this).toggleClass('login-social-facebook-on');
    });

    $(".login-social-t").hover(function () {
        $(this).toggleClass('login-social-t-on');
    });





    /**

        Password update

    **/

    $("#password_text").on('click', function () {
        $(this).css('display', 'none');
        $('#Password').css('display', 'block');
        $('#Password').focus();
    });

    $("#password_text").on('focus', function () {
        $(this).css('display', 'none');
        $('#Password').css('display', 'block');
        $('#Password').focus();
    });

    $("#password_text_repeat").on('click', function () {
        $(this).css('display', 'none');
        $('#RepeatPassword').css('display', 'block');
        $('#RepeatPassword').focus();
    });

    $("#password_text_repeat").on('focus', function () {
        $(this).css('display', 'none');
        $('#RepeatPassword').css('display', 'block');
        $('#RepeatPassword').focus();
    });

    //Register : Password
    $("#Password").live('focusout', function () {

        var Password = $('#Password').val();
        var RepeatPassword = $('#RepeatPassword').val();
        
        if (!isStrongPassword(Password)) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_NO_PARECE_SEGURA_PRUEBA_MEZCLANDO_NUMEROS_Y_LETRAS)");
        }
        else if ((Password != '') && (RepeatPassword != '') && (Password == RepeatPassword)) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_GENIAL_NO_LA_OLVIDES)");
        }
        else if ((Password == '') || (RepeatPassword == '')) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_DEBES_RELLENARLOS)");
        }
        else if (Password != RepeatPassword) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_LAS_CONTRASENAS_NO_COINCIDEN) <br />Translate(STR_POR_FAVOR_REVISA_QUE_SEAN_CORRECTAS)");
        }
    });

    //Register : RepeatPassword
    $("#RepeatPassword").live('focusout', function () {

        var Password = $('#Password').val();
        var RepeatPassword = $('#RepeatPassword').val();
        
        if (!isStrongPassword(Password)) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_NO_PARECE_SEGURA_PRUEBA_MEZCLANDO_NUMEROS_Y_LETRAS)");
        }
        else if ((Password != '') && (RepeatPassword != '') && (Password == RepeatPassword)) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_GENIAL_NO_LA_OLVIDES)");
        }
        else if ((Password == '') || (RepeatPassword == '')) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_DEBES_RELLENARLOS)");
        }
        else if (Password != RepeatPassword) {
            $("#password-update-tip").fadeIn("slow");
            $('#password-update-tip').html("Translate(STR_LAS_CONTRASENAS_NO_COINCIDEN) <br />Translate(STR_POR_FAVOR_REVISA_QUE_SEAN_CORRECTAS)");
        }
    });

    $("#password-update-button").hover(function () {
        $(this).toggleClass('disabled');
    });

    //Register : Mail
    $("#password-update-button").live('click', function () {
        SendUpdatePassword();
    });

    /**

        fi Password update

    **/
});

function SendPasswordRecoveryMail() {
    var mail = $('#password-recovery-mail').val();

    if (isValidEmailAddress(mail)) {
        $("#password-recovery-tip").css('display', 'none');
        
        $.post('PasswordRecoveryMail/?mail=' + mail, function (data) {
            //$('#landing-content-form').html(data);
            
            if ($.trim(data) == 'Error') {
                $("#password-recovery-tip").fadeIn("slow");
                $('#password-recovery-tip').html("Translate(STR_EL_USUARIO_NO_EXISTE)");
            }
            else if ($.trim(data) == 'ErrorMail') {
                $("#password-recovery-tip").fadeIn("slow");
                $('#password-recovery-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
            }
            else if ($.trim(data) == 'Success') {
                $("#password-recovery-button").addClass('disabled');
                $("#password-recovery-button").html("Translate(STR_MAIL_ENVIADO)");
            }
        });
    }
    else {
        $("#password-recovery-tip").fadeIn("slow");
    }
}

function SendUpdatePassword() {

    var Password = $('#Password').val();
    var RepeatPassword = $('#RepeatPassword').val();
    var Userid = $('#userid').val();

    if (!isStrongPassword(Password)) {
        $("#password-update-tip").fadeIn("slow");
        $('#password-update-tip').html("Translate(STR_NO_PARECE_SEGURA_PRUEBA_MEZCLANDO_NUMEROS_Y_LETRAS)");
    }
    else if ((Password != '') && (RepeatPassword != '') && (Password == RepeatPassword)) {
        $("#password-update-tip").fadeIn("slow");
        //$('#password-update-tip').html("Translate(STR_GENIAL_NO_LA_OLVIDES)");

        Password = encodeURIComponent(Password);
        $.post('/Account/DoUpdatePassword/?UserID=' + Userid + '&Password=' + Password, function (data) {
            parent.location.href = "/Account/LogOn";
        });
    }
    else if ((Password == '') || (RepeatPassword == '')) {
        $("#password-update-tip").fadeIn("slow");
        $('#password-update-tip').html("Translate(STR_DEBES_RELLENARLOS)");
    }
    else if (Password != RepeatPassword) {
        $("#password-update-tip").fadeIn("slow");
        $('#password-update-tip').html("Translate(STR_LAS_CONTRASENAS_NO_COINCIDEN) <br />Translate(STR_POR_FAVOR_REVISA_QUE_SEAN_CORRECTAS)");
    }
}