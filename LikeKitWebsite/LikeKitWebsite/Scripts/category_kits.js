﻿
$(window).load(function () {

    setWishlistTop();

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    //Ho fem així per evitar que al carrgar la pàgina amb el ratolí damunt de l'element, doni problemes
    $(".timeline-entry-image").live({
        mouseenter:
            function () {
                $(this).children('.timeline-entry-info:first').css('display', 'block');
            },
        mouseleave:
            function () {
                $(this).children('.timeline-entry-info:first').css('display', 'none');
            }
    }
    );

    //Ho fem així per evitar que al apretar el botó es comporti de forma estranya
    $(".timeline-entry-info-like-link").live({
        mouseenter:
            function () {
                $(this).addClass('timeline-entry-info-like-link-on');
            },
        mouseleave:
            function () {
                $(this).removeClass('timeline-entry-info-like-link-on');
            }
    });

    $(".timeline-entry-info-dislike-link").live({
        mouseenter:
            function () {
                $(this).addClass('timeline-entry-info-dislike-link-on');
            },
        mouseleave:
            function () {
                $(this).removeClass('timeline-entry-info-dislike-link-on');
            }
    }
    );

    $(".timeline-entry-info-comment-link").hover(function () {
        $(this).toggleClass('timeline-entry-info-comment-link-on');
    });

    $(".timeline-entry-info-rekit-link").hover(function () {
        $(this).toggleClass('timeline-entry-info-rekit-link-on');
    });

    //Mostrem els masonry de les llistes
    var $categorykits = $('#category-kits-container-1');

    $categorykits.masonry({
        itemSelector: '.timeline-entry',
        columnWidth: 252
    });

    $categorykits = $('#category-kits-container-2');

    $categorykits.masonry({
        itemSelector: '.timeline-entry',
        columnWidth: 252
    });

    $categorykits = $('#category-kits-container-3');

    $categorykits.masonry({
        itemSelector: '.timeline-entry',
        columnWidth: 252
    });

    $categorykits = $('#category-kits-container-4');

    $categorykits.masonry({
        itemSelector: '.timeline-entry',
        columnWidth: 252
    })

    $categorykits = $('#category-kits-container-5');

    $categorykits.masonry({
        itemSelector: '.timeline-entry',
        columnWidth: 252
    })


    $("#category-kits-main-header-5").live('click', function () {
        $(this).removeClass('category-kits-main-header-on');
        $(this).addClass('category-kits-main-header-selected');

        $("#category-kits-main-header-2").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-1").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-4").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-3").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');

        var height = $('#header').height() + $('#category-kits-main-header').height() + 90;

        if (($("#category-kits-header-5").offset() != null) && (typeof $("#category-kits-header-5").offset() != 'undefined'))
            $('html,body').animate({ scrollTop: $("#category-kits-header-5").offset().top - height });
    });

    $("#category-kits-main-header-4").live('click', function () {
        $(this).removeClass('category-kits-main-header-on');
        $(this).addClass('category-kits-main-header-selected');

        $("#category-kits-main-header-2").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-1").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-5").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-3").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');

        var height = $('#header').height() + $('#category-kits-main-header').height() + 90;

        if (($("#category-kits-header-4").offset() != null) && (typeof $("#category-kits-header-4").offset() != 'undefined'))
            $('html,body').animate({ scrollTop: $("#category-kits-header-4").offset().top - height });
    });

    $("#category-kits-main-header-3").live('click', function () {
        $(this).removeClass('category-kits-main-header-on');
        $(this).addClass('category-kits-main-header-selected');

        $("#category-kits-main-header-2").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-1").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-4").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-5").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');

        var height = $('#header').height() + $('#category-kits-main-header').height() + 90;

        if (($("#category-kits-header-3").offset() != null) && (typeof $("#category-kits-header-3").offset() != 'undefined'))
            $('html,body').animate({ scrollTop: $("#category-kits-header-3").offset().top - height});
    });

    $("#category-kits-main-header-2").live('click', function () {
        $(this).removeClass('category-kits-main-header-on');
        $(this).addClass('category-kits-main-header-selected');

        $("#category-kits-main-header-3").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-1").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-4").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-5").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');

        var height = $('#header').height() + $('#category-kits-main-header').height() + 90;

        if (($("#category-kits-header-2").offset() != null) && (typeof $("#category-kits-header-2").offset() != 'undefined'))
            $('html,body').animate({ scrollTop: $("#category-kits-header-2").offset().top - height});
    });

    $("#category-kits-main-header-1").live('click', function () {
        $(this).removeClass('category-kits-main-header-on');
        $(this).addClass('category-kits-main-header-selected');

        $("#category-kits-main-header-3").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-2").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-4").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');
        $("#category-kits-main-header-5").removeClass('category-kits-main-header-selected').removeClass('category-kits-main-header-on');

        var height = $('#header').height() + $('#category-kits-main-header').height() + 90;

        if (($("#category-kits-header-1").offset() != null) && (typeof $("#category-kits-header-1").offset() != 'undefined'))
            $('html,body').animate({ scrollTop: $("#category-kits-header-1").offset().top - height});
    });

    $("#category-kits-main-header-5").live('hover', function () {
        $(this).toggleClass('category-kits-main-header-on');
    });

    $("#category-kits-main-header-4").live('hover', function () {
        $(this).toggleClass('category-kits-main-header-on');
    });

    $("#category-kits-main-header-3").live('hover', function () {
        $(this).toggleClass('category-kits-main-header-on');
    });

    $("#category-kits-main-header-2").live('hover', function () {
        $(this).toggleClass('category-kits-main-header-on');
    });

    $("#category-kits-main-header-1").live('hover', function () {
        $(this).toggleClass('category-kits-main-header-on');
    });
});


function setWishlistTop() {
    var height = $('#header').height() + $('#category-kits-main-header').height();
    $('#category-kits-container').css("margin-top", height + "px");
}

function TimeLineSearch() {

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }

    //alert(search);

    $.post('/Timeline/TimelineSearch/?search=' + search + '&filter=', function (data) {
        $('#timeline').html(data);
        //alert(data);
        //Hauriem d'executar mazonry?
        var $timeline = $('#timeline');
        //var $timeline = $('#timeline');

        $timeline.imagesLoaded(function () {
            $timeline.masonry('reload');
            /*$timeline.masonry({
            itemSelector: '.timeline-entry',
            columnWidth: 240
            });*/
        });

    });
}

function ShowCategoryKits(id) {

    /*var div_id = '#category-kits-container-' + id;
    var div_header_id = '#category-kits-header-' + id;

    var show = 0;

    if ($(div_id).css('display') == 'none')
        show = 1;
    else
        show = 0;

    $('.category-kits-container').removeClass('show').addClass('hide');

    if (show == 0) {
        $(div_id).removeClass('show');
        $(div_id).addClass('hide');
    }
    else {
        $(div_id).removeClass('hide');
        $(div_id).addClass('show');
    }

    $(div_header_id).toggleClass('category-kits-header-on');

    var $categorykits = $(div_id);

    $categorykits.masonry({
    itemSelector: '.timeline-entry',
    columnWidth: 240
    });*/
}

function DeleteKit(event, id, category_id) {
    $.post('/Timeline/DeleteKit/?id=' + id, function (data) {

        var div_kit = '#timeline-entry-id-' + id;
        var div_category = '#category-kits-container-' + category_id;
        //var div_header_id = '#category-kits-header-' + category_id;

        var $categorykits = $(div_category);

        //Eliminem l'entrada del kit
        $(div_kit).remove();

        $categorykits.masonry({
            itemSelector: '.timeline-entry',
            columnWidth: 252
        });
    });

    // Don't propogate the event to the document
    if (event.stopPropagation) {
        event.stopPropagation();   // W3C model
    } else {
        event.cancelBubble = true; // IE model
    }

}