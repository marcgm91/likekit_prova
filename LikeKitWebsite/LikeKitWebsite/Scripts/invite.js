﻿
$(window).load(function () {

    setWishlistTop();

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    $("#invite-button").live('hover', function () {
        $(this).toggleClass('invite-button-on');
    });

    $("#invite-gmail").live('hover', function () {
        $(this).toggleClass('invite-gmail-on');
    });

    $("#invite-yahoo").live('hover', function () {
        $(this).toggleClass('invite-yahoo-on');
    });

    $("#invite-msn").live('hover', function () {
        $(this).toggleClass('invite-msn-on');
    });

    $("#invite-facebook").live('hover', function () {
        $(this).toggleClass('invite-facebook-on');
    });

    //Invite people throught input
    $("#invite-button").live('click', function () {

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Invite/Send']);
        }

        var invite = $('#invite-direct').val();

        invite = encodeURIComponent(invite);

        $.post(SUBDOMAIN_PATH + '/Timeline/doInvite/?invite=' + invite, function (data) {

            $('#invite-direct').val('');
            $("#invite-button").addClass('invite-button-on');
            $("#invite-button").html('Translate(STR_HECHO)')
            
            setTimeout(function () {
                $("#invite-button").removeClass('invite-button-on').html('Translate(STR_ENVIAR_INVITACION)');
            }, 3000);

        });
    });

    //Invite people throught gmail
    $("#invite-gmail").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/GoogleAuth';
    });

    //Invite people throught Yahoo
    $("#invite-yahoo").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/YahooAuth';
    });

    //Invite people throught Facebook
    $("#invite-facebook").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookInvite';
    });

    $(".invite-result-entry-follow").live('hover', function () {
        $(this).toggleClass('invite-result-entry-btn-active');
    });
    
    $(".invite-result-entry-following").live({
        mouseenter:
            function () {
                $(this).addClass('invite-result-entry-btn-active');
                $(this).html('Translate(STR_NO_SEGUIR)');
            },
        mouseleave:
            function () {
                $(this).removeClass('invite-result-entry-btn-active');
                $(this).html('Translate(STR_SIGUIENDO_MIN)');
            }
    }
    );
});

function setWishlistTop() {
    var height = $('#header').height();
    $('#invite-container').css("margin-top", height + "px");
}

function SendInvitation(mail, id) {

    if (_gaq) {
        _gaq.push(['_trackPageview', '/Invite/Send']);
    }

    $.post(SUBDOMAIN_PATH + '/Timeline/doInvite/?invite=' + mail, function (data) {
        
        var div_id = "#invite-nolikekit-result-link-" + id;
        $(div_id).replaceWith('<div id="invite-nolikekit-result-link-' + id + '" class="invite-nolikekit-result-link-disabled" >Translate(STR_INVITACION_ENVIADA)</div>');
    });
}

function SendFacebookInvitation(fbid, id) {

    if (_gaq) {
        _gaq.push(['_trackPageview', '/Invite/Send']);
    }

    $.post(SUBDOMAIN_PATH + '/Timeline/doFacebookInvite/?invite=' + fbid, function (data) {

        var div_id = "#invite-nolikekit-result-link-" + id;
        $(div_id).replaceWith('<div id="invite-nolikekit-result-link-' + id + '" class="invite-nolikekit-result-link-disabled" >Translate(STR_INVITACION_ENVIADA)</div>');
    });
}

function InviteUnFollow(id) {
    var url_unfollow = SUBDOMAIN_PATH + '/Timeline/UnFollow/?UserID=' + id;
    
    $.post(url_unfollow, function (data) {
        if ($.trim(data) == 'Success') {
            var div_id = "#invite-user-btn-" + id;
            $(div_id).replaceWith('<div id="invite-user-btn-' + id + '" class="invite-result-entry-follow invite-result-entry-btn" onclick="InviteFollow(' + id + ');">Translate(STR_SEGUIR)</div>');
        }
    });
}

function InviteFollow(id) {
    var url_follow = SUBDOMAIN_PATH + '/Timeline/Follow/?UserID=' + id;

    $.post(url_follow, function (data) {
        if ($.trim(data) == 'Success') {
            var div_id = "#invite-user-btn-" + id;
            $(div_id).replaceWith('<div id="invite-user-btn-' + id + '" class="invite-result-entry-following invite-result-entry-btn-on" onclick="InviteUnFollow(' + id + ');">Translate(STR_SIGUIENDO_MIN)</div>');
        }
    });
}