﻿
var timeline_filter = new Array();

//timeline_filter[0] = true;
timeline_filter[0] = false;
timeline_filter[1] = true;
timeline_filter[2] = false;
timeline_filter[3] = false;
timeline_filter[4] = false;
timeline_filter[5] = false;
timeline_filter[6] = false;
timeline_filter[7] = false;
timeline_filter[8] = false;
timeline_filter[9] = false;
timeline_filter[10] = false;
timeline_filter[11] = false;

//Age
timeline_filter[12] = false;
timeline_filter[13] = false;
timeline_filter[14] = false;
timeline_filter[15] = false;
timeline_filter[16] = false;

//Genere
timeline_filter[17] = false;
timeline_filter[18] = false;


var timeline_filter_texts = new Array();
timeline_filter_texts[0] = "Translate(STR_TRENDERS_MIN)";
timeline_filter_texts[1] = "Translate(STR_SIGUIENDO_MIN)";
timeline_filter_texts[2] = "Translate(STR_TODOS_MIN)";
timeline_filter_texts[3] = "Translate(STR_LIBROS_MIN)";
timeline_filter_texts[4] = "Translate(STR_CINE_MIN)";
timeline_filter_texts[5] = "Translate(STR_SERIES_MIN)";
timeline_filter_texts[6] = "Translate(STR_MUSICA_MIN)";
timeline_filter_texts[7] = "Translate(STR_TODOS_MIN)";
timeline_filter_texts[8] = "Translate(STR_MAS_COMENTADO_MIN)";
timeline_filter_texts[9] = "Translate(STR_MAS_RECOMENDADO_MIN)";
timeline_filter_texts[10] = "Translate(STR_TODOS_MIN)";
timeline_filter_texts[11] = "Translate(STR_AVANZADO_MIN)";


var adv_timeline_filter = new Array();

//timeline_filter[0] = true;
adv_timeline_filter[0] = false;
adv_timeline_filter[1] = true;
adv_timeline_filter[2] = false;
adv_timeline_filter[3] = false;
adv_timeline_filter[4] = false;
adv_timeline_filter[5] = false;
adv_timeline_filter[6] = false;
adv_timeline_filter[7] = false;
adv_timeline_filter[8] = false;
adv_timeline_filter[9] = false;
adv_timeline_filter[10] = false;

//Age
adv_timeline_filter[11] = false;
adv_timeline_filter[12] = false;
adv_timeline_filter[13] = false;
adv_timeline_filter[14] = false;
adv_timeline_filter[15] = false;
adv_timeline_filter[16] = false;

//Genere
adv_timeline_filter[17] = false;
adv_timeline_filter[18] = false;


var filtreSantJordi = false;
var filtreTrenders = false;


$(document).ready(function () {
    var search = getSearch();

    if ((search != '') && ((typeof search !== "undefined"))) {
        $('#timeline-search').val(search);
        TimeLineSearch();
    }
});


$(window).load(function () {

    //var $timeline = $('#timeline');

    setTimelineTop();



    /*$timeline.masonry({
    itemSelector: '.timeline-entry',
    columnWidth: 240
    });*/

    /**
        
    TIMER KIT COUNTER
        
    **/
    var timer = $.timer(function () {
        //alert('This message was sent by a timer.');

        var date = $('#timeline-date').last().html();
        //alert('Date : ' + date);

        var search = $('#timeline-search').val();

        if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
            search = '';
        }

        search = encodeURIComponent(search);
        //alert(search);

        var tags = $("#timeline-advanced-filter-tags").val();

        if (tags == 'Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)') {
            tags = "";
        }

        tags = encodeURIComponent(tags);

        $.post(SUBDOMAIN_PATH + '/Timeline/TimelineCounter/?date=' + date + '&filter=' + timeline_filter + '&tags=' + tags, function (data) {
            if ((data == '') || (data == '0')) {
                $('#timeline-counter').css('display', 'none');
                $('#timeline-counter-value').html('');
            }
            else {
                $('#timeline-counter').css('display', 'block');
                $('#timeline-counter-value').html(data);
            }
        });
    });

    timer.set({ time: 15000, autostart: true });

    $('#timeline-counter').live('click', function () {

        TimeLineSearch();

        $('#timeline-counter').css('display', 'none');
        $('#timeline-counter-value').html('');
    })

    /**

    FI TIMER KIT COUNTER

    **/

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected-gris');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();

        //LeftMenuScroll();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();

        //LeftMenuScroll();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    //Search
    $("#timeline-search").live('click', function () {

        $('#timeline-search').addClass('timeline-search-italic');

        var search = $('#timeline-search').val();
        if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
            $('#timeline-search').val('');
        }
    });


    $("#timeline-search").live('focusout', function () {
        var search = $('#timeline-search').val();
        if (search == '') {
            $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
            $('#timeline-search').removeClass('timeline-search-italic');
        }
    });

    //timeline-search Enter
    $("#timeline-search").keypress(function (e) {
        if (e.keyCode == 13) {
            TimeLineSearch();
        }
    });

    //Search click lupa
    $("#top-search-lupa").live('click', function () {
        var search = $('#timeline-search').val();
        if (search != 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
            TimeLineSearch();
        }
    });

    $("#top-header-wide-search-button").live('click', function () {
        var search = $('#timeline-search').val();
        if (search != 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
            TimeLineSearch();
        }
    });

    //Ho fem així per evitar que al carrgar la pàgina amb el ratolí damunt de l'element, doni problemes
    $(".timeline-entry-image").live({
        mouseenter:
            function () {
                $(this).children('.timeline-entry-info:first').css('display', 'block');
            },
        mouseleave:
            function () {
                $(this).children('.timeline-entry-info:first').css('display', 'none');
            }
    }
    );

    //Ho fem així per evitar que al apretar el botó es comporti de forma estranya
    $(".timeline-entry-info-like-link").live({
        mouseenter:
            function () {
                $(this).addClass('timeline-entry-info-like-link-on');
            },
        mouseleave:
            function () {
                $(this).removeClass('timeline-entry-info-like-link-on');
            }
    });

    $(".timeline-entry-info-dislike-link").live({
        mouseenter:
            function () {
                $(this).addClass('timeline-entry-info-dislike-link-on');
            },
        mouseleave:
            function () {
                $(this).removeClass('timeline-entry-info-dislike-link-on');
            }
    }
    );

    $(".timeline-entry-info-comment-link").hover(function () {
        $(this).toggleClass('timeline-entry-info-comment-link-on');
    });

    $(".timeline-entry-info-rekit-link").hover(function () {
        $(this).toggleClass('timeline-entry-info-rekit-link-on');
    });

    $("#timeline-noresult-close").live('click', function () {
        $("#timeline-noresult-modal").css('display', 'none');

        //Tornem a posar el text per defecte
        $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
    });

    $("#filter-likekit").live('click', function () {
        $(this).toggleClass('menu-filter-section-selected');
        $("#filter-likekit-content").toggle();

        //LeftMenuScroll();
    })

    $("#menu-left-option-12").live('click', function () {
        $("#timeline-advanced-filter-modal").css('display', 'block');

        //Mostrem el header del filtre avançat, i amaguem el principal
        $("#top-header-filter-adavanced").css('display', 'block');
        $("#top-header-filter").css('display', 'none');
    });

    $("#timeline-advanced-filter-close").live('click', function () {
        $("#timeline-advanced-filter-modal").css('display', 'none');

        //Mostrem el header del filtre principal, i amaguem l'avançat
        $("#top-header-filter-adavanced").css('display', 'none');
        $("#top-header-filter").css('display', 'block');

        ResetAdvancedFilter();
    });

    $("#timeline-advanced-noresult-close").live('click', function () {
        $("#timeline-advanced-filter-header").css("display", "block");
        $("#timeline-advanced-noresult-modal").css("display", "none");
    });

    $(".timeline-advanced-filter-column-fitler-option").hover(function () {
        $(this).children('.timeline-advanced-filter-column-fitler-option-round:first').toggleClass('timeline-advanced-filter-column-fitler-option-round-selected');
        $(this).children('.timeline-advanced-filter-column-fitler-option-text:first').toggleClass('timeline-advanced-filter-column-fitler-option-text-selected');
    });

    $("#timeline-advanced-filter-btn").live('click', function () {

        //Fem un reset del cercador textual
        $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
        $('#timeline-search').removeClass('timeline-search-italic');

        //Hem de refrescar el filtre principal
        MainFilterRefresh();
        TimeLineSearchAdvanced();
    });

    $("#timeline-advanced-filter-btn").hover(function () {
        $(this).toggleClass('timeline-advanced-filter-btn-on');
    });

    $("#timeline-advanced-filter-tags").live('click', function () {

        var tags = $('#timeline-advanced-filter-tags').val();
        if (tags == 'Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)') {
            $('#timeline-advanced-filter-tags').val('');
        }
    });

    $("#timeline-advanced-filter-tags").live('focusout', function () {
        var tags = $('#timeline-advanced-filter-tags').val();
        if (tags == '') {
            //$('#timeline-advanced-filter-tags').val('Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)');
        }
    });


    //Kit recommended tag select
    $(".timeline-advanced-filter-recommeded-tag").live('click', function () {
    });


    $("#top-header-filter-wide").hover(function () {
        if (!isTouchDevice()) {
            $('#top-header-filter-tooltip').toggle();
        }
    });

    //Filters
    $("#top-header-filter-container").live('click', function () {
        $("#timeline-advanced-filter-modal").css('display', 'block');

        //Mostrem el header del filtre avançat, i amaguem el principal
        $("#top-header-filter-adavanced").css('display', 'block');
        $("#top-header-filter").css('display', 'none');
    });
});

function Filter(id) {

    var div_id = "#menu-left-option-" + id;
  
    if (timeline_filter[id - 1] === true) {
        $(div_id).removeClass('menu-left-option-active');    
    }
    else {
        $(div_id).addClass('menu-left-option-active');    
    }

    //Actualitzem el filtre avançat
    var div_id_adv = "#timeline-advanced-filter-" + id;

    if (timeline_filter[id - 1] === true) {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').removeClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').removeClass('timeline-advanced-filter-column-fitler-option-text-active');

    }
    else {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').addClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').addClass('timeline-advanced-filter-column-fitler-option-text-active');
    }

    //Fem un reset del cercador textual
    $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
    $('#timeline-search').removeClass('timeline-search-italic');

    refreshFilter(id);
}


function AdvFilter(id) {

    //Actualitzem el filtre avançat

    var div_id_adv = "#timeline-advanced-filter-" + id;

    if (adv_timeline_filter[id - 1] === true) {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').removeClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').removeClass('timeline-advanced-filter-column-fitler-option-text-active');
        adv_timeline_filter[id - 1] = false;

    }
    else {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').addClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').addClass('timeline-advanced-filter-column-fitler-option-text-active');
        adv_timeline_filter[id - 1] = true;
    }
}

function refreshFilter(id) {

    //alert('id : ' + id + ' text : ' + timeline_filter_texts[id - 1]);
    if (id == 3) //Controlem els todos
    {
        if (timeline_filter[2] === true) {
            /*timeline_filter[0] = false;
            timeline_filter[1] = false;
            timeline_filter[2] = false;*/

            timeline_filter[0] = true;
            timeline_filter[1] = false;
            timeline_filter[2] = false;

            //Advanced
            adv_timeline_filter[0] = true;
            adv_timeline_filter[1] = false;
            adv_timeline_filter[2] = false;
        }
        else {
            /*timeline_filter[0] = true;
            timeline_filter[1] = true;
            timeline_filter[2] = true;*/

            timeline_filter[0] = false;
            timeline_filter[1] = false;
            timeline_filter[2] = true;

            //Advanced
            adv_timeline_filter[0] = false;
            adv_timeline_filter[1] = false;
            adv_timeline_filter[2] = true;
        }

        Draw(1);
        Draw(2);
        Draw(3);
    }
    else if (id == 8) //Controlem els todos
    {
        if (timeline_filter[7] === true) {
            timeline_filter[3] = false;
            timeline_filter[4] = false;
            timeline_filter[5] = false;
            timeline_filter[6] = false;
            timeline_filter[7] = false;

            //Advanced
            adv_timeline_filter[3] = false;
            adv_timeline_filter[4] = false;
            adv_timeline_filter[5] = false;
            adv_timeline_filter[6] = false;
            adv_timeline_filter[7] = false;
        }
        else {
            timeline_filter[3] = true;
            timeline_filter[4] = true;
            timeline_filter[5] = true;
            timeline_filter[6] = true;
            timeline_filter[7] = true;

            //Advanced
            adv_timeline_filter[3] = true;
            adv_timeline_filter[4] = true;
            adv_timeline_filter[5] = true;
            adv_timeline_filter[6] = true;
            adv_timeline_filter[7] = true;
        }

        Draw(4);
        Draw(5);
        Draw(6);
        Draw(7);
        Draw(8);
    }
    else if (id == 11) //Controlem els todos
    {
        if (timeline_filter[10] === true) {
            timeline_filter[8] = false;
            timeline_filter[9] = false;
            timeline_filter[10] = false;

            //Advanced
            adv_timeline_filter[8] = false;
            adv_timeline_filter[9] = false;
            adv_timeline_filter[10] = false;
        }
        else {
            timeline_filter[8] = true;
            timeline_filter[9] = true;
            timeline_filter[10] = true;

            //Advanced
            adv_timeline_filter[8] = true;
            adv_timeline_filter[9] = true;
            adv_timeline_filter[10] = true;
        }

        Draw(9);
        Draw(10);
        Draw(11);
    }
    else if (timeline_filter[id - 1] === true) {
        timeline_filter[id - 1] = false;

        //Advanced
        adv_timeline_filter[id - 1] = false;
    }
    else {
        timeline_filter[id - 1] = true;

        //Advanced
        adv_timeline_filter[id - 1] = true;
    }

    //controlem l'advanced
    //adv_timeline_filter[id - 1] = timeline_filter[id - 1];


    //Ara deshabilitem els todos, en cas d'habilitar una opció
    if ((timeline_filter[0] == true) || (timeline_filter[1] == true)) {
        timeline_filter[2] = false;
        
        //Advanced
        adv_timeline_filter[2] = false;

        Draw(3);
    }

    if ((timeline_filter[3] == false) || (timeline_filter[4] == false) || (timeline_filter[5] == false) || (timeline_filter[6] == false)) {
        timeline_filter[7] = false;

        //Advanced
        adv_timeline_filter[7] = false;

        Draw(8);
    }

    if ((timeline_filter[8] == false) || (timeline_filter[9] == false)) {
        timeline_filter[10] = false;

        //Advanced
        adv_timeline_filter[10] = false;

        Draw(11);
    }

    var div_output = "";
    var div_output_1 = "";
    var div_output_2 = "";
    var div_output_3 = "";
    var count = 0;

    for(i=0; i<12; i++) {

        //if ((timeline_filter[i] === true) && (i != 2) && (i != 7) && (i != 10)) {
        if ((timeline_filter[i] === true) && (i != 7) && (i != 10)) {

            //if(count < 3)
                div_output_1 = div_output_1 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";
            /*else if (count < 6)
                div_output_2 = div_output_2 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";
            else
                div_output_3 = div_output_3 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";*/
            count++;
        }
    }

    if(div_output_1 != "")
        div_output_1 = '<div id="top-header-filter-1" class="top-header-filter-container">' + div_output_1 + '</div>';
    /*if(div_output_2 != "")
        div_output_2 = '<div id="top-header-filter-2" class="top-header-filter-container">' + div_output_2 + '</div>';
    if(div_output_3 != "")
	    div_output_3 = '<div id="top-header-filter-3" class="top-header-filter-container">' + div_output_3 + '</div>';*/

    //$('#top-header-filter').html('<table><tr><td>' + div_output_1 + '</td><td>' + div_output_2 + '</td><td>' + div_output_3 + '</td></tr></table>');
    $('#top-header-filter').html(div_output_1 + div_output_2 + div_output_3);
    setTimelineTop();

    //Refresquem el timeline
    TimeLineSearch();

    //var height = $('#header').height();
    var height = 42;
    $('#timeline-kit-modal').css("top", height + "px");
}

function Draw(id) {

    var div_id = "#menu-left-option-" + id;

    if (timeline_filter[id - 1] === true) {
        $(div_id).addClass('menu-left-option-active');
    }
    else {
        $(div_id).removeClass('menu-left-option-active');
    }

    //Actualitzem el filtre avançat
    var div_id_adv = "#timeline-advanced-filter-" + id;

    if (timeline_filter[id - 1] === true) {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').addClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').addClass('timeline-advanced-filter-column-fitler-option-text-active');

    }
    else {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').removeClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').removeClass('timeline-advanced-filter-column-fitler-option-text-active');
    }
}

function setTimelineTop() {
    var height = $('#header').height();
    $('#timeline').css("margin-top", height + "px");
}

function TimeLineSearch() {

    //Amaguem el comptador
    $('#timeline-counter').css('display', 'none');

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }

    search = encodeURIComponent(search);
    //alert(search);

    var tags = $("#timeline-advanced-filter-tags").val();

    if (tags == 'Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)') {
        tags = "";
    }

    tags = encodeURIComponent(tags);

    $.post(SUBDOMAIN_PATH + '/Timeline/TimelineSearch/?search=' + search + '&filter=' + timeline_filter + '&tags=' + tags, function (data) {

        //if (data == '') {
        if(data.indexOf("timeline-entry") < 0) //No hi ha entries
        {
            $("#timeline-noresult-modal").fadeIn("fast");
        }
        else {
            $('#timeline-kits-container').html(data);
            //var $timeline = $('#timeline');
            var $timeline = $('#timeline-kits-container');
            $timeline.imagesLoaded(function () {
                $timeline.masonry('reload');
            });
        }
    });

}

function TimeLineSearchAdvanced() {

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }

    search = encodeURIComponent(search);
    //alert(search);

    var tags = $("#timeline-advanced-filter-tags").val();

    if (tags == 'Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)') {
        tags = "";
    }

    tags = encodeURIComponent(tags);

    $.post(SUBDOMAIN_PATH + '/Timeline/TimelineSearch/?search=' + search + '&filter=' + timeline_filter + '&tags=' + tags, function (data) {
        //if (data == '') {
        if(data.indexOf("timeline-entry") < 0) //No hi ha entries
        {

            $("#timeline-advanced-filter-header").css("display","none");
            $("#timeline-advanced-noresult-modal").fadeIn("fast");
        }
        else {

            $("#timeline-advanced-filter-header").css("display", "block");
            $("#timeline-advanced-filter-modal").css('display', 'none');
            //Mostrem el header del filtre principal, i amaguem l'avançat
            $("#top-header-filter-adavanced").css('display', 'none');
            $("#top-header-filter").css('display', 'block');

            $('#timeline-kits-container').html(data);
            //var $timeline = $('#timeline');
            var $timeline = $('#timeline-kits-container');
            $timeline.imagesLoaded(function () {
                $timeline.masonry('reload');
            });
        }

        setTimelineTop();
    });

}

function getSearch() {

    var search = getUrlVars()["search"];

    /*if (search)
        search = search.replace("%2f", "/");*/

    return search;
}

function ResetAdvancedFilter() {

    for (i = 0; i < 10; i++) {
        adv_timeline_filter[i] = timeline_filter[i];
    }

    //Age
    adv_timeline_filter[11] = false;
    adv_timeline_filter[12] = false;
    adv_timeline_filter[13] = false;
    adv_timeline_filter[14] = false;
    adv_timeline_filter[15] = false;
    adv_timeline_filter[16] = false;

    //Genere
    adv_timeline_filter[17] = false;
    adv_timeline_filter[18] = false;

    for (i = 0; i < 19; i++) {
        DrawAdv(i + 1)
    }
}

function DrawAdv(id) {

    //Actualitzem el filtre avançat
    var div_id_adv = "#timeline-advanced-filter-" + id;

    if (timeline_filter[id - 1] === true) {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').addClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').addClass('timeline-advanced-filter-column-fitler-option-text-active');

    }
    else {
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-round:first').removeClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id_adv).children('.timeline-advanced-filter-column-fitler-option-text:first').removeClass('timeline-advanced-filter-column-fitler-option-text-active');
    }
}

$("#St_Jordi").live('click', function () {
    filtreSantJordi = true;
    alert("StJordi xivato");


    //Fem un reset del cercador textual
    $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
    $('#timeline-search').removeClass('timeline-search-italic');

    //Hem de refrescar el filtre principal
    MainFilterRefresh();
    TimeLineSearchAdvanced();
});

function aplicarFiltreTrender() {
    filtreTrenders = true;

    //Fem un reset del cercador textual
    $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
    $('#timeline-search').removeClass('timeline-search-italic');

    //Hem de refrescar el filtre principal
    MainFilterRefresh();
    TimeLineSearchAdvanced();
}

function MainFilterRefresh() {

    for (i = 0; i < 19; i++) {
        //alert("i : " + adv_timeline_filter[i]);
        timeline_filter[i] = adv_timeline_filter[i];
        //document.writeln(adv_timeline_filter[i]);
        Draw(i+1)
    }

    if (filtreSantJordi == true) {
        timeline_filter[3] = true;
        timeline_filter[1] = false;
    } else if (filtreTrenders == true) {
        timeline_filter[0] = true;
        timeline_filter[1] = false;
    }

    //posem els textos

    var div_output = "";
    var div_output_1 = "";
    var div_output_2 = "";
    var div_output_3 = "";
    var count = 0;

    //Mirem si hem de posar el label "Avanzado"

    var tags = $("#timeline-advanced-filter-tags").val();

    if (tags == 'Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)') {
        tags = "";
    }

    var render_advanced_filter_label = false;

    if (tags != "")
        render_advanced_filter_label = true;
    else {

        for (j = 12; j < 19; j++) {

            if (timeline_filter[j] === true)
                render_advanced_filter_label = true;
        }
    }

    /*if (render_advanced_filter_label === true)
        div_output_1 = div_output_1 + "<div class='top-header-filter-text'>" + timeline_filter_texts[11] + "</div>";*/


    for (i = 0; i < 12; i++) {

        if ((timeline_filter[i] === true) && (i != 7) && (i != 10)) {

            div_output_1 = div_output_1 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";
            count++;
        }
    }

    if (div_output_1 != "")
        div_output_1 = '<div id="top-header-filter-1" class="top-header-filter-container">' + div_output_1 + '</div>';

    //$('#top-header-filter').html('<table><tr><td>' + div_output_1 + '</td><td>' + div_output_2 + '</td><td>' + div_output_3 + '</td></tr></table>');
    $('#top-header-filter').html(div_output_1 + div_output_2 + div_output_3);
}

function AdvFilterRecommendedTag(id) {

    //$(this).toggleClass('timeline-kit-recommended-selected');

    var div_id = "#timeline-advanced-filter-recommeded-tag-" + id;

    var values = $('#timeline-advanced-filter-tags').val();

    if (values == 'Translate(STR_ANADE_ETIQUETAS_PARA_TU_BUSQUEDA)') {
        $('#timeline-advanced-filter-tags').val('');
        values = "";
    }

    var tag = $(div_id).children('.timeline-advanced-filter-column-fitler-option-text:first').html();

    //Mirem si ja existeix als tags, si hi és l'esborrem, sinó, l'afegim
    if (values.indexOf(tag) >= 0) {
        values = values.replace("," + tag, "");
        values = values.replace(tag, "");

        $(div_id).children('.timeline-advanced-filter-column-fitler-option-round:first').removeClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id).children('.timeline-advanced-filter-column-fitler-option-text:first').removeClass('timeline-advanced-filter-column-fitler-option-text-active');
    }
    else {
        if (values.length > 0) {
            values = values + "," + tag;
        }
        else
            values = tag;
        $(div_id).children('.timeline-advanced-filter-column-fitler-option-round:first').addClass('timeline-advanced-filter-column-fitler-option-round-active');
        $(div_id).children('.timeline-advanced-filter-column-fitler-option-text:first').addClass('timeline-advanced-filter-column-fitler-option-text-active');
    }
    $('#timeline-advanced-filter-tags').val(values)
}

function LoadRecommendedTagsAdvancedFilter() {
    $.post(SUBDOMAIN_PATH + '/Timeline/GetAdvancedFilterRecommendedTags', function (data) {

        $('#timeline-advanced-filter-recommended-tags-container').html(data);

    });
}

function isTouchDevice() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        return true;
    }
    else
        return false;
}