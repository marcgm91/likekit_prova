﻿function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

var mail_sent = false;

$(document).ready(function () {

    //Landing Page, send button
    $("#landing-content-button").live('click', function () {

        if (!mail_sent) {
            var mail = $('#beta-mail').val();

            if (isValidEmailAddress(mail)) {
                $('#landing-content-button').html("<div class='landing-loader'></div>");
                $.post('LandingPage/SaveMail/?mail=' + mail, function (data) {
                    //$('#landing-content-form').html(data);

                    if ($.trim(data) == 'Error') {
                        $("#landing-content-msg").fadeIn("slow");
                        //$('#landing-content-msg').css('display', 'block');
                        $('#landing-content-msg').html("<div class='mail_already_exists'>Éste correo ya está inscrito, gracias.</div>");
                        $('#landing-content-button').html("Envía la petición");
                    }
                    else if ($.trim(data) == 'ErrorMail') {
                        $("#landing-content-msg").fadeIn("slow");
                        //$('#landing-content-msg').css('display', 'block');
                        $('#landing-content-msg').html("Error enviando el correo, el correo es correcto?");
                        $('#landing-content-button').html("Envía la petición");
                    }
                    else if ($.trim(data) == 'Success') {
                        $("#landing-content-msg").fadeOut("slow");
                        //$('#landing-content-msg').css('display', 'none');
                        $('#landing-content-button').html("Petición enviada");
                        $('#landing-content-button').addClass('landing-content-button-on');
                        mail_sent = true;
                    }
                });
            }
            else {
                //alert('Mail no válido!');
                $("#landing-content-msg").fadeIn("slow");
                //$('#landing-content-msg').css('display', 'block');
                $('#landing-content-msg').html("Parece que la dirección no es válida.<br />Por favor comprueba que sea correcta.");
            }
        }
    });

    //Landing Page, send button
    $("#landing-option-1").live('click', function () {
        $("#landing-content-form").css('display', 'block');
        $("#landing-login-content-msg").fadeOut("slow");
        $('#landing-option-1').addClass('landing-option-disabled');
        $("#landing-login-form").css('display', 'none');
        $('#landing-option-2').removeClass('landing-option-disabled');
    })

    //Landing Page, send button
    $("#beta-mail").live('click', function () {

        var mail = $('#beta-mail').val();

        if (mail == 'Tu dirección e-mail') {
            $('#beta-mail').val('');
        }
    });

    //Landing Page, login
    $("#landing-option-2").live('click', function () {
        $("#landing-content-form").css('display', 'none');
        $('#landing-option-1').removeClass('landing-option-disabled');
        $("#landing-login-form").css('display', 'block');
        $('#landing-option-2').addClass('landing-option-disabled');
    })

    $(".landing-social-twitter").hover(function () {
        $(this).toggleClass('landing-social-twitter-on');
    });

    $(".landing-social-facebook").hover(function () {
        $(this).toggleClass('landing-social-facebook-on');
    });

    $(".landing-social-t").hover(function () {
        $(this).toggleClass('landing-social-t-on');
    });

    //Landing Page, login
    $("#landing-social-twitter").live('click', function () {
        window.open('https://twitter.com/#!/Likekit');
    })

    $("#landing-social-facebook").live('click', function () {
        window.open('https://www.facebook.com/Likekit');
    })

    $("#landing-social-t").live('click', function () {
        window.open('http://likekit.tumblr.com/');
    })

    //Landing Page, username
    $("#username").live('click', function () {

        var username = $('#username').val();

        if (username == 'Dirección e-mail') {
            $('#username').val('');
        }
    });

    //Landing Page, username
    $("#password").live('click', function () {
        var password = $('#password').val();
        if (password == 'Contraseña') {
            $('.landing-login-password').html("<input id=\"password\" name=\"password\" type=\"password\"/>");
            $('#password').focus();
        }
    });

    $("#password").live('focus', function () {
        var password = $('#password').val();
        if (password == 'Contraseña') {
            $('.landing-login-password').html("<input id=\"password\" name=\"password\" type=\"password\"/>");
            $('#password').focus();
        }
    });

    //Landing Page, Login button
    $("#landing-login-button").live('click', function () {
        $("#landing-login-content-msg").fadeIn("slow");
        $('#landing-login-content-msg').html("Usuario o contraseña incorrectos.");
    });

});