﻿$(window).load(function () {

    setTimelineTop();

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    //Ho fem així per evitar que al carrgar la pàgina amb el ratolí damunt de l'element, doni problemes
    $(".nowwith-entry-image").live({
        mouseenter:
            function () {
                $(this).children('.nowwith-entry-info:first').css('display', 'block');
            },
        mouseleave:
            function () {
                $(this).children('.nowwith-entry-info:first').css('display', 'none');
            }
        }
    );

    //Ho fem així per evitar que al apretar el botó es comporti de forma estranya
    $(".nowwith-entry-info-like-link").live({
        mouseenter:
            function () {
                $(this).addClass('nowwith-entry-info-like-link-on');
            },
        mouseleave:
            function () {
                $(this).removeClass('nowwith-entry-info-like-link-on');
            }
    });

    $(".nowwith-entry-info-dislike-link").live({
            mouseenter:
            function () {
                $(this).addClass('nowwith-entry-info-dislike-link-on');
            },
            mouseleave:
            function () {
                $(this).removeClass('nowwith-entry-info-dislike-link-on');
            }
        }
    );

    //Hover now-with entry
    $(".profile-now-with-images-entry").live('hover', function () {
        $(this).children('.profile-now-with-close:first').toggle();
    });

    $(".profile-now-with-close-img").live('hover', function () {
        $(this).closest('.profile-now-with-images-entry').children('.profile-now-with-tip:first').toggle();
    });

    $(".profile-now-with-add-img").live('hover', function () {
        $(this).closest('.profile-now-with-images-entry').children('.profile-now-with-tip-add:first').toggle();
    })

    //Now with Search
    $("#now-with-product-search").live('click', function () {

        $('#now-with-product-search').addClass('timeline-search-italic');
        
        var search = $('#now-with-product-search').val();
        if (search == 'Translate(STR_AHORA_ESTOY_CON_MIN)') {
            $('#now-with-product-search').val('');
        }
    });

    //Now with Search Enter
    $("#now-with-product-search").keypress(function (e) {

        var search = $('#now-with-product-search').val();

        if ((search.length > 2) && (e.keyCode == 13)) {
            //NowWithSearch();

            //Busquem al WS
            $('#timeline-now-with-search-result-container').css('display', 'none');
            $('#timeline-now-with-webservice-search').css('display', 'block');
            $('#timeline-now-with-search').css('padding-bottom', '0');

            NowWithWSSearch();
        }
    });

    //Now with Search Keyup
    $("#now-with-product-search").keyup(function (e) {

        var search = $('#now-with-product-search').val();

        if ((search.length > 2) && (e.keyCode != 13)) {
            NowWithSearch();
        }
    });

    //Now With close
    $("#timeline-now-with-close").live('click', function () {
        $('#timeline-now-with-modal').css('display', 'none');
    });

    //Product WS Search
    $("#timeline-now-with-seach-button").click(function () {
        $('#timeline-now-with-search-result-container').css('display', 'none');
        $('#timeline-now-with-webservice-search').css('display', 'block');
        $('#timeline-now-with-search').css('padding-bottom', '0');

        NowWithWSSearch();
    });

    $("#timeline-now-with-seach-button").hover(function () {
        $(this).toggleClass('timeline-now-with-seach-button-on');
    });

    //WS Search more results
    $("#timeline-now-with-search-result-more-results").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-result-more-results-on');
        $("#timeline-now-with-search-result-more-results-container").toggle();
    });

    $("#timeline-now-with-search-container-category-books").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-books-on');
    });

    $("#timeline-now-with-search-container-category-series").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-series-on');
    });

    $("#timeline-now-with-search-container-category-music").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-music-on');
    });

    $("#timeline-now-with-search-container-category-cinema").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-cinema-on');
    });

    $("#timeline-now-with-search-container-category-books").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-books-selected');
        SetNowWithCategoryForm(1);
    });

    $("#timeline-now-with-search-container-category-series").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-series-selected');
        SetNowWithCategoryForm(2);
    });

    $("#timeline-now-with-search-container-category-music").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-music-selected');
        SetNowWithCategoryForm(4);
    });

    $("#timeline-now-with-search-container-category-cinema").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-cinema-selected');
        SetNowWithCategoryForm(3);
    });
});

function setTimelineTop() {
    var height = $('#header').height();
    $('#nowwith').css("margin-top", height + "px");
}

function TimeLineSearch() {

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }

    //alert(search);

    $.post(SUBDOMAIN_PATH + '/Timeline/TimelineSearch/?search=' + search + '&filter=' + timeline_filter, function (data) {
        $('#timeline-kits-container').html(data);
        //alert(data);
        //Hauriem d'executar mazonry?
        var $timeline = $('#timeline');
        //var $timeline = $('#timeline');

        $timeline.imagesLoaded(function () {
            $timeline.masonry('reload');
            /*$timeline.masonry({
            itemSelector: '.timeline-entry',
            columnWidth: 240
            });*/
        });

    });

}

function AddNowWith(userid) {
    $('#now-with-product-search').val('Translate(STR_AHORA_ESTOY_CON_MIN)');
    $('#timeline-now-with-modal').css('display', 'block');
    $("#timeline-now-with-form").css('display', 'none');
    $("#timeline-now-with-search").css('display', 'block');
    $('#timeline-now-with-search-result-container').css('display', 'none');
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}

function NowWithSearch() {

    var search = $('#now-with-product-search').val();

    if (search == 'Translate(STR_AHORA_ESTOY_CON_MIN)') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#now-with-product-categories').val();

    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-now-with-search-result-container').html(data);
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-now-with-search-result-container').css('display', 'block');
    });
}

function NowWithAddProduct(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithAddProductReload/?id=' + id, function (data) {
        //location.reload();
        var userid = $('#profile-user-id').val();
        $('#timeline-now-with-modal').css('display', 'none');

        $('#nowwith-kits-container').html(data);
        var $nowwith = $('#nowwith');

        $nowwith.imagesLoaded(function () {
            $nowwith.masonry('reload');
        });
    });
}

function DelNowWith(id, userid) {
    $.post(SUBDOMAIN_PATH + '/Timeline/DelNowWithReloadHtml/?id=' + id + '&userid=' + userid, function (data) {
        //location.reload();

        $('#nowwith-kits-container').html(data);
        var $nowwith = $('#nowwith');

        $nowwith.imagesLoaded(function () {
            $nowwith.masonry('reload');
        });
    });
}


/**
    
WS VERSION

**/

function NowWithWSSearch() {

    var search = $('#now-with-product-search').val();

    if (search == 'Translate(Recomendar)...') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#now-with-product-categories').val();

    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithWSSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-now-with-search-result-container').html(data);
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-now-with-search-result-container').css('display', 'block');
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-now-with-search').css('padding-bottom', '24px');
    });
}

function NowWithAddWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations) {

    ASIN = encodeURIComponent(ASIN);
    Title = encodeURIComponent(Title);
    Author = encodeURIComponent(Author);
    Year = encodeURIComponent(Year);
    Category = encodeURIComponent(Category);
    LargeImage = encodeURIComponent(LargeImage);
    URL = encodeURIComponent(URL);
    Description = encodeURIComponent(Description);
    EAN = encodeURIComponent(EAN);
    Observations = encodeURIComponent(Observations);

    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithWSAddProductReload/?ASIN=' + ASIN + "&Title=" + Title + "&Author=" + Author + "&Year=" + Year + "&Category=" + Category + "&LargeImage=" + LargeImage + "&URL=" + URL + "&Description=" + Description + "&EAN=" + EAN + "&Observations=" + Observations, function (data) {

        var userid = $('#profile-user-id').val();
        $('#timeline-now-with-modal').css('display', 'none');

        $('#nowwith-kits-container').html(data);
        var $nowwith = $('#nowwith');

        $nowwith.imagesLoaded(function () {
            $nowwith.masonry('reload');
        });
    });
}

/**

FI WS VERSION

**/


function SetNowWithCategoryForm(id_category) {
    var values = $('#now-with-product-categories').val();

    //Mirem si ja existeix a les categories, si hi és l'esborrem, sinó, l'afegim
    if (values.indexOf(id_category) >= 0) {
        values = values.replace("," + id_category, "");
        values = values.replace(id_category, "");
    }
    else {
        if (values.length > 0) {
            values = values + "," + id_category;
        }
        else
            values = id_category;
    }

    $('#now-with-product-categories').val(values);
}