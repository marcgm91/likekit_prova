﻿
//Funció que fà que es vegi el vídeo.
$(document).ready(function () {
    //Mirem si hem de mostrar el vídeo en funció de la cookie
    var show_video = $.cookie("show_video");

    if (show_video != 'yes') {

        //Guardem la cookie i mostrem el vídeo
        $.cookie("show_video", "yes", { expires: 30, path: '/' });

        //Mostrem el vídeo
        $("#home-video-modal").css('display', 'block');
        //$('iframe#home-video-src').attr('src', 'http://player.vimeo.com/video/50758737?autoplay=1');
        $('iframe#home-video-src').attr('src', 'https://www.youtube.com/embed/oD7h53ZWfe4?rel=0&autoplay=1');
        
    }
    else
        $('iframe#home-video-src').attr('src', '');
});

$(window).load(function () {

    setTimelineTop();

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    //Search
    $("#timeline-search").live('click', function () {

        $('#timeline-search').addClass('timeline-search-italic');

        var search = $('#timeline-search').val();
        if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
            $('#timeline-search').val('');
        }
    });


    $("#timeline-search").live('focusout', function () {
        var search = $('#timeline-search').val();
        if (search == '') {
            $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
            $('#timeline-search').removeClass('timeline-search-italic');
        }
    });


    //timeline-search Enter
    $("#timeline-search").keypress(function (e) {
        if (e.keyCode == 13) {
            TimeLineSearch();
        }
    });

    $("#timeline-noresult-close").live('click', function () {
        $("#timeline-noresult-modal").css('display', 'none');
    });

    $("#filter-likekit").live('click', function () {
        $(this).toggleClass('menu-filter-section-selected');
        $("#filter-likekit-content").toggle();
    })


    $("#menu-left-option-12").live('click', function () {
        $("#timeline-advanced-filter-modal").css('display', 'block');
    });

    $("#timeline-advanced-filter-close").live('click', function () {
        $("#timeline-advanced-filter-modal").css('display', 'none');
    });

    $(".timeline-advanced-filter-column-fitler-option").hover(function () {
        $(this).children('.timeline-advanced-filter-column-fitler-option-round:first').toggleClass('timeline-advanced-filter-column-fitler-option-round-selected');
        $(this).children('.timeline-advanced-filter-column-fitler-option-text:first').toggleClass('timeline-advanced-filter-column-fitler-option-text-selected');
    });

    $("#home-twitter").hover(function () {
        $(this).toggleClass('home-twitter-on');
    });

    $("#home-twitter").click(function () {
        window.open('http://twitter.com/likekit');
    });

    $("#home-facebook").hover(function () {
        $(this).toggleClass('home-facebook-on');
    });

    $("#home-facebook").click(function () {
        window.open('http://www.facebook.com/Likekit');
    });

    $("#home-tumblr").hover(function () {
        $(this).toggleClass('home-tumblr-on');
    });

    $("#home-tumblr").click(function () {
        window.open('http://likekit.tumblr.com/');
    });

    /*$("#que-es-likekit-content").hover(function () {
    $(this).toggleClass('active');
    });*/

    $("#que-es-likekit-content").live('click', function () {
        $("#home-video-modal").css('display', 'block');
        //$('iframe#home-video-src').attr('src', 'http://player.vimeo.com/video/50758737?autoplay=1');
        $('iframe#home-video-src').attr('src', 'https://www.youtube.com/embed/oD7h53ZWfe4?rel=0&autoplay=1');
    });

    $("#que-es-likekit-content").hover(function () {
        $(this).toggleClass('que-es-likekit-content-on');
    });

    $("#home-login-button").hover(function () {
        $(this).toggleClass('home-login-button-on');
    });

    $("#home-register-button").hover(function () {
        $(this).toggleClass('home-register-button-on');
    });

    $("#home-login-button").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
    });

    $("#home-register-button").live('click', function () {

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Register/Facebook']);
        }

        parent.location.href = SUBDOMAIN_PATH + '/Account/Register';
    });

    //Home video modal close
    $("#home-video-close").live('click', function () {
        $('#home-video-modal').css('display', 'none');
        $('iframe#home-video-src').attr('src', '');
    });


    //Ho fem així per evitar que al carrgar la pàgina amb el ratolí damunt de l'element, doni problemes
    $(".timeline-entry-image").live({
        mouseenter:
            function () {
                $(this).children('.timeline-entry-info:first').css('display', 'block');
            },
        mouseleave:
            function () {
                $(this).children('.timeline-entry-info:first').css('display', 'none');
            }
    }
    );

    //Add kit textarea
    $("#add_kit_comment").live('click', function () {

        var comment = $('#add_kit_comment').val();
        if (comment == 'Translate(STR_COMENTA_TU_TAMBIEN_ESTA_PUBLICACION)') {
            $('#add_kit_comment').val('');
        }
    });

    //Add kit textarea
    $("#add_kit_comment").live('focusout', function () {

        var comment = $('#add_kit_comment').val();
        if (comment == '') {
            $('#add_kit_comment').val('Translate(STR_COMENTA_TU_TAMBIEN_ESTA_PUBLICACION)');
        }
    });

    //Add kit textarea
    $("#add_kit_comment").live('focus', function () {

        var comment = $('#add_kit_comment').val();
        if (comment == 'Translate(STR_COMENTA_TU_TAMBIEN_ESTA_PUBLICACION)') {
            $('#add_kit_comment').val('');
        }
    });

    //Add kit textarea
    $("#add_kit_comment").live('keyup', function () {

        var kit_id = $('#kit-id').val();

        LoginRefKit(kit_id);

        /*var comment = $('#add_kit_comment').val();
        if (comment != '')
        $('#kit-view-container-comment-button').addClass('kit-view-container-comment-button-enabled');
        else
        $('#kit-view-container-comment-button').removeClass('kit-view-container-comment-button-enabled');*/
    });

    //Hover user kit
    $(".kit-view-container-users-entry").live('hover', function () {
        $(this).children('.kit-view-container-users-entry-info:first').toggle();
    });

    $("#kit-view-menu-info").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#kit-view-menu-info-content").toggle();
    })

    $("#kit-view-menu-tags").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#kit-view-menu-tags-content").toggle();
    })

    //Hover lo quiero
    $("#kit-user-loquiero").hover(function () {
        $(this).toggleClass('kit-user-loquiero-selected');
    });

    //Kit view comment
    $(".kit-view-comment").live('click', function () {
        $("#add_kit_comment").focus();
    })

    $("#top-home-container-right-twitter").click(function () {
        window.open('http://twitter.com/likekit');
    });

    $("#top-home-container-right-facebook").click(function () {
        window.open('http://www.facebook.com/Likekit');
    });

    $("#top-header-wide-login").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
    });

    $("#enllac_registrat-emergent-registrat").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
    });

    $("#top-home-container-fb-button").on('click', function () {
        //parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterRegister';

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Likekit/Entrar']);
        }

        parent.location.href = SUBDOMAIN_PATH + '/Account/Register';

    });

    $("#boto_uneixte-emergent-registrat").on('click', function () {
        //parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterRegister';

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Likekit/Entrar']);
        }

        parent.location.href = SUBDOMAIN_PATH + '/Account/Register';

    });

    //Close Lo quiero dialog
    $("#kit-user-loquiero-container-close").live('click', function () {
        $('#kit-user-loquiero-container').css('display', 'none');
    })
});

function Draw(id) {

    var div_id = "#menu-left-option-" + id;

    if (timeline_filter[id - 1] === true) {
        $(div_id).addClass('menu-left-option-active');
    }
    else {
        $(div_id).removeClass('menu-left-option-active');
    }
}

function setTimelineTop() {
    var height = $('#header').height();
    $('#timeline').css("margin-top", height + "px");
}

function TimeLineSearch() {

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }
    search = encodeURIComponent(search);
    //alert(search);

    $.post(SUBDOMAIN_PATH + '/Home/HomeSearch/?search=' + search, function (data) {
        if (data == '') {
            $("#timeline-noresult-modal").fadeIn("fast");
        }
        else {
            $('#timeline-kits-container_home').html(data);
            //var $timeline = $('#timeline');
            var $timeline = $('#timeline-kits-container_home');

            $timeline.imagesLoaded(function () {
                $timeline.masonry('reload');
            });
        }
    });

}

function LoginRefKit(id) {
    parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn?ReturnUrl=%2fTimeline%2fItem%2f' + id;
}

function LoQuiero(id, url) {
    $.post(SUBDOMAIN_PATH + '/Timeline/LoQuiero/?id=' + id, function (data) {
        window.open(url);
    });
}

function HomeKit(KitID) {
    //parent.location.href = "/Home/Item/" + KitID;
    //parent.location.href = SUBDOMAIN_PATH + KitID;
    parent.location.href = KitID;
}

function HomeFilter(type) {
    $('#type').val(type);
    
    $.post(SUBDOMAIN_PATH + '/Home/HomeFilter/?type=' + type, function (data) {
        if (data == '') {
            $("#timeline-noresult-modal").fadeIn("fast");
        }
        else {
            $('#timeline-kits-container_home').html(data);
            //var $timeline = $('#timeline');
            var $timeline = $('#timeline-kits-container_home');

            $timeline.imagesLoaded(function () {
                $timeline.masonry('reload');
            });
        }
    });

}

$(window).scroll(function () {
    if ($(this).scrollTop() != 0) {
        $('#footer-wide').css('display', 'none');
    } else {
        //$('.timeline-to-top').fadeOut();
    }

    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        if ($('#end').val() == 1) {
            $('#footer-wide').css('display', 'block');
        }
    }
});

function LoQuieroDialog() {

    $('#kit-user-loquiero-container').css({
        //position: 'absolute',
        //left: ($(window).width() - $('#kit-user-loquiero-container').outerWidth()) / 2,
        top: ($(window).height() - $('#kit-user-loquiero-container').outerHeight()) / 2
    });

    $('#kit-user-loquiero-container').css('display', 'block');
}


//Close "emergent registrat"
$("#emergent-registrat-close").live('click', function () {
    $('#emergent-registrat').css('display', 'none');
})

$(".kit-view-like").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
$(".kit-view-comment").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
$(".kit-view-kitit").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
$(".kit-view-wishlist").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
$(".kit-view-tweet").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
$(".kit-view-share").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
$(".kit-view-mail").live('click', function () {
    $('#emergent-registrat').css('display', 'block');
})
