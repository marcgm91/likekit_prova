﻿
var valid_form_mail = false;
var valid_form_password = false;

$(document).ready(function () {

    var vars = [], hash;
    var hashes = window.location.href.split('/');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    if ((vars[5] != '') && ((typeof vars[5] !== "undefined"))){
        //LogOff
    }
    else {
        //Mirem si hem de fer l'autologin
        var autologin = $.cookie("likekit_autologin");

        if (autologin != '') {
            //fem l'autologin
            $.post(SUBDOMAIN_PATH + '/Account/AutoLogOn/?UserID=' + autologin, function (data) {

                if (data) {
                    if (data.Success == "true") {

                        $.cookie("likekit_autologin", data.Message, { expires: 30, path: '/' });

                        var returnurl = $('#returnUrl').val();

                        parent.location.href = SUBDOMAIN_PATH + '/Timeline';
                    }
                    else if (data.Success == "false") {
                        $.cookie("likekit_autologin", "", { expires: 30, path: '/' });
                    }
                }
            });
        }
    }
});

$(window).load(function () {

    getReturnUrl();

    //Landing Page, login
    $("#landing-option-2").live('click', function () {
        $("#landing-content-form").css('display', 'none');
        $('#landing-option-1').removeClass('landing-option-disabled');
        $("#landing-login-form").css('display', 'block');
        $('#landing-option-2').addClass('landing-option-disabled');
    })

    $(".login-submit").hover(function () {
        $(this).toggleClass('login-submit-on');
    });

    $(".login-twitter").hover(function () {
        $(this).toggleClass('login-twitter-on');
    });

    $(".login-facebook").hover(function () {
        $(this).toggleClass('login-facebook-on');
    });

    $(".login-social-twitter").hover(function () {
        $(this).toggleClass('login-social-twitter-on');
    });

    $(".login-social-twitter").click(function () {
        window.open('http://twitter.com/likekit');
    });

    $(".login-social-facebook").hover(function () {
        $(this).toggleClass('login-social-facebook-on');
    });

    $(".login-social-facebook").click(function () {
        window.open('http://www.facebook.com/Likekit');
    });

    $(".login-social-t").hover(function () {
        $(this).toggleClass('login-social-t-on');
    });

    $(".login-social-t").click(function () {
        window.open('http://likekit.tumblr.com/');
    });

    $(".login-label").hover(function () {
        $(this).toggleClass('login-label-active');
    });

    //Login Page, username
    $("#UserName").on('click', function () {

        var username = $('#UserName').val();
        
        if (username == 'Translate(STR_TU_DIRECCION_EMAIL)') {
            $('#UserName').val('');
            $('#UserName').addClass('login-italic');
        }
    });

    //Login Page, username
    $("#UserName").on('focus', function () {

        var username = $('#UserName').val();

        if (username == 'Translate(STR_TU_DIRECCION_EMAIL)') {
            $('#UserName').val('');
            $('#UserName').addClass('login-italic');
        }
    });

    $("#UserName").live('focusout', function () {
        CheckEmail();
    });

    $("#UserName").live('keyup', function (e) {
        if (e.keyCode == 13) {
            CheckEmail();
            $("#password_text").focus();
        }
    });

    $("#password_text").on('click', function () {
        $(this).css('display', 'none');
        $('#Password').css('display', 'block');
        $('#Password').focus();
    });

    $("#password_text").on('focus', function () {
        $(this).css('display', 'none');
        $('#Password').css('display', 'block');
        $('#Password').focus();
    });

    $("#Password").live('focusout', function () {
        CheckPassword();
    });

    $("#Password").live('keyup', function (e) {
        if (e.keyCode == 13) {
            Login();
        }
    });

    $("#login-twitter").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterLogOn';
    });

    $("#login-facebook").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
    });

    $("#password-recovery-link").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/PasswordRecovery';
    });

    //Login Page, login button
    $("#login-submit-btn").live('click', function () {
        Login();
    });


    $("#rememberme").live('click', function () {
        var check_remember = $('#check_remember').val();

        $(this).toggleClass('login-label-selected');

        $('#rememberme-check').toggleClass('rememberme-on');

        if (check_remember == "0") {
            $('#check_remember').val("1");
        }
        else {
            $('#check_remember').val("0");
        }
    });


});

function CheckEmail() {

    var mail = $('#UserName').val();

    if (isValidEmailAddress(mail)) {
        $.post(SUBDOMAIN_PATH + '/Account/CheckMailLogin/?mail=' + mail, function (data) {
            
            if ($.trim(data) == 'Error') {
                $("#login-error-username-tip").fadeIn("slow");
                $('#login-error-username-tip').html("Translate(STR_PARECE_QUE_LA_DIRECCION_NO_ES_VALIDA_POR_FAVOR_COMPRUEBA_QUE_SEA_CORRECTA)");
                $("#login-username").addClass('login-editor-field-error');

                valid_form_mail = false;
            }
            else if ($.trim(data) == 'ErrorMail') {
                $("#login-error-username-tip").fadeIn("slow");
                $('#login-error-username-tip').html("Translate(STR_PARECE_QUE_LA_DIRECCION_NO_ES_VALIDA_POR_FAVOR_COMPRUEBA_QUE_SEA_CORRECTA)");
                $("#login-username").addClass('login-editor-field-error');
                valid_form_mail = false;
            }
            else if ($.trim(data) == 'Success') {
                //Ara comprovem el login
                $("#login-error-username-tip").fadeOut("slow");
                $("#login-username").removeClass('login-editor-field-error');
                valid_form_mail = true;
            }
        });
    }
    else {
        $("#login-error-username-tip").fadeIn("slow");
        $('#login-error-username-tip').html("Translate(STR_PARECE_QUE_LA_DIRECCION_NO_ES_VALIDA_POR_FAVOR_COMPRUEBA_QUE_SEA_CORRECTA)");
        $("#login-username").addClass('login-editor-field-error');
        valid_form_mail = false;
    }
}

function CheckPassword() {

    var password = $('#Password').val();
    
    if (password == '') {
        $("#register-password-tip").fadeIn("slow");
        $('#register-password-tip').html("Translate(STR_LA_CONTRASENA_NO_PUEDE_ESTAR_VACIA)");
        $("#login-password").addClass('login-editor-field-error');
        valid_form_password = false;
    }
    else {
        $("#login-error-password-tip").fadeOut("slow");
        $("#login-password").removeClass('login-editor-field-error');
        valid_form_password = true;
    }
}

function Login() {

    CheckEmail();
    CheckPassword();

    if ((valid_form_mail) && (valid_form_password)) {
        var mail = $('#UserName').val();
        var password = encodeURIComponent($('#Password').val());
        $.post(SUBDOMAIN_PATH + '/Account/LogOn/?UserName=' + mail + '&Password=' + password, function (data) {

            if (data) {
                if (data.Success == "true") {
                    var returnurl = $('#returnUrl').val();

                    var check_remember = $('#check_remember').val();

                    //Mirem si hem de guardar la cookie
                    if (check_remember == "1") {
                        //alert(data.Message);
                        $.cookie("likekit_autologin", data.Message, { expires: 30, path: '/' });
                    }

                    if (returnurl != '')
                        parent.location.href = returnurl;
                    else
                        parent.location.href = SUBDOMAIN_PATH + '/Timeline';
                }
                else if (data.Success == "false") {
                    $("#login-error-username-tip").fadeIn("slow");
                    $('#login-error-username-tip').html(data.Message);
                }
            }

            /*alert('submit');

            if ($.trim(data) == 'Error') {
            $("#login-error-username-tip").fadeIn("slow");
            $('#login-error-username-tip').html("Translate(STR_PARECE_QUE_LA_DIRECCION_NO_ES_VALIDA_POR_FAVOR_COMPRUEBA_QUE_SEA_CORRECTA)");

            }
            else if ($.trim(data) == 'ErrorMail') {
            $("#login-error-username-tip").fadeIn("slow");
            $('#login-error-username-tip').html("Translate(STR_PARECE_QUE_LA_DIRECCION_NO_ES_VALIDA_POR_FAVOR_COMPRUEBA_QUE_SEA_CORRECTA)");
            }
            else if ($.trim(data) == 'Success') {
            //Ara comprovem el login
            $("#login-error-username-tip").fadeOut("slow");
            }*/
        });
    }
}

function getReturnUrl() {

    //getUrlVars();
    var returnurl = getUrlVars()["ReturnUrl"];
    
    if (returnurl)
        returnurl = returnurl.replace("%2f", "/");
    $('#returnUrl').val(returnurl);
}