﻿
$(document).ready(function () {

    setTimelineTop();

    $("#about-main-mediakit-social-twitter").hover(function () {
        $(this).toggleClass('about-main-mediakit-social-twitter-on');
    });

    $("#about-main-mediakit-social-twitter").click(function () {
        window.open('http://twitter.com/likekit');
    });

    $("#about-main-mediakit-social-facebook").hover(function () {
        $(this).toggleClass('about-main-mediakit-social-facebook-on');
    });

    $("#about-main-mediakit-social-facebook").click(function () {
        window.open('http://www.facebook.com/Likekit');
    });

    $("#about-main-mediakit-social-tumblr").hover(function () {
        $(this).toggleClass('about-main-mediakit-social-tumblr-on');
    });

    $("#about-main-mediakit-social-tumblr").click(function () {
        window.open('http://likekit.tumblr.com/');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    $("#about-main-mediakit-btn").hover(function () {
        $(this).toggleClass('about-main-mediakit-btn-on');
    });

});

function setTimelineTop() {
    var height = $('#header').height();
    $('#about-container').css("margin-top", height + "px");
}