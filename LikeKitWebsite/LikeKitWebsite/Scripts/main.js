﻿//Empty if not beta
//var SUBDOMAIN_PATH = '/beta';// '/beta';
var SUBDOMAIN_PATH = '';// '/beta'; // '/beta';

var MAX_COUNTER = 1000;

//Detext scroll direction
var lastScrollTop = 0;

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

var kit_selected_tags = Array();
var kit_form_selected_categories = Array();

function isStrongPassword(password) {

    var count = 0;

    //validate the length
    if (password.length > 7) {
        count = count + 1;
    }
    else {
        return false;
    }
    //validate letter
	if (password.match(/[A-z]/)) {
	    count = count + 1;
    } 

    //validate capital letter
	if (password.match(/[A-Z]/)) {
	    count = count + 1;
    } 

    //validate number
	if (password.match(/\d/)) {
	    count = count + 1;
    }

    if (count > 2)
        return true;
    else
        return false;
}

var mail_sent = false;

$(document).ready(function () {

    //No posem res al src del vídeo, així no es carrega en segon pla
    $('iframe#home-video-src').attr('src', '');

    //IMPORTANT: Fem que el menú esquerra no sigui fixe en resolucions verticals més petites de 800px 
    /*if ($(window).height() < 800) {
    $('#left_menu').css('position', 'absolute');
    }*/

    //Mirem si hem d'amagar o no la "bombolleta" amb les interaccions
    var num_interactions = $('#menu-user-interactions-value').html();

    if (num_interactions == 0)
        $('#menu-user-interactions-value').css('display', 'none');
    else
        $('#menu-user-interactions-value').css('display', 'block');

    var num_interactions_inside = $('#menu-user-interactions-inside-value').html();

    if (num_interactions_inside == 0)
        $('#menu-user-interactions-inside-value').css('display', 'none');
    else
        $('#menu-user-interactions-inside-value').css('display', 'block');

    //Kit main button
    $('#main-kit-button').click(function () {
        $('.overlay-container').fadeIn(function () {

            window.setTimeout(function () {
                $('.window-container').addClass('window-container-visible');
            }, 100);

        });
    });

    //Modal window close button
    $('.close').live('click', function () {
        $('.overlay-container').fadeOut().end().find('.window-container').removeClass('window-container-visible');
    });

    //Model window, preventing back content click
    $('#overlay-container').click(function () {
        return false;
    });

    //Kit other recommendation users
    $("#kit-other-users").live('click', function () {
        var product_id = 1;

        $.post(SUBDOMAIN_PATH + '/Timeline/KitOtherUsers/?id=' + product_id, function (data) {
            $('#window-content').html(data);
        });

        $('.overlay-container').fadeIn(function () {

            window.setTimeout(function () {
                $('.window-container').addClass('window-container-visible');
            }, 100);

        });
    });

    //Logout
    $("#logout").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOff';
    });

    //Logo
    //$("#logo-vertical").live('click', function () {
    /*$("#top-header-wide-logo").live('click', function () {
    parent.location.href = SUBDOMAIN_PATH + '/Timeline';
    });*/

    //Logo
    $("#logo-vertical-home").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Home';
    });

    //Logo kit
    //$("#top-kit-button").live('click', function () {
    $("#top-header-kit-button").live('click', function () {

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Kit/StartKit']);
        }

        //Mostem i fem reset de les dades
        kit_selected_tags = Array();
        kit_form_selected_categories = Array();

        //Fem un reset dels filtres per categoria
        ResetKitCategories();

        //$('html,body').animate({ scrollTop: 0 }, 'slow');

        //Calculem el top del #timeline-kit-modal (segons el filtre)
        var height = $('#header').height();
        if ($('#filter-likekit').length > 0) {
            height = 42; //Forcem a 60 per quedar per sobre dels filtres
        }

        $('#header').css("z-index", "1");

        $('#timeline-kit-modal').css("top", height + "px");

        $('#kit-product-search').val('Translate(STR_QUE_QUIERES_RECOMENDAR)');
        $('#timeline-kit-modal').css('display', 'block');
        $("#timeline-kit-form").css('display', 'none');
        $("#timeline-kit-search").css('display', 'block');
        $('#timeline-kit-search-result-container').css('display', 'none');

        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
        $('#timeline-kit-back').css('display', 'none');

        $("#kit-product-search").focus();
        $("#kit-product-search").caretToStart();

        KitAccessLog();
    });

    //$("#top-kit-button").hover(function () {
    $("#top-header-kit-button").hover(function () {
        $(this).toggleClass('top-header-kit-button-on');
    });

    //Kit close
    $("#timeline-kit-close").live('click', function () {
        $('#timeline-kit-modal').css('display', 'none');
        $('#header').css("z-index", "10");
        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
        $('#timeline-kit-back').css('display', 'none');
        $('#timeline-kit-published').css('display', 'none');
        $('#timeline-kit-form-loading-publish').css('display', 'none');

        //Esborrem el comentari
        $('#kit-form-comment').val('Translate(STR_TU_RECOMENDACION)');
    });

    //Kit back
    $("#timeline-kit-back").live('click', function () {

        //Mostem i fem reset de les dades
        kit_selected_tags = Array();
        //$('html,body').animate({ scrollTop: 0 }, 'slow');

        //$('#kit-product-search').val('Translate(STR_QUE_QUIERES_RECOMENDAR)');
        $('#timeline-kit-modal').css('display', 'block');
        $("#timeline-kit-form").css('display', 'none');
        $("#timeline-kit-search").css('display', 'block');
        $('#timeline-kit-search-result-container').css('display', 'block');

        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
        $('#timeline-kit-back').css('display', 'none');
        $('#timeline-kit-form-loading-publish').css('display', 'none');

        //Esborrem el comentari
        $('#kit-form-comment').val('Translate(STR_TU_RECOMENDACION)');
        //Esborrem els tags
        $('kit_form_tags').val('');
    });

    //Kit Search click
    $("#kit-product-search").live('click', function () {

        $('#kit-product-search').addClass('timeline-search-italic');

        var search = $('#kit-product-search').val();
        if (search == 'Translate(STR_QUE_QUIERES_RECOMENDAR)') {
            $('#kit-product-search').val('');
        }
    });

    //Kit Search focus
    $("#kit-product-search").live('focus', function () {
        $('#kit-product-search').addClass('timeline-search-italic');
    });

    //Kit Search Enter
    $("#kit-product-search").keypress(function (e) {

        var search = $('#kit-product-search').val();

        if (search == 'Translate(STR_QUE_QUIERES_RECOMENDAR)') {
            $('#kit-product-search').val('');
        }

        if ((search.length > 2) && (e.keyCode == 13)) {
            //KitSearch();

            //Busquem al WS
            $('#timeline-kit-search-result-container').css('display', 'none');
            $('#timeline-kit-webservice-search').css('display', 'block');
            $('#timeline-kit-search').css('padding-bottom', '0');

            KitWSSearch();
        }
    });

    //Kit Search Enter Keyup
    $("#kit-product-search").keyup(function (e) {

        var search = $('#kit-product-search').val();

        if ((search.length > 2) && (e.keyCode != 13)) {
            KitSearch();
        }

    });

    //Kit show info
    $("#timeline-kit-product-moreinfo-title").live('click', function () {

        $('#timeline-kit-product-moreinfo-title').toggleClass('moreinfo-selected');
        $("#timeline-kit-product-moreinfo-container").toggle();

        /*var search = $('#kit-product-search').val();
        if (search == 'Translate(STR_QUE_QUIERES_RECOMENDAR)') {
        $('#kit-product-search').val('');
        }*/
    });

    //Kit show recomended tags
    $("#timeline-kit-recommended-tags-title").live('click', function () {

        $('#timeline-kit-recommended-tags-title').toggleClass('timeline-kit-recommended-tags-moreinfo-selected');
        $("#timeline-kit-recommended-tags-container").toggle();
    });

    //Kit comment
    $("#kit-form-comment").live('click', function () {

        //$('#kit-product-search').addClass('timeline-search-italic');
        var comment = $('#kit-form-comment').val();
        if (comment == 'Translate(STR_TU_RECOMENDACION)') {
            $('#kit-form-comment').val('');
        }
    });

    $("#kit-form-comment").live('focus', function () {
        var comment = $('#kit-form-comment').val();
        if (comment == 'Translate(STR_TU_RECOMENDACION)') {
            $('#kit-form-comment').val('');
        }
    });

    $("#kit-form-comment").live('keyup', function (e) {

        var comment = $('#kit-form-comment').val();
        if (comment != '') {
            $("#timeline-kit-button2,#timeline-kit-button").addClass('timeline-kit-button-on');
        }
        else {
            $("#timeline-kit-button2,#timeline-kit-button").removeClass('timeline-kit-button-on');
        }

        var comment = $('#kit-form-comment').val();

        //Recalculem el counter
        var counter = MAX_COUNTER - comment.length;
        $('#timeline-kit-comment-counter').html(counter);

        //Si passa de MAX_COUNTER, posem els MAX_COUNTER primers caràcters
        if ($(this).val().length > MAX_COUNTER)
            $(this).val($(this).val().substr(0, MAX_COUNTER));
    });

    //Kit recommended tag select
    $(".timeline-kit-recommended-tags-text").live('click', function () {

        $(this).toggleClass('timeline-kit-recommended-selected');

        var values = $('#kit_recommended_tags').val();

        var tag = $(this).html();
        //Mirem si ja existeix als tags, si hi és l'esborrem, sinó, l'afegim
        if (values.indexOf(tag) >= 0) {
            values = values.replace("," + tag, "");
            values = values.replace(tag, "");
        }
        else {
            if (values.length > 0) {
                values = values + "," + tag;
            }
            else
                values = tag;
        }
        $('#kit_recommended_tags').val(values)
    });

    //Kit add tags select
    //$("#timeline-kit-add-tags").live('click', function () {
    $("#timeline-kit-add-tags-input").live('click', function () {
        //$(this).css('display', 'none');

        var tags = $("#timeline-kit-add-tags-input").val();

        if (tags == 'Translate(STR_AGREGAR_ETIQUETAS)') {
            $("#timeline-kit-add-tags-input").val("");
        }

        $('#timeline-kit-add-tags-container').removeClass('white');
        $("#timeline-kit-add-tags-container").css('display', 'block');

        //Tags textarea focus
        //$("#kit_form_tags").focus();
        //$("#timeline-kit-add-tags-input").focus();

    });

    //Kit add tags input Enter
    $("#timeline-kit-add-tags-input").live('keyup', function (e) {
        if (e.keyCode == 13) {

            if ($(this).val() != 'Translate(STR_AGREGAR_ETIQUETAS)') {
                var tags_temp = $(this).val();
                $(this).val("Translate(STR_AGREGAR_ETIQUETAS)");

                //Afegim els tags a la llista
                var tags = $("#kit_form_tags").val();

                if (tags.length > 0) {
                    tags = tags + "," + tags_temp;
                }
                else
                    tags = tags_temp;

                $("#kit_form_tags").val(tags);

                $("#kit_form_tags").addClass('white');
                $("#kit-form-comment").focus();
            }
        }
    });

    //Kit add tags input Enter
    $("#timeline-kit-add-tags-input").live('focusout', function (e) {

        if ($(this).val() != 'Translate(STR_AGREGAR_ETIQUETAS)') {
            var tags_temp = $(this).val();
            $(this).val("Translate(STR_AGREGAR_ETIQUETAS)");

            //Afegim els tags a la llista
            var tags = $("#kit_form_tags").val();

            if (tags.length > 0) {
                tags = tags + "," + tags_temp;
            }
            else
                tags = tags_temp;

            $("#kit_form_tags").val(tags);

            $("#kit_form_tags").addClass('white');
            $("#kit-form-comment").focus();
        }
    });

    //Kit add tags Enter
    $("#kit_form_tags").live('keyup', function (e) {
        if (e.keyCode == 13) {
            $(this).addClass('white');
            $("#kit-form-comment").focus();
        }
    });

    //Kit add tags Enter
    $("#kit_form_tags").live('click', function (e) {

        var tags = $("#kit_form_tags").val();

        if (tags == 'Translate(STR_AGREGAR_ETIQUETAS)') {
            $("#kit_form_tags").val("");
        }
    });

    //Kit add tags Enter
    $("#kit_form_tags").live('focus', function (e) {

        var tags = $("#kit_form_tags").val();

        if (tags == 'Translate(STR_AGREGAR_ETIQUETAS)') {
            $("#kit_form_tags").val("");
        }
    });

    $("#timeline-kit-add-tags-container").live('click', function () {
        $('#timeline-kit-add-tags-container').removeClass('white');
    });

    //Kit valoration
    /*$("#timeline-kit-product-valoration-1").live('hover', function () {
    $(this).toggleClass('timeline-kit-product-valoration-1-on');
    });

    $("#timeline-kit-product-valoration-2").live('hover', function () {
    $(this).toggleClass('timeline-kit-product-valoration-2-on');
    });

    $("#timeline-kit-product-valoration-3").live('hover', function () {
    $(this).toggleClass('timeline-kit-product-valoration-3-on');
    });

    $("#timeline-kit-product-valoration-4").live('hover', function () {
    $(this).toggleClass('timeline-kit-product-valoration-4-on');
    });

    $("#timeline-kit-product-valoration-5").live('hover', function () {
    $(this).toggleClass('timeline-kit-product-valoration-5-on');
    });*/


    //Kit submit button
    $("#timeline-kit-button2,#timeline-kit-button").live('click', function () {

        var comment = $("#kit-form-comment").val();
        var valoration = $("#kit_valoration").val();
        if ((comment != '') && (comment != 'Translate(STR_TU_RECOMENDACION)')) {

            //ga
            if (_gaq) {
                _gaq.push(['_trackPageview', '/Kit/doKit']);
            }

            var url = "";

            //Primer mirem si és un producte d'amazon
            var amazon_product = $("#amazon_product").val();

            if (amazon_product == "1") {
                //Agafem els valors
                var comment = $("#kit-form-comment").val();
                var tags = $("#kit_form_tags").val();

                if (tags == 'Translate(STR_AGREGAR_ETIQUETAS)') {
                    tags = "";
                }

                var recommended_tags = $('#kit_recommended_tags').val();

                var amazon_asin = $('#amazon_asin').val();
                var amazon_title = $('#amazon_title').val();
                var amazon_author = $('#amazon_author').val();
                var amazon_year = $('#amazon_year').val();
                var amazon_category = $('#amazon_category').val();
                var amazon_image = $('#amazon_image').val();
                var amazon_url = $('#amazon_url').val();

                var amazon_description = $('#amazon_description').val();
                var amazon_ean = $('#amazon_ean').val();
                var amazon_observations = $('#amazon_observations').val();

                comment = encodeURIComponent(comment);
                tags = encodeURIComponent(tags);
                recommended_tags = encodeURIComponent(recommended_tags);
                amazon_title = encodeURIComponent(amazon_title);
                amazon_author = encodeURIComponent(amazon_author);
                amazon_year = encodeURIComponent(amazon_year);
                amazon_category = encodeURIComponent(amazon_category);
                amazon_image = encodeURIComponent(amazon_image);
                amazon_url = encodeURIComponent(amazon_url);
                amazon_description = encodeURIComponent(amazon_description);
                amazon_ean = encodeURIComponent(amazon_ean);
                amazon_observations = encodeURIComponent(amazon_observations);

                url = SUBDOMAIN_PATH + '/Timeline/AddWSKit/?amazon_asin=' + amazon_asin + '&amazon_title=' + amazon_title + '&amazon_author=' + amazon_author + '&amazon_year=' + amazon_year + '&amazon_category=' + amazon_category + '&amazon_image=' + amazon_image + '&amazon_url=' + amazon_url + '&comment=' + comment + '&tags=' + tags + '&recommended_tags=' + recommended_tags + '&valoration=' + valoration + '&amazon_description=' + amazon_description + '&amazon_ean=' + amazon_ean + '&amazon_observations=' + amazon_observations;
            }
            else {

                //Agafem els valors
                var comment = $("#kit-form-comment").val();
                var tags = $("#kit_form_tags").val();

                if (tags == 'Translate(STR_AGREGAR_ETIQUETAS)') {
                    tags = "";
                }

                var recommended_tags = $('#kit_recommended_tags').val();
                var kit_product_id = $('#kit_product_id').val();
                var ref_kit = $('#ref_kit').val();
                var kit_id = $('#kit_id').val();

                comment = encodeURIComponent(comment);
                tags = encodeURIComponent(tags);
                recommended_tags = encodeURIComponent(recommended_tags);

                if (kit_id > 0) {
                    url = SUBDOMAIN_PATH + '/Timeline/EditKit/?product_id=' + kit_product_id + '&comment=' + comment + '&tags=' + tags + '&recommended_tags=' + recommended_tags + '&kit_id=' + kit_id + '&valoration=' + valoration;
                }
                else {
                    url = SUBDOMAIN_PATH + '/Timeline/AddKit/?product_id=' + kit_product_id + '&comment=' + comment + '&tags=' + tags + '&recommended_tags=' + recommended_tags + '&ref_kit=' + ref_kit + '&valoration=' + valoration;
                }
            }

            if ((comment != '') && (comment != 'Translate(STR_TU_RECOMENDACION)')) {

                //Amaguem el diàleg amb les dades del kit
                $('#timeline-kit-form').css('display', 'none');
                //Mostrem el rellotget
                $('#timeline-kit-form-loading-publish').css('display', 'block');

                $.post(url, function (data) {
                    //$('#timeline-kit-modal').css('display', 'none');
                    //location.reload();

                    //Amaguem el rellotget
                    $('#timeline-kit-form-loading-publish').css('display', 'none');

                    //Hem de mostrar el diàleg de kit publicat
                    $('#timeline-kit-published').css('display', 'block');

                    //Amaguem el botó back
                    $("#timeline-kit-back").css('display', 'none');

                    //Guardem l'id del kit
                    $('#published_kit_id').val(data);

                    var ref_kit = $('#ref_kit').val();

                    //Si estem a la vista del kit, hem d'actualitzar el comptador
                    if (($('#kit-view-options-kits-values').length > 0) && (ref_kit != "")) {

                        //Actualitzem el comptador de comentaris
                        $.post(SUBDOMAIN_PATH + '/Timeline/GetNumReKits/?id=' + ref_kit, function (data) {
                            $('#kit-view-options-kits-values').text(data);
                        });
                    }

                });
            }
        }
    });


    /**

    SOCIAL KIT

    **/

    //Botons de kit publicat
    /*$(".timeline-kit-published-button").hover(function () {
    $(this).toggleClass('timeline-kit-published-button-on');
    });*/

    $("#timeline-kit-published-button-view").hover(function () {
        $(this).toggleClass('timeline-kit-published-button-on');
    });

    $("#timeline-kit-published-button-twitter").hover(function () {
        $(this).toggleClass('timeline-kit-published-button-on');
        $("#timeline-kit-published-button-twitter-img").toggleClass('timeline-kit-published-button-twitter-img-on');
    });

    $("#timeline-kit-published-button-facebook").hover(function () {
        $(this).toggleClass('timeline-kit-published-button-on');
        $("#timeline-kit-published-button-facebook-img").toggleClass('timeline-kit-published-button-facebook-img-on');
    });

    $("#timeline-kit-published-button-facebook").live('click', function () {
        var kit_id = $('#published_kit_id').val();

        //Calculem el top del #timeline-kit-modal (segons el filtre)
        var height = $('#header').height();
        $('#timeline-kit-modal').css("top", height + "px");

        $('#timeline-kit-modal').css('display', 'none');

        $('#header').css("z-index", "10");

        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
        $('#timeline-kit-back').css('display', 'none');
        $('#timeline-kit-published').css('display', 'none');

        //Hauriem de recuperar el text per facebook
        $.post(SUBDOMAIN_PATH + '/Timeline/GetSocialKitText/?id=' + kit_id + '&type=2', function (data) {

            $('#facebook-social-kit-published-text').val(data);
            $('#facebook-social-kit-published-modal').css('display', 'block');

        });
    });

    $("#timeline-kit-published-button-twitter").live('click', function () {
        var kit_id = $('#published_kit_id').val();

        //Calculem el top del #timeline-kit-modal (segons el filtre)
        var height = $('#header').height();
        $('#timeline-kit-modal').css("top", height + "px");

        $('#timeline-kit-modal').css('display', 'none');

        $('#header').css("z-index", "10");

        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
        $('#timeline-kit-back').css('display', 'none');
        $('#timeline-kit-published').css('display', 'none');


        //Hauriem de recuperar el text per twitter
        $.post(SUBDOMAIN_PATH + '/Timeline/GetSocialKitText/?id=' + kit_id + '&type=1', function (data) {

            $('#twitter-social-kit-published-text').val(data);

            var comment = $('#twitter-social-kit-published-text').val();
            console.log("comentari:>>> "+comment+"  <<<.");
            //Recalculem el counter
            var counter = 120 - comment.length;
            $('#social-kit-published-text-counter').html(counter);
            $('#twitter-social-kit-published-modal').css('display', 'block');
        });
    });

    //Close Twitter form
    $("#timeline-kit-published-button-view").live('click', function () {
        //$('#twitter-social-kit-published-modal').css('display', 'none');
        var kit_id = $('#published_kit_id').val();
        parent.location.href = SUBDOMAIN_PATH + "/Timeline/Item/" + kit_id;
    })

    $("#twitter-social-kit-published-text").live('keyup', function (e) {

        var comment = $('#twitter-social-kit-published-text').val();
        if (comment != '') {
            $('#twitter-social-kit-published-button').addClass('social-kit-published-button-on');
        }
        else {
            $('#twitter-social-kit-published-button').removeClass('social-kit-published-button-on');
        }

        var comment = $('#twitter-social-kit-published-text').val();

        //Recalculem el counter
        var counter = 120 - comment.length;
        $('#social-kit-published-text-counter').html(counter);

        //Si passa de 140, posem els 140 primers caràcters
        if ($(this).val().length > 120)
            $(this).val($(this).val().substr(0, 120));
    });

    $("#facebook-social-kit-published-text").live('keyup', function (e) {

        var comment = $('#facebook-social-kit-published-text').val();
        if (comment != '') {
            $('#facebook-social-kit-published-button').addClass('social-kit-published-button-on');
        }
        else {
            $('#facebook-social-kit-published-button').removeClass('social-kit-published-button-on');
        }
    });


    //Close Twitter form
    $("#twitter-social-kit-published-close").live('click', function () {
        $('#twitter-social-kit-published-modal').css('display', 'none');
    })

    //Close Facebook form
    $("#facebook-social-kit-published-close").live('click', function () {
        $('#facebook-social-kit-published-modal').css('display', 'none');
    })


    //Close Twitter connect form
    $("#twitter-connect-kit-close").live('click', function () {
        $('#twitter-connect-kit-modal').css('display', 'none');
    })

    //Close Facebook connect form
    $("#facebook-connect-kit-close").live('click', function () {
        $('#facebook-connect-kit-modal').css('display', 'none');
    })

    $("#twitter-connect-kit-button-container").live('hover', function () {
        $(this).toggleClass('social-connect-kit-button-on');
        $('#twitter-connect-kit-button').toggleClass('social-connect-button-twitter-on');
    });

    $("#facebook-connect-kit-button-container").live('hover', function () {
        $(this).toggleClass('social-connect-kit-button-on');
        $('#facebook-connect-kit-button').toggleClass('social-connect-button-facebook-on');
    });

    //Do kit Tweet
    $("#twitter-social-kit-published-button").live('click', function () {

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Kit/Share']);
        }

        var kit_id = $('#published_kit_id').val();
        var comment = $('#twitter-social-kit-published-text').val();

        comment = encodeURIComponent(comment);

        $('#twitter-social-kit-published-modal').css('display', 'none');

        var url = SUBDOMAIN_PATH + '/Timeline/doTweet/?kit_id=' + kit_id + '&comment=' + comment;
        console.log("URL=>>>   "+url+"   <<<.");
                if ((comment != '')) {
            $.post(url, function (data) {

                //location.reload();
            });
        }
    })

    //Do kit facebook publish
    $("#facebook-social-kit-published-button").live('click', function () {

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Kit/Share']);
        }

        var kit_id = $('#published_kit_id').val();
        var comment = $('#facebook-social-kit-published-text').val();

        comment = encodeURIComponent(comment);

        var url = SUBDOMAIN_PATH + '/Timeline/doFacebookPublish/?kit_id=' + kit_id + '&comment=' + comment;
        $('#facebook-social-kit-published-modal').css('display', 'none');

        if ((comment != '')) {
            $.post(url, function (data) {

                //location.reload();
            });
        }
    });


    /**

    FI SOCIAL KIT

    **/


    $(".sub-menu-content-text").hover(function () {
        $(this).toggleClass('active');
    });

    $("#timeline-social-twitter").hover(function () {
        $(this).toggleClass('timeline-social-twitter-on');
    });

    $("#timeline-social-twitter").click(function () {
        window.open('http://twitter.com/likekit');
    });

    $("#timeline-facebook").hover(function () {
        $(this).toggleClass('timeline-facebook-on');
    });

    $("#timeline-facebook").click(function () {
        window.open('http://www.facebook.com/Likekit');
    });

    $("#timeline-tumblr").hover(function () {
        $(this).toggleClass('timeline-tumblr-on');
    });

    $("#timeline-tumblr").click(function () {
        window.open('http://likekit.tumblr.com/');
    });

    $("#timeline-kit-seach-button").hover(function () {
        $(this).toggleClass('timeline-kit-seach-button-on');
    });

    //Product WS Search
    $("#timeline-kit-seach-button").click(function () {
        $('#timeline-kit-search-result-container').css('display', 'none');
        $('#timeline-kit-webservice-search').css('display', 'block');
        $('#timeline-kit-search').css('padding-bottom', '0');

        KitWSSearch();
    });

    $(".timeline-entry-info-kit-link").live({
        mouseenter:
            function () {
                $(this).addClass('timeline-entry-info-kit-link-on');
            },
        mouseleave:
            function () {
                $(this).removeClass('timeline-entry-info-kit-link-on');
            }
    });

    $("#menu-inicio").hover(function () {
        $(this).toggleClass('menu-inicio-on');
    });

    //REKIT


    //WS Search more results
    $("#timeline-kit-search-result-more-results").live('click', function () {
        $(this).toggleClass('timeline-kit-search-result-more-results-on');
        $("#timeline-kit-search-result-more-results-container").toggle();
    });

    $("#timeline-kit-product-valoration-1").live('hover', function () {
        $(this).toggleClass('timeline-kit-product-valoration-1-on');
    });

    $("#timeline-kit-product-valoration-2").live('hover', function () {
        $(this).toggleClass('timeline-kit-product-valoration-2-on');
    });

    $("#timeline-kit-product-valoration-3").live('hover', function () {
        $(this).toggleClass('timeline-kit-product-valoration-3-on');
    });

    $("#timeline-kit-product-valoration-4").live('hover', function () {
        $(this).toggleClass('timeline-kit-product-valoration-4-on');
    });

    $("#timeline-kit-product-valoration-5").live('hover', function () {
        $(this).toggleClass('timeline-kit-product-valoration-5-on');
    });

    $("#timeline-kit-search-container-category-books").live('hover', function () {
        $(this).toggleClass('timeline-kit-search-container-category-books-on');
    });

    $("#timeline-kit-search-container-category-series").live('hover', function () {
        $(this).toggleClass('timeline-kit-search-container-category-series-on');
    });

    $("#timeline-kit-search-container-category-music").live('hover', function () {
        $(this).toggleClass('timeline-kit-search-container-category-music-on');
    });

    $("#timeline-kit-search-container-category-cinema").live('hover', function () {
        $(this).toggleClass('timeline-kit-search-container-category-cinema-on');
    });

    $("#timeline-kit-search-container-category-books").live('click', function () {
        $(this).toggleClass('timeline-kit-search-container-category-books-selected');
        SetKitCategoryForm(1);
    });

    $("#timeline-kit-search-container-category-series").live('click', function () {
        $(this).toggleClass('timeline-kit-search-container-category-series-selected');
        SetKitCategoryForm(2);
    });

    $("#timeline-kit-search-container-category-music").live('click', function () {
        $(this).toggleClass('timeline-kit-search-container-category-music-selected');
        SetKitCategoryForm(4);
    });

    $("#timeline-kit-search-container-category-cinema").live('click', function () {
        $(this).toggleClass('timeline-kit-search-container-category-cinema-selected');
        SetKitCategoryForm(3);
    });

    //Home video modal close
    $("#home-video-close").live('click', function () {
        $('#home-video-modal').css('display', 'none');
        $('iframe#home-video-src').attr('src', '');
    });

    //Likekit Menú
    $("#top-header-menu-likekit").live({
        mouseenter:
            function () {
                $('#top-header-likekit-menu').css('display', 'block');
            },
        mouseleave:
            function () {
                $('#top-header-likekit-menu').css('display', 'none');
            }
    });

    //User Menú
    $("#top-header-menu-user").live({
        mouseenter:
            function () {
                $('#top-header-user-menu').css('display', 'block');
            },
        mouseleave:
            function () {
                $('#top-header-user-menu').css('display', 'none');
            }
    });
});

$(window).scroll(function () {
    if ($(this).scrollTop() != 0) {
        $('.timeline-to-top').fadeIn();
    } else {
        $('.timeline-to-top').fadeOut();
    }

    //LeftMenuScroll();
});

$(window).resize(function () {
    //LeftMenuScroll();
});

function Profile(UserID) {
    if (_gaq) {
        _gaq.push(['_trackPageview', '/Profile/View']);
    }
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/Profile/" + UserID;
}

function EditProfile(UserID) {
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/EditProfile/" + UserID;
}

function Kit(KitID) {

    if (_gaq) {
        _gaq.push(['_trackPageview', '/Kit/View']);
    }

    //parent.location.href = "/Timeline/Item/" + KitID;
    parent.location.href = SUBDOMAIN_PATH + KitID;
}

function Blog() {
    window.open('http://www.likekit.com/blog/');
}

function Help() {
    //parent.location.href = "/Timeline/Help";
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/About/1";
}

function MediaKit() {
    //parent.location.href = "/Timeline/MediaKit";
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/About/2";
}

function Team() {
    //parent.location.href = "/Timeline/Team";
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/About/3";
}

function Legal() {
    //parent.location.href = "/Timeline/Legal";
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/About/4";
}

function KitSearch() {

    var search = $('#kit-product-search').val();

    if (search == 'Translate(STR_QUE_QUIERES_RECOMENDAR)') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#kit-product-categories').val();

    $.post(SUBDOMAIN_PATH + '/Timeline/SearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-kit-search-result-container').html(data);
        $('#timeline-kit-search-result-container').css('display', 'block');
        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');   
    });
}

function KitLoadProduct(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/LoadKitProductData/?id=' + id, function (data) {
        $("#timeline-kit-form").css('display', 'block');
        $("#timeline-kit-search").css('display', 'none');
        $('#timeline-kit-form-product-data').html(data);
        //Mostrem el botó back
        $("#timeline-kit-back").css('display', 'block');
    });
}

function ReKit(event,id) {
    //Hauriem d'amagar primer l'info del kit
    //$('.timeline-entry-info').css('display','none');
    $.post(SUBDOMAIN_PATH + '/Timeline/LoadReKitProductData/?id=' + id, function (data) {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        $("#timeline-kit-form").css('display', 'block');
        $("#timeline-kit-search").css('display', 'none');
        $('#timeline-kit-form-product-data').html(data);
        $('#timeline-kit-modal').css('display', 'block');
    });

    // Don't propogate the event to the document
    if (event.stopPropagation) {
        event.stopPropagation();   // W3C model
    } else {
        event.cancelBubble = true; // IE model
    }
}

function EditKit(event, id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/LoadEditKitProductData/?id=' + id, function (data) {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        $('#timeline-kit-form-product-data').html(data);
        //Ara hem d'obtenir el comentari
        $.post(SUBDOMAIN_PATH + '/Timeline/LoadKitComment/?id=' + id, function (data) {

            $("#timeline-kit-form").css('display', 'block');
            $("#timeline-kit-search").css('display', 'none');

            $("#kit-form-comment").val(data);

            if (data != '')
                $("#timeline-kit-button2,#timeline-kit-button").addClass('timeline-kit-button-on');

            $('#timeline-kit-add-tags-container').addClass('white');
            $("#timeline-kit-add-tags-container").css('display', 'block');
            $('#timeline-kit-modal').css('display', 'block');
            $('#header').css("z-index", "1");
        });
    });

    function DeleteKit(event, id) {
        
    }
    
    // Don't propogate the event to the document
    if (event.stopPropagation) {
        event.stopPropagation();   // W3C model
    } else {
        event.cancelBubble = true; // IE model
    }

}

$("#kit-user-delete").live('click', function () {
    $('#emergent-missatge-esborrat-kit').css('display', 'block');
});

function DeleteKit(id) {
    $.post('/Timeline/DeleteKit/?id=' + id);

    $('#div-missatge-confirmacio').css('display', 'none');


    //Possar que es torni invisible el contingut del kit de sota.

    $('#emergent-missatge-esborrat-kit-close-1').css('display', 'none');

    $('.timeline-copyright-container').css('display','none');
    $('#twitter-social-kit-modal').css('display', 'none');
    $('#facebook-social-kit-modal').css('display', 'none');
    $('#mail-social-kit-modal').css('display', 'none');
    $('#kit-view-container-data').css('display', 'none');
    $('#kit-view-container-comments').css('display', 'none');
    $('#kit-view-container-comment-entry').css('display', 'none');
    $('#kit-view-container-users').css('display', 'none');


    $('#kit-view-container').css('background-color', 'transparent');
    $('#kit-view-container').css('border', 'none');
    $('#kit-view-container').css('box-shadow', 'none');


    $('#emergent-sha-eliminat-correctament').css('display', 'block');
    $('#emergent-missatge-esborrat-kit-close-2').css('display', 'block');
}

$('#boto-no-missatge-pregunta-esborrar-kit').live('click', function () {
    $('#emergent-missatge-esborrat-kit').css('display', 'none');
});

$('#boto-aceptar-esborrat-kit').live('click', function () {
    $('#emergent-missatge-esborrat-kit').css('display', 'none');

    parent.location.href = SUBDOMAIN_PATH + '/Timeline';
});

$('#emergent-missatge-esborrat-kit-close-2').live('click', function () {
    $('#emergent-missatge-esborrat-kit').css('display', 'none');

    parent.location.href = SUBDOMAIN_PATH + '/Timeline';
});

$('#emergent-missatge-esborrat-kit-close-1').live('click', function () {
    $('#emergent-missatge-esborrat-kit').css('display', 'none');
    //Possar que es torni a mostrar el contingut anterior si s'ha fet invisible.
});

function ProductKit(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/LoadKitProductData/?id=' + id, function (data) {
        $('#timeline-kit-modal').css('display', 'block');
        $('#header').css("z-index", "1");
        $("#timeline-kit-form").css('display', 'block');
        $("#timeline-kit-search").css('display', 'none');
        $('#timeline-kit-form-product-data').html(data);
    });
}

function AddToWishList(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/AddToWishList/?id=' + id, function (data) {
        parent.location.href = SUBDOMAIN_PATH + '/Timeline/Wishlist/' + data;
    });
}

function Interactions(UserID) {
    parent.location.href = SUBDOMAIN_PATH + "/Timeline/Interactions/" + UserID;
}

function SearchPeopleLink(){
    parent.location.href= SUBDOMAIN_PATH + '/Timeline/SearchPeople';
}

function InviteLink(){
    parent.location.href = SUBDOMAIN_PATH + '/Timeline/Invite';
}

function Like(event,id) {

    if (_gaq) {
        _gaq.push(['_trackEvent', 'Kit', 'Like', 'kit id', id]);
        _gaq.push(['_trackPageview', '/kit/like']);
    }

    $.post(SUBDOMAIN_PATH + '/Timeline/DoLike/?id=' + id, function (data) {

        var div_id = '#timeline-kit-like-' + id;
        var div_name = 'timeline-kit-like-' + id;

        $(div_id).replaceWith('<div id="' + div_name + '" class="timeline-entry-info-dislike-link" onclick="DisLike(event,' + id + ',' + data + ');">Translate(STR_DISLIKE)</div>');
        UpdateNumLikes(id);
    });
        
    // Don't propogate the event to the document
    if (event.stopPropagation) {
        event.stopPropagation();   // W3C model
    } else {
        event.cancelBubble = true; // IE model
    }
}

function DisLike(event,kitid, id) {

    $.post(SUBDOMAIN_PATH + '/Timeline/DoDisLike/?id=' + id, function (data) {

        var div_id = '#timeline-kit-like-' + kitid;
        var div_name = 'timeline-kit-like-' + kitid;

        $(div_id).replaceWith('<div id="' + div_name + '" class="timeline-entry-info-like-link" onclick="Like(event,' + kitid + ');">Translate(STR_LIKE)</div>');
        UpdateNumLikes(kitid);
    });

    // Don't propogate the event to the document
    if (event.stopPropagation) {
        event.stopPropagation();   // W3C model
    } else {
        event.cancelBubble = true; // IE model
    }
}

function UpdateNumLikes(id) {
    //Actualitzem el número de likes
    $.post(SUBDOMAIN_PATH + '/Timeline/GetNumLikes/?id=' + id, function (data) {
        var div_id = '#timeline-entry-options-number-' + id;
        $(div_id).text(data);
    });
}

function KitComment(event, id) {

    parent.location.href = SUBDOMAIN_PATH + '/Timeline/Item/' + id+ '#kit-view-container-comment-entry-container';

    // Don't propogate the event to the document
    if (event.stopPropagation) {
        event.stopPropagation();   // W3C model
    } else {
        event.cancelBubble = true; // IE model
    }

}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

/**
    
    WS VERSION

**/

function KitWSSearch() {

    var search = $('#kit-product-search').val();

    if (search == 'Translate(STR_QUE_QUIERES_RECOMENDAR)') {
        search = '';
    }

    var categories = $('#kit-product-categories').val();

    $.post(SUBDOMAIN_PATH + '/Timeline/WSSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-kit-search-result-container').html(data);
        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search-result-container').css('display', 'block');
        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
    });
}


function KitLoadWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations) {

    ASIN = encodeURIComponent(ASIN);
    Title = encodeURIComponent(Title);
    Author = encodeURIComponent(Author);
    Year = encodeURIComponent(Year);
    Category = encodeURIComponent(Category);
    LargeImage = encodeURIComponent(LargeImage);
    URL = encodeURIComponent(URL);
    Description = encodeURIComponent(Description);
    EAN = encodeURIComponent(EAN);
    Observations = encodeURIComponent(Observations);

    $.post(SUBDOMAIN_PATH + '/Timeline/LoadWSKitProductData/?ASIN=' + ASIN + "&Title=" + Title + "&Author=" + Author + "&Year=" + Year + "&Category=" + Category + "&LargeImage=" + LargeImage + "&URL=" + URL + "&Description=" + Description + "&EAN=" + EAN + "&Observations=" + Observations, function (data) {
        $("#timeline-kit-form").css('display', 'block');
        $("#timeline-kit-search").css('display', 'none');
        $('#timeline-kit-form-product-data').html(data);
        //Mostrem el botó back
        $("#timeline-kit-back").css('display', 'block');
    });
}

/**

    FI WS VERSION

**/

function SetValoration(number) {

    if (number == 1) {
        $('#timeline-kit-product-valoration-1').addClass('timeline-kit-product-valoration-1-selected');
        $('#timeline-kit-product-valoration-2').removeClass('timeline-kit-product-valoration-2-selected').removeClass('timeline-kit-product-valoration-2-on');
        $('#timeline-kit-product-valoration-3').removeClass('timeline-kit-product-valoration-3-selected').removeClass('timeline-kit-product-valoration-3-on');
        $('#timeline-kit-product-valoration-4').removeClass('timeline-kit-product-valoration-4-selected').removeClass('timeline-kit-product-valoration-4-on');
        $('#timeline-kit-product-valoration-5').removeClass('timeline-kit-product-valoration-5-selected').removeClass('timeline-kit-product-valoration-5-on');
    }
    else if (number == 2) {
        $('#timeline-kit-product-valoration-2').addClass('timeline-kit-product-valoration-2-selected');
        $('#timeline-kit-product-valoration-1').removeClass('timeline-kit-product-valoration-1-selected').removeClass('timeline-kit-product-valoration-1-on');
        $('#timeline-kit-product-valoration-3').removeClass('timeline-kit-product-valoration-3-selected').removeClass('timeline-kit-product-valoration-3-on');
        $('#timeline-kit-product-valoration-4').removeClass('timeline-kit-product-valoration-4-selected').removeClass('timeline-kit-product-valoration-4-on');
        $('#timeline-kit-product-valoration-5').removeClass('timeline-kit-product-valoration-5-selected').removeClass('timeline-kit-product-valoration-5-on');
    }
    else if (number == 3) {
        $('#timeline-kit-product-valoration-3').addClass('timeline-kit-product-valoration-3-selected');
        $('#timeline-kit-product-valoration-1').removeClass('timeline-kit-product-valoration-1-selected').removeClass('timeline-kit-product-valoration-1-on');
        $('#timeline-kit-product-valoration-2').removeClass('timeline-kit-product-valoration-2-selected').removeClass('timeline-kit-product-valoration-2-on');
        $('#timeline-kit-product-valoration-4').removeClass('timeline-kit-product-valoration-4-selected').removeClass('timeline-kit-product-valoration-4-on');
        $('#timeline-kit-product-valoration-5').removeClass('timeline-kit-product-valoration-5-selected').removeClass('timeline-kit-product-valoration-5-on');
    }
    else if (number == 4) {
        $('#timeline-kit-product-valoration-4').addClass('timeline-kit-product-valoration-4-selected');
        $('#timeline-kit-product-valoration-1').removeClass('timeline-kit-product-valoration-1-selected').removeClass('timeline-kit-product-valoration-1-on');
        $('#timeline-kit-product-valoration-2').removeClass('timeline-kit-product-valoration-2-selected').removeClass('timeline-kit-product-valoration-2-on');
        $('#timeline-kit-product-valoration-3').removeClass('timeline-kit-product-valoration-3-selected').removeClass('timeline-kit-product-valoration-3-on');
        $('#timeline-kit-product-valoration-5').removeClass('timeline-kit-product-valoration-5-selected').removeClass('timeline-kit-product-valoration-5-on');
    }
    else if (number == 5) {
        $('#timeline-kit-product-valoration-5').addClass('timeline-kit-product-valoration-5-selected');
        $('#timeline-kit-product-valoration-1').removeClass('timeline-kit-product-valoration-1-selected').removeClass('timeline-kit-product-valoration-1-on');
        $('#timeline-kit-product-valoration-2').removeClass('timeline-kit-product-valoration-2-selected').removeClass('timeline-kit-product-valoration-2-on');
        $('#timeline-kit-product-valoration-3').removeClass('timeline-kit-product-valoration-3-selected').removeClass('timeline-kit-product-valoration-3-on');
        $('#timeline-kit-product-valoration-4').removeClass('timeline-kit-product-valoration-4-selected').removeClass('timeline-kit-product-valoration-4-on');
    }



    $('#kit_valoration').val(number);
}

function KitAccessLog() {
    $.post(SUBDOMAIN_PATH + '/Timeline/KitAccessLog', function (data) {
    });
}


function LeftMenuScroll() {


    var bHeight = 485; //$('#left_menu').height();

    //Mirem si cal sumar menu-user-content
    /*if ($('#menu-user-content').css('display') == 'block')
        bHeight = bHeight + 108;

    //Mirem si cal sumar sobre-likekit-content
    if ($('#sobre-likekit-content').css('display') == 'block')
        bHeight = bHeight + 90;

    //Mirem si cal sumar filter-likekit-content
    if ($('#filter-likekit-content').css('display') == 'block')
        bHeight = bHeight + 282;*/

    var wHeight = $(window).height();
    var windowScrollTop = $(window).scrollTop() + 60;

    //console.log("windowScrollTop : " + windowScrollTop + " bHeight : " + bHeight + " wHeight : " + wHeight);

    //Hauriem de controlar l'scroll del menu lateral
    //if (($(window)) && (wHeight < 800) && ((bHeight + 60) > wHeight)) {
    if ((bHeight + 60) > wHeight) {

        $('#left_menu').css('position', 'absolute');

        //console.log("windowScrollTop : " + windowScrollTop + " bHeight : " + bHeight + " wHeight : " + wHeight + " display : " + $('#menu-user-content').css('display'));

        var st = $(window).scrollTop();
        if (st > lastScrollTop) {

            if ((windowScrollTop + wHeight) > (bHeight + 60)) {
                $('#left_menu').css({
                    position: 'fixed',
                    bottom: 15, 
                    top:'auto'
                });
                //alert('bottom');
                //console.log("down fixed bottom");
            }
            else if (windowScrollTop + bHeight >= wHeight) {
                $('#left_menu').css({
                    position: 'absolute',
                    bottom: 15
                });
                //alert('bottom');
                //console.log("down absolute");
            }
            else {
                $('#left_menu').css({
                    position: 'fixed',
                    top:60
                });

                //console.log("down fixed top");
            }
        } else {

            if ((windowScrollTop + wHeight) < (bHeight + 60)) {
                $('#left_menu').css({
                    position: 'fixed',
                    bottom: 'auto',
                    top: 60
                });
            }
            else {
                $('#left_menu').css({
                    position: 'fixed',
                    bottom: 15
                });
            }

            //console.log("up fixed");
        }

        lastScrollTop = st;
    }
    else
    {
        //$('#left_menu').css('position', 'fixed');
        $('#left_menu').css({
            position: 'fixed',
            bottom: 'auto',
            top: 60
        });
    }
}

(function ($) {
    // Behind the scenes method deals with browser
    // idiosyncrasies and such
    $.caretTo = function (el, index) {
        if (el.createTextRange) {
            var range = el.createTextRange();
            range.move("character", index);
            range.select();
        } else if (el.selectionStart != null) {
            el.focus();
            el.setSelectionRange(index, index);
        }
    };

    // The following methods are queued under fx for more
    // flexibility when combining with $.fn.delay() and
    // jQuery effects.

    // Set caret to a particular index
    $.fn.caretTo = function (index, offset) {
        return this.queue(function (next) {
            if (isNaN(index)) {
                var i = $(this).val().indexOf(index);

                if (offset === true) {
                    i += index.length;
                } else if (offset) {
                    i += offset;
                }

                $.caretTo(this, i);
            } else {
                $.caretTo(this, index);
            }

            next();
        });
    };

    // Set caret to beginning of an element
    $.fn.caretToStart = function () {
        return this.caretTo(0);
    };

    // Set caret to the end of an element
    $.fn.caretToEnd = function () {
        return this.queue(function (next) {
            $.caretTo(this, $(this).val().length);
            next();
        });
    };
} (jQuery));



function SetKitCategoryForm(id_category)
{
    var values = $('#kit-product-categories').val();

    //Mirem si ja existeix a les categories, si hi és l'esborrem, sinó, l'afegim
    if (values.indexOf(id_category) >= 0) {
        values = values.replace("," + id_category, "");
        values = values.replace(id_category, "");
    }
    else {
        if (values.length > 0) {
            values = values + "," + id_category;
        }
        else
            values = id_category;
    }
    
    $('#kit-product-categories').val(values);
}


function LoQuiero(id, url) {

    if (_gaq) {
        _gaq.push(['_trackPageview', '/Kit/Loquiero']);
    }

    $.post(SUBDOMAIN_PATH + '/Timeline/LoQuiero/?id=' + id, function (data) {
        //window.open(url);
    });
}

function ResetKitCategories() {

    $('#kit-product-categories').val('');

    $("#timeline-kit-search-container-category-books").removeClass('timeline-kit-search-container-category-books-on').removeClass('timeline-kit-search-container-category-books-selected');
    $("#timeline-kit-search-container-category-series").removeClass('timeline-kit-search-container-category-series-on').removeClass('timeline-kit-search-container-category-series-selected');
    $("#timeline-kit-search-container-category-music").removeClass('timeline-kit-search-container-category-music-on').removeClass('timeline-kit-search-container-category-music-selected');
    $("#timeline-kit-search-container-category-cinema").removeClass('timeline-kit-search-container-category-cinema-on').removeClass('timeline-kit-search-container-category-cinema-selected');
}

function Video() {
    $("#home-video-modal").css('display', 'block');
    $('iframe#home-video-src').attr('src', 'https://www.youtube.com/embed/oD7h53ZWfe4?rel=0&autoplay=1');
}