﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;

namespace LikeKitWebsite.Scripts
{
    public partial class GetJs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String script = String.Empty;

            String absolutePath = String.Format("{0}Scripts\\{1}", ConfigurationManager.AppSettings["MainPath"], Request["js"]);

            //Eliminem els paràmetres després del .js
            int jsExtPos = absolutePath.LastIndexOf(".js");
            if (jsExtPos >= 0)
                absolutePath = absolutePath.Substring(0, jsExtPos + 3);

            if (File.Exists(absolutePath))
            {
                using (StreamReader reader = new StreamReader(absolutePath))
                {
                    script =  reader.ReadToEnd();
                }
            }

            string translated = TranslateScript(script);

            Response.ContentType = "text/javascript";
            Response.Write(translated);
        }


        private static Regex REGEX = new Regex(@"Translate\(([^\))]*)\)", RegexOptions.Singleline | RegexOptions.Compiled);

        /// <summary>
        /// Translates the text keys in the script file. The format is Translate(key).
        /// </summary>
        /// <param name="text">The text in the script file.</param>
        /// <returns>A localized version of the script</returns>
        private string TranslateScript(string text)
        {
            MatchCollection matches = REGEX.Matches(text);

            System.Globalization.CultureInfo ci = null;

            if (HttpContext.Current.Session["culture"] != null)
            {
                ci = new System.Globalization.CultureInfo(HttpContext.Current.Session["culture"].ToString());
            }
            else
            {
                ci = new System.Globalization.CultureInfo("es");
            }

            foreach (Match match in matches)
            {
                object obj = LikeKitWebsite.Globals.LocaleManager.GetObject(match.Groups[1].Value, ci);

                if (obj != null)
                {
                    text = text.Replace(match.Value, CleanText(obj.ToString()));
                }
            }

            return text;
        }

        /// <summary>
        /// Cleans the localized script text from making invalid javascript.
        /// </summary>
        private static string CleanText(string text)
        {
            
            text = text.Replace("\\", "\\\\");
            text = text.Replace("'", "&#39;");

            return text;
        }
    }
}