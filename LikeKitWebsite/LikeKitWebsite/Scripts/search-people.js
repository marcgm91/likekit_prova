﻿
$(window).load(function () {

    setWishlistTop();

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    $("#search-people-button").live('hover', function () {
        $(this).toggleClass('search-people-button-on');
    });

    //Search People input click
    $("#search-people-input").live('click', function () {

        $('#search-people-input').addClass('timeline-search-italic');
        
        var search = $('#search-people-input').val();
        if (search == 'Translate(STR_QUIEN_QUIERES_ENCONTRAR)') {
            $('#search-people-input').val('');
        }
    });

    $("#search-people-input").live('focusout', function () {
        var search = $('#search-people-input').val();
        if (search == '') {
            $('#search-people-input').val('Translate(STR_QUIEN_QUIERES_ENCONTRAR)');
            $('#search-people-input').removeClass('timeline-search-italic');
        }
    });

    //Search People input Enter
    $("#search-people-input").keypress(function (e) {
        if (e.keyCode == 13) {
            SearchPeople();
        }
    });

    $("#search-people-button").live('click', function () {
        SearchPeople();
    });

    $("#search-people-noresult-close").live('click', function () {
        $("#search-people-noresult-modal").css('display', 'none');
    });

    $(".search-people-result-entry-follow").live('hover', function () {
        $(this).toggleClass('search-people-result-entry-btn-active');
    });
    
    $(".search-people-result-entry-following").live({
        mouseenter:
            function () {
                $(this).addClass('search-people-result-entry-btn-active');
                $(this).html('Translate(STR_NO_SEGUIR)');
            },
        mouseleave:
            function () {
                $(this).removeClass('search-people-result-entry-btn-active');
                $(this).html('Translate(STR_SIGUIENDO_MIN)');
            }
    }
    );
});


function setWishlistTop() {
    var height = $('#header').height();
    $('#search-people-container').css("margin-top", height + "px");
}


function SearchPeople() {

    var search = $('#search-people-input').val();

    if (search == 'Translate(STR_QUIEN_QUIERES_ENCONTRAR)') {
        search = '';
    }

    search = encodeURIComponent(search);
    //alert(search);

    $.post('/Timeline/doSearchPeople/?search=' + search, function (data) {
        if (data == '') {
            $("#search-people-noresult-modal").fadeIn("fast");
        }
        else {
            $('#search-people-result-container').html(data);
        }
    });
}

function SearchPeopleUnFollow(id) {
    var url_unfollow = '/Timeline/UnFollow/?UserID=' + id;
    
    $.post(url_unfollow, function (data) {
        if ($.trim(data) == 'Success') {
            var div_id = "#search-people-user-btn-" + id;
            $(div_id).replaceWith('<div id="search-people-user-btn-' + id + '" class="search-people-result-entry-follow search-people-result-entry-btn" onclick="SearchPeopleFollow(' + id + ');">Translate(STR_SEGUIR)</div>');
        }
    });
}

function SearchPeopleFollow(id) {
    var url_follow = '/Timeline/Follow/?UserID=' + id;

    $.post(url_follow, function (data) {
        if ($.trim(data) == 'Success') {
            var div_id = "#search-people-user-btn-" + id;
            $(div_id).replaceWith('<div id="search-people-user-btn-' + id + '" class="search-people-result-entry-following search-people-result-entry-btn-on" onclick="SearchPeopleUnFollow(' + id + ');">Translate(STR_SIGUIENDO_MIN)</div>');
        }
    });
}
