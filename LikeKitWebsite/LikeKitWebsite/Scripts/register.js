﻿
var array_selected_items = Array();

$(document).ready(function () {

    $(".login-twitter").hover(function () {
        $(this).toggleClass('login-twitter-on');
    });

    $(".login-facebook").hover(function () {
        $(this).toggleClass('login-facebook-on');
    });

    $(".login-social-twitter").hover(function () {
        $(this).toggleClass('login-social-twitter-on');
    });

    $(".login-social-facebook").hover(function () {
        $(this).toggleClass('login-social-facebook-on');
    });

    $(".register-social-button-facebook").hover(function () {
        $(this).toggleClass('register-social-button-facebook-on');
    });

    $(".register-social-button-twitter").hover(function () {
        $(this).toggleClass('register-social-button-twitter-on');
    });

    $(".login-social-twitter").click(function () {
        window.open('http://twitter.com/likekit');
    });

    $(".login-social-facebook").click(function () {
        window.open('http://www.facebook.com/Likekit');
    });

    $(".login-social-t").hover(function () {
        $(this).toggleClass('login-social-t-on');
    });

    $(".login-label").hover(function () {
        $(this).toggleClass('login-label-active');
    });

    $(".register-social-button").hover(function () {
        $(this).toggleClass('register-social-button-on');
    });

    $("#register-step-1-btn").hover(function () {
        $(this).toggleClass('register-step-1-btn-on');
    });

    $("#register-step-4-btn").hover(function () {
        $(this).toggleClass('register-step-4-btn-on');
    });

    $("#register-activation-btn").hover(function () {
        $(this).toggleClass('register-activation-on');
    });

    //Register Page, Email
    $("#Email").live('click', function () {

        var value = $('#Email').val();

        if (value == 'Translate(STR_TU_DIRECCION_EMAIL)') {
            $('#Email').val('');
            $('#Email').addClass('login-italic');
        }
    });

    //Login Page, Email
    $("#Email").live('focus', function () {

        var value = $('#Email').val();

        if (value == 'Translate(STR_TU_DIRECCION_EMAIL)') {
            $('#Email').val('');
            $('#Email').addClass('login-italic');
        }
    });

    //Login Page, Email
    $("#Email").live('focusout', function () {

        var value = $('#Email').val();

        if (value == '') {
            $('#Email').val('Translate(STR_TU_DIRECCION_EMAIL)');
            $('#Email').removeClass('login-italic');
        }
    });


    //Register Page, FirstName
    $("#FirstName").on('click', function () {

        var value = $('#FirstName').val();

        if (value == 'Translate(STR_NOMBRE)') {
            $('#FirstName').val('');
        }
    });

    //Login Page, FirstName
    $("#FirstName").on('focus', function () {

        var value = $('#FirstName').val();

        if (value == 'Translate(STR_NOMBRE)') {
            $('#FirstName').val('');
        }
    });

    //Login Page, FirstName
    $("#FirstName").on('focusout', function () {

        var value = $('#FirstName').val();

        if (value == '') {
            $('#FirstName').val('Translate(STR_NOMBRE)');
        }
    });

    //Register Page, LastName
    $("#LastName").on('click', function () {

        var value = $('#LastName').val();

        if (value == 'Translate(STR_APELLIDOS)') {
            $('#LastName').val('');
        }
    });

    //Login Page, LastName
    $("#LastName").on('focus', function () {

        var value = $('#LastName').val();

        if (value == 'Translate(STR_APELLIDOS)') {
            $('#LastName').val('');
        }
    });

    //Login Page, LastName
    $("#LastName").on('focusout', function () {

        var value = $('#LastName').val();

        if (value == '') {
            $('#LastName').val('Translate(STR_APELLIDOS)');
        }
    });

    $("#password_text").on('click', function () {
        $(this).css('display', 'none');
        $('#Password').css('display', 'block');
        $('#Password').focus();
    });

    $("#password_text").on('focus', function () {
        $(this).css('display', 'none');
        $('#Password').css('display', 'block');
        $('#Password').focus();
    });

    $("#password_text_repeat").on('click', function () {
        $(this).css('display', 'none');
        $('#RepeatPassword').css('display', 'block');
        $('#RepeatPassword').focus();
    });

    $("#password_text_repeat").on('focus', function () {
        $(this).css('display', 'none');
        $('#RepeatPassword').css('display', 'block');
        $('#RepeatPassword').focus();
    });

    $("#login-twitter").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterLogOn';
    });

    $("#login-facebook").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
    });

    /*$("#").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
    });*/

    //Register : Mail
    $("#Email").on('focusout', function () {

        var mail = $('#Email').val();

        if (isValidEmailAddress(mail)) {
            $('#register-username-tip').addClass('register-tip-error');
            $("#register-username-tip").fadeIn("slow");

            $.post('CheckMail/?mail=' + mail, function (data) {
                //$('#landing-content-form').html(data);

                if ($.trim(data) == 'Error') {
                    $('#register-username-tip').html("Translate(STR_ESTE_CORREO_YA_ESTA_INSCRITO_GRACIAS)");
                    $('#register-editor-field-email').addClass('register-editor-field-error');

                }
                else if ($.trim(data) == 'ErrorMail') {
                    $('#register-username-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
                    $('#register-editor-field-email').addClass('register-editor-field-error');
                }
                else if ($.trim(data) == 'Success') {
                    $('#register-username-tip').removeClass('register-tip-error');
                    $('#register-editor-field-email').removeClass('register-editor-field-error');
                    $('#register-username-tip').html("Translate(STR_PERFECTO)");
                }
            });
        }
        else {
            $('#register-username-tip').addClass('register-tip-error');
            $("#register-username-tip").fadeIn("slow");
            $('#register-username-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
            $('#register-editor-field-email').addClass('register-editor-field-error');
        }
    });

    //Register : FirstName
    $("#FirstName").on('focusout', function () {

        var firstname = $('#FirstName').val();
        var lastname = $('#LastName').val();

        if ((firstname != '') && (lastname != '') && (firstname != 'Translate(STR_NOMBRE)') && (lastname != 'Translate(STR_APELLIDOS)')) {
            $('#register-firstlastname-tip').removeClass('register-tip-error');
            $("#register-firstlastname-tip").fadeIn("slow");
            $('#register-editor-field-firstname').removeClass('register-editor-field-error');
            $('#register-editor-field-lastname').removeClass('register-editor-field-error');
            $('#register-firstlastname-tip').html("Translate(STR_ENCANTADOS_DE_CONOCERTE)");
        }
        else {
            $('#register-firstlastname-tip').addClass('register-tip-error');
            $("#register-firstlastname-tip").fadeIn("slow");
            $('#register-editor-field-firstname').addClass('register-editor-field-error');
            $('#register-firstlastname-tip').html("Translate(STR_DEBES_RELLENARLOS)");
        }
    });

    //Register : LastName
    $("#LastName").on('focusout', function () {

        var firstname = $('#FirstName').val();
        var lastname = $('#LastName').val();

        if ((firstname != '') && (lastname != '') && (firstname != 'Translate(STR_NOMBRE)') && (lastname != 'Translate(STR_APELLIDOS)')) {
            $('#register-firstlastname-tip').removeClass('register-tip-error');
            $("#register-firstlastname-tip").fadeIn("slow");
            $('#register-editor-field-firstname').removeClass('register-editor-field-error');
            $('#register-editor-field-lastname').removeClass('register-editor-field-error');
            $('#register-firstlastname-tip').html("Translate(STR_ENCANTADOS_DE_CONOCERTE)");
        }
        else {
            $('#register-firstlastname-tip').addClass('register-tip-error');
            $("#register-firstlastname-tip").fadeIn("slow");
            $('#register-editor-field-lastname').addClass('register-editor-field-error');
            $('#register-firstlastname-tip').html("Translate(STR_DEBES_RELLENARLOS)");
        }
    });

    //Register : Password
    $("#Password").on('focusout', function () {

        var Password = $('#Password').val();
        var RepeatPassword = $('#RepeatPassword').val();

        if (!isStrongPassword(Password)) {
            $("#register-password-tip").css('display', 'none');
            $("#register-password-tip-long").fadeIn("slow");
            $('#register-password').addClass('register-editor-field-error');
            $('#register-repeat-password').addClass('register-editor-field-error');
            $('#register-password-tip-long').html("Translate(STR_NO_PARECE_SEGURA_PRUEBA_MEZCLANDO_NUMEROS_Y_LETRAS)");
        }
        else if (Password != '') {
            $("#register-password-tip-long").css('display', 'none');
            $('#register-password-tip').removeClass('register-tip-error');
            $('#register-password').removeClass('register-editor-field-error');
            $('#register-repeat-password').removeClass('register-editor-field-error');
            $("#register-password-tip").fadeIn("slow");
            $('#register-password-tip').html("Translate(STR_GENIAL_NO_LA_OLVIDES)");
        }
        else if (Password == '') {
            $("#register-password-tip-long").css('display', 'none');
            $('#register-password-tip').addClass('register-tip-error');
            $("#register-password-tip").fadeIn("slow");
            $('#register-password').addClass('register-editor-field-error');
            $('#register-repeat-password').addClass('register-editor-field-error');
            $('#register-password-tip').html("Translate(STR_DEBES_RELLENARLAS)");
        }
    });

    //Register : RepeatPassword
    $("#RepeatPassword").on('focusout', function () {

        var Password = $('#Password').val();
        var RepeatPassword = $('#RepeatPassword').val();

        if (!isStrongPassword(Password)) {
            $("#register-password-tip").css('display', 'none');
            $("#register-password-tip-long").fadeIn("slow");
            $('#register-password').addClass('register-editor-field-error');
            $('#register-repeat-password').addClass('register-editor-field-error');
            $('#register-password-tip-long').html("Translate(STR_NO_PARECE_SEGURA_PRUEBA_MEZCLANDO_NUMEROS_Y_LETRAS)");
        }
        else if (Password != '') {
            $("#register-password-tip-long").css('display', 'none');
            $('#register-password-tip').removeClass('register-tip-error');
            $("#register-password-tip").fadeIn("slow");
            $('#register-password').removeClass('register-editor-field-error');
            $('#register-repeat-password').removeClass('register-editor-field-error');
            $('#register-password-tip').html("Translate(STR_GENIAL_NO_LA_OLVIDES)");
        }
        else if (Password == '') {
            $("#register-password-tip-long").css('display', 'none');
            $('#register-password-tip').addClass('register-tip-error');
            $("#register-password-tip").fadeIn("slow");
            $('#register-password').addClass('register-editor-field-error');
            $('#register-repeat-password').addClass('register-editor-field-error');
            $('#register-password-tip').html("Translate(STR_DEBES_RELLENARLAS)");
        }
    });

    $("#register-legal-check").on('click', function () {
        $(this).toggleClass('register-legal-check-on');
        var legal_check = $('#legal').val();
        if (legal_check == 1)
            $('#legal').val(0);
        else
            $('#legal').val(1);
    });

    $("#top-home-container-fb-button").on('click', function () {

        if (_gaq) {
            _gaq.push(['_trackPageview', '/Likekit/Entrar']);
        }

        parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';

    });

    $("#container-fb-button").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
    });

    $("#a_registreCorreuE").on('click', function () {
        $('#register-step-0-form').css('display', 'none');
        $('#register-step-1-form').css('display', 'block');
    });

    /*$("#register-legal-link").on('click', function () {
    $("#signup-legal-modal").css('display', 'block');
    });*/

    //Register : step-1 button
    $("#register-step-1-btn").on('click', function () {
        var mail = $('#Email').val();
        //var firstname = $('#FirstName').val();
        var firstname = "";
        //var lastname = $('#LastName').val();
        var lastname = "";
        var Password = $('#Password').val();
        //var RepeatPassword = $('#RepeatPassword').val();
        var RepeatPassword = Password;
        var legal_check = $('#legal').val();

        var is_valid = true;


        $.get('CheckMail/?mail=' + mail, function (data) {

            if ($.trim(data) == 'Success') {

                if (!isValidEmailAddress(mail)) {
                    is_valid = false;
                    $("#register-username-tip").fadeIn("slow");
                    $('#register-username-tip').addClass('register-tip-error');
                    $('#register-editor-field-email').addClass('register-editor-field-error');
                    $('#register-username-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
                }
                else if ((Password == '') || (!isStrongPassword(Password))) {
                    is_valid = false;
                    $("#register-password-tip-long").css('display', 'none');
                    $("#register-password-tip").fadeIn("slow");
                    $('#register-password-tip').addClass('register-tip-error');
                    $('#register-password').addClass('register-editor-field-error');
                    $('#register-repeat-password').addClass('register-editor-field-error');
                    $('#register-password-tip').html("Translate(STR_DEBES_RELLENARLAS)");
                }
                else if (legal_check == 0) {
                    is_valid = false;
                    //$('#register-step-1-form').css('display', 'none');

                    $('#register-editor-field-email').removeClass('register-editor-field-error');
                    $('#register-editor-field-firstname').removeClass('register-editor-field-error');
                    $('#register-editor-field-lastname').removeClass('register-editor-field-error');
                    $('#register-password').removeClass('register-editor-field-error');
                    $('#register-repeat-password').removeClass('register-editor-field-error');

                    $("#signup-legal-error-modal").css('display', 'block');
                }


                if (is_valid === true) { //Step 2
                    //Crida la funció "ferRegistre()", que és l'encarregada de inserir les dades que acaba de possar el nou usuari, a la BD.
                    ferRegistre();

                    $('#register-editor-field-email').removeClass('register-editor-field-error');
                    $('#register-editor-field-firstname').removeClass('register-editor-field-error');
                    $('#register-editor-field-lastname').removeClass('register-editor-field-error');
                    $('#register-password').removeClass('register-editor-field-error');
                    $('#register-repeat-password').removeClass('register-editor-field-error');

                    $('#formulari').css('display', 'none');
                    //$('#register-step-1-form').css('margin-top', '-11%');
                    $('#register-step-1-form').css('display', 'none');
                    $('#register-content-3').css('display', 'block');
                }
            }
            /*else { 
            
            }*/
        });
    });



    //Hover product register entry
    $(".register-product-image.noselect").on('hover', function () {
        $(this).children('.register-product-image-selection:first').toggle();
    });

    $(".register-product-image.noselect").on('click', function () {
        $(this).removeClass('noselect');
        $(this).addClass('select');
        $(this).children('.register-product-image-selection:first').css('display', 'block');
    });

    $(".register-product-image.select").on('click', function () {
        $(this).removeClass('select');
        $(this).addClass('noselect');
        $(this).children('.register-product-image-selection:first').css('display', 'block');
    });


    //Register : step-2 button
    $("#register-step-2-btn").on('click', function () {

        //Ara ja no cal que hi hagi items seleccionats
        //if (array_selected_items.length > 0) { //Step 3
        $('#register-content-2').css('display', 'none');
        $('#register-content-3').css('display', 'block');
        //}
    });

    $("#register-step-3-btn").hover(function () {
        $(this).toggleClass('register-step-3-btn-on');
    });

    //Register : step-3 button
    /*$("#register-step-3-btn").on('click', */
    function ferRegistre() {
        //$('#register-content-3').css('display', 'none');

        //Registre normal
        //Aquí hem de registrar
        var mail = $('#Email').val();
        //MarcGM(2-5-2013): Lo que fà això és possar com a "firstname" la part d'abans de la "@" del e-amil.
        var firstnameMail = mail.split("@", 1);
        //var firstname = $('#FirstName').val();
        var firstname = firstnameMail;
        //MarcGM: La variable "lastname", està cambiada perquè ara el registre es fà només possant el email.
        //var lastname = $('#LastName').val();
        var lastname = "";
        var password = $('#Password').val();
        var registerFollowings = $("input[id='RegisterFollowings']").map(function () { return $(this).val(); }).get();

        if (registerFollowings == "")
            registerFollowings = "0";

        mail = encodeURIComponent(mail);
        firstname = encodeURIComponent(firstname);
        lastname = encodeURIComponent(lastname);
        Password = encodeURIComponent(password);

        var url_register = 'DoRegister/?mail=' + mail + '&firstname=' + firstname + '&lastname=' + lastname + '&password=' + password + '&selected_items=' + array_selected_items + '&register_followings=' + registerFollowings;

        $.post(url_register, function (data) {
            if ($.trim(data) == 'Error') {
            }
            else if ($.trim(data) == 'ErrorMail') {
            }
            else if ($.trim(data) == 'Success') {
                $('#register-content-3').css('display', 'block');
            }
        });

    }

    //Register : step-3 button
    $("#register-step-3-btn").on('click', function () {
        //var twitterRegister = $('#TwitterRegister').val();
        //var facebookRegister = $('#FacebookRegister').val();
        //Mirem si és un registre de twitter o facebook
        /*if ((twitterRegister == 1) || (facebookRegister == 1))
        parent.location.href = SUBDOMAIN_PATH + '/Timeline';
        else*/
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
    });

    /*$("#register-step-3-btn").on('click', function () {
    var invite = $('#Invite').val();
        
    if ((invite == 1))
    parent.location.href = SUBDOMAIN_PATH + '/Account/DoInvite';
    else
    parent.location.href = SUBDOMAIN_PATH + '/Timeline';
    });*/

    /*$("#register-step-3-btn").on('click', function () {
    //$('#register-content-3').css('display', 'none');
    var registerFollowings = $("input[id='RegisterFollowings']").map(function () { return $(this).val(); }).get();
    var invite = $('#Invite').val();

    if (registerFollowings == "")
    registerFollowings = "0";

    /*var url_register = 'DoInviteRegister/?register_followings=' + registerFollowings + '&Invite=' + invite;

    $.post(url_register, function (data) {
    if ($.trim(data) == 'Error') {
    }
    else if ($.trim(data) == 'ErrorMail') {
    }
    else if ($.trim(data) == 'Success') {
    parent.location.href = SUBDOMAIN_PATH + '/Timeline/Index';
    }
    });*/
    //});

    //Register : activation button
    $("#register-activation-btn").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
    });

    $("#register-twitter").on('click', function () {

        var legal_check = $('#legal').val();

        if (legal_check == 0) {
            $("#signup-legal-error-modal").css('display', 'block');
        }
        else {
            parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterRegister';
        }
    });

    $("#register-facebook").on('click', function () {

        var legal_check = $('#legal').val();

        if (legal_check == 0) {
            $("#signup-legal-error-modal").css('display', 'block');
        }
        else {
            parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
        }
    });

    /**

    SOCIAL REGISTER

    **/

    //Register Page, Email
    $("#mail_social").on('click', function () {

        var value = $('#mail_social').val();

        if (value == 'E-mail') {
            $('#mail_social').val('');
            $('#mail_social').addClass('login-italic');
        }
    });

    //Login Page, Email
    $("#mail_social").on('focus', function () {

        var value = $('#mail_social').val();

        if (value == 'E-mail') {
            $('#mail_social').val('');
            $('#mail_social').addClass('login-italic');
        }
    });

    //Login Page, Email
    $("#mail_social").on('focusout', function () {

        var value = $('#mail_social').val();

        if (value == '') {
            $('#mail_social').val('E-mail');
            $('#mail_social').removeClass('login-italic');
        }
    });


    //Register Page, FirstName
    $("#firstname_social").on('click', function () {

        var value = $('#firstname_social').val();

        if (value == 'Translate(STR_NOMBRE)') {
            $('#firstname_social').val('');
        }
    });

    //Login Page, FirstName
    $("#firstname_social").on('focus', function () {

        var value = $('#firstname_social').val();

        if (value == 'Translate(STR_NOMBRE)') {
            $('#firstname_social').val('');
        }
    });

    //Login Page, FirstName
    $("#firstname_social").on('focusout', function () {

        var value = $('#firstname_social').val();

        if (value == '') {
            $('#firstname_social').val('Translate(STR_NOMBRE)');
        }
    });

    //Register Page, LastName
    $("#lastname_social").on('click', function () {

        var value = $('#lastname_social').val();

        if (value == 'Translate(STR_APELLIDOS)') {
            $('#lastname_social').val('');
        }
    });

    //Login Page, LastName
    $("#lastname_social").on('focus', function () {

        var value = $('#lastname_social').val();

        if (value == 'Translate(STR_APELLIDOS)') {
            $('#lastname_social').val('');
        }
    });

    //Login Page, LastName
    $("#lastname_social").on('focusout', function () {

        var value = $('#lastname_social').val();

        if (value == '') {
            $('#lastname_social').val('Translate(STR_APELLIDOS)');
        }
    });

    //Register : Mail
    $("#mail_social").on('focusout', function () {

        var mail = $('#mail_social').val();

        if (isValidEmailAddress(mail)) {
            $("#register-content-social-data-tip").fadeOut("slow");
            $.post('CheckMail/?mail=' + mail, function (data) {
                //$('#landing-content-form').html(data);

                if ($.trim(data) == 'Error') {
                    $('#register-content-social-data-tip').html("Translate(STR_ESTE_CORREO_YA_ESTA_INSCRITO_GRACIAS)");
                    $("#register-content-social-data-tip").fadeIn("slow");
                }
                else if ($.trim(data) == 'ErrorMail') {
                    $('#register-content-social-data-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
                    $("#register-content-social-data-tip").fadeIn("slow");
                }
                else if ($.trim(data) == 'Success') {
                    //$('#register-content-social-data-tip').html("Translate(STR_PERFECTO)");
                }
            });
        }
        else {
            $("#register-content-social-data-tip").fadeIn("slow");
            $('#register-content-social-data-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
        }
    });

    //Register : register-content-social-data-btn
    $("#register-content-social-data-btn").on('click', function () {

        var mail = $('#mail_social').val();
        var firstname = $("#firstname_social").val();
        var lastname = $("#lastname_social").val();

        if (isValidEmailAddress(mail)) {
            $("#register-content-social-data-tip").fadeOut("slow");

            $.post('CheckMail/?mail=' + mail, function (data) {

                if ($.trim(data) == 'Error') {
                    $('#register-content-social-data-tip').html("Translate(STR_ESTE_CORREO_YA_ESTA_INSCRITO_GRACIAS)");
                    $("#register-content-social-data-tip").fadeIn("slow");
                }
                else if ($.trim(data) == 'ErrorMail') {
                    $('#register-content-social-data-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
                    $("#register-content-social-data-tip").fadeIn("slow");
                }
                else if ($.trim(data) == 'Success') {
                    if ((firstname != '') && (lastname != '')) {
                        $('#register-content-social-data').css('display', 'none');
                        $('#register-content-2').css('display', 'block');
                        $('#register-content-3').css('display', 'none');
                    }
                }
            });
        }
        else {
            $("#register-content-social-data-tip").fadeIn("slow");
            $('#register-content-social-data-tip').html("Translate(STR_LA_DIRECCION_DE_CORREO_NO_ES_CORRECTA)");
        }
    });

    /**

    FI SOCIAL REGISTER

    **/


    $("#register-legal-link").on('click', function () {
        $("#signup-legal-modal").css('display', 'block');
    });

    $("#signup-legal-close").on('click', function () {
        $("#signup-legal-modal").css('display', 'none');
    });

    $("#signup-legal-error-close").on('click', function () {
        $("#signup-legal-error-modal").css('display', 'none');
    });



    $("#register-check-invite").on('click', function () {
        $(this).toggleClass('register-check-invite-notchecked');
        var invite = $('#Invite').val();
        if(invite == 0){
            $('#Invite').val(1);
        }else{
            $('#Invite').val(0);
        }
    });
});

$(".login-register-link").on('click', function () {
    parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
});

function AddRegisterProduct(id){
    //Mirem si existeix l'element a l'array, sinó, l'afegim. Si existeix s'ha d'esborrar
    var pos = $.inArray(id, array_selected_items);

    //alert(pos);

    if(pos >= 0){
        array_selected_items.splice(pos, 1);
    }else{
        array_selected_items.push(id);
    }

    //alert(array_selected_items.length);

    //Ara ja no cal que hi hagi items seleccionats
    /*if (array_selected_items.length > 0) //Si hi ha items seleccionats, activem el botó.
        $('#register-step-2-btn').addClass('register-step-2-btn-on');
    else
        $('#register-step-2-btn').removeClass('register-step-2-btn-on');*/
}


$("#register-step-3-btn-social-register").live('click', function () {
    window.fbAsyncInit = function () {
        FB.init({
            appId: '400049130015461',                        // App ID from the app dashboard
            channelUrl: 'http://likekit.com/Scripts/friend-selector/channel.html', // Channel file for x-domain comms
            status: true,                                 // Check Facebook Login status
            xfbml: true                                  // Look for social plugins on the page
        });
    }

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));

    FB.login(function (response) {
        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', function (response) {
                console.log('Good to see you, ' + response.name + '.');
                $(".bt-fs-dialog").click();
            });
            console.log("inici sessio Facebook correcte");
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    });
    //Aquesta variable guarda si s'ha cridat, ja, un cop a ".bs-fs-fialog". Si és "false", no s'ha cridat cap cop.
    var comptador = false;

    $(".bt-fs-dialog").live('click', function () {
        $(document).ready(function () {
            $(".bt-fs-dialog").fSelector({
                onStart: function (response) {
                    setTimeout("$('#fs-select-all').click()", 1000);
                },
                onSubmit: function (response) {
                    var selected_friends = [];
                    $.each(response, function (k, v) {
                        selected_friends[k] = v;
                    });
                },
                onClose: function (response) {
                    parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';
                }
            });
        });
        if (comptador == false) {
            comptador = true;
            $(".bt-fs-dialog").click();
        }
    });
});

$("#register-step-3-btn-social-register").live('hover', function () {
    $(this).toggleClass('register-step-3-btn-social-register-on');
});

$("#container-fb-button").hover(function () {
    $(this).toggleClass('register-social-button-facebook-on');
});