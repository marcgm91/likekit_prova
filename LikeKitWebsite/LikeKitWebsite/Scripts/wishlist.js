﻿
$(window).load(function () {

    setWishlistTop();

    $(".menu-left-option").hover(function () {
        $(this).toggleClass('menu-left-option-selected');
    });

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    //Hover wishlist entry
    $(".wishlist-entry-image").live('hover', function () {
        $(this).children('.wishlist-entry-info:first').toggle();
    });


    //WishList Search
    $("#wishlist-product-search").live('click', function () {

        $('#wishlist-product-search').addClass('timeline-search-italic');
        
        var search = $('#wishlist-product-search').val();
        if (search == 'Translate(STR_ANADIR_AL_WISHLIST)') {
            $('#wishlist-product-search').val('');
        }
    });

    //Wishlist Search Enter
    $("#wishlist-product-search").keypress(function (e) {
        var search = $('#wishlist-product-search').val();

        if ((search.length > 2) && (e.keyCode == 13)) {
            //WishListSearch();

            //Busquem al WS
            $('#timeline-wishlist-search-result-container').css('display', 'none');
            $('#timeline-wishlist-webservice-search').css('display', 'block');
            $('#timeline-wishlist-search').css('padding-bottom', '0');

            WishListWSSearch();
        }
    });

    //Wishlist Search Keyup
    $("#wishlist-product-search").keyup(function (e) {

        var search = $('#wishlist-product-search').val();

        if ((search.length > 2) && (e.keyCode != 13)) {
            WishListSearch();
        }
    });

    //Now With close
    $("#timeline-wishlist-close").live('click', function () {
        $('#timeline-wishlist-modal').css('display', 'none');
    });

    //Product WS Search
    $("#timeline-wishlist-seach-button").click(function () {
        $('#timeline-wishlist-search-result-container').css('display', 'none');
        $('#timeline-wishlist-webservice-search').css('display', 'block');
        $('#timeline-wishlist-search').css('padding-bottom', '0');

        WishListWSSearch();
    });

    $("#timeline-wishlist-seach-button").hover(function () {
        $(this).toggleClass('timeline-wishlist-seach-button-on');
    });

    //WS Search more results
    $("#timeline-wishlist-search-result-more-results").live('click', function () {
        $(this).toggleClass('timeline-wishlist-search-result-more-results-on');
        $("#timeline-wishlist-search-result-more-results-container").toggle();
    });


    $("#timeline-wishlist-search-container-category-books").live('hover', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-books-on');
    });

    $("#timeline-wishlist-search-container-category-series").live('hover', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-series-on');
    });

    $("#timeline-wishlist-search-container-category-music").live('hover', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-music-on');
    });

    $("#timeline-wishlist-search-container-category-cinema").live('hover', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-cinema-on');
    });

    $("#timeline-wishlist-search-container-category-books").live('click', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-books-selected');
        SetWishListCategoryForm(1);
    });

    $("#timeline-wishlist-search-container-category-series").live('click', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-series-selected');
        SetWishListCategoryForm(2);
    });

    $("#timeline-wishlist-search-container-category-music").live('click', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-music-selected');
        SetWishListCategoryForm(4);
    });

    $("#timeline-wishlist-search-container-category-cinema").live('click', function () {
        $(this).toggleClass('timeline-wishlist-search-container-category-cinema-selected');
        SetWishListCategoryForm(3);
    });

    //Close Lo quiero dialog
    $("#kit-user-loquiero-container-close").live('click', function () {
        $('#kit-user-loquiero-container').css('display', 'none');
    })
});

function Filter(id) {

    var div_id = "#menu-left-option-" + id;
  
    if (timeline_filter[id - 1] === true) {
        $(div_id).removeClass('menu-left-option-active');    
    }
    else {
        $(div_id).addClass('menu-left-option-active');    
    }

    refreshFilter(id);
}

function refreshFilter(id) {

    //alert('id : ' + id + ' text : ' + timeline_filter_texts[id - 1]);
    if (id == 3) //Controlem els todos
    {
        if (timeline_filter[2] === true) {
            timeline_filter[0] = false;
            timeline_filter[1] = false;
            timeline_filter[2] = false;
        }
        else {
            timeline_filter[0] = true;
            timeline_filter[1] = true;
            timeline_filter[2] = true;
        }

        Draw(1);
        Draw(2);
        Draw(3);
    }
    else if (id == 8) //Controlem els todos
    {
        if (timeline_filter[7] === true) {
            timeline_filter[3] = false;
            timeline_filter[4] = false;
            timeline_filter[5] = false;
            timeline_filter[6] = false;
            timeline_filter[7] = false;
        }
        else {
            timeline_filter[3] = true;
            timeline_filter[4] = true;
            timeline_filter[5] = true;
            timeline_filter[6] = true;
            timeline_filter[7] = true;
        }

        Draw(4);
        Draw(5);
        Draw(6);
        Draw(7);
        Draw(8);
    }
    else if (id == 11) //Controlem els todos
    {
        if (timeline_filter[10] === true) {
            timeline_filter[8] = false;
            timeline_filter[9] = false;
            timeline_filter[10] = false;
        }
        else {
            timeline_filter[8] = true;
            timeline_filter[9] = true;
            timeline_filter[10] = true;
        }

        Draw(9);
        Draw(10);
        Draw(11);
    }
    else if (timeline_filter[id - 1] === true)
        timeline_filter[id - 1] = false;
    else
        timeline_filter[id - 1] = true;


    //Ara deshabilitem els todos, en cas de deshabilitar una opció
    if ((timeline_filter[0] == false) || (timeline_filter[1] == false)) {
        timeline_filter[2] = false;
        Draw(3);
    }

    if ((timeline_filter[3] == false) || (timeline_filter[4] == false) || (timeline_filter[5] == false) || (timeline_filter[6] == false)) {
        timeline_filter[7] = false;
        Draw(8);
    }

    if ((timeline_filter[8] == false) || (timeline_filter[9] == false)) {
        timeline_filter[10] = false;
        Draw(11);
    }

    var div_output = "";
    var div_output_1 = "";
    var div_output_2 = "";
    var div_output_3 = "";
    var count = 0;

    for(i=0; i<12; i++) {

        if ((timeline_filter[i] === true) && (i != 2) && (i != 7) && (i != 10)) {

            if(count < 3)
                div_output_1 = div_output_1 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";
            else if (count < 6)
                div_output_2 = div_output_2 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";
            else
                div_output_3 = div_output_3 + "<div class='top-header-filter-text'>" + timeline_filter_texts[i] + "</div>";
            count++;
        }
    }

    if(div_output_1 != "")
        div_output_1 = '<div id="top-header-filter-1" class="top-header-filter-container">' + div_output_1 + '</div>';
    if(div_output_2 != "")
        div_output_2 = '<div id="top-header-filter-2" class="top-header-filter-container">' + div_output_2 + '</div>';
    if(div_output_3 != "")
	    div_output_3 = '<div id="top-header-filter-3" class="top-header-filter-container">' + div_output_3 + '</div>';

    $('#top-header-filter').html('<table><tr><td>' + div_output_1 + '</td><td>' + div_output_2 + '</td><td>' + div_output_3 + '</td></tr></table>');
    setTimelineTop();

    //Refresquem el timeline
    //TimeLineSearch();
}

function Draw(id) {

    var div_id = "#menu-left-option-" + id;

    if (timeline_filter[id - 1] === true) {
        $(div_id).addClass('menu-left-option-active');
    }
    else {
        $(div_id).removeClass('menu-left-option-active');
    }
}

function setWishlistTop() {
    var height = $('#header').height();
    $('#wishlist').css("margin-top", height + "px");
}

function TimeLineSearch() {

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }

    //alert(search);

    $.post('/Timeline/TimelineSearch/?search=' + search + '&filter=', function (data) {
        $('#timeline').html(data);
        //alert(data);
        //Hauriem d'executar mazonry?
        var $timeline = $('#timeline');
        //var $timeline = $('#timeline');

        $timeline.imagesLoaded(function () {
            $timeline.masonry('reload');
            /*$timeline.masonry({
            itemSelector: '.timeline-entry',
            columnWidth: 240
            });*/
        });

    });
}

function DelWishList(id) {
    $.post('/Timeline/DelWishList/?id=' + id, function (data) {
        location.reload();
    });
}

function AddWishListForm(userid) {
    $('#wishlist-product-search').val('Translate(STR_ANADIR_AL_WISHLIST)');
    $('#timeline-wishlist-modal').css('display', 'block');
    $("#timeline-wishlist-form").css('display', 'none');
    $("#timeline-wishlist-search").css('display', 'block');
    $('#timeline-wishlist-search-result-container').css('display', 'none');
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}

function WishListSearch() {

    var search = $('#wishlist-product-search').val();

    if (search == 'Translate(STR_ANADIR_AL_WISHLIST)') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#wishlist-product-categories').val();

    $.post('/Timeline/WishListSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-wishlist-search-result-container').html(data);
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-wishlist-search-result-container').css('display', 'block');
    });
}

function WishListAddProduct(id) {
    $.post('/Timeline/WishListAddProductReload/?id=' + id, function (data) {
        //location.reload();
        var userid = $('#profile-user-id').val();
        $('#timeline-wishlist-modal').css('display', 'none');

        $('#wishlist-entry-container').html(data);
        var $wishlist = $('#wishlist');

        $wishlist.imagesLoaded(function () {
            $wishlist.masonry('reload');
        });
    });
}

/**
    
WS VERSION

**/

function WishListWSSearch() {

    var search = $('#wishlist-product-search').val();
    
    if (search == 'Translate(STR_RECOMENDAR)...') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#wishlist-product-categories').val();

    $.post('/Timeline/WishListWSSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-wishlist-search-result-container').html(data);
        $('#timeline-wishlist-webservice-search').css('display', 'none');
        $('#timeline-wishlist-search-result-container').css('display', 'block');
        $('#timeline-wishlist-webservice-search').css('display', 'none');
        $('#timeline-wishlist-search').css('padding-bottom', '24px');
    });
}


function WishListAddWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations) {

    ASIN = encodeURIComponent(ASIN);
    Title = encodeURIComponent(Title);
    Author = encodeURIComponent(Author);
    Year = encodeURIComponent(Year);
    Category = encodeURIComponent(Category);
    LargeImage = encodeURIComponent(LargeImage);
    URL = encodeURIComponent(URL);
    Description = encodeURIComponent(Description);
    EAN = encodeURIComponent(EAN);
    Observations = encodeURIComponent(Observations);

    $.post('/Timeline/WishListWSAddProductReload/?ASIN=' + ASIN + "&Title=" + Title + "&Author=" + Author + "&Year=" + Year + "&Category=" + Category + "&LargeImage=" + LargeImage + "&URL=" + URL + "&Description=" + Description + "&EAN=" + EAN + "&Observations=" + Observations, function (data) {
        var userid = $('#profile-user-id').val();
        $('#timeline-wishlist-modal').css('display', 'none');

        $('#wishlist-entry-container').html(data);
        var $wishlist = $('#wishlist');

        $wishlist.imagesLoaded(function () {
            $wishlist.masonry('reload');
        });
    });
}

/**

FI WS VERSION

**/

function SetWishListCategoryForm(id_category) {
    var values = $('#wishlist-product-categories').val();

    //Mirem si ja existeix a les categories, si hi és l'esborrem, sinó, l'afegim
    if (values.indexOf(id_category) >= 0) {
        values = values.replace("," + id_category, "");
        values = values.replace(id_category, "");
    }
    else {
        if (values.length > 0) {
            values = values + "," + id_category;
        }
        else
            values = id_category;
    }

    $('#wishlist-product-categories').val(values);
}

function LoadLoQuieroDialog(product_id) {

    $.post('/Timeline/LoadLoQuiero/?product_id=' + product_id, function (data) {

        $('#kit-user-loquiero-container').replaceWith(data);

        $('#kit-user-loquiero-container').css({
            //position: 'absolute',
            //left: ($(window).width() - $('#kit-user-loquiero-container').outerWidth()) / 2,
            top: ($(window).height() - $('#kit-user-loquiero-container').outerHeight()) / 2
        });

        $('#kit-user-loquiero-container').css('display', 'block');
    });
    
}