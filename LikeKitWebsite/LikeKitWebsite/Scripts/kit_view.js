﻿
//$(document).ready(function () {
$(window).load(function () {

    setTimelineTop();

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    //Add kit textarea
    $("#add_kit_comment").live('click', function () {
        
        var comment = $('#add_kit_comment').val();
        if (comment == 'Translate(STR_COMENTA_TU_TAMBIEN_ESTA_PUBLICACION)') {
            $('#add_kit_comment').val('');
        }
    });

    //Add kit textarea
    $("#add_kit_comment").live('focusout', function () {

        var comment = $('#add_kit_comment').val();
        if (comment == '') {
            $('#add_kit_comment').val('Translate(STR_COMENTA_TU_TAMBIEN_ESTA_PUBLICACION)');
        }
    });

    //Add kit textarea
    $("#add_kit_comment").live('focus', function () {

        var comment = $('#add_kit_comment').val();
        if (comment == 'Translate(STR_COMENTA_TU_TAMBIEN_ESTA_PUBLICACION)') {
            $('#add_kit_comment').val('');
        }
    });

    //Add kit textarea
    $("#add_kit_comment").live('keyup', function () {

        var comment = $('#add_kit_comment').val();
        if (comment != '')
            $('#kit-view-container-comment-button').addClass('kit-view-container-comment-button-enabled');
        else
            $('#kit-view-container-comment-button').removeClass('kit-view-container-comment-button-enabled');
    });

    //Hover user kit
    $(".kit-view-container-users-entry").live('hover', function () {
        $(this).children('.kit-view-container-users-entry-info:first').toggle();
    });

    $("#kit-view-menu-info").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#kit-view-menu-info-content").toggle();
    })

    $("#kit-view-menu-tags").live('click', function () {
        //$(this).toggleClass('menu-left-section-selected');
        $(this).toggleClass('kit-view-menu-tags-hidden');
        $("#kit-view-menu-tags-content").toggle();
    })

    //Hover lo quiero
    $("#kit-user-loquiero").hover(function () {
        $(this).toggleClass('kit-user-loquiero-selected');
    });

    //Kit view comment
    $(".kit-view-comment").live('click', function () {
        $("#add_kit_comment").focus();
    })

    //Show Delete comment
    $(".kit-view-container-comments-entry").live({
        mouseenter:
            function () {
                $(this).children('.kit-comment-delete:first').css('display', 'block');
            },
        mouseleave:
            function () {
                $(this).children('.kit-comment-delete:first').css('display', 'none');
            }
    });

    //Delete comment hover
    $(".kit-comment-delete").live('hover', function () {
        $(this).toggleClass('kit-comment-delete-on');
    });


    $("#twitter-social-text").live('keyup', function (e) {

        var comment = $('#twitter-social-text').val();
        if (comment != '') {
            $('#twitter-social-kit-button').addClass('social-kit-button-on');
        }
        else {
            $('#twitter-social-kit-button').removeClass('social-kit-button-on');
        }

        var comment = $('#twitter-social-text').val();

        //Recalculem el counter
        var counter = 120 - comment.length;
        $('#social-kit-text-counter').html(counter);

        //Si passa de 120, posem els 120 primers caràcters
        if ($(this).val().length > 120)
            $(this).val($(this).val().substr(0, 120));
    });

    $("#facebook-social-text").live('keyup', function (e) {

        var comment = $('#facebook-social-text').val();
        if (comment != '') {
            $('#facebook-social-kit-button').addClass('social-kit-button-on');
        }
        else {
            $('#facebook-social-kit-button').removeClass('social-kit-button-on');
        }
    });

    $("#mail-social-text").live('keyup', function (e) {

        var comment = $('#mail-social-text').val();

        if (comment != '') {
            $('#mail-social-kit-button').addClass('social-kit-button-on');
        }
        else {
            $('#mail-social-kit-button').removeClass('social-kit-button-on');
        }
    });

    //Close Twitter form
    $("#twitter-social-kit-close").live('click', function () {
        $('#twitter-social-kit-modal').css('display', 'none');
    })

    //Close Facebook form
    $("#facebook-social-kit-close").live('click', function () {
        $('#facebook-social-kit-modal').css('display', 'none');
    })

    //Close Mail form
    $("#mail-social-kit-close").live('click', function () {
        $('#mail-social-kit-modal').css('display', 'none');
    })

    //Close Twitter connect form
    $("#twitter-connect-kit-close").live('click', function () {
        $('#twitter-connect-kit-modal').css('display', 'none');
    })

    //Close Facebook connect form
    $("#facebook-connect-kit-close").live('click', function () {
        $('#facebook-connect-kit-modal').css('display', 'none');
    })

    $("#twitter-connect-kit-button-container").live('hover', function () {
        $(this).toggleClass('social-connect-kit-button-on');
        $('#twitter-connect-kit-button').toggleClass('social-connect-button-twitter-on');
    });

    $("#facebook-connect-kit-button-container").live('hover', function () {
        $(this).toggleClass('social-connect-kit-button-on');
        $('#facebook-connect-kit-button').toggleClass('social-connect-button-facebook-on');
    });

    //Do kit Tweet
    $("#twitter-social-kit-button").live('click', function () {
        var kit_id = $('#kit-id').val();
        var comment = $('#twitter-social-text').val();

        comment = encodeURIComponent(comment);

        var url = '/Timeline/doTweet/?kit_id=' + kit_id + '&comment=' + comment;

        if ((comment != '')) {
            $.post(url, function (data) {
                $('#twitter-social-kit-modal').css('display', 'none');
                //location.reload();
            });
        }
    })

    //Do kit facebook publish
    $("#facebook-social-kit-button").live('click', function () {
        var kit_id = $('#kit-id').val();
        var comment = $('#facebook-social-text').val();

        comment = encodeURIComponent(comment);

        var url = '/Timeline/doFacebookPublish/?kit_id=' + kit_id + '&comment=' + comment;

        if ((comment != '')) {
            $.post(url, function (data) {
                $('#facebook-social-kit-modal').css('display', 'none');
                //location.reload();
            });
        }
    })

    //Password recovery
    $("#kit-social-mail").on('click', function () {

        var mail = $('#kit-social-mail').val();

        if (mail == 'Dirección e-mail') {
            $('#kit-social-mail').val('');
            $('#kit-social-mail').addClass('login-italic');
        }
    });


    $("#kit-social-mail").on('focus', function () {

        var mail = $('#kit-social-mail').val();

        if (mail == 'Dirección e-mail') {
            $('#kit-social-mail').val('');
            $('#kit-social-mail').addClass('login-italic');
        }
    });

    //Register : RepeatPassword
    $("#kit-social-mail").live('focusout', function () {

        var mail = $('#kit-social-mail').val();

        if (!isValidEmailAddress(mail)) {
            $("#kit-social-tip").fadeIn("slow");
        }
        else
            $("#kit-social-tip").fadeOut("slow");
    });

    //Do kit mail publish
    $("#mail-social-kit-button").live('click', function () {
        var kit_id = $('#kit-id').val();
        var comment = $('#mail-social-text').val();

        comment = encodeURIComponent(comment);

        var mail = $('#kit-social-mail').val();

        if (!isValidEmailAddress(mail)) {
            $("#kit-social-tip").fadeIn("slow");
        }
        else {
            $("#kit-social-tip").fadeOut("slow");

            mail = encodeURIComponent(mail);
            var url = '/Timeline/doKitMail/?kit_id=' + kit_id + '&comment=' + comment + '&mail=' + mail;

            if ((comment != '')) {
                $.post(url, function (data) {
                    $('#mail-social-kit-modal').css('display', 'none');
                });
            }
        }
    })

    //Quan està enabled
    $(".kit-view-container-comment-button-enabled").live('hover', function () {
        $(this).toggleClass('kit-view-container-comment-button-enabled-on');
    });
    
    //Close Lo quiero dialog
    $("#kit-user-loquiero-container-close").live('click', function () {
        $('#kit-user-loquiero-container').css('display', 'none');
    })
});

function setTimelineTop() {
    var height = $('#header').height();
    $('#timeline').css("margin-top", height + "px");
}

//Comment button
function AddKitComment(id) {
    var comment = $('#add_kit_comment').val();
    if (comment != '') {

        comment = encodeURIComponent(comment);

        $.post('/Timeline/AddKitComment/?KitID=' + id + '&comment=' + comment, function (data) {
            $('#kit-view-container-comments-masonry').html(data);

            var $kits = $('#kit-view-container-comments-masonry');

            /*$kits.imagesLoaded(function () {
                $kits.masonry('reload');
            });*/

            //Actualitzem el comptador de comentaris
            $.post('/Timeline/GetNumKitComments/?id=' + id, function (data) {
                $('#kit-view-options-comments-values').text(data);
            });

        });

        $('#add_kit_comment').val('');
        $('#kit-view-container-comment-button').removeClass('kit-view-container-comment-button-enabled');
    }
}

//Comment button
function KitCommentDelete(id, kitID) {

    $.post('/Timeline/DelKitComment/?KitID=' + kitID + '&KitCommentID=' + id, function (data) {
        $('#kit-view-container-comments-masonry').html(data);

        var $kits = $('#kit-view-container-comments-masonry');

        //Actualitzem el comptador de comentaris
        $.post('/Timeline/GetNumKitComments/?id=' + id, function (data) {
            $('#kit-view-options-comments-values').text(data);
        });

        /*$kits.imagesLoaded(function () {
        $kits.masonry('reload');
        });*/

    });
}

$(window).load(function () {

    var identifier = window.location.hash; //gets everything after the hashtag i.e. #home

    if (identifier == "#kit-view-container-comment-entry-container") {
        $("#add_kit_comment").focus();
        //$('html,body').animate({ scrollTop: 0 }, 'slow');
       }
});

function KitViewLike(id) {

    $.post('/Timeline/DoLike/?id=' + id, function (data) {
        $('#kit-view-like').replaceWith('<div id="kit-view-like" class="kit-view-dislike" onclick="KitViewDisLike(' + id + ');">Dislike</div>');

        //Actualitzem el número de likes
        $.post('/Timeline/GetNumLikes/?id=' + id, function (data) {
            $('#kit-view-options-likes-value').text(data);
        });
    });
}

function KitViewDisLike(id) {

    $.post('/Timeline/DoDisLikeKitId/?Kitid=' + id, function (data) {
        $('#kit-view-like').replaceWith('<div id="kit-view-like" class="kit-view-like" onclick="KitViewLike(' + id + ');">Like</div>');

        //Actualitzem el número de likes
        $.post('/Timeline/GetNumLikes/?id=' + id, function (data) {
            $('#kit-view-options-likes-value').text(data);
        });
    });
}

function Tweet(id) {

    var comment = $('#twitter-social-text').val();

    //Recalculem el counter
    var counter = 120 - comment.length;
    $('#social-kit-text-counter').html(counter);

    if (comment != '') {
        $('#twitter-social-kit-button').addClass('social-kit-button-on');
    }
    else {
        $('#twitter-social-kit-button').removeClass('social-kit-button-on');
    }

    $('#twitter-social-kit-modal').css('display', 'block');
}

function FacebookShare(id) {

    var comment = $('#facebook-social-text').val();
    if (comment != '') {
        $('#facebook-social-kit-button').addClass('social-kit-button-on');
    }
    else {
        $('#facebook-social-kit-button').removeClass('social-kit-button-on');
    }

    $('#facebook-social-kit-modal').css('display', 'block');
}

function KitMail() {

    var comment = $('#mail-social-text').val();

    if (comment != '') {
        $('#mail-social-kit-button').addClass('social-kit-button-on');
    }
    else {
        $('#mail-social-kit-button').removeClass('social-kit-button-on');
    }

    $('#mail-social-kit-modal').css('display', 'block');
}

function ConnectToTweet(id) {
    $('#twitter-connect-kit-modal').css('display', 'block');
}

function ConnectToFacebookShare(id) {
    $('#facebook-connect-kit-modal').css('display', 'block');
}

function SocialTwitterConnect() {
    var kit_id = $('#kit-id').val();

    parent.location.href = '/Account/TwitterKitConnect/' + kit_id;
}

function SocialFacebookConnect() {
    var kit_id = $('#kit-id').val();

    parent.location.href = '/Account/FacebookKitConnect/' + kit_id;
}

function LoQuieroDialog() {


    //$(document).ready(function () {
        //$(window).resize(function () {

            $('#kit-user-loquiero-container').css({
                //position: 'absolute',
                //left: ($(window).width() - $('#kit-user-loquiero-container').outerWidth()) / 2,
                top: ($(window).height() - $('#kit-user-loquiero-container').outerHeight()) / 2
            });

        //});

        // To initially run the function:
        //$(window).resize();

    //});

    $('#kit-user-loquiero-container').css('display', 'block');
}