﻿$(document).ready(function () {


    //Search
    $("#timeline-search").live('click', function () {

        $('#timeline-search').addClass('timeline-search-italic');

        var search = $('#timeline-search').val();
        if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
            $('#timeline-search').val('');
        }
    });


    $("#timeline-search").live('focusout', function () {
        var search = $('#timeline-search').val();
        if (search == '') {
            $('#timeline-search').val('Translate(STR_BUSCAR_RECOMENDACIONES)');
            $('#timeline-search').removeClass('timeline-search-italic');
        }
    });


    //timeline-search Enter
    $("#timeline-search").keypress(function (e) {
        if (e.keyCode == 13) {
            TimeLineGoToSearch();
        }
    });

});


function TimeLineGoToSearch() {

    var search = $('#timeline-search').val();

    if (search == 'Translate(STR_BUSCAR_RECOMENDACIONES)') {
        search = '';
    }

    //search = encodeURIComponent(search);
    
    parent.location.href = '/Timeline/?search=' + search
}