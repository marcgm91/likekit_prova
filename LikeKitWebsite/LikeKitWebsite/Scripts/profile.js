﻿
$(document).ready(function () {

    $.globalEval("var api_crop_image_profile;");
    $.globalEval("var xField;");
    $.globalEval("var yField;");
    $.globalEval("var widthField;");
    $.globalEval("var heightField;");

    setTimelineTop();

    $("#menu-user").live('click', function () {

        $(this).toggleClass('menu-left-section-selected');
        //$("#menu-user-content").css('display', 'block');
        //$("#menu-user-interactions").css('display', 'none');
        $("#menu-user-content").toggle();
        $("#menu-user-interactions").toggle();
    })

    $("#sobre-likekit").live('click', function () {
        //$("#sobre-likekit-content").css('display', 'block');
        $(this).toggleClass('menu-left-section-selected');
        $("#sobre-likekit-content").toggle();
    })

    $("#menu-left-logo-small").hover(function () {
        $("#menu-left-logo-small-tip").toggle();
    });

    $(".profile-data-right-menu-option").hover(function () {
        $(this).toggleClass('profile-data-right-menu-option-on');
    });

    $("#profile-following-menu").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#profile-following-container").toggle();

        //Tanquem followers
        $("#profile-followers-menu").removeClass('menu-left-section-selected');
        $("#profile-followers-container").hide();
    })

    $("#profile-followers-menu").live('click', function () {
        $(this).toggleClass('menu-left-section-selected');
        $("#profile-followers-container").toggle();

        //Tanquem followings
        $("#profile-following-menu").removeClass('menu-left-section-selected');
        $("#profile-following-container").hide();
    })

    $("#profile-following-close").live('click', function () {
        $("#profile-following-menu").toggleClass('menu-left-section-selected');
        $("#profile-following-container").toggle();
    })

    $("#profile-follower-close").live('click', function () {
        $("#profile-followers-menu").toggleClass('menu-left-section-selected');
        $("#profile-followers-container").toggle();
    })

    //Hover now-with entry
    $(".profile-now-with-images-entry").live('hover', function () {
        $(this).children('.profile-now-with-close:first').toggle();
    });

    $(".profile-now-with-close-img").live('hover', function () {
        $(this).closest('.profile-now-with-images-entry').children('.profile-now-with-tip:first').toggle();
    });

    $(".profile-now-with-add-img").live('hover', function () {
        $(this).closest('.profile-now-with-images-entry').children('.profile-now-with-tip-add:first').toggle();
    })

    //Logout
    $("#logout-profile").live('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOff';
    });

    //Now with Search
    $("#now-with-product-search").live('click', function () {

        $('#now-with-product-search').addClass('timeline-search-italic');

        var search = $('#now-with-product-search').val();
        if (search == 'Translate(STR_AHORA_ESTOY_CON_MIN)') {
            $('#now-with-product-search').val('');
        }
    });

    //Now with Search Enter
    $("#now-with-product-search").keypress(function (e) {

        var search = $('#now-with-product-search').val();

        if ((search.length > 2) && (e.keyCode == 13)) {
            //NowWithSearch();

            //Busquem al WS
            $('#timeline-now-with-search-result-container').css('display', 'none');
            $('#timeline-now-with-webservice-search').css('display', 'block');
            $('#timeline-now-with-search').css('padding-bottom', '0');

            NowWithWSSearch();
        }
    });

    //Now with Search Keyup
    $("#now-with-product-search").keyup(function (e) {

        var search = $('#now-with-product-search').val();

        if ((search.length > 2) && (e.keyCode != 13)) {
            NowWithSearch();
        }
    });

    //Now With close
    $("#timeline-now-with-close").live('click', function () {
        $('#timeline-now-with-modal').css('display', 'none');
    });

    //Edit profile, social buttons
    $(".profile-data-edit-social-button").live('hover', function () {
        $(this).toggleClass('profile-data-edit-social-button-on');
    });

    $(".twitter-desconnectable").live({
        mouseenter:
            function () {
                $(this).addClass('profile-data-edit-social-button-disabled');
                $('#profile-twitter-connect').html('Translate(STR_DESCONECTAR_DE_TWITTER)');
            },
        mouseleave:
            function () {
                $(this).removeClass('profile-data-edit-social-button-disabled');
                $('#profile-twitter-connect').html('Translate(STR_CONECTADO_CON_TWITTER)');
            }
    }
    );

    $(".facebook-desconnectable").live({
        mouseenter:
            function () {
                $(this).addClass('profile-data-edit-social-button-disabled');
                $('#profile-facebook-connect').html('Translate(STR_DESCONECTAR_DE_FACEBOOK)');
            },
        mouseleave:
            function () {
                $(this).removeClass('profile-data-edit-social-button-disabled');
                $('#profile-facebook-connect').html('Translate(STR_CONECTADO_CON_FACEBOOK)');
            }
    }
    );

    $("#profile-data-edit-gender-down").live('click', function () {

        $(this).toggleClass('profile-data-edit-label-down-up');
        $("#profile-data-edit-gender").toggle();
    })

    //Edit profile, social buttons
    $(".profile-data-edit-gender-option").hover(function () {
        $(this).toggleClass('active');
    });

    $("#profile-data-edit-gender-male").live('click', function () {

        $(this).removeClass('selected');
        $("#profile-data-edit-gender-female").removeClass('selected');
        $(this).addClass('selected');
        $('#gender').val('Translate(STR_HOMBRE)');
    })

    $("#profile-data-edit-gender-female").live('click', function () {

        $(this).removeClass('selected');
        $("#profile-data-edit-gender-male").removeClass('selected');
        $(this).addClass('selected');
        $('#gender').val('Translate(STR_MUJER)');
    })

    $("#profile-edit-button").hover(function () {
        $(this).toggleClass('profile-edit-button-enabled');
    });

    //Edit Profile, perms toggle
    $("#profile-data-edit-permisos-down").live('click', function () {

        $(this).toggleClass('profile-data-edit-label-down-up');
        $("#profile-data-edit-perms-info").toggle();
    })

    //Edit Profile, widget toggle
    $("#profile-data-edit-widget-down").live('click', function () {

        $(this).toggleClass('profile-data-edit-label-down-up');
        $("#profile-data-edit-widget-info").toggle();
    });

    $(".profile-data-edit-perm").hover(function () {
        $(this).toggleClass('active');
    });

    $("#profile-edit-delete-button").hover(function () {
        $(this).toggleClass('profile-edit-delete-button-active');
    });

    //Show Delete profile modal
    $("#profile-edit-delete-button").live('click', function () {
        $('#profile-delete-modal').css('display', 'block');
    })

    //Hide Delete profile modal
    $("#profile-delete-button-no").live('click', function () {
        $('#profile-delete-modal').css('display', 'none');
    })

    //Profile clos button
    $("#profile-delete-close").live('click', function () {
        $('#profile-delete-modal').css('display', 'none');
    })

    //Profile delete button hover
    $(".profile-delete-button").hover(function () {
        $(this).toggleClass('profile-delete-button-on');
    });

    $("#edit-profile-language").live('click', function () {

        $(this).toggleClass('profile-data-edit-label-down-up');
        $("#profile-data-edit-language").toggle();
    })

    $(".profile-data-edit-language-option").hover(function () {
        $(this).toggleClass('selected');
    });

    $("#profile-data-edit-language-1").live('click', function () {
        $('#language').val(1);
        $("#profile-data-edit-language-1").addClass('active');
        $("#profile-data-edit-language-2").removeClass('active').removeClass('selected');
    });

    $("#profile-data-edit-language-2").live('click', function () {
        $('#language').val(2);
        $("#profile-data-edit-language-1").removeClass('active').removeClass('selected');
        $("#profile-data-edit-language-2").addClass('active');
    });

    //Profile button click
    $("#profile-edit-button").live('click', function () {

        //Agafem els valors
        var email = $("#email").val();
        var password = $("#password").val();
        var password_repeat = $('#password-repeat').val();
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var address = $('#address').val();
        var web = $('#web').val();
        var about = $('#about').val();
        var birthday = $('#birthday').val();
        var gender = $('#gender').val();
        var language = $('#language').val();
        var formaRegistre = $('#comprovacioFormaRegistre').val();

        email = encodeURIComponent(email);
        password = encodeURIComponent(password);
        password_repeat = encodeURIComponent(password_repeat);
        firstname = encodeURIComponent(firstname);
        lastname = encodeURIComponent(lastname);
        address = encodeURIComponent(address);
        web = encodeURIComponent(web);
        about = encodeURIComponent(about);
        birthday = encodeURIComponent(birthday);
        gender = encodeURIComponent(gender);

        var error = false;

        if (password != password_repeat) {
            error = true;
            alert("Translate(STR_LAS_CONTRASENAS_NO_COINCIDEN)");
        }

        if (!checkDate($('#birthday').val())) {
            error = true;
        }

        var perms = 0;

        //Mirem els permisos
        var profile_perm_1 = $("#profile_perm_1").val();
        if (profile_perm_1 == "1")
            perms = perms + 1;
        var profile_perm_2 = $("#profile_perm_2").val();
        if (profile_perm_2 == "1")
            perms = perms + 2;

        var profile_perm_3 = $("#profile_perm_3").val();
        if (profile_perm_3 == "1")
            perms = perms + 4;

        var profile_perm_4 = $("#profile_perm_4").val();
        if (profile_perm_4 == "1")
            perms = perms + 8;

        var profile_perm_5 = $("#profile_perm_5").val();
        if (profile_perm_5 == "1")
            perms = perms + 16;

        var profile_perm_6 = $("#profile_perm_6").val();
        if (profile_perm_6 == "1")
            perms = perms + 32;

        var profile_perm_7 = $("#profile_perm_7").val();
        if (profile_perm_7 == "1")
            perms = perms + 64;

        var profile_perm_8 = $("#profile_perm_8").val();
        if (profile_perm_8 == "1")
            perms = perms + 128;

        var profile_perm_9 = $("#profile_perm_9").val();
        if (profile_perm_9 == "1")
            perms = perms + 256;

        //var perms = profile_perm_1 + profile_perm_2 + profile_perm_3 + profile_perm_4 + profile_perm_5 + profile_perm_6 + profile_perm_7 + profile_perm_8 + profile_perm_9;

        if (error == false) {
            var url = SUBDOMAIN_PATH + '/Timeline/UpdateProfile/?email=' + email + '&password=' + password + '&firstname=' + firstname + '&lastname=' + lastname + '&address=' + address + '&web=' + web + '&about=' + about + '&birthday=' + birthday + '&gender=' + gender + '&perms=' + perms + '&language=' + language;

            $.post(url, function (data) {
                //location.reload();
                $("#profile-edit-button").removeClass('profile-edit-button-enabled');
                $("#profile-edit-button").addClass('profile-saved-button');
                $("#profile-edit-button").html('Translate(STR_HECHO)')

                UpdateProfileStatus();

                setTimeout(function () {
                    $("#profile-edit-button").removeClass('profile-saved-button').html('Translate(STR_GUARDAR_CAMBIOS)');
                }, 3000);
            });
        }
    })

    $("#edit-profile-data-photo").live({
        mouseenter:
            function () {
                $("#edit-profile-data-photo-edit").css('display', 'block');
            },
        mouseleave:
            function () {
                $("#edit-profile-data-photo-edit").css('display', 'none');
            }
    }
    );

    var button = $('#edit-profile-data-photo-change-image'), interval;
    var user_id = $('#user_id').val();

    if ($('#edit-profile-data-photo-change-image').length != 0) {
        var boto_ajax = new AjaxUpload(button, {
            action: SUBDOMAIN_PATH + '/Timeline/UpdateProfileImage',
            name: 'file',
            onSubmit: function (file, ext) {
            },
            onComplete: function (file, response) {
                if (response != '') {
                    $('#edit-profile-img').attr("src", "/Content/files/" + response);
                }
                $("#edit-profile-data-photo-edit").css('display', 'none');


                //Carreguem la imatge original
                $.post(SUBDOMAIN_PATH + '/Timeline/GetOriginalImage', function (data) {
                    $('#profile-crop-image-img').attr("src", "/Timeline/GetImageThumbnail/?image=" + data + "&width=350&height=350");
                    $('#profile-crop-image-img').attr("width", "350");
                });
            }
        });
    }

    $("#edit-profile-data-photo-change-image").live('click', function () {
        $("#edit-profile-data-photo-change-image-hidden").click();
    })

    $("#ajaxupload_button").live({
        mouseenter:
            function () {
                $("#edit-profile-data-photo-edit").css('display', 'block');
            },
        mouseleave:
            function () {
                //$("#edit-profile-data-photo-edit").css('display', 'none');
            }
    }
    );

    //Show crop image dialog
    $("#edit-profile-data-photo-edit-image").live('click', function () {

        //Desactivem l'ajaxupload per evitar problemes
        $("#ajaxupload_button").css('display', 'none');

        //Carreguem la imatge original
        api_crop_image_profile = $.Jcrop('#profile-crop-image-img', {
            setSelect: [0, 0, 220, 220],
            allowResize: false,
            minSize: [220, 220],
            maxSize: [220, 220],
            aspectRatio: 1,
            //boxWidth: 350,
            onChange: showCoordsProfilePhoto,
            onSelect: showCoordsProfilePhoto
        });
        /*});*/

        $('#profile-crop-image-modal').css('display', 'block');
    });


    $("#profile-crop-image-close").live('click', function () {

        $('#profile-crop-image-modal').css('display', 'none');
        $("#ajaxupload_button").css('display', 'block');

        if (api_crop_image_profile)
            api_crop_image_profile.destroy();
    });

    //Do image crop
    $("#profile-crop-image-button").live('click', function () {

        var url_crop_profile_image = '/Timeline/CropProfileImage?x=' + xField + '&y=' + yField;

        $.post(url_crop_profile_image, function (data) {
            if (data != '') {
                $('#edit-profile-img').attr("src", "/Content/files/" + data);
            }
            $("#edit-profile-data-photo-edit").css('display', 'none');
        });

        $('#profile-crop-image-modal').css('display', 'none');
        $("#ajaxupload_button").css('display', 'block');

        if (api_crop_image_profile)
            api_crop_image_profile.destroy();
    });

    $("#profile-delete-button-yes").live('click', function () {
        $.post(SUBDOMAIN_PATH + '/Timeline/DeleteProfile', function (data) {
            parent.location.href = '/Account/LogOff';
        });
    });

    $(".profile-follow-btn").live('hover', function () {
        $(this).toggleClass('profile-follow-btn-on');
    });

    $(".profile-following-btn").live({
        mouseenter:
            function () {
                $(this).addClass('profile-follow-btn-on');
                $(this).html('Translate(STR_NO_SEGUIR)');
            },
        mouseleave:
            function () {
                $(this).removeClass('profile-follow-btn-on');
                $(this).html('Translate(STR_SIGUIENDO_MIN)');
            }
    }
    );

    //Afegir kit des de la llista de kits de les categories del perfil
    $(".profile-kits-section-entry-empty").live('click', function () {

        //Mostem i fem reset de les dades
        kit_selected_tags = Array();
        $('html,body').animate({ scrollTop: 0 }, 'slow');

        $('#kit-product-search').val('Translate(STR_RECOMENDAR)...');
        $('#timeline-kit-modal').css('display', 'block');
        $("#timeline-kit-form").css('display', 'none');
        $("#timeline-kit-search").css('display', 'block');
        $('#timeline-kit-search-result-container').css('display', 'none');

        $('#timeline-kit-webservice-search').css('display', 'none');
        $('#timeline-kit-search').css('padding-bottom', '24px');
    });


    //Profile % tips
    /*$(".profile-stats").live('hover', function () {
    //$("#profile-stats-tip").css('display', 'block');
    $("#profile-stats-tip").toggle();
    });*/

    $(".profile-stats").live({
        mouseenter:
            function () {
                $("#profile-stats-tip").css('display', 'block');
            },
        mouseleave:
            function () {
                $("#profile-stats-tip").css('display', 'none');
            }
    }
    );


    /*
    1-	Para completar tu perfil puedes añadir tu correo electrónico.
    2-	Para completar tu perfil puedes añadir tu nombre. 
    3-	Para completar tu perfil puedes añadir tus apellidos.
    4-	Para completar tu perfil puedes añadir si eres hombre o mujer. Tus datos personales no se mostrarán en tu perfil público.
    5-	Para completar tu perfil puedes añadir tu fecha de nacimiento. Tus datos personales no se mostrarán en tu perfil público.
    6-	Para completar tu perfil puedes añadir tu localización.
    7-	Para completar tu perfil puedes añadir tu web/blog.
    8-	Para completar tu perfil puedes escribir algo sobre ti.
    9-	Felicidades! Tu perfil está completo.
    */

    //Product WS Search
    $("#timeline-now-with-seach-button").click(function () {
        $('#timeline-now-with-search-result-container').css('display', 'none');
        $('#timeline-now-with-webservice-search').css('display', 'block');
        $('#timeline-now-with-search').css('padding-bottom', '0');

        NowWithWSSearch();
    });

    $("#timeline-now-with-seach-button").hover(function () {
        $(this).toggleClass('timeline-now-with-seach-button-on');
    });

    //WS Search more results
    $("#timeline-now-with-search-result-more-results").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-result-more-results-on');
        $("#timeline-now-with-search-result-more-results-container").toggle();
    });

    $("#profile-data-edit-perm-1").live('click', function () {
        var profile_perm_1 = $("#profile_perm_1").val();
        if (profile_perm_1 == "1") {
            $("#profile_perm_1").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_1").val("1");
            $(this).addClass('selected');

            //desmarquem els altres relacionats amb l'enviament de correus
            $("#profile_perm_4").val("0");
            $("#profile-data-edit-perm-4").removeClass('selected');

            $("#profile_perm_5").val("0");
            $("#profile-data-edit-perm-5").removeClass('selected');

            $("#profile_perm_6").val("0");
            $("#profile-data-edit-perm-6").removeClass('selected');

            $("#profile_perm_7").val("0");
            $("#profile-data-edit-perm-7").removeClass('selected');

            $("#profile_perm_8").val("0");
            $("#profile-data-edit-perm-8").removeClass('selected');

            $("#profile_perm_9").val("0");
            $("#profile-data-edit-perm-9").removeClass('selected');
        }
    })

    $("#profile-data-edit-perm-2").live('click', function () {
        var profile_perm_2 = $("#profile_perm_2").val();
        if (profile_perm_2 == "1") {
            $("#profile_perm_2").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_2").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-3").live('click', function () {
        var profile_perm_3 = $("#profile_perm_3").val();
        if (profile_perm_3 == "1") {
            $("#profile_perm_3").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_3").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-4").live('click', function () {
        var profile_perm_4 = $("#profile_perm_4").val();
        if (profile_perm_4 == "1") {
            $("#profile_perm_4").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_4").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-5").live('click', function () {
        var profile_perm_5 = $("#profile_perm_5").val();
        if (profile_perm_5 == "1") {
            $("#profile_perm_5").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_5").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-6").live('click', function () {
        var profile_perm_6 = $("#profile_perm_6").val();
        if (profile_perm_6 == "1") {
            $("#profile_perm_6").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_6").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-7").live('click', function () {
        var profile_perm_7 = $("#profile_perm_7").val();
        if (profile_perm_7 == "1") {
            $("#profile_perm_7").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_7").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-8").live('click', function () {
        var profile_perm_8 = $("#profile_perm_8").val();
        if (profile_perm_8 == "1") {
            $("#profile_perm_8").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_8").val("1");
            $(this).addClass('selected');
        }
    })

    $("#profile-data-edit-perm-9").live('click', function () {
        var profile_perm_9 = $("#profile_perm_9").val();
        if (profile_perm_9 == "1") {
            $("#profile_perm_9").val("0");
            $(this).removeClass('selected');
        }
        else {
            $("#profile_perm_9").val("1");
            $(this).addClass('selected');
        }
    })

    $("#timeline-now-with-search-container-category-books").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-books-on');
    });

    $("#timeline-now-with-search-container-category-series").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-series-on');
    });

    $("#timeline-now-with-search-container-category-music").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-music-on');
    });

    $("#timeline-now-with-search-container-category-cinema").live('hover', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-cinema-on');
    });

    $("#timeline-now-with-search-container-category-books").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-books-selected');
        SetNowWithCategoryForm(1);
    });

    $("#timeline-now-with-search-container-category-series").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-series-selected');
        SetNowWithCategoryForm(2);
    });

    $("#timeline-now-with-search-container-category-music").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-music-selected');
        SetNowWithCategoryForm(4);
    });

    $("#timeline-now-with-search-container-category-cinema").live('click', function () {
        $(this).toggleClass('timeline-now-with-search-container-category-cinema-selected');
        SetNowWithCategoryForm(3);
    });

    //Profile, invite button
    $("#profile-data-invite").live('hover', function () {
        $(this).toggleClass('profile-data-invite-on');
    });

    //Pel profile públic
    $("#top-header-wide-login").on('click', function () {
        parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
    });

    //Profile wishlist
    $(".profile-data-wishlist").live('hover', function () {
        $(this).toggleClass('profile-data-wishlist-on');
    });
});

function setTimelineTop() {
    var height = $('#header').height();
    $('#profile-container').css("margin-top", height + "px");
}

function ProfileUnFollow(id, classname) {
    var url_unfollow = SUBDOMAIN_PATH + '/Timeline/UnFollow/?UserID=' + id;
    
    $.post(url_unfollow, function (data) {
        if ($.trim(data) == 'Success') {
            //location.reload();
            var div_id = '#profile-' + classname + '-btn-' + id;
            var id_name = 'profile-' + classname + '-btn-' + id;
            $(div_id).replaceWith('<div id="' + id_name + '" class="profile-follow-btn left14" onclick="ProfileFollow(' + id + ',\'' + classname + '\');">Translate(STR_SEGUIR)</div>');

            var profile_user = $('#profile-user-id').val();
            UpdateNumFollows(profile_user);
        }
    });
}

function ProfileFollow(id, classname) {
    var url_follow = SUBDOMAIN_PATH + '/Timeline/Follow/?UserID=' + id;

    $.post(url_follow, function (data) {
        if ($.trim(data) == 'Success') {
            //location.reload();
            var div_id = '#profile-' + classname + '-btn-' + id;
            var id_name = 'profile-' + classname + '-btn-' + id;
            $(div_id).replaceWith('<div id="' + id_name + '" class="profile-following-btn left14" onclick="ProfileUnFollow(' + id + ',\'' + classname + '\');">Translate(STR_SIGUIENDO_MIN)</div>');

            var profile_user = $('#profile-user-id').val();
            UpdateNumFollows(profile_user);
        }
    });
}

function MainProfileUnFollow(id) {
    var url_unfollow = SUBDOMAIN_PATH + '/Timeline/UnFollow/?UserID=' + id;

    $.post(url_unfollow, function (data) {

        if ($.trim(data) == 'Success') {
            //location.reload();

            $('#main-profile-follow-btn').replaceWith('<div id="main-profile-follow-btn" class="profile-follow-btn left20 top4" onclick="MainProfileFollow(' + id + ');">Translate(STR_SEGUIR)</div>');

            var profile_user = $('#profile-user-id').val();
            UpdateNumFollowersOther(profile_user);
            UpdateFollowersContainerOther(profile_user);
        }
    });
}

function MainProfileFollow(id) {
    var url_follow = SUBDOMAIN_PATH + '/Timeline/Follow/?UserID=' + id;

    $.post(url_follow, function (data) {

        if ($.trim(data) == 'Success') {
            //location.reload();
            $('#main-profile-follow-btn').replaceWith('<div id="main-profile-follow-btn" class="profile-following-btn left20 top4" onclick="MainProfileUnFollow(' + id + ');">Translate(STR_SIGUIENDO_MIN)</div>');

            var profile_user = $('#profile-user-id').val();
            UpdateNumFollowersOther(profile_user);
            UpdateFollowersContainerOther(profile_user);
        }
    });
}

function GoToWishlist(id) {
    parent.location.href = SUBDOMAIN_PATH + '/Timeline/Wishlist/' + id;
}

function AddNowWith() {
    $('#now-with-product-search').val('Translate(STR_AHORA_ESTOY_CON_MIN)');
    $('#timeline-now-with-modal').css('display', 'block');
    $("#timeline-now-with-form").css('display', 'none');
    $("#timeline-now-with-search").css('display', 'block');
    $('#timeline-now-with-search-result-container').css('display', 'none');
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}

function NowWithSearch() {

    var search = $('#now-with-product-search').val();

    if (search == 'Translate(STR_AHORA_ESTOY_CON_MIN)') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#now-with-product-categories').val();

    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-now-with-search-result-container').html(data);
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-now-with-search-result-container').css('display', 'block');
    });
}

function NowWithAddProduct(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithAddProduct/?id=' + id, function (data) {
        //location.reload();
        var userid = $('#profile-user-id').val();
        $('#timeline-now-with-modal').css('display', 'none');
        ReloadNowWith(userid);
    });
}

function DelNowWith(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/DelNowWith/?id=' + id, function (data) {
        //location.reload();
        var userid = $('#profile-user-id').val();
        ReloadNowWith(userid);
    });
}

function ReloadNowWith(id) {
    $.post(SUBDOMAIN_PATH + '/Timeline/ReloadNowWith/?id=' + id, function (data) {
        $('#profile-now-with').html(data + '<div class="profile-now-with-footer" onclick="parent.location.href=\'/Timeline/NowWith/' + id + '\';">Translate(STR_AHORA_ESTOY_CON)...</div>');
    });
}


function showCoordsProfilePhoto(c) {
    xField = c.x;
    yField = c.y;
    widthField = c.w;
    heightField = c.h;
}

function checkDate(dateValue) { 
    // regular expression to match required date format 
    re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

    if (dateValue != '' && !dateValue.match(re)) {
        alert("Formato de fecha no válido (dd/mm/yyyy) : " + dateValue); 
        return false;
    }

    return true;
}

function ProfileTwitterConnect() {
    parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterConnect';
}

function ProfileTwitterDisconnect() {

    $.post(SUBDOMAIN_PATH + '/Account/TwitterDisconnect', function (data) {
        
        if ($.trim(data) == 'Success') {
            $('#profile-data-edit-twitter').replaceWith('<div id="profile-data-edit-twitter" onclick="ProfileTwitterConnect();" class="profile-data-edit-social-button"><div id="profile-twitter-connect" class="profile-data-edit-social-button-twitter">Translate(STR_CONECTAR_CON_TWITTER)</div><div class="profile-data-edit-social-button-border"></div></div>');
        }
    });
}

function ProfileFacebookConnect() {
    parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookConnect';
}

function ProfileFacebookDisconnect() {

    $.post(SUBDOMAIN_PATH + '/Account/FacebookDisconnect', function (data) {
        
        if ($.trim(data) == 'Success') {
            $('#profile-data-edit-facebook').replaceWith('<div id="profile-data-edit-facebook" onclick="ProfileFacebookConnect();" class="profile-data-edit-social-button"><div id="profile-facebook-connect" class="profile-data-edit-social-button-facebook">Translate(STR_CONECTAR_CON_FACEBOOK)</div><div class="profile-data-edit-social-button-border"></div></div>');
        }
    });
}

function UpdateProfileStatus() {
    $.post(SUBDOMAIN_PATH + '/Timeline/getProfileStatus', function (data) {
        $('#edit-profile-stats').html(data);
    });
}


/**
    
WS VERSION

**/

function NowWithWSSearch() {

    var search = $('#now-with-product-search').val();

    if (search == 'Translate(STR_RECOMENDAR)...') {
        search = '';
    }

    //Mirem les categories
    var categories = $('#now-with-product-categories').val();

    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithWSSearchProduct/?search=' + search + '&categories=' + categories, function (data) {
        $('#timeline-now-with-search-result-container').html(data);
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-now-with-search-result-container').css('display', 'block');
        $('#timeline-now-with-webservice-search').css('display', 'none');
        $('#timeline-now-with-search').css('padding-bottom', '24px');
    });
}

function NowWithAddWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations) {

    ASIN = encodeURIComponent(ASIN);
    Title = encodeURIComponent(Title);
    Author = encodeURIComponent(Author);
    Year = encodeURIComponent(Year);
    Category = encodeURIComponent(Category);
    LargeImage = encodeURIComponent(LargeImage);
    URL = encodeURIComponent(URL);
    Description = encodeURIComponent(Description);
    EAN = encodeURIComponent(EAN);
    Observations = encodeURIComponent(Observations);

    $.post(SUBDOMAIN_PATH + '/Timeline/NowWithWSAddProductReload/?ASIN=' + ASIN + "&Title=" + Title + "&Author=" + Author + "&Year=" + Year + "&Category=" + Category + "&LargeImage=" + LargeImage + "&URL=" + URL + "&Description=" + Description + "&EAN=" + EAN + "&Observations=" + Observations, function (data) {
        var userid = $('#profile-user-id').val();
        $('#timeline-now-with-modal').css('display', 'none');
        ReloadNowWith(userid);
    });
}

/**

FI WS VERSION

**/

function UpdateNumFollows(id) {
    //Actualitzem el número de likes
    $.post(SUBDOMAIN_PATH + '/Timeline/GetNumFollows/?id=' + id, function (data) {
        if (data != "")
            $('#profile-num-followings').text(data);
    });
}

function UpdateNumFollowersOther(id) {
    //Actualitzem el número de likes
    $.post(SUBDOMAIN_PATH + '/Timeline/GetNumFollowersOther/?id=' + id, function (data) {
        if (data != "")
            $('#profile-num-followers').text(data);
    });
}

function UpdateFollowersContainerOther(id) {
    //Actualitzem el contenidor dels seguidors
    $.post(SUBDOMAIN_PATH + '/Timeline/GetFollowersContainerOther/?id=' + id, function (data) {
        if (data != "")
            $('#profile-followers-container').html(data);
    });
}

function SetNowWithCategoryForm(id_category) {
    var values = $('#now-with-product-categories').val();

    //Mirem si ja existeix a les categories, si hi és l'esborrem, sinó, l'afegim
    if (values.indexOf(id_category) >= 0) {
        values = values.replace("," + id_category, "");
        values = values.replace(id_category, "");
    }
    else {
        if (values.length > 0) {
            values = values + "," + id_category;
        }
        else
            values = id_category;
    }

    $('#now-with-product-categories').val(values);
}

/*
*   EMERGENT "REGISTRAT":>>>
*/
//Close "emergent registrat"
$("#emergent-registrat-close").live('click', function () {
    $('#emergent-registrat').css('display', 'none');
})

$("#enllac_registrat-emergent-registrat").live('click', function () {
    parent.location.href = SUBDOMAIN_PATH + '/Account/LogOn';
});
/*$("#boto_entrar_facebook-emergent-registrat").live('click', function () {
    //parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterRegister';

    if (_gaq) {
        _gaq.push(['_trackPageview', '/Likekit/Entrar']);
    }

    parent.location.href = SUBDOMAIN_PATH + '/Account/FacebookLogOn';

});*/
$("#boto_uneixte-emergent-registrat").live('click', function () {
    //parent.location.href = SUBDOMAIN_PATH + '/Account/TwitterRegister';

    if (_gaq) {
        _gaq.push(['_trackPageview', '/Likekit/Entrar']);
    }

    parent.location.href = SUBDOMAIN_PATH + '/Account/Register';

});
/*
*   EMERGENT "REGISTRAT" pantalla de perfil:>>>
*/
function mostrarEmergent() {
    $('#emergent-registrat').css('display', 'block');
}

$("kit-view-container-comment-button").live('focus', function () {
    $('#emergent-registrat').css('display', 'block');
})