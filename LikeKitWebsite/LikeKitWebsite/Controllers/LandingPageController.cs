﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LikeKitWebsite.Models;
using LikeKitWebsite.Repository;

namespace LikeKitWebsite.Controllers
{
    public class LandingPageController : EController<BetaUsersRepository>
    {
        //
        // GET: /LandingPage/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaveMail(String mail)
        {
            //Save mail
            BetaUser _betaUser = new BetaUser();
            _betaUser.Mail = mail;
            
            //BetaUsersRepository betaUserRp = new BetaUsersRepository();
            //BetaUserResult result = betaUserRp.CreateBetaUser(_betaUser);
            BetaUserResult result = Repository.CreateBetaUser(_betaUser);

            String Message = String.Empty;
            Message = result.Message;

            return PartialView("LandingPageMessage", Message);
        }
    }
}
