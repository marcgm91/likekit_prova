﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using LikeKitWebsite.Models;
using System.Net;
using Facebook;
using System.Text;
using Twitterizer;
using LikeKitWebsite.Repository;
using System.Configuration;
using Endepro.Common.Google;
using Endepro.Common.Yahoo;
using Endepro.Common.Social;
using System.Globalization;


namespace LikeKitWebsite.Controllers
{
    public class AccountController : EController<UsersRepository>
    {
        private const string FacebookAppId = "400049130015461";
        private const string FacebookAppsecret = "9a0f666f5dff906ea573af136a5e75f6";
        private const string FacebookScope = "email,user_about_me,publish_stream";

        //HA DE SER AQUÍ ON S'HA DE AFEGIR L'USUARI A LA BASE DE DADES A PARTIR DEL LES DADES A LA URL POSSADES A LA URL. (MarcGM)
        private string FacebookRedirectUri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookCallback";
        private const string TwitterConsumerKey = "q8P43ZQD4tu4Qi1DQznA";
        private const string TwitterConsumerSecret = "Z5qSrOu0NqUlDki6mvdiBkKf8KMomduaxfnu6Z4MvU";


        //https://github.com/facebook-csharp-sdk/facebook-aspnet-sample

        private readonly FacebookClient _fb;

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            String culture = "es";

            if (HttpContext.Session["culture"] != null)
                culture = HttpContext.Session["culture"].ToString();
            else
            {
                //Primer mirem el lang del navegador
                if (Request != null)
                {
                    var l = Request.UserLanguages;
                    string browserlanguage = l[0];

                    HttpContext.Session["culture"] = browserlanguage;
                }
                else
                {
                    String address = Request.ServerVariables["REMOTE_ADDR"];
                    TimeLineRepository TlRP = new TimeLineRepository(Repository.Database);

                    culture = TlRP.GetIPCulture(address);
                    HttpContext.Session["culture"] = culture;
                }
            }

            CultureInfo ci = CultureInfo.GetCultureInfo(culture);

            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
        }


        public AccountController()
            : this(new FacebookClient())
        {

        }

        public AccountController(FacebookClient fb)
        {
            _fb = fb;
        }

        //////////////////////////////////////////////////////////////////////////////
        //YAHOO
        //////////////////////////////////////////////////////////////////////////////
        //
        // GET: /Account/YahooAuth
        [Authorize]
        public ActionResult YahooAuth(string returnUrl)
        {
            YahooAuthSession._consumerKey = "dj0yJmk9ZkcxdFNjMHVpSkZnJmQ9WVdrOWVIYzNlR2g2TXpJbWNHbzlORFl6TkRFM01qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTFj";
            YahooAuthSession._consumerSecretKey = "db2bbb1134dada83fd854ea934b3752eb2ded09a";
            YahooAuthSession._urlCallback = ConfigurationManager.AppSettings["MainURL"] + "Account/YahooOauth2Callback";
            
            return Redirect(YahooAuthSession.GetRequestToken());
        }
        [Authorize]
        public ActionResult YahooOauth2Callback(string oauth_token, string oauth_verifier)
        {
            YahooAuthSession.SetAccessToken(oauth_token, oauth_verifier);

             List<InviteContact> contacts = YahooAuthSession.RetriveContacts();

             Int64 UserID = 0;
             if (Session[Globals.SessionUserID] != null)
                 UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

             //Hem de parsejar els resultats
             String sHtml = Repository.InviteProcessContacts(contacts, UserID, InviteType.Yahoo);

             Invite invite = new Invite();
             invite.Show = true;
             invite.Type = InviteType.Yahoo;
             invite.HtmlOutput = sHtml;

             return View("../Timeline/Invite", (object)invite);
        }


        //////////////////////////////////////////////////////////////////////////////
        //GOOGLE
        //////////////////////////////////////////////////////////////////////////////
        //
        // GET: /Account/GoogleAuth
        [Authorize]
        public ActionResult GoogleAuth(string returnUrl)
        {
            String url = ConfigurationManager.AppSettings["MainURL"] + "Account/GoogleOauth2Callback";

            GoogleAuth authGoogle = new GoogleAuth("LikeKit", "666525549036.apps.googleusercontent.com", "z25RauU89z0F8y0DvLw-w3eF", url);

            return Redirect(authGoogle.CreateAuthURL());
        }

        //
        // GET: /Account/GoogleOauth2Callback
        [Authorize]
        public ActionResult GoogleOauth2Callback(string code)
        {

            String url = ConfigurationManager.AppSettings["MainURL"] + "Account/GoogleOauth2Callback";
            Invite invite = new Invite();

            try
            {
                GoogleAuth authGoogle = new GoogleAuth("LikeKit", "666525549036.apps.googleusercontent.com", "z25RauU89z0F8y0DvLw-w3eF", url);

                authGoogle.SetAccessCode(code);

                List<InviteContact> contacts = authGoogle.GetContacts();

                Int64 UserID = 0;
                if (Session[Globals.SessionUserID] != null)
                    UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

                //Hem de parsejar els resultats
                String sHtml = Repository.InviteProcessContacts(contacts, UserID, InviteType.Gmail);

            
                invite.Show = true;
                invite.Type = InviteType.Gmail;
                invite.HtmlOutput = sHtml;
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "doFacebookInvite : " + invite);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Timeline, 1);
            }

            return View("../Timeline/Invite", (object)invite);
        }

      
        //////////////////////////////////////////////////////////////////////////////
        //FACEBOOK
        //////////////////////////////////////////////////////////////////////////////

        //
        // GET: /Account/FacebookLogOn

        public ActionResult FacebookLogOn(string returnUrl)
        {
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 1"), Models.Type.Register, 1);
            String csrfToken = Guid.NewGuid().ToString(); //A la variable "crsfToken es guarda un número aleatori ÚNIC (guid) convertit a string.
            Session["fb_csrf_token"] = csrfToken;
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 2"), Models.Type.Register, 1);
            String state = Convert.ToBase64String(Encoding.UTF8.GetBytes(_fb.SerializeJson(new { returnUrl = returnUrl, csrf = csrfToken })));

            Uri fbLoginUrl = _fb.GetLoginUrl(
                new
                {
                    client_id = FacebookAppId,
                    client_secret = FacebookAppsecret,
                    redirect_uri = FacebookRedirectUri,
                    response_type = "code",
                    scope = FacebookScope,
                    state = state
                });
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 3"), Models.Type.Register, 1);
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 4 >>> {0}", fbLoginUrl.AbsoluteUri), Models.Type.Register, 1);
            return Redirect(fbLoginUrl.AbsoluteUri);
        }

        public ActionResult FacebookLogOn2(string returnUrl)
        {
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 1(2)"), Models.Type.Register, 1);
            String csrfToken = Guid.NewGuid().ToString(); //A la variable "crsfToken" es guarda un número aleatori ÚNIC (guid) convertit a string.
            Session["fb_csrf_token"] = csrfToken;
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 2(2)"), Models.Type.Register, 1);
            String state = Convert.ToBase64String(Encoding.UTF8.GetBytes(_fb.SerializeJson(new { returnUrl = returnUrl, csrf = csrfToken })));

            Uri fbLoginUrl = _fb.GetLoginUrl(
                new
                {
                    client_id = FacebookAppId,
                    client_secret = FacebookAppsecret,
                    //A "redirect_uri" ha d'anar la url del "ActionResult" que cridarà després a la pàgina de vista on hi haurà el emergent de viralització:>>>
                    redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/Viralizacion",
                    response_type = "code",
                    scope = FacebookScope,
                    state = state
                });
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 3(2)"), Models.Type.Register, 1);
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 4(2) >>> {0}", fbLoginUrl.AbsoluteUri), Models.Type.Register, 1);
            Globals.SaveLog(Repository.Database, String.Format("FacebookLogOn : 5(2) >>> {0}", FacebookRedirectUri), Models.Type.Register, 1);

            return Redirect(fbLoginUrl.AbsoluteUri);
        }

        public ActionResult Viralizacion()
        {
            return View();
        }

        public ActionResult FacebookConnect(string returnUrl)
        {
            String csrfToken = Guid.NewGuid().ToString();
            Session["fb_csrf_token"] = csrfToken;

            String state = Convert.ToBase64String(Encoding.UTF8.GetBytes(_fb.SerializeJson(new { returnUrl = returnUrl, csrf = csrfToken })));

            Uri fbLoginUrl = _fb.GetLoginUrl(
                new
                {
                    client_id = FacebookAppId,
                    client_secret = FacebookAppsecret,
                    redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookConnectCallback",
                    response_type = "code",
                    scope = FacebookScope,
                    state = state
                });

            return Redirect(fbLoginUrl.AbsoluteUri);
        }

        public ActionResult FacebookInvite(string returnUrl)
        {
            String csrfToken = Guid.NewGuid().ToString();
            Session["fb_csrf_token"] = csrfToken;

            String state = Convert.ToBase64String(Encoding.UTF8.GetBytes(_fb.SerializeJson(new { returnUrl = returnUrl, csrf = csrfToken })));

            Uri fbLoginUrl = _fb.GetLoginUrl(
                new
                {
                    client_id = FacebookAppId,
                    client_secret = FacebookAppsecret,
                    redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookInviteCallback",
                    response_type = "code",
                    scope = FacebookScope,
                    state = state
                });

            return Redirect(fbLoginUrl.AbsoluteUri);
        }

        //
        // GET: /Account/FacebookCallback

        public ActionResult FacebookCallback(string code, string state)
        {
            Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 1 >>> code : {0} state : {1}", code, state), Models.Type.Register, 1);
            Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 2 >>> {0}", code), Models.Type.Register, 1);
            if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(state))
            {
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 2_a >>> {0}", code), Models.Type.Register, 1);
                //return RedirectToAction("LogOn", "Account");
                return RedirectToAction("Index", "Home");
            }
            Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 2_b >>> {0}", code), Models.Type.Register, 1);
            // first validate the csrf token
            dynamic decodedState;
            try
            {
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 3"), Models.Type.Register, 1);
                decodedState = _fb.DeserializeJson(Encoding.UTF8.GetString(Convert.FromBase64String(state)), null);
                Globals.SaveLog(Repository.Database, String.Format("decodedState >>> : {0}", decodedState), Models.Type.Register, 1);
                var exepectedCsrfToken = Session["fb_csrf_token"] as string;
                // make the fb_csrf_token invalid
                Session["fb_csrf_token"] = null;

                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 4 token : {0}", exepectedCsrfToken), Models.Type.Register, 1);

                /*if (!(decodedState is IDictionary<string, object>) || !decodedState.ContainsKey("csrf") || string.IsNullOrWhiteSpace(exepectedCsrfToken) || exepectedCsrfToken != decodedState.csrf)
                {
                    //return RedirectToAction("LogOn", "Account");
                    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 5"), Models.Type.Register, 1);
                    return RedirectToAction("Index", "Home");
                }*/
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 5.2 token."), Models.Type.Register, 1);

            }
            catch
            {
                //log exception
                //return RedirectToAction("LogOn", "Account");
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 6"), Models.Type.Register, 1);
                return RedirectToAction("Index", "Home");
            }

            try
            {
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7"), Models.Type.Register, 1);
                dynamic result = _fb.Post("oauth/access_token",
                                          new
                                          {
                                              client_id = FacebookAppId,
                                              client_secret = FacebookAppsecret,
                                              redirect_uri = FacebookRedirectUri,
                                              code = code
                                          });
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.0.2"), Models.Type.Register, 1);

                //XIVATOSS! (MarcGM__11-7-2013): //NO LLEGA A ESTOS XIVATOS (EL ÚLTIMO ES EL "FacebookCallback : 2"). MIRAR DONDE ESTÁ EL ERROR!
                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.1 result : {0}", result.access_token), Models.Type.Register, 1);
                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.2 result : {0}", result.client_id), Models.Type.Register, 1);
                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.3 result : {0}", result.client_secret), Models.Type.Register, 1);
                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.4 result : {0}", result.redirect_uri), Models.Type.Register, 1);
                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.5 result : {0}", result.code), Models.Type.Register, 1);

                Session["fb_access_token"] = result.access_token;

                if (result.ContainsKey("expires"))
                    Session["fb_expires_in"] = DateTime.Now.AddSeconds(result.expires);

                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 8 "), Models.Type.Register, 1);

                //TODO
                //me
                _fb.AccessToken = Session["fb_access_token"].ToString();
                dynamic me = _fb.Get("me");
                string id = me.id;
                string name = me.name;
                string firstname = me.first_name;
                string lastname = me.last_name;
                string gender = me.gender;
                string email = me.email;
                //string locale = me.locale;

                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 9 id : {0} name : {1} mail {2}", id, name, email), Models.Type.Register, 1);
                
                //http://stackoverflow.com/questions/4859219/getting-user-profile-picture-using-facebook-c-sharp-sdk-from-codeplex
                string picSquare = "https://graph.facebook.com/" + me.id + "/picture?type=large";

                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 10 picsquare : {0}", picSquare), Models.Type.Register, 1);
                //Ara mirem si aquest usuari existeix
                JsonLogin _login = new JsonLogin();
                _login = Repository.LoginUserFacebook(id, email, _fb.AccessToken, picSquare);

                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 11 login : {0}", _login.success.ToString()), Models.Type.Register, 1);

                if (_login.success)
                {
                    //Afegim l'entrada a l'AccessLog i actualitzem el lastLogin de l'usuari
                    UsersRepository.UpdateLastLogin(Repository.Database,_login.ID);

                    Session[Globals.SessionUserID] = _login.ID;
                    Session[Globals.SessionUserName] = _login.UserName;
                    Session[Globals.SessionUserPassword] = "";
                    Session[Globals.SessionUserUserID] = _login.UserID;
                    Session[Globals.SessionUserName] = _login.UserName;

                    if (_login.FirstName != "")
                        Session[Globals.SessionFirstName] = _login.FirstName;
                    else
                        Session[Globals.SessionFirstName] = firstname;
                    Session[Globals.SessionLastName] = _login.LastName;
                    Session[Globals.SessionPicture] = _login.Picture;
                    Session[Globals.SessionAbout] = _login.About;
                    Session[Globals.SessionWeb] = _login.Web;
                    Session[Globals.SessionNumInteractions] = _login.NumInteractions;

                    String culture = "es";
                    
                    if (_login.LanguageID == 2)
                            culture = "ca";
                    else
                            culture = "es";

                     HttpContext.Session["culture"] = culture;

                    CultureInfo ci = CultureInfo.GetCultureInfo(culture);

                    System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = ci;

                    //Auth
                    FormsAuthentication.SetAuthCookie(firstname, false);
                    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 12 "), Models.Type.Register, 1);
                    return RedirectToAction("Index", "Timeline");
                }
                else
                {
                    RegisterModel rm = new RegisterModel();


                    List<InviteContact> contacts = Social.FacebookGetFriends(_fb.AccessToken);

                    //String sHtml = Repository.InviteProcessContacts(contacts, UserID, InviteType.Facebook);
                    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 13 login : {0}", _fb.AccessToken), Models.Type.Register, 1);
                    ProductsRepository pr = new ProductsRepository(Repository.Database);
                    rm.UserName = String.Format("{0}", email);
                    rm.FirstName = firstname;
                    rm.LastName = lastname;
                    rm.FacebookRegister = 1;
                    rm.FacebookUserId = id;
                    rm.FacebookAccessToken = _fb.AccessToken;
                    rm.RegisterCategoryProducts = pr.GetRegisterCategoryProducts();
                    rm.Gender = gender;
                    rm.Picsquare = picSquare;

                    JsonLogin _login_register = new JsonLogin();

                    Language _lang = new Language();
                    _lang.ID = 1;

                    if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                        _lang.ID = 2;
                    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 14"), Models.Type.Register, 1);
                    _login_register = Repository.RegisterFacebook(rm.FacebookUserId, rm.UserName, rm.FirstName, rm.LastName, rm.Gender, rm.Picsquare, rm.FacebookAccessToken, rm.UserName, _lang.ID);
                    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 15"), Models.Type.Register, 1);
                    if (_login_register.ID > 0)
                    {
                        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 16 ID :{0}", _login_register.ID), Models.Type.Register, 1);
                        //Fem el login
                        if (_login_register.success)
                        {
                            Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 17 success :{0}", _login_register.success.ToString()), Models.Type.Register, 1);
                            Session[Globals.SessionUserID] = _login_register.ID;
                            Session[Globals.SessionUserName] = _login_register.UserName;
                            Session[Globals.SessionUserPassword] = _login_register.Password;
                            Session[Globals.SessionUserUserID] = _login_register.UserID;
                            Session[Globals.SessionUserName] = _login_register.UserName;
                            Session[Globals.SessionFirstName] = _login_register.FirstName;
                            Session[Globals.SessionLastName] = _login_register.LastName;
                            Session[Globals.SessionPicture] = _login_register.Picture;
                            Session[Globals.SessionAbout] = _login_register.About;
                            Session[Globals.SessionWeb] = _login_register.Web;
                            Session[Globals.SessionNumInteractions] = _login_register.NumInteractions;

                            Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 18"), Models.Type.Register, 1);
                            //Mostrem els usuaris que seguim

                            rm.RegisterFollowings = Repository.GetRegisterFollowingsFacebook(contacts, _login_register.ID, _lang.ID);

                            String register_followings = Repository.GetRegisterFollowingsFacebookCommaSeparated(contacts, _login_register.ID, _lang.ID);

                            if ((register_followings != String.Empty) && (register_followings != "") && (register_followings != "0"))
                            {
                                //Ara recorrem els usuaris
                                string[] tokensizedUsers;
                                tokensizedUsers = register_followings.Split(new char[] { ',' });

                                int lenUsers = tokensizedUsers.Length;

                                for (int i = 0; i < lenUsers; i++)
                                {
                                    string tmp = tokensizedUsers[i];

                                    if (tmp != String.Empty)
                                    {
                                        Int64 ID = Int64.Parse(tmp);

                                        UserRelation ur = new UserRelation();
                                        ur.User = new User();
                                        ur.User.ID = _login_register.ID;
                                        ur.Follow = new User();
                                        ur.Follow.ID = ID;

                                        Globals.SaveLog(Repository.Database, String.Format("DoInviteRegister UserRelation : {0}", ID), Models.Type.Register, 1);

                                        UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
                                        Int64 follow_id = _urRP.CreateUserRelation(ur);

                                        /*TimeLineRepository _tRP = new TimeLineRepository(Repository.Database);

                                        //Creem la notificació
                                        _tRP.CreateNotify(ID, UserID, follow_id, NotifyType.Follow);

                                        //Mirem si li enviem el correu.
                                        User user_tmp = Repository.GetUser(ID);

                                        user_tmp.Perm7 = Globals.GetPerms(user_tmp.Perms, (int)UserPerms.Follow);

                                        if (user_tmp.Perm7 == true)
                                        {
                                            _tRP.FollowMail(user_tmp.ID, UserID);
                                        }*/
                                    }
                                }
                            }
                        }

                        FormsAuthentication.SetAuthCookie(_login_register.UserName, false);
                    }

                    return View("SocialRegister", rm);        
                }
                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 19"), Models.Type.Register, 1);
                //Auth user
                /*FormsAuthentication.SetAuthCookie(name, false);

                if (decodedState.ContainsKey("returnUrl"))
                {
                    if (Url.IsLocalUrl(decodedState.returnUrl))
                        return Redirect(decodedState.returnUrl);
                }*/

                //return RedirectToAction("LogOn", "Account");
                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                // log exception
                Globals.SaveLog(Repository.Database, String.Format("FacebookCallback__EXECEPTION:>>> {0}", e), Models.Type.Register, 1);
                //return RedirectToAction("LogOn", "Account");
                return RedirectToAction("Index", "Home");
            }
        }


        //FUNCIÓN ORIGINAL (SIN MODIFICACIONES(16-7-2013)): >>>

        //// GET: /Account/FacebookCallback

        //public ActionResult FacebookCallback(string code, string state)
        //{
        //    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 1 >>> code : {0} state : {1}", code, state), Models.Type.Register, 1);
        //    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 2 >>> {0}", code), Models.Type.Register, 1);
        //    if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(state))
        //    {
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 2_a >>> {0}", code), Models.Type.Register, 1);
        //        //return RedirectToAction("LogOn", "Account");
        //        return RedirectToAction("Index", "Home");
        //    }
        //    Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 2_b >>> {0}", code), Models.Type.Register, 1);
        //    // first validate the csrf token
        //    dynamic decodedState;
        //    try
        //    {
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 3"), Models.Type.Register, 1);
        //        decodedState = _fb.DeserializeJson(Encoding.UTF8.GetString(Convert.FromBase64String(state)), null);
        //        Globals.SaveLog(Repository.Database, String.Format("decodedState >>> : {0}", decodedState), Models.Type.Register, 1);
        //        var exepectedCsrfToken = Session["fb_csrf_token"] as string;
        //        // make the fb_csrf_token invalid
        //        Session["fb_csrf_token"] = null;

        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 4 token : {0}", exepectedCsrfToken), Models.Type.Register, 1);

        //        if (!(decodedState is IDictionary<string, object>) || !decodedState.ContainsKey("csrf") || string.IsNullOrWhiteSpace(exepectedCsrfToken) || exepectedCsrfToken != decodedState.csrf)
        //        {
        //            //return RedirectToAction("LogOn", "Account");
        //            Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 5"), Models.Type.Register, 1);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 5.2 token."), Models.Type.Register, 1);

        //    }
        //    catch
        //    {
        //        //log exception
        //        //return RedirectToAction("LogOn", "Account");
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 6"), Models.Type.Register, 1);
        //        return RedirectToAction("Index", "Home");
        //    }

        //    try
        //    {
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7"), Models.Type.Register, 1);
        //        dynamic result = _fb.Post("oauth/access_token",
        //                                  new
        //                                  {
        //                                      client_id = FacebookAppId,
        //                                      client_secret = FacebookAppsecret,
        //                                      redirect_uri = FacebookRedirectUri,
        //                                      code = code
        //                                  });

        //        //XIVATOSS! (MarcGM__11-7-2013): //NO LLEGA A ESTOS XIVATOS (EL ÚLTIMO ES EL "FacebookCallback : 2"). MIRAR DONDE ESTÁ EL ERROR!
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.1 result : {0}", result.access_token), Models.Type.Register, 1);
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.2 result : {0}", result.client_id), Models.Type.Register, 1);
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.3 result : {0}", result.client_secret), Models.Type.Register, 1);
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.4 result : {0}", result.redirect_uri), Models.Type.Register, 1);
        //        Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 7.5 result : {0}", result.code), Models.Type.Register, 1);

        //        Session["fb_access_token"] = result.access_token;

        //        if (result.ContainsKey("expires"))
        //            Session["fb_expires_in"] = DateTime.Now.AddSeconds(result.expires);

        //        //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 8 "), Models.Type.Register, 1);

        //        //TODO
        //        //me
        //        _fb.AccessToken = Session["fb_access_token"].ToString();
        //        dynamic me = _fb.Get("me");
        //        string id = me.id;
        //        string name = me.name;
        //        string firstname = me.first_name;
        //        string lastname = me.last_name;
        //        string gender = me.gender;
        //        string email = me.email;
        //        //string locale = me.locale;

        //        //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 9 id : {0} name : {1} mail {2}", id, name, email), Models.Type.Register, 1);

        //        //http://stackoverflow.com/questions/4859219/getting-user-profile-picture-using-facebook-c-sharp-sdk-from-codeplex
        //        string picSquare = "https://graph.facebook.com/" + me.id + "/picture?type=large";

        //        //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 10 picsquare : {0}", picSquare), Models.Type.Register, 1);
        //        //Ara mirem si aquest usuari existeix
        //        JsonLogin _login = new JsonLogin();
        //        _login = Repository.LoginUserFacebook(id, email, _fb.AccessToken, picSquare);

        //        //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 11 login : {0}", _login.success.ToString()), Models.Type.Register, 1);

        //        if (_login.success)
        //        {
        //            //Afegim l'entrada a l'AccessLog i actualitzem el lastLogin de l'usuari
        //            UsersRepository.UpdateLastLogin(Repository.Database, _login.ID);

        //            Session[Globals.SessionUserID] = _login.ID;
        //            Session[Globals.SessionUserName] = _login.UserName;
        //            Session[Globals.SessionUserPassword] = "";
        //            Session[Globals.SessionUserUserID] = _login.UserID;
        //            Session[Globals.SessionUserName] = _login.UserName;

        //            if (_login.FirstName != "")
        //                Session[Globals.SessionFirstName] = _login.FirstName;
        //            else
        //                Session[Globals.SessionFirstName] = firstname;
        //            Session[Globals.SessionLastName] = _login.LastName;
        //            Session[Globals.SessionPicture] = _login.Picture;
        //            Session[Globals.SessionAbout] = _login.About;
        //            Session[Globals.SessionWeb] = _login.Web;
        //            Session[Globals.SessionNumInteractions] = _login.NumInteractions;

        //            String culture = "es";

        //            if (_login.LanguageID == 2)
        //                culture = "ca";
        //            else
        //                culture = "es";

        //            HttpContext.Session["culture"] = culture;

        //            CultureInfo ci = CultureInfo.GetCultureInfo(culture);

        //            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
        //            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;

        //            //Auth
        //            FormsAuthentication.SetAuthCookie(firstname, false);
        //            //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 12 "), Models.Type.Register, 1);
        //            return RedirectToAction("Index", "Timeline");
        //        }
        //        else
        //        {
        //            RegisterModel rm = new RegisterModel();


        //            List<InviteContact> contacts = Social.FacebookGetFriends(_fb.AccessToken);

        //            //String sHtml = Repository.InviteProcessContacts(contacts, UserID, InviteType.Facebook);
        //            //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 13 login : {0}", _fb.AccessToken), Models.Type.Register, 1);
        //            ProductsRepository pr = new ProductsRepository(Repository.Database);
        //            rm.UserName = String.Format("{0}", email);
        //            rm.FirstName = firstname;
        //            rm.LastName = lastname;
        //            rm.FacebookRegister = 1;
        //            rm.FacebookUserId = id;
        //            rm.FacebookAccessToken = _fb.AccessToken;
        //            rm.RegisterCategoryProducts = pr.GetRegisterCategoryProducts();
        //            rm.Gender = gender;
        //            rm.Picsquare = picSquare;

        //            JsonLogin _login_register = new JsonLogin();

        //            Language _lang = new Language();
        //            _lang.ID = 1;

        //            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
        //                _lang.ID = 2;
        //            //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 14"), Models.Type.Register, 1);
        //            _login_register = Repository.RegisterFacebook(rm.FacebookUserId, rm.UserName, rm.FirstName, rm.LastName, rm.Gender, rm.Picsquare, rm.FacebookAccessToken, rm.UserName, _lang.ID);
        //            //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 15"), Models.Type.Register, 1);
        //            if (_login_register.ID > 0)
        //            {
        //                //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 16 ID :{0}", _login_register.ID), Models.Type.Register, 1);
        //                //Fem el login
        //                if (_login_register.success)
        //                {
        //                    //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 17 success :{0}", _login_register.success.ToString()), Models.Type.Register, 1);
        //                    Session[Globals.SessionUserID] = _login_register.ID;
        //                    Session[Globals.SessionUserName] = _login_register.UserName;
        //                    Session[Globals.SessionUserPassword] = _login_register.Password;
        //                    Session[Globals.SessionUserUserID] = _login_register.UserID;
        //                    Session[Globals.SessionUserName] = _login_register.UserName;
        //                    Session[Globals.SessionFirstName] = _login_register.FirstName;
        //                    Session[Globals.SessionLastName] = _login_register.LastName;
        //                    Session[Globals.SessionPicture] = _login_register.Picture;
        //                    Session[Globals.SessionAbout] = _login_register.About;
        //                    Session[Globals.SessionWeb] = _login_register.Web;
        //                    Session[Globals.SessionNumInteractions] = _login_register.NumInteractions;

        //                    //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 18"), Models.Type.Register, 1);
        //                    //Mostrem els usuaris que seguim

        //                    rm.RegisterFollowings = Repository.GetRegisterFollowingsFacebook(contacts, _login_register.ID, _lang.ID);

        //                    String register_followings = Repository.GetRegisterFollowingsFacebookCommaSeparated(contacts, _login_register.ID, _lang.ID);

        //                    if ((register_followings != String.Empty) && (register_followings != "") && (register_followings != "0"))
        //                    {
        //                        //Ara recorrem els usuaris
        //                        string[] tokensizedUsers;
        //                        tokensizedUsers = register_followings.Split(new char[] { ',' });

        //                        int lenUsers = tokensizedUsers.Length;

        //                        for (int i = 0; i < lenUsers; i++)
        //                        {
        //                            string tmp = tokensizedUsers[i];

        //                            if (tmp != String.Empty)
        //                            {
        //                                Int64 ID = Int64.Parse(tmp);

        //                                UserRelation ur = new UserRelation();
        //                                ur.User = new User();
        //                                ur.User.ID = _login_register.ID;
        //                                ur.Follow = new User();
        //                                ur.Follow.ID = ID;

        //                                Globals.SaveLog(Repository.Database, String.Format("DoInviteRegister UserRelation : {0}", ID), Models.Type.Register, 1);

        //                                UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
        //                                Int64 follow_id = _urRP.CreateUserRelation(ur);

        //                                /*TimeLineRepository _tRP = new TimeLineRepository(Repository.Database);

        //                                //Creem la notificació
        //                                _tRP.CreateNotify(ID, UserID, follow_id, NotifyType.Follow);

        //                                //Mirem si li enviem el correu.
        //                                User user_tmp = Repository.GetUser(ID);

        //                                user_tmp.Perm7 = Globals.GetPerms(user_tmp.Perms, (int)UserPerms.Follow);

        //                                if (user_tmp.Perm7 == true)
        //                                {
        //                                    _tRP.FollowMail(user_tmp.ID, UserID);
        //                                }*/
        //                            }
        //                        }
        //                    }
        //                }

        //                FormsAuthentication.SetAuthCookie(_login_register.UserName, false);
        //            }

        //            return View("SocialRegister", rm);
        //        }
        //        //Globals.SaveLog(Repository.Database, String.Format("FacebookCallback : 19"), Models.Type.Register, 1);
        //        //Auth user
        //        /*FormsAuthentication.SetAuthCookie(name, false);

        //        if (decodedState.ContainsKey("returnUrl"))
        //        {
        //            if (Url.IsLocalUrl(decodedState.returnUrl))
        //                return Redirect(decodedState.returnUrl);
        //        }*/

        //        //return RedirectToAction("LogOn", "Account");
        //        return RedirectToAction("Index", "Home");
        //    }
        //    catch
        //    {
        //        // log exception
        //        //return RedirectToAction("LogOn", "Account");
        //        return RedirectToAction("Index", "Home");
        //    }
        //}


        //////////////////////////////////////////////////////////////////////////////
        //TWITTER
        //////////////////////////////////////////////////////////////////////////////

        public ActionResult TwitterLogOn(string oauth_token, string oauth_verifier, string ReturnUrl)
        {
            if (string.IsNullOrEmpty(oauth_token) || string.IsNullOrEmpty(oauth_verifier))
            {
                UriBuilder builder = new UriBuilder(this.Request.Url);
                builder.Query = string.Concat(
                    builder.Query,
                    string.IsNullOrEmpty(builder.Query) ? string.Empty : "&",
                    "ReturnUrl=",
                    ReturnUrl);

                string token = OAuthUtility.GetRequestToken(
                    TwitterConsumerKey,
                    TwitterConsumerSecret,
                    builder.ToString()).Token;

                return Redirect(OAuthUtility.BuildAuthorizationUri(token, true).ToString());
            }

            var tokens = OAuthUtility.GetAccessToken(
                TwitterConsumerKey,
                TwitterConsumerSecret,
                oauth_token,
                oauth_verifier);

            //TODO
            decimal TwitterUserId = tokens.UserId;
            string ScreenName = tokens.ScreenName;
            string TwitterAccessKey = tokens.Token;
            string TwitterAccessSecret = tokens.TokenSecret;

            //Ara mirem si aquest usuari existeix
             JsonLogin _login = new JsonLogin();
             _login = Repository.LoginUserTwitter(TwitterUserId.ToString(), ScreenName, "", "", TwitterAccessKey + "|" + TwitterAccessSecret);

             if (_login.success == false)
             {
                 RegisterModel rm = new RegisterModel();

                 ProductsRepository pr = new ProductsRepository(Repository.Database);
                 rm.UserName = String.Format("{0}", ScreenName);
                 rm.FirstName = ScreenName;
                 rm.LastName = ScreenName;
                 rm.TwitterRegister = 1;
                 rm.TwitterUserId = TwitterUserId;
                 rm.TwitterAccessKey = TwitterAccessKey;
                 rm.TwitterAccessSecret = TwitterAccessSecret;
                 rm.RegisterCategoryProducts = pr.GetRegisterCategoryProducts();
                 rm.RegisterFollowings = Repository.GetRegisterFollowings();

                 return View("SocialRegister", rm);             
             }

             if (_login.success)
             {
                 //Afegim l'entrada a l'AccessLog i actualitzem el lastLogin de l'usuari
                 UsersRepository.UpdateLastLogin(Repository.Database, _login.ID);

                 Session[Globals.SessionUserID] = _login.ID;
                 Session[Globals.SessionUserName] = _login.UserName;
                 Session[Globals.SessionUserPassword] = "";
                 Session[Globals.SessionUserUserID] = _login.UserID;
                 Session[Globals.SessionUserName] = _login.UserName;

                 if (_login.FirstName != "")
                     Session[Globals.SessionFirstName] = _login.FirstName;
                 else
                     Session[Globals.SessionFirstName] = ScreenName;
                 Session[Globals.SessionLastName] = _login.LastName;
                 Session[Globals.SessionPicture] = _login.Picture;
                 Session[Globals.SessionAbout] = _login.About;
                 Session[Globals.SessionWeb] = _login.Web;
                 Session[Globals.SessionNumInteractions] = _login.NumInteractions;

                 String culture = "es";

                 if (_login.LanguageID == 2)
                     culture = "ca";
                 else
                     culture = "es";

                 HttpContext.Session["culture"] = culture;

                 CultureInfo ci = CultureInfo.GetCultureInfo(culture);

                 System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                 System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
             }

             //Auth
             FormsAuthentication.SetAuthCookie(ScreenName, false);

            if (string.IsNullOrEmpty(ReturnUrl))
                return RedirectToAction("Index", "Timeline");
            else
                return Redirect(ReturnUrl);
        }

        public ActionResult TwitterRegister(string oauth_token, string oauth_verifier, string ReturnUrl)
        {
            if (string.IsNullOrEmpty(oauth_token) || string.IsNullOrEmpty(oauth_verifier))
            {
                UriBuilder builder = new UriBuilder(this.Request.Url);
                builder.Query = string.Concat(
                    builder.Query,
                    string.IsNullOrEmpty(builder.Query) ? string.Empty : "&",
                    "ReturnUrl=",
                    ReturnUrl);

                string token = OAuthUtility.GetRequestToken(
                    TwitterConsumerKey,
                    TwitterConsumerSecret,
                    builder.ToString()).Token;

                return Redirect(OAuthUtility.BuildAuthorizationUri(token, true).ToString());
            }

            var tokens = OAuthUtility.GetAccessToken(
                TwitterConsumerKey,
                TwitterConsumerSecret,
                oauth_token,
                oauth_verifier);

            decimal TwitterUserId = tokens.UserId;
            string ScreenName = tokens.ScreenName;
            string TwitterAccessKey = tokens.Token;
            string TwitterAccessSecret = tokens.TokenSecret;

            //Mirem si ja està registrat
            //Ara mirem si aquest usuari existeix
            JsonLogin _login = new JsonLogin();
            _login = Repository.LoginUserTwitter(TwitterUserId.ToString(), ScreenName, "", "", TwitterAccessKey + "|" + TwitterAccessSecret);

            if (_login.success)
            {
                Session[Globals.SessionUserID] = _login.ID;
                Session[Globals.SessionUserName] = _login.UserName;
                Session[Globals.SessionUserPassword] = "";
                Session[Globals.SessionUserUserID] = _login.UserID;
                Session[Globals.SessionUserName] = _login.UserName;

                if (_login.FirstName != "")
                    Session[Globals.SessionFirstName] = _login.FirstName;
                else
                    Session[Globals.SessionFirstName] = ScreenName;
                Session[Globals.SessionLastName] = _login.LastName;
                Session[Globals.SessionPicture] = _login.Picture;
                Session[Globals.SessionAbout] = _login.About;
                Session[Globals.SessionWeb] = _login.Web;
                Session[Globals.SessionNumInteractions] = _login.NumInteractions;

                //Auth
                FormsAuthentication.SetAuthCookie(ScreenName, false);

                if (string.IsNullOrEmpty(ReturnUrl))
                    return RedirectToAction("Index", "Timeline");
                else
                    return Redirect(ReturnUrl);
            }
            else
            {
                RegisterModel rm = new RegisterModel();

                ProductsRepository pr = new ProductsRepository(Repository.Database);
                rm.UserName = String.Format("{0}", ScreenName);
                rm.FirstName = ScreenName;
                rm.LastName = ScreenName;
                rm.TwitterRegister = 1;
                rm.TwitterUserId = TwitterUserId;
                rm.TwitterAccessKey = TwitterAccessKey;
                rm.TwitterAccessSecret = TwitterAccessSecret;
                rm.RegisterCategoryProducts = pr.GetRegisterCategoryProducts();
                rm.RegisterFollowings = Repository.GetRegisterFollowings();

                String twitter_picture = String.Format("https://api.twitter.com/1/users/profile_image?screen_name={0}&size=original", ScreenName);
                rm.Picsquare = twitter_picture;


                //Nova versió : Aquí ja registrem
                JsonLogin _login_register = new JsonLogin();

                //Fem el registre via twitter
                _login_register = Repository.RegisterTwitter(TwitterUserId.ToString(), ScreenName, ScreenName, ScreenName, "", "", "", "", TwitterAccessKey + "|" + TwitterAccessSecret, ScreenName);

                if (_login_register.ID > 0)
                {
                    //Fem el login
                    if (_login_register.success)
                    {
                        Session[Globals.SessionUserID] = _login_register.ID;
                        Session[Globals.SessionUserName] = _login_register.UserName;
                        Session[Globals.SessionUserPassword] = _login_register.Password;
                        Session[Globals.SessionUserUserID] = _login_register.UserID;
                        Session[Globals.SessionUserName] = _login_register.UserName;
                        Session[Globals.SessionFirstName] = _login_register.FirstName;
                        Session[Globals.SessionLastName] = _login_register.LastName;
                        Session[Globals.SessionPicture] = _login_register.Picture;
                        Session[Globals.SessionAbout] = _login_register.About;
                        Session[Globals.SessionWeb] = _login_register.Web;
                        Session[Globals.SessionNumInteractions] = _login_register.NumInteractions;
                    }

                    FormsAuthentication.SetAuthCookie(_login_register.UserName, false);
                }

                return View("SocialRegister", rm);
            }
        }

        //////////////////////////////////////////////////////////////////////////////
        //SYSTEM
        //////////////////////////////////////////////////////////////////////////////
        
        //
        // GET: /Account/LogOn

        public ActionResult LogOn(Int64 id = 0)
        {
            return View(new LogOnModel());
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Globals.SaveLog(Repository.Database, "username : " + model.UserName + " password : " + model.Password, Models.Type.Register, 1);

                    if (new LikekitMembershipProvider().ValidateUser(model.UserName, model.Password))
                    {
                        //Fem serivr el repository per omplir les dades de sessió
                        JsonLogin _login = new JsonLogin();
                        _login = Repository.GetUserData(model.UserName, model.Password);

                        //Globals.SaveLog(Repository.Database, "username : " + model.UserName + " password : " + model.Password, Models.Type.Register, 1);

                        if (_login.success)
                        {
                            //Afegim l'entrada a l'AccessLog i actualitzem el lastLogin de l'usuari
                            UsersRepository.UpdateLastLogin(Repository.Database, _login.ID);

                            Session[Globals.SessionUserID] = _login.ID;
                            Session[Globals.SessionUserName] = model.UserName;
                            Session[Globals.SessionUserPassword] = model.Password;
                            Session[Globals.SessionUserUserID] = _login.UserID;
                            Session[Globals.SessionUserName] = _login.UserName;
                            Session[Globals.SessionFirstName] = _login.FirstName;
                            Session[Globals.SessionLastName] = _login.LastName;
                            Session[Globals.SessionPicture] = _login.Picture;
                            Session[Globals.SessionAbout] = _login.About;
                            Session[Globals.SessionWeb] = _login.Web;
                            Session[Globals.SessionNumInteractions] = _login.NumInteractions;

                            String culture = "es";

                            if (_login.LanguageID == 2)
                                culture = "ca";
                            else
                                culture = "es";

                            HttpContext.Session["culture"] = culture;

                            CultureInfo ci = CultureInfo.GetCultureInfo(culture);

                            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
                        }

                        FormsAuthentication.SetAuthCookie(model.UserName, true);
                        if (!String.IsNullOrEmpty(returnUrl))
                        {
                            return Json(new { Success = "true", Message = _login.UserID, ReturnUrl = returnUrl });
                        }
                        else
                        {
                            return Json(new { Success = "true", Message = _login.UserID, ReturnUrl = "" });
                        }
                    }
                    else
                    {
                        return Json(new { Success = "false", Message = "El usuario o la contraseña son incorrectos.", ReturnUrl = "" });
                    }
                }
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "LogOn");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Register, 1);
            }
            // If we got this far, something failed, redisplay form
            //return View(model);
            return Json(new { Success = "false", Message = "El usuario o la contraseña son incorrectos.", ReturnUrl = "" });

        }

        [HttpPost]
        public ActionResult AutoLogOn(String UserID)
        {
            try
            {
                //Fem serivr el repository per omplir les dades de sessió
                JsonLogin _login = new JsonLogin();
                _login = Repository.GetUserDataAutoLogin(UserID);

                if (_login.success)
                {
                    //Afegim l'entrada a l'AccessLog i actualitzem el lastLogin de l'usuari
                    UsersRepository.UpdateLastLogin(Repository.Database, _login.ID);

                    Session[Globals.SessionUserID] = _login.ID;
                    Session[Globals.SessionUserName] = _login.UserName;
                    Session[Globals.SessionUserPassword] = String.Empty;
                    Session[Globals.SessionUserUserID] = _login.UserID;
                    Session[Globals.SessionUserName] = _login.UserName;
                    Session[Globals.SessionFirstName] = _login.FirstName;
                    Session[Globals.SessionLastName] = _login.LastName;
                    Session[Globals.SessionPicture] = _login.Picture;
                    Session[Globals.SessionAbout] = _login.About;
                    Session[Globals.SessionWeb] = _login.Web;
                    Session[Globals.SessionNumInteractions] = _login.NumInteractions;

                    String culture = "es";

                    if (_login.LanguageID == 2)
                        culture = "ca";
                    else
                        culture = "es";

                    HttpContext.Session["culture"] = culture;

                    CultureInfo ci = CultureInfo.GetCultureInfo(culture);

                    System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = ci;

                    LogOnModel model = new LogOnModel();
                    model.UserName = _login.UserName;
                    model.Password = _login.Password;

                    FormsAuthentication.SetAuthCookie(model.UserName, true);

                    return Json(new { Success = "true", Message = _login.UserID, ReturnUrl = "" });
                }
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "LogOn");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Register, 1);
            }
            // If we got this far, something failed, redisplay form
            //return View(model);
            return Json(new { Success = "false", Message = "El usuario o la contraseña son incorrectos.", ReturnUrl = "" });

        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();

            //return RedirectToAction("LogOn/1", "Account");
            return RedirectToAction("Index", "Home");
        }


        [Authorize]
        public ActionResult ProvaViralitzacio()
        {
            return View();
        }


        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            /*User user = Repository.GetUser(id);

            //Omplim les dades dels Kits separats per categories;
            user.ProfileCategoryKits = Repository.GetProfileCategoryKits(id);


            //Omplim les dades dels Kits separats per categories;
            user.ProfileCategoryKits = Repository.GetProfileCategoryKits(id);*/

            RegisterModel rm = new RegisterModel();

            ProductsRepository pr = new ProductsRepository(Repository.Database);
            rm.RegisterCategoryProducts=pr.GetRegisterCategoryProducts();
            rm.RegisterFollowings = Repository.GetRegisterFollowings();
            //return View(new RegisterModel());
            return View(rm);
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public ActionResult DoRegister(String mail, String firstname, String lastname, String password, String selected_items, String register_followings)
        {
            JsonUserResult result = new JsonUserResult();

            try
            {
                User _user = new User();
                _user.UserName = mail;
                _user.Password = password;
                _user.FirstName = firstname;
                _user.LastName = lastname;

                //Mirem de configurar l'idioma
                Language _lang = new Language();
                _lang.ID = 1;

                if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                    _lang.ID = 2;

                _user.Language = _lang;

                User _user_rp = Repository.CreateUser(_user);

                if (_user_rp.ID > 0)
                {
                    //Recorrem els productes seleccionats
                    string[] tokensizedStrs;
                    tokensizedStrs = selected_items.Split(new char[] { ',' });

                    int len = tokensizedStrs.Length;

                    for (int i = 0; i < len; i++)
                    {
                        string tmp = tokensizedStrs[i];

                        if (tmp != String.Empty)
                        {
                            Int64 ProductID = Int64.Parse(tmp);

                            UserLikeProduct ul = new UserLikeProduct();
                            ul.Product = new Product();
                            ul.Product.ID = ProductID;
                            ul.User = new User();
                            ul.User.ID = _user_rp.ID;

                            UserLikesRepository _ulRP = new UserLikesRepository(Repository.Database);
                            _ulRP.CreateUserLikeProduct(ul);
                        }
                    }

                    if ((register_followings != String.Empty) && (register_followings != "") && (register_followings != "0"))
                    {
                        //Ara recorrem els usuaris
                        string[] tokensizedUsers;
                        tokensizedUsers = register_followings.Split(new char[] { ',' });

                        int lenUsers = tokensizedUsers.Length;

                        for (int i = 0; i < lenUsers; i++)
                        {
                            string tmp = tokensizedUsers[i];

                            if (tmp != String.Empty)
                            {
                                Int64 ID = Int64.Parse(tmp);

                                UserRelation ur = new UserRelation();
                                ur.User = new User();
                                ur.User.ID = _user_rp.ID;
                                ur.Follow = new User();
                                ur.Follow.ID = ID;

                                UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
                                _urRP.CreateUserRelation(ur);
                            }
                        }
                    }

                    //Resultat del procés
                    result.Success = true;
                    result.Message = String.Format("Success");
                }
                else
                {
                    result.Success = false;
                    result.Message = String.Format("Error");
                }
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Register, 1);
            }

            String Message = String.Empty;
            Message = result.Message;

            return PartialView("Html", (object)Message);
        }

        [HttpPost]
        
        public ActionResult DoRegisterSocial(String userName, String firstname, String lastname, String twitterUserId, String twitterAccessKey, String twitterAccessSecret, int twitterRegister, String selected_items, String register_followings, String facebookUserId, String facebookAccessToken, int facebookRegister, String picsquare, String gender, String socialName)
        {
            JsonUserResult result = new JsonUserResult();
            JsonLogin _login = new JsonLogin();


            Language _lang = new Language();
            _lang.ID = 1;

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                _lang.ID = 2;

            //Mirem si és un registre de twitter o facebook
            if (twitterRegister ==  1)
            {
                decimal TwitterUserId = Decimal.Parse(twitterUserId);
                string ScreenName = userName;
                string TwitterAccessKey = twitterAccessKey;
                string TwitterAccessSecret = twitterAccessSecret;

                _login = Repository.RegisterTwitter(TwitterUserId.ToString(), userName, firstname, lastname, "", "", "", "", TwitterAccessKey + "|" + TwitterAccessSecret, socialName);
            }
            else if(facebookRegister == 1)
            {
                //_login = Repository.RegisterTwitter(TwitterUserId.ToString(), ScreenName, "", "", "", "", "", "", TwitterAccessKey + "|" + TwitterAccessSecret);            
                _login = Repository.RegisterFacebook(facebookUserId, userName, firstname, lastname, gender, picsquare, facebookAccessToken, socialName, _lang.ID);
            }

            if (_login.ID > 0)
            {
                //Tokenizer selected items
                string[] tokensizedStrs;
                tokensizedStrs = selected_items.Split(new char[] { ',' });

                int len = tokensizedStrs.Length;

                for (int i = 0; i < len; i++)
                {
                    string tmp = tokensizedStrs[i];

                    if (tmp != String.Empty)
                    {
                        Int64 ProductID = Int64.Parse(tmp);

                        UserLikeProduct ul = new UserLikeProduct();
                        ul.Product = new Product();
                        ul.Product.ID = ProductID;
                        ul.User = new User();
                        ul.User.ID = _login.ID;

                        UserLikesRepository _ulRP = new UserLikesRepository(Repository.Database);
                        _ulRP.CreateUserLikeProduct(ul);
                    }
                }

                //Ara recorrem els usuaris
                string[] tokensizedUsers;
                tokensizedUsers = register_followings.Split(new char[] { ',' });

                int lenUsers = tokensizedUsers.Length;

                for (int i = 0; i < lenUsers; i++)
                {
                    string tmp = tokensizedUsers[i];

                    if (tmp != String.Empty)
                    {
                        Int64 ID = Int64.Parse(tmp);

                        UserRelation ur = new UserRelation();
                        ur.User = new User();
                        ur.User.ID = _login.ID;
                        ur.Follow = new User();
                        ur.Follow.ID = ID;

                        UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
                        _urRP.CreateUserRelation(ur);
                    }
                }

                //Resultat del procés
                result.Success = true;
                result.Message = String.Format("Success");


                //Fem el login
                if (_login.success)
                {
                    Session[Globals.SessionUserID] = _login.ID;
                    Session[Globals.SessionUserName] = _login.UserName;
                    Session[Globals.SessionUserPassword] = _login.Password;
                    Session[Globals.SessionUserUserID] = _login.UserID;
                    Session[Globals.SessionUserName] = _login.UserName;
                    Session[Globals.SessionFirstName] = _login.FirstName;
                    Session[Globals.SessionLastName] = _login.LastName;
                    Session[Globals.SessionPicture] = _login.Picture;
                    Session[Globals.SessionAbout] = _login.About;
                    Session[Globals.SessionWeb] = _login.Web;
                    Session[Globals.SessionNumInteractions] = _login.NumInteractions;
                }

                FormsAuthentication.SetAuthCookie(_login.UserName, false);
            }
            else
            {
                result.Success = false;
                result.Message = String.Format("Error");
            }

            String Message = String.Empty;
            Message = result.Message;

            return PartialView("Html", (object)Message);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }


        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public ActionResult CheckMail(String mail)
        {
            JsonUserResult result = Repository.CheckMail(mail);

            String Message = String.Empty;
            Message = result.Message;

            return PartialView("Html", (object)Message);
        }

        public ActionResult CheckMailLogin(String mail)
        {
            JsonUserResult result = Repository.CheckMailLogin(mail);

            String Message = String.Empty;
            Message = result.Message;

            return PartialView("Html", (object)Message);
        }

        public ActionResult Activation(String id)
        {
            String Message = String.Empty;

            if (Repository.ActivateUser(id))
                Message = String.Format("Felicidades, tu usuario ya está activo, ya puedes entrar en LikeKit!");
            else
                Message = String.Format("Perdona, ha ocurrido un error, contacta con el administrador");

            return View((object)Message);
        }


        public ActionResult PasswordRecovery()
        {
            return View();
        }

        public ActionResult PasswordRecoveryMail(String mail)
        {
            String Message = String.Empty;
            //Busquem l'usuari per mail
            User user = Repository.GetUserByMail(mail);

            bool isFBTW = false;

            //Mirem si és un usuari de Facebook o Twitter
            //if ((user.FacebookID != String.Empty) || (user.FacebookID != "") || (user.TwitterID != String.Empty) || (user.TwitterID != ""))
            if((int)user.RegisterType > 0)
                isFBTW = true;

            if ((user != null) && (user.ID > 0) && (!isFBTW))
            {
                //Canviem el password i fem un update.
                /*String PasswordHash = Endepro.Common.Crypto.GetMD5Hash(String.Format("{0}{1}", user.UserName, System.DateTime.UtcNow));
                PasswordHash = PasswordHash.Substring(0, 8);
                String Password = Endepro.Common.Crypto.GetMD5Hash(PasswordHash);
                user.Password = Password;

                Repository.UpdateUserPassword(user);*/

                //Enviem el correu
                //Carreguem les dades del template
                String Subject = String.Empty;
                String Body = String.Empty;
                String To = String.Empty;
                String From = String.Empty;

                MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Repository.Database);

                short _lang = 1;

                if(user.Language != null)
                    _lang = (short)user.Language.ID;

                MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.PasswordRecovery, _lang);

                mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

                String url_change_password = String.Format("{0}/Account/UpdatePassword/?id={1}", ConfigurationManager.AppSettings["MainURL"],user.UserID);

                //Evitem el problema de duplicar les barres, ja que això genera problemes
                url_change_password = url_change_password.Replace("//", "/");
                url_change_password = url_change_password.Replace("http:/", "http://");

                mail_template.Body = mail_template.Body.Replace("[CHANGE_PASSWORD_URL]", url_change_password);

                String url_cancel_change_password = String.Format("{0}/Account/CancelUpdatePassword/?id={1}", ConfigurationManager.AppSettings["MainURL"],user.UserID);

                //Evitem el problema de duplicar les barres, ja que això genera problemes
                url_cancel_change_password = url_cancel_change_password.Replace("//", "/");
                url_cancel_change_password = url_cancel_change_password.Replace("http:/", "http://");

                mail_template.Body = mail_template.Body.Replace("[CANCEL_PASSWORD_URL]", url_cancel_change_password);

                //mail_template.Body = mail_template.Body.Replace("[NEW_PASSWORD]", PasswordHash);

                //Enviem un correu
                try
                {
                    Globals.SendMail(Repository.Database, mail_template.Subject, mail_template.Body, user.UserName);
                    Message = "Success";
                }
                catch (Exception ex)
                {
                    //Guardem log
                    String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                    Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Register, 1);
                    Message = "Error";
                }
            }
            else
                Message = "Error";

            return PartialView("Html", (object)Message);
        }

        public ActionResult UpdatePassword(String id)
        {
            return View((object)id);
        }

        public ActionResult DoUpdatePassword(String UserID, String Password)
        {
            User user = Repository.GetUserByUserID(UserID);

            bool isFBTW = false;

            //Mirem si és un usuari de Facebook o Twitter
            if ((user.FacebookID != String.Empty) || (user.FacebookID != "") || (user.TwitterID != String.Empty) || (user.TwitterID != ""))
                isFBTW = true;

            if ((user != null) && (user.ID > 0) && (!isFBTW))
            {
                //Canviem el password i fem un update.
                String PasswordHash = Endepro.Common.Crypto.GetMD5Hash(Password);
                user.Password = PasswordHash;

                Repository.UpdateUserPassword(user);
            }
            //return RedirectToAction("LogOn", "Account");
            return RedirectToAction("Index", "Home");
            //return View((object)UserID);
        }

        public ActionResult TwitterConnect(string oauth_token, string oauth_verifier, string ReturnUrl)
        {
            if (string.IsNullOrEmpty(oauth_token) || string.IsNullOrEmpty(oauth_verifier))
            {
                UriBuilder builder = new UriBuilder(this.Request.Url);
                builder.Query = string.Concat(
                    builder.Query,
                    string.IsNullOrEmpty(builder.Query) ? string.Empty : "&",
                    "ReturnUrl=",
                    ReturnUrl);

                string token = OAuthUtility.GetRequestToken(
                    TwitterConsumerKey,
                    TwitterConsumerSecret,
                    builder.ToString()).Token;

                return Redirect(OAuthUtility.BuildAuthorizationUri(token, true).ToString());
            }

            var tokens = OAuthUtility.GetAccessToken(
                TwitterConsumerKey,
                TwitterConsumerSecret,
                oauth_token,
                oauth_verifier);

            decimal TwitterUserId = tokens.UserId;
            string ScreenName = tokens.ScreenName;
            string TwitterAccessKey = tokens.Token;
            string TwitterAccessSecret = tokens.TokenSecret;

            //Mirem si ja està registrat
            //Ara mirem si aquest usuari existeix
            /*JsonLogin _login = new JsonLogin();
            _login = Repository.LoginUserTwitter(TwitterUserId.ToString(), ScreenName, "", "", TwitterAccessKey + "|" + TwitterAccessSecret);*/


            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            user.TwitterAccessToken = TwitterAccessKey + "|" + TwitterAccessSecret;
            user.TwitterID = TwitterUserId.ToString();
            Repository.UpdateUserTwitterConnect(user);

            String Message = String.Empty;
            Message = "Success";

            //return PartialView("Html", (object)Message);
            return RedirectToAction("EditProfile/" + ID, "Timeline");
        }

        public ActionResult TwitterDisconnect()
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            user.TwitterAccessToken = String.Empty;
            user.TwitterID = String.Empty;
            Repository.UpdateUserTwitterConnect(user);

            String Message = String.Empty;
            Message = "Success";

            //return PartialView("Html", (object)Message);
            //return RedirectToAction("EditProfile/" + ID, "Timeline");
            return PartialView("Html", (object)Message);
        }

        public ActionResult FacebookConnectCallback(string code, string state)
        {
            Int64 ID = 0;

            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(state))
                return RedirectToAction("EditProfile/" + ID, "Timeline");

            // first validate the csrf token
            dynamic decodedState;
            try
            {
                decodedState = _fb.DeserializeJson(Encoding.UTF8.GetString(Convert.FromBase64String(state)), null);
                var exepectedCsrfToken = Session["fb_csrf_token"] as string;
                // make the fb_csrf_token invalid
                Session["fb_csrf_token"] = null;
                if (!(decodedState is IDictionary<string, object>) || !decodedState.ContainsKey("csrf") || string.IsNullOrWhiteSpace(exepectedCsrfToken) || exepectedCsrfToken != decodedState.csrf)
                {
                    return RedirectToAction("EditProfile/" + ID, "Timeline");
                }
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "FacebookConnectCallback");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
                // log exception
                return RedirectToAction("EditProfile/" + ID, "Timeline");
            }

            try
            {
                dynamic result = _fb.Post("oauth/access_token",
                                          new
                                          {
                                              client_id = FacebookAppId,
                                              client_secret = FacebookAppsecret,
                                              redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookConnectCallback",
                                              code = code
                                          });
                Session["fb_access_token"] = result.access_token;

                if (result.ContainsKey("expires"))
                    Session["fb_expires_in"] = DateTime.Now.AddSeconds(result.expires);

                //TODO
                //me
                _fb.AccessToken = Session["fb_access_token"].ToString();
                dynamic me = _fb.Get("me");
                string id = me.id;
                string name = me.name;
                string firstname = me.first_name;
                string lastname = me.last_name;
                string gender = me.gender;
                string email = me.email;
                //string locale = me.locale;

                User user = Repository.GetUser(ID);
                user.FacebookAccessToken = _fb.AccessToken;
                user.FacebookID = id.ToString();
                Repository.UpdateUserFacebookConnect(user);

                return RedirectToAction("EditProfile/" + ID, "Timeline");
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "FacebookConnectCallback");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
                // log exception
                return RedirectToAction("EditProfile/" + ID, "Timeline");
            }
        }

        public ActionResult FacebookDisconnect()
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            user.FacebookAccessToken = String.Empty;
            user.FacebookID = String.Empty;
            Repository.UpdateUserFacebookConnect(user);

            String Message = String.Empty;
            Message = "Success";

            return PartialView("Html", (object)Message);
        }

        public ActionResult TwitterKitConnect(string oauth_token, string oauth_verifier, string ReturnUrl, Int64 id)
        {
            if (string.IsNullOrEmpty(oauth_token) || string.IsNullOrEmpty(oauth_verifier))
            {
                UriBuilder builder = new UriBuilder(this.Request.Url);
                builder.Query = string.Concat(
                    builder.Query,
                    string.IsNullOrEmpty(builder.Query) ? string.Empty : "&",
                    "ReturnUrl=",
                    ReturnUrl);

                string token = OAuthUtility.GetRequestToken(
                    TwitterConsumerKey,
                    TwitterConsumerSecret,
                    builder.ToString()).Token;

                return Redirect(OAuthUtility.BuildAuthorizationUri(token, true).ToString());
            }

            var tokens = OAuthUtility.GetAccessToken(
                TwitterConsumerKey,
                TwitterConsumerSecret,
                oauth_token,
                oauth_verifier);

            decimal TwitterUserId = tokens.UserId;
            string ScreenName = tokens.ScreenName;
            string TwitterAccessKey = tokens.Token;
            string TwitterAccessSecret = tokens.TokenSecret;

            //Mirem si ja està registrat
            //Ara mirem si aquest usuari existeix
            /*JsonLogin _login = new JsonLogin();
            _login = Repository.LoginUserTwitter(TwitterUserId.ToString(), ScreenName, "", "", TwitterAccessKey + "|" + TwitterAccessSecret);*/


            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            user.TwitterAccessToken = TwitterAccessKey + "|" + TwitterAccessSecret;
            user.TwitterID = TwitterUserId.ToString();
            Repository.UpdateUserTwitterConnect(user);

            String Message = String.Empty;
            Message = "Success";

            //return PartialView("Html", (object)Message);
            return RedirectToAction("Item/" + id, "Timeline");
        }

        public ActionResult FacebookKitConnect(string returnUrl, Int64 id)
        {
            String csrfToken = Guid.NewGuid().ToString();
            Session["fb_csrf_token"] = csrfToken;

            String state = Convert.ToBase64String(Encoding.UTF8.GetBytes(_fb.SerializeJson(new { returnUrl = returnUrl, csrf = csrfToken })));

            Uri fbLoginUrl = _fb.GetLoginUrl(
                new
                {
                    client_id = FacebookAppId,
                    client_secret = FacebookAppsecret,
                    redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookKitConnectCallback/?id=" + id,
                    response_type = "code",
                    scope = FacebookScope,
                    state = state
                });

            return Redirect(fbLoginUrl.AbsoluteUri);
        }

        public ActionResult FacebookKitConnectCallback(string code, string state, Int64 id)
        {
            Int64 UserID = 0;

            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(state))
                return RedirectToAction("Item/" + id, "Timeline");

            // first validate the csrf token
            dynamic decodedState;
            try
            {
                decodedState = _fb.DeserializeJson(Encoding.UTF8.GetString(Convert.FromBase64String(state)), null);
                var exepectedCsrfToken = Session["fb_csrf_token"] as string;
                // make the fb_csrf_token invalid
                Session["fb_csrf_token"] = null;

                if (!(decodedState is IDictionary<string, object>) || !decodedState.ContainsKey("csrf") || string.IsNullOrWhiteSpace(exepectedCsrfToken) || exepectedCsrfToken != decodedState.csrf)
                {
                    return RedirectToAction("Item/" + id, "Timeline");
                }
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "FacebookKitConnectCallback");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
                // log exception
                return RedirectToAction("Item/" + id, "Timeline");
            }

            try
            {
                dynamic result = _fb.Post("oauth/access_token",
                                          new
                                          {
                                              client_id = FacebookAppId,
                                              client_secret = FacebookAppsecret,
                                              redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookKitConnectCallback/?id=" + id,
                                              code = code
                                          });

                Session["fb_access_token"] = result.access_token;

                if (result.ContainsKey("expires"))
                    Session["fb_expires_in"] = DateTime.Now.AddSeconds(result.expires);

                //TODO
                //me
                _fb.AccessToken = Session["fb_access_token"].ToString();
                dynamic me = _fb.Get("me");
                string id_tmp = me.id;
                string name = me.name;
                string firstname = me.first_name;
                string lastname = me.last_name;
                string gender = me.gender;
                string email = me.email;
                //string locale = me.locale;

                User user = Repository.GetUser(UserID);

                user.FacebookAccessToken = _fb.AccessToken;
                user.FacebookID = id_tmp.ToString();
                Repository.UpdateUserFacebookConnect(user);

                return RedirectToAction("Item/" + id, "Timeline");
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "FacebookKitConnectCallback");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);

                // log exception
                return RedirectToAction("Item/" + id, "Timeline");
            }
        }

        public ActionResult FacebookInviteCallback(string code, string state)
        {
            String test = String.Empty;

            if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(state))
            {
                //return RedirectToAction("LogOn", "Account");
                return RedirectToAction("Index", "Home");
            }

            // first validate the csrf token
            dynamic decodedState;
            try
            {
                decodedState = _fb.DeserializeJson(Encoding.UTF8.GetString(Convert.FromBase64String(state)), null);
                var exepectedCsrfToken = Session["fb_csrf_token"] as string;
                // make the fb_csrf_token invalid
                Session["fb_csrf_token"] = null;

                if (!(decodedState is IDictionary<string, object>) || !decodedState.ContainsKey("csrf") || string.IsNullOrWhiteSpace(exepectedCsrfToken) || exepectedCsrfToken != decodedState.csrf)
                {
                    //return RedirectToAction("LogOn", "Account");
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                // log exception
                //return RedirectToAction("LogOn", "Account");
                return RedirectToAction("Index", "Home");
            }

            try
            {
                dynamic result = _fb.Post("oauth/access_token",
                                          new
                                          {
                                              client_id = FacebookAppId,
                                              client_secret = FacebookAppsecret,
                                              redirect_uri = ConfigurationManager.AppSettings["MainURL"] + "Account/FacebookInviteCallback",
                                              code = code
                                          });

                Session["fb_access_token"] = result.access_token;

                if (result.ContainsKey("expires"))
                    Session["fb_expires_in"] = DateTime.Now.AddSeconds(result.expires);
                test = "6";
                //TODO
                //me
                _fb.AccessToken = Session["fb_access_token"].ToString();

                List<InviteContact> contacts = Social.FacebookGetFriends(_fb.AccessToken);

                Int64 UserID = 0;
                if (Session[Globals.SessionUserID] != null)
                    UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

                //Hauriem de mirar si l'usuari està connectat a Facebook

                _fb.AccessToken = Session["fb_access_token"].ToString();
                dynamic me = _fb.Get("me");
                string id = me.id;

                User user = Repository.GetUser(UserID);
                if ((user.FacebookID == "") || (user.FacebookID == String.Empty))
                {
                    user.FacebookAccessToken = _fb.AccessToken;
                    user.FacebookID = id.ToString();
                    Repository.UpdateUserFacebookConnect(user);
                }


                //Hem de parsejar els resultats
                String sHtml = String.Empty;
                //No mostrem els resultats, només el botó de send
                //String sHtml = Repository.InviteProcessContacts(contacts, UserID, InviteType.Facebook);

                Invite invite = new Invite();
                invite.Show = true;
                invite.Type = InviteType.Facebook;
                invite.HtmlOutput = sHtml;

                return View("../Timeline/Invite", (object)invite);
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "FacebookInviteCallback " + test);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);

                // log exception
                //return RedirectToAction("LogOn", "Account");
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult DoInviteRegister(String register_followings, String Invite)
        {
            /*TODO
            Seguir als register register_followings
            Invitar si cal*/

            try
            {
                Int64 UserID = 0;

                //Globals.SaveLog(Repository.Database, String.Format("DoInviteRegister : 1"), Models.Type.Register, 1);
                //Globals.SaveLog(Repository.Database, String.Format("DoInviteRegister register_followings : {0}, invite {1}", register_followings, Invite), Models.Type.Register, 1);

                if (Session[Globals.SessionUserID] != null)
                {
                    UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

                    //Globals.SaveLog(Repository.Database, String.Format("DoInviteRegister UserID : {0}", UserID), Models.Type.Register, 1);

                    User user = Repository.GetUser(UserID);

                    /*if ((register_followings != String.Empty) && (register_followings != "") && (register_followings != "0"))
                    {
                        //Ara recorrem els usuaris
                        string[] tokensizedUsers;
                        tokensizedUsers = register_followings.Split(new char[] { ',' });

                        int lenUsers = tokensizedUsers.Length;

                        for (int i = 0; i < lenUsers; i++)
                        {
                            string tmp = tokensizedUsers[i];

                            if (tmp != String.Empty)
                            {
                                Int64 ID = Int64.Parse(tmp);

                                UserRelation ur = new UserRelation();
                                ur.User = new User();
                                ur.User.ID = UserID;
                                ur.Follow = new User();
                                ur.Follow.ID = ID;

                                //Globals.SaveLog(Repository.Database, String.Format("DoInviteRegister UserRelation : {0}", ID), Models.Type.Register, 1);

                                UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
                                Int64 follow_id = _urRP.CreateUserRelation(ur);

                                
                            }
                        }
                    }*/

                    //Globals.SaveLog(Repository.Database, String.Format("Invite  value {0}", Invite), Models.Type.Register, 1);
                    //Globals.SaveLog(Repository.Database, String.Format("Invite  user FacebookAccessToken {0}", user.FacebookAccessToken), Models.Type.Register, 1);
                    //Ara el procés d'invitació és fa via facebook api
                    if(Invite == "1")
                    {
                        //Globals.SaveLog(Repository.Database, String.Format("Invite 1"), Models.Type.Register, 1);

                        /*List<InviteContact> contacts = Social.FacebookGetFriends(user.FacebookAccessToken);

                        String sHtml = Repository.InviteProcessContacts(contacts, UserID, InviteType.Facebook);

                        foreach (InviteContact contact in contacts)
                        {
                            String mail = contact.Email;
                            String Name = contact.Name;
                            User user_tmp = null;

                            Globals.SaveLog(Repository.Database, String.Format("Invite mail: {0} name : {1}", mail, Name), Models.Type.Register, 1);

                            UsersRepository _urRP = new UsersRepository(Repository.Database);
                            user_tmp = _urRP.GetUserByFacebookID(mail);

                            if (user_tmp == null)
                            {
                                //User user = Repository.GetUser(UserID);
                                Globals.SaveLog(Repository.Database, "Invite facebook : " + contact.Email, Models.Type.Register, 1);

                                String message = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_FACEBOOK_INVITE_TEXT);
                                String Image = ConfigurationManager.AppSettings["MainURL"] + "Content/images/avatar.jpg";
                                String link = ConfigurationManager.AppSettings["MainURL"];
                                String name = "";
                                String caption = "";
                                String description = "";

                                TimeLineRepository _tlRP = new TimeLineRepository(Repository.Database);
                                _tlRP.FacebookInvite(user, message, link, name, caption, Image, description, contact.Email);
                            }
                        }*/
                    }
                }
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Register, 1);
            }

            String Message = "Success";

            return PartialView("Html", (object)Message);
        }

        #endregion
    }
}
