﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LikeKitWebsite.Models;
using System.Data;
using LikeKitWebsite.Repository;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Web.Helpers;
using System.Net;


namespace LikeKitWebsite.Controllers
{
    public class HomeController : EController<TimeLineRepository>
    {
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            String culture = "es";

            try
            {
                if (HttpContext != null)
                {
                    if (HttpContext.Session["culture"] != null)
                        culture = HttpContext.Session["culture"].ToString();
                    else
                    {
                        //Primer mirem el lang del navegador
                        if (Request != null)
                        {
                            var l = Request.UserLanguages;
                            string browserlanguage = l[0];

                            if (browserlanguage == "ca-ES")
                                browserlanguage = "ca";
                            else
                                browserlanguage = "es";

                            HttpContext.Session["culture"] = browserlanguage;
                        }
                        else
                        {
                            String address = Request.ServerVariables["REMOTE_ADDR"];
                            culture = Repository.GetIPCulture(address);
                            HttpContext.Session["culture"] = culture;
                        }
                    }

                    CultureInfo ci = CultureInfo.GetCultureInfo(culture);

                    System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
                }
            }
            catch (Exception ex)
            {
                //Globals.SaveLog(Repository.Database, String.Format("Home initialize : {0}", ex.Message.ToString()), Models.Type.Timeline, 1);
            }
        }

        public ActionResult SetLanguage(string lang)
        {
            HttpContext.Session["culture"] = lang;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Index()
        {
            short _lang = 1;

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                _lang = 2;

            var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, "",15,0,0).ToString();
            Int64 counter = Repository.GetHomeTimelineCounter(DateTime.Now, _lang, "");

            HomePage home_page = new HomePage();
            home_page.Timeline = timeline;
            home_page.Counter = counter;

            return View((object)home_page);
        }

        public ActionResult Item(Int64 id)
        {
            Kit kit = Repository.GetKit(id, 0);

            //Carreguem els tags
            KitsRepository kRP = new KitsRepository(Repository.Database);
            kit.HtmlTags = kRP.GetHtmlKitTags(kit.ID);
            kit.StringTags = kRP.GetStringKitTags(kit.ID);

            kit.Comment = LikeKitWebsite.Globals.HtmlEncodingLinks(kit.Comment);

            switch (kit.Product.ProductCategory.ID)
            { 
                case 1:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIBROS_MIN;
                    kit.SchemaType = "itemscope itemtype=\"http://schema.org/Book\"";
                    break;
                case 2:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SERIES_MIN;
                    kit.SchemaType = "itemscope itemtype=\"http://schema.org/TVSeries\"";
                    break;
                case 3:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CINE_MIN;
                    kit.SchemaType = "itemscope itemtype=\"http://schema.org/Movie\"";
                    break;
                case 4:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUSICA_MIN;
                    kit.SchemaType = "itemscope itemtype=\"http://schema.org/MusicAlbum\"";
                    break;
            }

            //Limitem la descripció del producte a 400 caràcters
            kit.Product.Description = Globals.WordCut(kit.Product.Description, 400, new char[] { ' ' });
            kit.HomeKitUserUrlFriendly = String.Format("{0}perfilp/{1}/{2}", ConfigurationManager.AppSettings["MainURL"], Globals.GenerateUrlFriendly(kit.User.FirstName + " " + kit.User.LastName), kit.User.ID);
            kit.HtmlOtherKits = kRP.GetHomeHtmlOtherKits(id);
            kit.LoquieroHTML = kRP.GetLoquieroHTML(kit.Product.ID, kit.User.ID);

            return View(kit);
        }

        public ActionResult HomeSearch(String Search, String filter)
        {
            short _lang = 1;

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                _lang = 2;

            var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, Search).ToString();
            return PartialView("Html", (object)timeline);
        }

        public ActionResult HomeCategory(String Search, String Category)
        {
            short _lang = 1;

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                _lang = 2;

            Int16 Type = 0;
            if (Category == "cine")
                Type = 1;
            else if (Category == "libros")
                Type = 2;
            else if (Category == "series")
                Type = 4;
            else if (Category == "musica")
                Type = 3;
            else if (Category == "trenders")
                Type = 5;
            else if (Category == "cartelera")
                Type = 6;

            var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, Search,15,0,Type).ToString();
            //return PartialView("Html", (object)timeline);
            Int64 counter = Repository.GetHomeTimelineCounter(DateTime.Now, _lang, "");

            //Aquí es calcularà el títol de la pàgina de comodí.
            var titolComodi = Repository.GetTitolPaginaComodi(Type);
            //Aquí es calcularà la descripció de la pàgina de comodí.
            var descripcioComodi = Repository.GetDescripcioComodi(Type);

            HomePage home_page = new HomePage();
            home_page.Timeline = timeline;
            home_page.Counter = counter;
            home_page.TitolComodi = titolComodi;
            home_page.DescripcioComodi = descripcioComodi;
            if (Type == 5) //Pàgina de trenders
            {
                home_page.MetaTitle = "Likekit - Trenders";
                home_page.Metadescription = "Los Trenders son los Liketers que crean tendencia gracias a la repercusión que consiguen con sus kits. Descubre todas sus recomendaciones.";
                home_page.MetaKeywords = "Trenders, puntos, recomendaciones";
                home_page.IconePagComodi = "../../Content/images/img_pagTrenders.jpg";
            }
            else if (Type == 6) //Pàgina de cartellera
            {
                home_page.MetaTitle = "Likekit - Cartelera";
                home_page.Metadescription = "Las últimas recomendaciones de las películas en cartelera. Descubre qué puedes ir a ver al cine sin equivocarte.";
                home_page.MetaKeywords = "Cine, estrenos, cartelera, recomendaciones";
                home_page.IconePagComodi = "../../Content/images/img_pagCartellera.jpg";
            }
            if (Type == 5 || Type == 6)
            {
                return View("Comodi", (object)home_page);
            }
            else
            {
                return View("Index", (object)home_page);
            }
        }

        public ActionResult HomeTimelinePager(String date, Int16 start, Int16 limit, String search, Int16 type)
        {
            try
            {
                short _lang = 1;

                if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                    _lang = 2;

                //FIX
                //if ((search == "Buscar recomanacions") || (search == "undefined"))
                search = "";

                //Fem una conversió de la data
                var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, search, limit, start, type).ToString();
                return PartialView("Html", (object)timeline);    
            }
            catch
            {
                String sError = String.Format("<div class=\"timeline-entry\">Error</div>");
                return PartialView("Html", (object)sError);
            }
            
        }

        public ActionResult HomeFilter(Int16 type)
        {
            try
            {
                short _lang = 1;

                if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                    _lang = 2;

                //Fem una conversió de la data
                var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, "", 15, 0, type).ToString();
                return PartialView("Html", (object)timeline);
            }
            catch
            {
                String sError = String.Format("<div class=\"timeline-entry\">Error</div>");
                return PartialView("Html", (object)sError);
            }

        }

        public ActionResult Help()
        {
            return View();
        }

        public ActionResult MediaKit()
        {
            return View();
        }

        public ActionResult Team()
        {
            return View();
        }

        public ActionResult Legal()
        {
            return View();
        }

        public ActionResult About(Int64 id)
        {
            StaticPagesRepository hRP = new StaticPagesRepository(Repository.Database);

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
            {
                id = id + 4;
            }

            StaticPage static_page = hRP.GetStaticPage(id);

            return View(static_page);
        }

        public ActionResult Sitemap(Int64 id)
        {
            String sitemap = String.Empty;
            Globals.SaveLog(Repository.Database, String.Format("Sitemap : {0}", id.ToString()), Models.Type.Register, 1);
            sitemap = Repository.GenerateSitemap(id);

            return PartialView("Sitemap", (object)sitemap);
        }

        public ActionResult Profile(Int64 id)
        {
            User user = null;
            String test = String.Empty;
            try
            {
                user = Repository.GetUser(id);

                Int64 SessionUserID = 0;
                
                //Omplim les dades dels Kits separats per categories;
                user.ProfileCategoryKits = Repository.GetHomeProfileCategoryKits(id, SessionUserID);

                UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);
                user.NumFollowers = urRP.GetNumUserRelations(id, 2);
                user.NumFollowings = urRP.GetNumUserRelations(id, 1);

                KitsRepository kRP = new KitsRepository(Repository.Database);
                user.NumKits = kRP.GetUserNumKits(id);
                user.NumLikes = kRP.GetNumLikes(id);
                //user.NumReKits = kRP.GetUserNumReKits(id);
                //user.NumComments = kRP.GetNumUserComments(id);

                user.NumLikesToUser = kRP.GetNumLikesToUser(id);
                user.NumReKitsToUser = kRP.GetUserNumReKitsToUser(id);
                user.NumCommentsToUser = kRP.GetNumUserCommentsToUser(id);


                user.NumTrenderPoints = kRP.GetNumTrenderPoints();

                user.NumUserPoints = user.NumKits + user.NumLikesToUser + user.NumReKitsToUser + user.NumCommentsToUser;

                user.ProfileFollowers = urRP.GetHtmlHomeProfileRelations(id, 2, SessionUserID);
                user.ProfileFollowings = urRP.GetHtmlHomeProfileRelations(id, 1, SessionUserID);

                UserStatusRepository usRP = new UserStatusRepository(Repository.Database);
                user.ProfileNowWith = usRP.GetHtmlProfileNowWith(id, SessionUserID);
                user.ProfileUsersLikeUser = Repository.GetHomeUsersLikeUserHtml(id, SessionUserID);

                user.isFollowing = urRP.isFollower(SessionUserID, id);

                user.FirstNameUpper = user.FirstName.ToUpper();
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "Profile " + test);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
            }
            return View(user);
        }

        //public ActionResult Comodi()
        //{
        //    short _lang = 1;

        //    if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
        //        _lang = 2;

        //    var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, "", 15, 0, 0).ToString();
        //    Int64 counter = Repository.GetHomeTimelineCounter(DateTime.Now, _lang, "");

        //    HomePage home_page = new HomePage();
        //    home_page.Timeline = timeline;
        //    home_page.Counter = counter;

        //    return View((object)home_page);
        //}

        //public ActionResult Comodi()
        //{
        //    return View();
        //}


        public ActionResult Comodi()
        {
            short _lang = 1;

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
                _lang = 2;

            var timeline = Repository.GetHomeTimeline(DateTime.Now, _lang, "", 15, 0, 0).ToString();
            Int64 counter = Repository.GetHomeTimelineCounter(DateTime.Now, _lang, "");

            HomePage home_page = new HomePage();
            home_page.Timeline = timeline;
            home_page.Counter = counter;

            return View((object)home_page);
        }

        public ActionResult Prova_js()
        {
            return View();
        }


    }
}
