﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace LikeKitWebsite.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View();
        }

        // GET: /Error/HttpError404 
        public ActionResult HttpError404(string message)
        {
            message = "culture : ";
            if (HttpContext.Session["culture"] != null)
                message = HttpContext.Session["culture"].ToString();

            String culture = "es";
            if (HttpContext.Session["culture"] != null)
                culture = HttpContext.Session["culture"].ToString();

            CultureInfo ci = CultureInfo.GetCultureInfo(culture);

            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
           
            return View("Error", (object)message);
        }


        // GET: /Error/HttpError500 
        public ActionResult HttpError500(string message)
        {
            return View("Error", (object)message);
        }


        // GET: /Error/General 
        public ActionResult General(string message)
        {
            return View("Error", (object)message);
        }     
    }
}
