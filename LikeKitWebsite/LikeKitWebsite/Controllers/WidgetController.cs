﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LikeKitWebsite.Models;
using System.Data;
using LikeKitWebsite.Repository;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Web.Helpers;

namespace LikeKitWebsite.Controllers
{
    public class WidgetController : EController<TimeLineRepository>
    {
        //
        // GET: /Widget/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Data(String id)
        {
            UsersRepository usrRP = new UsersRepository(Repository.Database);

            User user = usrRP.GetUserByUserToken(id);

            UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);
            user.NumFollowers = urRP.GetNumUserRelations(user.ID, 2);

            Widget widget = new Widget();
            widget.UserID = user.ID;
            widget.UserPicture = user.Picture;
            widget.UserFirstName = user.FirstName;
            widget.UserLastName = user.LastName;
            widget.UserNumFollowers = user.NumFollowers;
            widget.HtmlLastKits = Repository.WidgetLastKits(user.ID);


            return View(widget);
        }
    }
}
