﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LikeKitWebsite.Repository;

namespace LikeKitWebsite.Controllers
{
    [HandleError()]
    public abstract class EController<T> : Controller where T : new()
    {
        private T _repository;

        public T Repository { get {return (T)_repository;} }

        public EController()
        {
            _repository = new T();

            ((BaseRepository)((object)_repository)).OpenDatabase();
        }

        protected override void Dispose(bool disposing)
        {
            ((BaseRepository)((object)_repository)).CloseDatabase();
            base.Dispose(disposing);
        }
    }
}