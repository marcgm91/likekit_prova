﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LikeKitWebsite.Models;
using System.Data;
using LikeKitWebsite.Repository;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Web.Helpers;
using System.Web.Hosting;

namespace LikeKitWebsite.Controllers
{
    public class TimelineController : EController<TimeLineRepository>
    {
        /*protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // If session exists
            if (filterContext.HttpContext.Session != null)
            {
                //if new session
                if (filterContext.HttpContext.Session.IsNewSession)
                {
                    string cookie = filterContext.HttpContext.Request.Headers["Cookie"];

                    //if cookie exists and sessionid index is greater than zero

                    if ((cookie != null) && (cookie.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        //redirect to desired session 
                        //expiration action and controller
                        filterContext.Result = RedirectToAction("LogOn", "Account");
                        return;
                    }
                }
            }

            //otherwise continue with action
            base.OnActionExecuting(filterContext);
        }*/

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            /*String culture = "es";

            if (HttpContext.Session["culture"] != null)
                culture = HttpContext.Session["culture"].ToString();

            CultureInfo ci = CultureInfo.GetCultureInfo(culture);

            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;*/

            String culture = "es";

            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
            {
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

                User user = Repository.GetUser(ID);

                if (user != null)
                {
                    //RedirectToAction("LogOn", "Account");

                    if (HttpContext.Session["culture"] != null)
                        culture = HttpContext.Session["culture"].ToString();
                    else if (user.Language != null)
                    {
                        if (user.Language.ID == 2)
                            culture = "ca";
                        else
                            culture = "es";
                    }
                    else
                    {
                        //Primer mirem el lang del navegador
                        if (Request != null)
                        {
                            var l = Request.UserLanguages;
                            string browserlanguage = l[0];

                            HttpContext.Session["culture"] = browserlanguage;
                        }
                        else
                        {
                            String address = Request.ServerVariables["REMOTE_ADDR"];
                            culture = Repository.GetIPCulture(address);
                            HttpContext.Session["culture"] = culture;
                        }
                    }

                    CultureInfo ci = CultureInfo.GetCultureInfo(culture);

                    System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
                }
            }
        }


        //
        // GET: /Timeline/

        public ActionResult SetLanguage(string lang)
        {
            HttpContext.Session["culture"] = lang;

            return RedirectToAction("Index", "Timeline");
        }

        [Authorize]
        public ActionResult Index()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            //Default filter, FOLLOWING
            var timeline = Repository.GetTimeline(DateTime.Now, UserID, "", "false,true,false,false,false,false,false,false,false,false,false,false", "").ToString();

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)timeline);
        }

        [Authorize]
        public ActionResult Item(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Kit kit = Repository.GetKit(id, UserID);

            //Carreguem els tags
            KitsRepository kRP = new KitsRepository(Repository.Database);
            kit.HtmlTags = kRP.GetHtmlKitTags(kit.ID);
            kit.StringTags = kRP.GetStringKitTags(kit.ID);

            kit.Comment = LikeKitWebsite.Globals.HtmlEncodingLinks(kit.Comment);

            switch (kit.Product.ProductCategory.ID)
            {
                case 1:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIBROS_MIN;
                    break;
                case 2:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SERIES_MIN;
                    break;
                case 3:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CINE_MIN;
                    break;
                case 4:
                    kit.CategoryName = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUSICA_MIN;
                    break;
            }

            //Limitem la descripció del producte a 400 caràcters
            kit.Product.Description = Globals.WordCut(kit.Product.Description, 400, new char[] { ' ' });

            JsonKitValoration midValues = new JsonKitValoration();
            midValues = kRP.GetKitMidValoration(kit.Product.ID);

            kit.MidValoration = midValues.MidValoration;
            kit.NumKitsMidValoration = midValues.NumKitsMidValoration;

            UserLikesRepository ulRP = new UserLikesRepository(Repository.Database);
            Int64 ul = ulRP.Exists(UserID, id);

            kit.isLike = false;
            if (ul > 0)
                kit.isLike = true;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            //Mirem si podem publicar a twitter i/o facebook
            if (UserID > 0)
            {
                User user = Repository.GetUser(UserID);


                kit.canTwitterPublish = false;
                if (user.TwitterID != "")
                    kit.canTwitterPublish = true;

                kit.canFacebookPublish = false;
                if (user.FacebookID != "")
                    kit.canFacebookPublish = true;

                //Guardem l'AccessLog
                AccessLog al = new AccessLog();
                AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

                al.DeviceType = DeviceType.Web;
                al.ObjectType = AccesLogObjectType.Kit;
                al.OperationType = OperationType.View;
                al.User = user;
                al.RefID = id;
                al.IP = String.Empty;
                al.SessionID = String.Empty;
                al.Observations = String.Empty;

                alRP.CreateAccessLog(al);
            }

            //Textos de twitter i facebook
            //String strURL = Endepro.Common.Common.MakeTinyUrl(ConfigurationManager.AppSettings["MainURL"] + "/Timeline/Item/" + kit.ID);
            //strURL = strURL.Replace("//", "/");

            if (UserID == kit.User.ID)
            {
                kit.TwitterText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_TWITTER_TEXT, kit.Product.Name);
                kit.FacebookText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_FACEBOOK_TEXT);
                kit.MailText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_MAIL_TEXT, kit.Product.Name);
            }
            else
            {
                kit.TwitterText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_TWITTER_TEXT_OTHER, kit.Product.Name);
                kit.FacebookText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_FACEBOOK_TEXT_OTHER);
                kit.MailText = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_MAIL_TEXT_OTHER, kit.Product.Name);            
            }

            kit.LoquieroHTML = kRP.GetLoquieroHTML(kit.Product.ID, kit.User.ID);
            //kit.LoquieroButtonHTML = kRP.GetLoquieroHTML(kit.Product.ID, kit.User.ID);


            return View(kit);
        }

        [Authorize]
        public ActionResult Help()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String Result = String.Empty;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)Result);
        }

        [Authorize]
        public ActionResult MediaKit()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String Result = String.Empty;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)Result);
        }

        [Authorize]
        public ActionResult Team()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String Result = String.Empty;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)Result);
        }

        [Authorize]
        public ActionResult Legal()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String Result = String.Empty;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)Result);
        }

        [Authorize]
        public ActionResult Profile(Int64 id)
        {
            User user = null;
            String test = String.Empty;
            try
            {
                user = Repository.GetUser(id);

                Int64 SessionUserID = 0;
                if (Session[Globals.SessionUserID] != null)
                    SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());
                //Omplim les dades dels Kits separats per categories;
                user.ProfileCategoryKits = Repository.GetProfileCategoryKits(id, SessionUserID);

                UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);
                user.NumFollowers = urRP.GetNumUserRelations(id, 2);
                user.NumFollowings = urRP.GetNumUserRelations(id, 1);
                
                KitsRepository kRP = new KitsRepository(Repository.Database);
                user.NumKits = kRP.GetUserNumKits(id);
                user.NumLikes = kRP.GetNumLikes(id);
                //user.NumReKits = kRP.GetUserNumReKits(id);
                //user.NumComments = kRP.GetNumUserComments(id);

                user.NumLikesToUser = kRP.GetNumLikesToUser(id);
                user.NumReKitsToUser = kRP.GetUserNumReKitsToUser(id);
                user.NumCommentsToUser = kRP.GetNumUserCommentsToUser(id);


                user.NumTrenderPoints = kRP.GetNumTrenderPoints();

                user.NumUserPoints = user.NumKits + user.NumLikesToUser + user.NumReKitsToUser + user.NumCommentsToUser;

                user.ProfileFollowers = urRP.GetHtmlProfileRelations(id, 2, SessionUserID);
                user.ProfileFollowings = urRP.GetHtmlProfileRelations(id, 1, SessionUserID);

                UserStatusRepository usRP = new UserStatusRepository(Repository.Database);
                user.ProfileNowWith = usRP.GetHtmlProfileNowWith(id, SessionUserID);
                user.ProfileUsersLikeUser = Repository.GetUsersLikeUserHtml(id, SessionUserID);

                user.isFollowing = urRP.isFollower(SessionUserID, id);

                user.FirstNameUpper = user.FirstName.ToUpper();

                //Recalculem el número d'interaccions
                GetNumUserInteractions();

            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "Profile " + test);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
            }
            return View(user);
        }

        [Authorize]
        public ActionResult EditProfile(Int64 id)
        {
            User user = Repository.GetUser(id);

            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            if (SessionUserID != id)
                return RedirectToAction("Profile/" + id, "Timeline");

            //Omplim les dades dels Kits separats per categories;
            user.ProfileCategoryKits = Repository.GetProfileCategoryKits(id, SessionUserID);

            UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);

            user.NumFollowers = urRP.GetNumUserRelations(id, 2);
            user.NumFollowings = urRP.GetNumUserRelations(id, 1);

            KitsRepository kRP = new KitsRepository(Repository.Database);
            user.NumKits = kRP.GetUserNumKits(id);
            user.NumLikes = kRP.GetNumLikes(id);

            user.ProfileFollowers = urRP.GetHtmlProfileRelations(id, 2, SessionUserID);
            user.ProfileFollowings = urRP.GetHtmlProfileRelations(id, 1, SessionUserID);

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);
            user.ProfileNowWith = usRP.GetHtmlProfileNowWith(id, SessionUserID);

            user.isFollowing = urRP.isFollower(SessionUserID, id);

            user.FirstNameUpper = user.FirstName.ToUpper();

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            //Permisos
            user.Perm1 = Globals.GetPerms(user.Perms, (int)UserPerms.NoNotificacions);
            user.Perm2 = Globals.GetPerms(user.Perms, (int)UserPerms.TwitterPublish);
            user.Perm3 = Globals.GetPerms(user.Perms, (int)UserPerms.FacebookPublish);
            user.Perm4 = Globals.GetPerms(user.Perms, (int)UserPerms.KitComment);
            user.Perm5 = Globals.GetPerms(user.Perms, (int)UserPerms.KitLike);
            user.Perm6 = Globals.GetPerms(user.Perms, (int)UserPerms.ReKit);
            user.Perm7 = Globals.GetPerms(user.Perms, (int)UserPerms.Follow);
            user.Perm8 = Globals.GetPerms(user.Perms, (int)UserPerms.RecieveRecommendations);
            user.Perm9 = Globals.GetPerms(user.Perms, (int)UserPerms.LikekitNews);

            if(user.Perm1 == true)
                user.Perm1Css = " selected";
            if (user.Perm2 == true)
                user.Perm2Css = " selected";
            if (user.Perm3 == true)
                user.Perm3Css = " selected";
            if (user.Perm4 == true)
                user.Perm4Css = " selected";
            if (user.Perm5 == true)
                user.Perm5Css = " selected";
            if (user.Perm6 == true)
                user.Perm6Css = " selected";
            if (user.Perm7 == true)
                user.Perm7Css = " selected";
            if (user.Perm8 == true)
                user.Perm8Css = " selected";
            if (user.Perm9 == true)
                user.Perm9Css = " selected";

            user.GenderString = "";
            if(user.Gender == UserGender.Hombre)
                user.GenderString = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HOMBRE;
            else if (user.Gender == UserGender.Mujer)
                user.GenderString = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUJER;

            user.WidgetURL = String.Format("{0}Widget/Data/{1}", ConfigurationManager.AppSettings["MainURL"], user.UserToken);

            return View(user);
        }

        [Authorize]
        public ActionResult UpdateProfile(String email, String password, String firstname, String lastname, String address, String web, String about, String birthday, String gender, String perms, String language)
        {
            String test = String.Empty;
            try
            {
                test = "1";
                Int64 ID = 0;
                if (Session[Globals.SessionUserID] != null)
                    ID = Int64.Parse(Session[Globals.SessionUserID].ToString());
                test = "2";
                User user = Repository.GetUser(ID);
                test = "3";
                user.FirstName = firstname;
                user.LastName = lastname;
                user.Address = address;

                Int64 LanguageID = 1;

                if (language != "")
                    LanguageID = Int64.Parse(language);

                //Language
                Language _lang = new Language
                {
                    ID = LanguageID
                };
                user.Language = _lang;

                String culture_tmp = "es";
                if (user.Language != null)
                {
                    if (user.Language.ID == 2)
                        culture_tmp = "ca";
                    else
                        culture_tmp = "es";

                    HttpContext.Session["culture"] = culture_tmp;

                    CultureInfo ci = CultureInfo.GetCultureInfo(culture_tmp);

                    System.Threading.Thread.CurrentThread.CurrentCulture = ci;
                    System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
                }

                user.Web = web;

                if (about.Length > 100)
                    about = about.Substring(0, 100);

                user.About = about;
                user.Perms = Int64.Parse(perms);
                test = "4";
                IFormatProvider culture = new CultureInfo("es-ES", true);

                if (birthday != String.Empty)
                    user.Birthday = DateTime.ParseExact(birthday, "dd/MM/yyyy", culture);

                if(gender == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_HOMBRE)
                    user.Gender = UserGender.Hombre;
                else if (gender == LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUJER)
                    user.Gender = UserGender.Mujer;
                else
                    user.Gender = UserGender.Hombre;

                test = "5";

                UsersRepository usRP = new UsersRepository(Repository.Database);
                usRP.UpdateUser(user);

                if (password != "")
                {
                    //String PasswordHash = Endepro.Common.Crypto.GetMD5Hash(String.Format("{0}", password));
                    //PasswordHash = PasswordHash.Substring(0, 8);
                    String Password = Endepro.Common.Crypto.GetMD5Hash(password);
                    user.Password = Password;

                    usRP.UpdateUserPassword(user);
                }
                test = "6";
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "UpdateProfile " + test);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
            }

            String Result = "Success";

            return PartialView("Html", (object)Result);
        }

        [Authorize]
        public ActionResult getProfileStatus()
        {
            String sHtml = String.Empty;

            try
            {
                Int64 ID = 0;
                if (Session[Globals.SessionUserID] != null)
                    ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

                User user = Repository.GetUser(ID);

                sHtml = "<div class=\"profile-stats-text\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_TU_PERFIL_ESTA_A_UN + " " + user.ProfilePercentage + "%</div>\n" +
                "<div class=\"profile-stats-gauge\">\n" +
                    "<div class=\"profile-stats-gauge-value\" style=\"width:" + user.ProfilePercentageWidth + "px;\"></div>\n" +
                "</div>\n";
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "getProfileStatus");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.EditProfile, 1);
            }

            return PartialView("Html", (object)sHtml);
        }

        
        [Authorize]
        public ActionResult UpdateProfileImage()
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            String photo_name = String.Empty;
            String photo_name_destination = String.Empty;

            if (Request.Files[0].FileName != string.Empty)
            {
                string strFileName = Path.GetFileName(Request.Files[0].FileName);
                string strExtension = Path.GetExtension(Request.Files[0].FileName).ToLower();

                String upload_path = ConfigurationManager.AppSettings["PathImages"];
                String Key = Globals.GetMD5(String.Format("{0}{1}", user.UserID, DateTime.UtcNow));

                photo_name = String.Format("{0}{1}", Key, strExtension);

                String sFile = Globals.PathCombine(upload_path, photo_name);
                Request.Files[0].SaveAs(sFile);

                UsersRepository usRP = new UsersRepository(Repository.Database);
                usRP.UpdateOriginalImage(ID, photo_name);                

                //Ara hauriem de mirar el tema de les mides, i guardar-ho a la 'Picture' (300x300)
                /*System.Drawing.Image OriginalImage = System.Drawing.Image.FromFile(sFile);

                long new_width = 300;
                long new_height = 0;

                //max 300 x 300
                if (OriginalImage.Width <= new_width)
                {
                    new_width = OriginalImage.Width;
                }

                new_height = OriginalImage.Height * new_width / OriginalImage.Width;

                if (new_height > 300)
                {
                    new_height = 300;
                    new_width = OriginalImage.Width * new_height / OriginalImage.Height;
                }*/

                String KeyDestination = Globals.GetMD5(String.Format("Crop{0}{1}", user.UserID, DateTime.UtcNow));

                photo_name_destination = String.Format("{0}{1}", KeyDestination, strExtension);

                String sDestinationImage = Globals.PathCombine(upload_path, photo_name_destination);

                Globals.ImageCropCenter(sFile, sDestinationImage, 300, 300);
                usRP.UpdateImage(ID, photo_name_destination);                
            }

            String Result = photo_name_destination;
            return PartialView("Html", (object)Result);

            /*
            String Result = "Success";
            return PartialView("Html", (object)Result);*/
        }

        public void GetImageThumbnail(String image, int width, int height)
        {
            if (image != String.Empty)
            {
                String upload_path = ConfigurationManager.AppSettings["PathImages"];
                String sFile = Globals.PathCombine(upload_path, image);

                new WebImage(sFile)
                        .Resize(width, height, true, true) // Resizing the image to 100x100 px on the fly...
                        .Crop(1, 1) //Cropping it to remove 1px border at top and left sides (bug in WebImage)
                        .Write();
            }
            else
                new WebImage(HostingEnvironment.MapPath(@"~/Content/images/default_user.png")).Write();
        }

        [Authorize]
        public ActionResult CropProfileImage(int x, int y)
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            String photo_name = user.OriginalPicture;
            String photo_name_destination = String.Empty;

            if (photo_name != string.Empty)
            {
                string strFileName = Path.GetFileName(photo_name);
                string strExtension = Path.GetExtension(photo_name).ToLower();

                String upload_path = ConfigurationManager.AppSettings["PathImages"];

                //String sFile = Globals.PathCombine(upload_path, photo_name_destination);

                String KeyDestination = Globals.GetMD5(String.Format("Crop{0}{1}", user.UserID, DateTime.UtcNow));
                photo_name_destination = String.Format("{0}{1}", KeyDestination, strExtension);
                String sDestinationImage = Globals.PathCombine(upload_path, photo_name_destination);

                //Original Picture
                String OriginalFile = Globals.PathCombine(upload_path, photo_name);
                System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFile);

                System.Drawing.Image ResizedImage = Globals.ResizeImage(FullsizeImage, 350, FullsizeImage.Height, true);
                //Ara hem de fer el crop de la imatge d'amplada 350, que és el que hem fet amb el JCrop.
                System.Drawing.Image FinalImage = Globals.ImageCropXY(ResizedImage, 220, 220, x, y);
                FinalImage.Save(sDestinationImage);

                UsersRepository usRP = new UsersRepository(Repository.Database);
                usRP.UpdateImage(ID, photo_name_destination);

                FullsizeImage.Dispose();
                ResizedImage.Dispose();
                FinalImage.Dispose();

            }

            String Result = photo_name_destination;
            return PartialView("Html", (object)Result);
        }

        [Authorize]
        public ActionResult GetOriginalImage()
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            String Result = user.OriginalPicture;

            return PartialView("Html", (object)Result);
        }

        [Authorize]
        public ActionResult Interactions(Int64 id)
        {
            if (Session[Globals.SessionUserID] != null && Int64.Parse(Session[Globals.SessionUserID].ToString()) != id)
                return RedirectToAction("Index", "Timeline");

            User user = Repository.GetUser(id);

            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            UsersRepository uRP = new UsersRepository(Repository.Database);
            String interactions = uRP.GetUserInteraction(id);

            //Ara les marquem com a llegides
            Repository.UpdateReadUserNotifies(SessionUserID);

            return View((object)interactions);
        }

        [Authorize]
        public ActionResult Wishlist(Int64 id)
        {
            User user = Repository.GetUser(id);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            WishListsRepository wlRP = new WishListsRepository(Repository.Database);

            WishList wl = new WishList();

            wl.HtmlHeaderUserImage = user.Picture;
            wl.HtmlHeaderUserName = String.Format("{0} {1}", user.FirstName, user.LastName);
            wl.HtmlOutput = wlRP.GetHtmlWishList(user.ID,UserID).ToString();

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)wl);
        }

        /*public JsonResult SearchProduct(string bc, int limit)
        {
            //Product query by name
            return Json(Repository.SearchProducts(bc, limit), JsonRequestBehavior.AllowGet);
        }*/


        public ActionResult SearchProduct(String search, String categories)
        {
            var products = Repository.SearchProductsKitHtml(search, categories, 15);

            AccessLog al = new AccessLog();
            AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = new User
            {
                ID = UserID
            };

            //De moment no afegim la cerca, ja que genera moltes entrades
            /*al.DeviceType = DeviceType.Web;
            al.ObjectType = AccesLogObjectType.Kit;
            al.OperationType = OperationType.SearchProduct;
            al.User = user;
            al.RefID = 0;
            al.IP = String.Empty;
            al.SessionID = String.Empty;
            al.Observations = search;

            alRP.CreateAccessLog(al);

            //No hem trobat resultats
            if (products.ToString() == String.Empty)
            {
                al.DeviceType = DeviceType.Web;
                al.ObjectType = AccesLogObjectType.Kit;
                al.OperationType = OperationType.ProductNotFound;
                al.User = user;
                al.RefID = 0;
                al.IP = String.Empty;
                al.SessionID = String.Empty;
                al.Observations = search;

                alRP.CreateAccessLog(al);
            }*/

            return PartialView("Html", (object)products);
        }

        public ActionResult WSSearchProduct(String search, String categories)
        {
            var products = Repository.WSSearchProductsKitHtml(search, categories, 20);

            AccessLog al = new AccessLog();
            AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = new User
            {
                ID = UserID
            };
            
            String str_result = "<div class=\"timeline-kit-search-result-noresults-title\">" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_SEARCH_NORESULT + "</div>";
            //No hem trobat resultats
            if (products.ToString() == str_result)
            {
                al.DeviceType = DeviceType.Web;
                al.ObjectType = AccesLogObjectType.Kit;
                al.OperationType = OperationType.ProductNotFound;
                al.User = user;
                al.RefID = 0;
                al.IP = String.Empty;
                al.SessionID = String.Empty;
                al.Observations = String.Empty;

                alRP.CreateAccessLog(al);
            }

            return PartialView("Html", (object)products);
        }

        public ActionResult NowWithSearchProduct(String search, String categories)
        {
            var products = Repository.SearchProductsNowWithHtml(search, categories, 15);
            return PartialView("Html", (object)products);
        }

        public ActionResult NowWithWSSearchProduct(String search, String categories)
        {
            var products = Repository.WSSearchProductsNowWithHtml(search, categories, 20);
            return PartialView("Html", (object)products);
        }

        public ActionResult WishListSearchProduct(String search, String categories)
        {
            var products = Repository.SearchProductsWishListHtml(search, categories, 15);
            return PartialView("Html", (object)products);
        }

        public ActionResult WishListWSSearchProduct(String search, String categories)
        {
            var products = Repository.WSSearchProductsWishListHtml(search, categories, 20);
            return PartialView("Html", (object)products);
        }

        public ActionResult LoadKitProductData(Int64 id)
        {
            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            var product_data = pRP.KitProductDataHtml(id);
            return PartialView("Html", (object)product_data);
        }

        public ActionResult LoadWSKitProductData(String ASIN, String Title, String Author, String Year, String Category, String LargeImage, String URL, String Description, String EAN, String Observations)
        {
            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            var product_data = pRP.KitWSProductDataHtml(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations);
            return PartialView("Html", (object)product_data);
        }

        public ActionResult AddToWishList(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());
            Repository.AddToWishList(id, userid);

            String Message = String.Empty;
            //Message = "Success";
            Message = String.Format("{0}", userid);

            return PartialView("Html", (object)Message);
        }

        public ActionResult TimelineSearch(String Search, String filter, String tags)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            var timeline = Repository.GetTimeline(DateTime.Now,UserID, Search, filter, tags).ToString();
            return PartialView("Html", (object)timeline);
        }

        public ActionResult TimelineSearchPager(String date, Int16 start, Int16 limit, String Search, String filter, String tags)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            //FIX
            if (Search == "Buscar recomanacions")
                Search = "";
            if (tags == "Afegeix etiquetes per millorar la teva recerca")
                tags = "";

            try
            {
                //Fem una conversió de la data
                var timeline = Repository.GetTimeline(DateTime.Now, UserID, Search, filter, tags, limit, start).ToString();
                return PartialView("Html", (object)timeline);
            }
            catch
            {
                String sError = String.Format("<div class=\"timeline-entry\">Error</div>");
                return PartialView("Html", (object)sError);
            }

        }

        public ActionResult AddKitComment(Int64 KitID, String Comment)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            KitCommentsRepository kcR = new KitCommentsRepository(Repository.Database);

            KitComment kc = new KitComment();
            kc.Kit = new Kit();
            kc.Kit.ID = KitID;
            kc.User = new User();
            kc.User.ID = UserID;
            kc.Comment = Comment;
            kc.Valoration = (Double)0;
            kc.Visible = true;

            Int64 comment_id = kcR.CreateKitComment(kc);


            //Mirem el kit
            KitsRepository ktRP = new KitsRepository(Repository.Database);
            Kit kit = ktRP.GetKit(KitID, UserID);

            //Creem la notificació pels usuaris que també han fet commentaris d'aquest kit
            Repository.CommentedCommentsNotifies(KitID, UserID);

            //Només creem la notificació i el correu si el creador del kit no és l'usuari que fa el comentari
            if (kit.User.ID != UserID)
            {
                //Creem la notificació
                Repository.CreateNotify(kit.User.ID, UserID, comment_id, NotifyType.Comment);

                //Mirem si li enviem el correu.
                User user = Repository.GetUser(kit.User.ID);

                user.Perm4 = Globals.GetPerms(user.Perms, (int)UserPerms.KitComment);

                if (user.Perm4 == true)
                {
                    Repository.KitCommentMail(kit.User.ID, UserID, KitID);
                }
            }

            KitsRepository kR = new KitsRepository(Repository.Database);

            //var comments = kR.GetHtmlKitComments(KitID);
            var comments = kR.GetHtmlKitComments(KitID, UserID);
            return PartialView("Html", (object)comments);
        }

        public ActionResult DelKitComment(Int64 KitID, Int64 KitCommentID)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            KitCommentsRepository kcR = new KitCommentsRepository(Repository.Database);

            KitComment kc = new KitComment();
            kc.ID = KitCommentID;
            kcR.DeleteKitComment(kc);

            KitsRepository kR = new KitsRepository(Repository.Database);

            Repository.DeleteRelatedNotify(KitCommentID, NotifyType.Comment);

            //var comments = kR.GetHtmlKitComments(KitID);
            var comments = kR.GetHtmlKitComments(KitID, UserID);
            return PartialView("Html", (object)comments);
        }

        public ActionResult UnFollow(Int64 UserID)
        {
            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String message = "";

            if (SessionUserID > 0)
            {
                UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
                Int64 relation_id = _urRP.getRelationID(SessionUserID, UserID);

                _urRP.UnFollow(SessionUserID, UserID);

                Repository.DeleteRelatedNotify(relation_id, NotifyType.Follow);

                message = "Success";
            }
            else
                message = "Error";


            return PartialView("Html", (object)message);
        }

        public ActionResult Follow(Int64 UserID)
        {
            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String message = "";

            if (SessionUserID > 0)
            {
                UserRelation ur = new UserRelation();
                ur.User = new User();
                ur.User.ID = SessionUserID;
                ur.Follow = new User();
                ur.Follow.ID = UserID;

                UserRelationsRepository _urRP = new UserRelationsRepository(Repository.Database);
                Int64 follow_id = _urRP.CreateUserRelation(ur);

                //Creem la notificació
                Repository.CreateNotify(UserID, SessionUserID, follow_id, NotifyType.Follow);

                //Mirem si li enviem el correu.
                User user = Repository.GetUser(UserID);

                user.Perm7 = Globals.GetPerms(user.Perms, (int)UserPerms.Follow);

                if (user.Perm7 == true)
                {
                    Repository.FollowMail(UserID, SessionUserID);
                }

                message = "Success";
            }
            else
                message = "Error";

            return PartialView("Html", (object)message);
        }

        public ActionResult AddKit(Int64 product_id, String comment, String tags, String recommended_tags, Int64 ref_kit, String valoration)
        {
            Int64 kit_id = 0;

            try
            {

                Int64 UserID = 0;
                if (Session[Globals.SessionUserID] != null)
                    UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

                KitsRepository kRP = new KitsRepository(Repository.Database);
                Kit kit = new Kit();
                kit.Comment = comment;
                kit.Product = new Product();
                kit.Product.ID = product_id;
                kit.User = new User();
                kit.User.ID = UserID;
                if (valoration == "")
                    valoration = "1";
                kit.Valoration = Double.Parse(valoration);

                if (ref_kit > 0)
                {
                    kit.ParentKit = new Kit();
                    kit.ParentKit.ID = ref_kit;
                }

                kit.Visible = true;

                kit_id = kRP.CreateKit(kit);

                if (ref_kit > 0)
                {
                    Kit Rekit = kRP.GetKit(ref_kit, UserID);
                    //Creem la notificació
                    Repository.CreateNotify(Rekit.User.ID, UserID, kit_id, NotifyType.ReKit);

                    //Mirem si li enviem el correu.
                    User user = Repository.GetUser(Rekit.User.ID);

                    user.Perm6 = Globals.GetPerms(user.Perms, (int)UserPerms.ReKit);

                    if (user.Perm6 == true)
                    {
                        Repository.ReKitMail(Rekit.User.ID, UserID, kit_id);
                    }
                }

                //Ara hem d'afegir els tags.
                //Recorrem els productes seleccionats
                string[] tokensizedStrs;
                tokensizedStrs = tags.Split(new char[] { ',' });

                int len = tokensizedStrs.Length;

                for (int i = 0; i < len; i++)
                {
                    string tmp = tokensizedStrs[i];

                    if (tmp != String.Empty)
                    {
                        KitTag kittag = new KitTag();
                        kittag.Kit = new Kit();
                        kittag.Kit.ID = kit.ID;
                        kittag.Name = tmp;

                        KitTagsRepository ktRP = new KitTagsRepository(Repository.Database);
                        ktRP.CreateKitTag(kittag);
                    }
                }

                //Recommended Tags
                string[] tokensizedStrsRecommended;
                tokensizedStrsRecommended = recommended_tags.Split(new char[] { ',' });

                int lenRecommended = tokensizedStrsRecommended.Length;

                for (int i = 0; i < lenRecommended; i++)
                {
                    string tmp = tokensizedStrsRecommended[i];

                    if (tmp != String.Empty)
                    {
                        KitTag kittag = new KitTag();
                        kittag.Kit = new Kit();
                        kittag.Kit.ID = kit.ID;
                        kittag.Name = tmp;

                        KitTagsRepository ktRP = new KitTagsRepository(Repository.Database);
                        ktRP.CreateKitTag(kittag);
                    }
                }

                //Guardem l'Access Log si no és un rekit
                if (ref_kit == 0)
                {
                    AccessLog al = new AccessLog();
                    AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

                    //Busquem l'entrada corresponent on s'ha començat a fer la cerca, per tancar-la
                    al = alRP.GetLastAccessLogFromUser(UserID, OperationType.OpenKitForm);

                    if (al != null)
                    {
                        al.Published = DateTime.Now;
                        alRP.UpdateAccessLog(al);
                    }
                }

                User user_tmp = Repository.GetUser(UserID);
                String strURL = String.Empty;

                strURL = ConfigurationManager.AppSettings["MainURL"] + "Home/Item/" + kit_id;
                strURL = Endepro.Common.Common.MakeTinyUrl(strURL);

                ProductsRepository pR = new ProductsRepository(Repository.Database);
                kit.Product = pR.GetProduct(kit.Product.ID);

                //Ara mirem si hi ha permisos per fer un tweet
                if (Globals.GetPerms(user_tmp.Perms, (int)UserPerms.TwitterPublish) == true)
                {
                    comment = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_TWITTER_TEXT, kit.Product.Name);

                    //110 per posar la url
                    if (comment.Length > 110)
                        comment = comment.Substring(0, 110);

                    comment = comment + " " + strURL;

                    Repository.TwitterPublish(user_tmp, comment);
                }

                if (Globals.GetPerms(user_tmp.Perms, (int)UserPerms.FacebookPublish) == true)
                {
                    User user = Repository.GetUser(UserID);

                    String ProductImage = kit.Product.Image;
                    comment = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_FACEBOOK_TEXT);

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!System.IO.File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/images/default_product.png";
                    else
                        ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/files/" + ProductImage;

                    //ProductImage = ConfigurationManager.AppSettings["MainURL"] + ProductImage;
                    Repository.FacebookPublish(user_tmp, comment, strURL, kit.Product.Name, "Likekit", ProductImage, kit.Product.Name);

                }

                String Message = "Ok";
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "AddKit ");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.InsertKit, 1);
            }

            return PartialView("Html", (object)kit_id.ToString());
        }


        public ActionResult AddWSKit(String amazon_asin, String amazon_title, String amazon_author, String amazon_year, String amazon_category, String amazon_image, String amazon_url, String comment, String tags, String recommended_tags, String valoration, String amazon_description, String amazon_ean, String amazon_observations)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            Int64 kit_id = 0;

            try
            {
                Int64 product_id = 0;

                //Mirem que no existeixi el producte
                product_id = pRP.WSProductExists(amazon_asin);
                if(product_id == 0)
                    product_id = pRP.CreateWSProduct(amazon_asin,amazon_title,amazon_author,amazon_year,amazon_category,amazon_image,amazon_url,amazon_description,amazon_ean,amazon_observations);

                KitsRepository kRP = new KitsRepository(Repository.Database);
                Kit kit = new Kit();
                kit.Comment = comment;
                kit.Product = new Product();
                kit.Product.ID = product_id;
                kit.User = new User();
                kit.User.ID = UserID;

                kit.Visible = true;
                if (valoration == "")
                    valoration = "1";
                kit.Valoration = Double.Parse(valoration);
                kit_id = kRP.CreateKit(kit);

                //Ara hem d'afegir els tags.
                //Recorrem els productes seleccionats
                string[] tokensizedStrs;
                tokensizedStrs = tags.Split(new char[] { ',' });

                int len = tokensizedStrs.Length;

                for (int i = 0; i < len; i++)
                {
                    string tmp = tokensizedStrs[i];

                    if (tmp != String.Empty)
                    {
                        KitTag kittag = new KitTag();
                        kittag.Kit = new Kit();
                        kittag.Kit.ID = kit.ID;
                        kittag.Name = tmp;

                        KitTagsRepository ktRP = new KitTagsRepository(Repository.Database);
                        ktRP.CreateKitTag(kittag);
                    }
                }

                //Recommended Tags
                string[] tokensizedStrsRecommended;
                tokensizedStrsRecommended = recommended_tags.Split(new char[] { ',' });

                int lenRecommended = tokensizedStrsRecommended.Length;

                for (int i = 0; i < lenRecommended; i++)
                {
                    string tmp = tokensizedStrsRecommended[i];

                    if (tmp != String.Empty)
                    {
                        KitTag kittag = new KitTag();
                        kittag.Kit = new Kit();
                        kittag.Kit.ID = kit.ID;
                        kittag.Name = tmp;

                        KitTagsRepository ktRP = new KitTagsRepository(Repository.Database);
                        ktRP.CreateKitTag(kittag);
                    }
                }

                //Guardem l'Access Log
                AccessLog al = new AccessLog();
                AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

                //Busquem l'entrada corresponent on s'ha començat a fer la cerca, per tancar-la
                al = alRP.GetLastAccessLogFromUser(UserID, OperationType.OpenKitForm);
            
                if (al != null)
                {
                    al.Published = DateTime.Now;
                    alRP.UpdateAccessLog(al);
                }

                User user_tmp = Repository.GetUser(UserID);
                String strURL = String.Empty;

                strURL = ConfigurationManager.AppSettings["MainURL"] + "Home/Item/" + kit_id;
                strURL = Endepro.Common.Common.MakeTinyUrl(strURL);

                kit.Product = pRP.GetProduct(kit.Product.ID);

                //Ara mirem si hi ha permisos per fer un tweet
                if (Globals.GetPerms(user_tmp.Perms, (int)UserPerms.TwitterPublish) == true)
                {
                    comment = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_TWITTER_TEXT, kit.Product.Name);

                    //110 per posar la url
                    if (comment.Length > 110)
                        comment = comment.Substring(0, 110);

                    comment = comment + " " + strURL;

                    Repository.TwitterPublish(user_tmp, comment);
                }

                if (Globals.GetPerms(user_tmp.Perms, (int)UserPerms.FacebookPublish) == true)
                {
                    User user = Repository.GetUser(UserID);

                    String ProductImage = kit.Product.Image;
                    comment = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_FACEBOOK_TEXT);

                    if (ProductImage != "")
                    {
                        String img_path = ConfigurationManager.AppSettings["PathImages"];
                        String sFile = Globals.PathCombine(img_path, ProductImage);

                        if (!System.IO.File.Exists(sFile))
                            ProductImage = "";
                    }

                    if (ProductImage == "")
                        ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/images/default_product.png";
                    else
                        ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/files/" + ProductImage;

                    //ProductImage = ConfigurationManager.AppSettings["MainURL"] + ProductImage;
                    Repository.FacebookPublish(user_tmp, comment, strURL, kit.Product.Name, "Likekit", ProductImage, kit.Product.Name);

                }

                String Message = "Ok";
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "AddWSKit ");
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.InsertKit, 1);
            }

            return PartialView("Html", (object)kit_id.ToString());
        }

        public ActionResult EditKit(Int64 product_id, String comment, String tags, String recommended_tags, Int64 kit_id, String valoration)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            KitsRepository kRP = new KitsRepository(Repository.Database);

            Kit kit = kRP.GetKit(kit_id, UserID);

            kit.Comment = comment;
            if (valoration == "")
                valoration = "1";
            kit.Valoration = Double.Parse(valoration);

            kRP.UpdateKit(kit);

            //Esborrem els tags, pels canvis.
            kRP.DeleteTags(kit.ID);

            //Ara hem d'afegir els tags.
            //Recorrem els productes seleccionats
            string[] tokensizedStrs;
            tokensizedStrs = tags.Split(new char[] { ',' });

            int len = tokensizedStrs.Length;

            for (int i = 0; i < len; i++)
            {
                string tmp = tokensizedStrs[i];

                if (tmp != String.Empty)
                {
                    KitTag kittag = new KitTag();
                    kittag.Kit = new Kit();
                    kittag.Kit.ID = kit.ID;
                    kittag.Name = tmp;

                    KitTagsRepository ktRP = new KitTagsRepository(Repository.Database);
                    ktRP.CreateKitTag(kittag);
                }
            }

            //Recommended Tags
            string[] tokensizedStrsRecommended;
            tokensizedStrsRecommended = recommended_tags.Split(new char[] { ',' });

            int lenRecommended = tokensizedStrsRecommended.Length;

            for (int i = 0; i < lenRecommended; i++)
            {
                string tmp = tokensizedStrsRecommended[i];

                if (tmp != String.Empty)
                {
                    KitTag kittag = new KitTag();
                    kittag.Kit = new Kit();
                    kittag.Kit.ID = kit.ID;
                    kittag.Name = tmp;

                    KitTagsRepository ktRP = new KitTagsRepository(Repository.Database);
                    ktRP.CreateKitTag(kittag);
                }
            }

            String Message = "Ok";

            return PartialView("Html", (object)kit_id.ToString());
        }

        public ActionResult LoadReKitProductData(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Kit kit = Repository.GetKit(id, UserID);
            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            var product_data = pRP.KitProductDataHtml(kit.Product.ID, 0, id);
            return PartialView("Html", (object)product_data);
        }

        public ActionResult LoadEditKitProductData(Int64 id)
        {
            Kit kit = Repository.GetKit(id);
            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            var product_data = pRP.KitProductDataHtml(kit.Product.ID, id, 0);
            return PartialView("Html", (object)product_data);
        }

        public ActionResult LoadKitComment(Int64 id)
        {
            Kit kit = Repository.GetKit(id);

            return PartialView("Html", (object)kit.Comment);
        }

        public ActionResult NowWithAddProduct(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());
            Repository.NowWithAddProduct(id, userid);

            String Message = String.Empty;
            Message = "Success";

            return PartialView("Html", (object)Message);
        }

        public ActionResult DelNowWith(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());

            UserStatus us = new UserStatus();
            us.ID = id;

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);
            usRP.DeleteUserStatus(us);

            String Message = String.Empty;
            Message = "Success";

            return PartialView("Html", (object)Message);
        }


        public ActionResult ReloadNowWith(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);

            String NowWith = String.Empty;
            NowWith = usRP.GetHtmlProfileNowWith(id, userid);

            return PartialView("Html", (object)NowWith);
        }

        public ActionResult DelWishList(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());

            WishList wl = new WishList();
            wl.ID = id;

            WishListsRepository wlRP = new WishListsRepository(Repository.Database);
            wlRP.DeleteWishList(wl);

            String Message = String.Empty;
            //Message = "Success";
            Message = String.Format("{0}", userid);

            return PartialView("Html", (object)Message);
        }

        public ActionResult DoLike(Int64 id)
        {
            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String message = "";

            if (SessionUserID > 0)
            {
                User usr = Repository.GetUser(SessionUserID);
                Kit kit = Repository.GetKit(id);

                UserLike ul = new UserLike();
                ul.Kit = kit;
                ul.User = usr;

                UserLikesRepository ulRP = new UserLikesRepository(Repository.Database);
                Int64 result = ulRP.CreateUserLike(ul);

                //Creem la notificació
                Repository.CreateNotify(kit.User.ID, SessionUserID, result, NotifyType.Like);

                //Mirem si li enviem el correu.
                User user = Repository.GetUser(kit.User.ID);

                user.Perm5 = Globals.GetPerms(user.Perms, (int)UserPerms.KitLike);

                if (user.Perm5 == true)
                {
                    Repository.LikeMail(kit.User.ID, SessionUserID, id);
                }

                message = String.Format("{0}",result);
            }
            else
                message = "Error";

            return PartialView("Html", (object)message);
        }

        public ActionResult DoDisLike(Int64 id)
        {
            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String message = "";

            if (SessionUserID > 0)
            {
                //User usr = Repository.GetUser(SessionUserID);
                //Kit kit = Repository.GetKit(id);

                UserLike ul = new UserLike();
                ul.ID = id;

                UserLikesRepository ulRP = new UserLikesRepository(Repository.Database);
                ulRP.DeleteUserLike(ul);

                Repository.DeleteRelatedNotify(id, NotifyType.Like);

                message = "Success";
            }
            else
                message = "Error";

            return PartialView("Html", (object)message);
        }

        public ActionResult DoDisLikeKitId(Int64 Kitid)
        {
            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String message = "";

            if (SessionUserID > 0)
            {

                UserLike ul = new UserLike();
                UserLikesRepository ulRP = new UserLikesRepository(Repository.Database);

                Int64 id = ulRP.Exists(SessionUserID, Kitid);

                if (id > 0)
                {
                    ul.ID = id;
                    ulRP.DeleteUserLike(ul);

                    Repository.DeleteRelatedNotify(id, NotifyType.Like);

                    message = "Success";
                }
                else
                    message = "Error";
            }
            else
                message = "Error";

            return PartialView("Html", (object)message);
        }

        [Authorize]
        public ActionResult NowWith(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);

            User user = Repository.GetUser(id);

            UserStatus us = new UserStatus();
            us.HtmlOutput = usRP.GetNowWith(id, UserID).ToString();
            us.HtmlHeaderUserImage = user.Picture;

            

            if (id != UserID)
                us.HtmlHeaderTitle = String.Format("{0} {1}\n" + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_AHORA_ESTA_CON_MIN, user.FirstName, user.LastName);
            else
                us.HtmlHeaderTitle = String.Format("\n"  + LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_AHORA_ESTAS_CON_MIN + "...", user.FirstName, user.LastName);

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)us);
        }

        public ActionResult DelNowWithReloadHtml(Int64 id, Int64 userid)
        {
            Int64 SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            UserStatus us = new UserStatus();
            us.ID = id;

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);
            usRP.DeleteUserStatus(us);

            String Message = String.Empty;
            Message = usRP.GetNowWith(userid, SessionUserID).ToString();

            return PartialView("Html", (object)Message);
        }

        public ActionResult NowWithAddProductReload(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());
            Repository.NowWithAddProduct(id, userid);

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);

            String Message = String.Empty;
            Message = usRP.GetNowWith(userid, userid).ToString();

            return PartialView("Html", (object)Message);
        }

        public class ViewDataUploadFilesResult
        {
            public string name { get; set; }
            public int size { get; set; }
            public string type { get; set; }
            public string url { get; set; }
            public string delete_url { get; set; }
            public string thumbnail_url { get; set; }
            public string delete_type { get; set; }
        }


        public ActionResult DisableProfile()
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            UsersRepository usRP = new UsersRepository(Repository.Database);
            usRP.DisableUser(user);

            String Result = "Success";

            return PartialView("Html", (object)Result);
        }

        public ActionResult DeleteProfile()
        {
            Int64 ID = 0;
            if (Session[Globals.SessionUserID] != null)
                ID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(ID);

            UsersRepository usRP = new UsersRepository(Repository.Database);
            usRP.DeleteUser(user);

            String Result = "Success";

            return PartialView("Html", (object)Result);
        }

        public ActionResult WishListAddProductReload(Int64 id)
        {
            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());
            Repository.AddToWishList(id, userid);

            WishListsRepository wlRP = new WishListsRepository(Repository.Database);

            String Message = String.Empty;
            Message = wlRP.GetHtmlWishList(userid, userid).ToString();

            return PartialView("Html", (object)Message);
        }

        [Authorize]
        public ActionResult Likes(Int64 id)
        {
            User user = Repository.GetUser(id);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            UserLikesRepository ulRP = new UserLikesRepository(Repository.Database);

            UserLike ul = new UserLike();

            ul.HtmlHeaderUserImage = user.Picture;
            ul.HtmlHeaderUserName = String.Format("{0} {1}", user.FirstName, user.LastName);
            ul.HtmlOutput = ulRP.GetHtmlLikes(user.ID, UserID).ToString();

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)ul);
        }

        public void GetNumUserInteractions()
        {
            String sUserID = String.Empty;

            if(Session[Globals.SessionUserID] != null)
                sUserID = Session[Globals.SessionUserID].ToString();

            if((sUserID != "") && (sUserID != String.Empty))
            {
                Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());

                UsersRepository uRP = new UsersRepository(Repository.Database);
                Int64 num_interactions = 0;
                num_interactions = uRP.GetNumUserInteraction(userid);

                Session[Globals.SessionNumInteractions] = num_interactions;
            }
        }


        public ActionResult DoDisLikeReload(Int64 id)
        {
            Int64 SessionUserID = 0;
            if (Session[Globals.SessionUserID] != null)
                SessionUserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String message = "";

            if (SessionUserID > 0)
            {
                //User usr = Repository.GetUser(SessionUserID);
                //Kit kit = Repository.GetKit(id);

                UserLike ul = new UserLike();
                ul.ID = id;

                UserLikesRepository ulRP = new UserLikesRepository(Repository.Database);
                ulRP.DeleteUserLike(ul);


                message = ulRP.GetHtmlLikes(SessionUserID, SessionUserID).ToString();
                //message = "Success";
            }
            else
                message = "Error";

            return PartialView("Html", (object)message);
        }



        [Authorize]
        public ActionResult CategoryKits(Int64 id, Int16 Category)
        {
            User user = Repository.GetUser(id);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            WishListsRepository wlRP = new WishListsRepository(Repository.Database);

            ProductCategory pc = new ProductCategory();

            pc.HtmlHeaderUserImage = user.Picture;

            if(Category == 1)
                pc.HtmlHeaderCategoryName = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_LIBROS_DE);
            else if (Category == 2)
                pc.HtmlHeaderCategoryName = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_SERIES_DE);
            else if (Category == 3)
                pc.HtmlHeaderCategoryName = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_CINE_DE);
            else if (Category == 4)
                pc.HtmlHeaderCategoryName = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_MUSICA_DE);
            
            pc.HtmlHeaderUserName = String.Format("{0} {1}", user.FirstName, user.LastName);
            pc.HtmlOutput = Repository.GetHtmlCategoryKits(user.ID, UserID, Category).ToString();

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View((object)pc);
        }


        [Authorize]
        public ActionResult doTweet(Int64 kit_id, String comment)
        {
            Globals.SaveLog(Repository.Database, "hhhh", Models.Type.Timeline, 1);
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(UserID);
            Globals.SaveLog(Repository.Database, String.Format("Kit_id =: {0}, comment =: {1}, user =: {2}",kit_id,comment,user), Models.Type.Timeline, 1);
            //110 per posar la url
            if (comment.Length > 110)
                comment = comment.Substring(0, 110);

            String strURL = ConfigurationManager.AppSettings["MainURL"] + "Home/Item/" + kit_id;
            strURL = Endepro.Common.Common.MakeTinyUrl(strURL);

            comment = comment + " " + strURL;

            Repository.TwitterPublish(user, comment);

            String message = String.Empty;

            return PartialView("Html", (object)message);
        }

        [Authorize]
        public ActionResult doFacebookPublish(Int64 kit_id, String comment)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());


            try
            {
                User user = Repository.GetUser(UserID);
                Kit kit = Repository.GetKit(kit_id, UserID);

                String strURL = ConfigurationManager.AppSettings["MainURL"] + "Home/Item/" + kit_id;
                //strURL = strURL.Replace("//", "/");
                strURL = Endepro.Common.Common.MakeTinyUrl(strURL);

                String ProductImage = kit.Product.Image;

                //ProductImage = ConfigurationManager.AppSettings["MainURL"] + ProductImage;

                if (ProductImage != "")
                {
                    String img_path = ConfigurationManager.AppSettings["PathImages"];
                    String sFile = Globals.PathCombine(img_path, ProductImage);

                    if (!System.IO.File.Exists(sFile))
                        ProductImage = "";
                }

                if (ProductImage == "")
                    ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/images/default_product.png";
                else
                    ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/files/" + ProductImage;


                //ProductImage = ProductImage.Replace("//", "/");

                Repository.FacebookPublish(user, comment, strURL, kit.Product.Name, "Likekit", ProductImage, kit.Product.Name);
            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "doFacebookPublish kit : " + kit_id + " comment : " + comment);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Timeline, 1);
            }

            String message = String.Empty;

            return PartialView("Html", (object)message);
        }



        [Authorize]
        public ActionResult doKitMail(Int64 kit_id, String comment, String mail)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            User user = Repository.GetUser(UserID);
            Kit kit = Repository.GetKit(kit_id, UserID);

            String strURL = Endepro.Common.Common.MakeTinyUrl(ConfigurationManager.AppSettings["MainURL"] + "Home/Item/" + kit.ID);

            String ProductImage = kit.Product.Image;

            if (ProductImage != "")
            {
                String img_path = ConfigurationManager.AppSettings["PathImages"];
                String sFile = Globals.PathCombine(img_path, ProductImage);

                if (!System.IO.File.Exists(sFile))
                    ProductImage = "";
            }

            if (ProductImage == "")
                ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/images/default_product.png";
            else
                ProductImage = ConfigurationManager.AppSettings["MainURL"] + "Content/files/" + ProductImage;

            String Subject = String.Empty;
            String Body = String.Empty;
            String To = String.Empty;
            String From = String.Empty;

            MailTemplatesRepository mail_template_repository = new MailTemplatesRepository(Repository.Database);

            short _lang = 1;

            MailTemplate mail_template = mail_template_repository.GetMailTemplate((Int16)ObjectType.UserRecommendation, _lang);

            mail_template.Subject = LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_RECOMMENDATION_MAIL_SUBJECT;

            mail_template.Body = mail_template.Body.Replace("[IMAGES_URL]", ConfigurationManager.AppSettings["URLImages"]);

            mail_template.Body = mail_template.Body.Replace("[MESSAGE]", comment);

            //Evitem el problema de duplicar les barres, ja que això genera problemes
            strURL = strURL.Replace("//", "/");
            strURL = strURL.Replace("http:/", "http://");

            mail_template.Body = mail_template.Body.Replace("[KIT_URL]", strURL);

            String message = String.Empty;
            //Enviem un correu
            try
            {
                Globals.SendMail(Repository.Database, mail_template.Subject, mail_template.Body, mail);
                message = "Success";
            }
            catch (Exception ex)
            {
                //Guardem log
                String ex_msg = String.Format("{0} {1}", ex.Message, ex.StackTrace);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Timeline, 1);
                message = "Error";
            }            

            return PartialView("Html", (object)message);
        }

        public ActionResult NowWithWSAddProductReload(String ASIN, String Title, String Author, String Year, String Category, String LargeImage, String URL, String Description, String EAN, String Observations)
        {

            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            Int64 product_id = 0;

            //Mirem que no existeixi el producte
            product_id = pRP.WSProductExists(ASIN);

            if (product_id == 0)
                product_id = pRP.CreateWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations);

            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());
            Repository.NowWithAddProduct(product_id, userid);

            UserStatusRepository usRP = new UserStatusRepository(Repository.Database);

            String Message = String.Empty;
            Message = usRP.GetNowWith(userid, userid).ToString();

            return PartialView("Html", (object)Message);
        }

        public ActionResult WishListWSAddProductReload(String ASIN, String Title, String Author, String Year, String Category, String LargeImage, String URL, String Description, String EAN, String Observations)
        {
            ProductsRepository pRP = new ProductsRepository(Repository.Database);

            Int64 product_id = 0;

            //Mirem que no existeixi el producte
            product_id = pRP.WSProductExists(ASIN);

            if (product_id == 0)
                product_id = pRP.CreateWSProduct(ASIN, Title, Author, Year, Category, LargeImage, URL, Description, EAN, Observations);


            Int64 userid = Int64.Parse(Session[Globals.SessionUserID].ToString());
            Repository.AddToWishList(product_id, userid);

            WishListsRepository wlRP = new WishListsRepository(Repository.Database);

            String Message = String.Empty;
            Message = wlRP.GetHtmlWishList(userid, userid).ToString();

            return PartialView("Html", (object)Message);
        }

        [Authorize]
        public ActionResult SearchPeople()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String Result = String.Empty;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            //mostrem els recomanats
            UsersRepository usRP = new UsersRepository(Repository.Database);
            Result = usRP.GetRecommendedSearchPeople();

            return View((object)Result);
        }

        public ActionResult doSearchPeople(String search)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            var users = Repository.SearchPeopleHtml(search, 15, UserID);
            return PartialView("Html", (object)users);
        }

        [Authorize]
        public ActionResult Invite()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String Result = String.Empty;

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            Invite invite = new Invite();
            invite.Show = false;
            invite.HtmlOutput = String.Empty;

            return View((object)invite);
        }

        public ActionResult doInvite(String invite)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            //Recorrem els mails
            string[] tokensizedStrs;
            tokensizedStrs = invite.Split(new char[] { ',' });

            int len = tokensizedStrs.Length;

            for (int i = 0; i < len; i++)
            {
                string tmp = tokensizedStrs[i];

                if (tmp != String.Empty)
                {
                    Repository.Invite(tmp, UserID);
                }
            }

            String Result = String.Empty;

            return PartialView("Html", (object)Result);
        }

        public ActionResult doFacebookInvite(String invite)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            try
            {
                User user = Repository.GetUser(UserID);

                String message = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_FACEBOOK_INVITE_TEXT);
                String Image = ConfigurationManager.AppSettings["MainURL"] + "Content/images/avatar.jpg";
                String link = ConfigurationManager.AppSettings["MainURL"];
                String name = "";
                String caption = "";
                String description = "";

                Repository.FacebookInvite(user, message, link, name, caption, Image, description, invite);

            }
            catch (Exception ex)
            {
                String ex_msg = String.Format("{0} {1} : {2}", ex.Message, ex.StackTrace, "doFacebookInvite : " + invite);
                Globals.SaveLog(Repository.Database, ex_msg, Models.Type.Timeline, 1);
            }

            String Result = String.Empty;

            return PartialView("Html", (object)Result);
        }

        public ActionResult DeleteKit(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());


            KitsRepository kR = new KitsRepository(Repository.Database);
            Kit kit = new Kit();
            kit.ID = id;

            kR.DeleteKit(kit);

            //var comments = kR.GetHtmlKitComments(KitID);
            //var comments = kR.GetHtmlKitComments(KitID, UserID);
            String comments = String.Empty;
            return PartialView("Html", (object)comments);
        }

        public ActionResult GetAdvancedFilterRecommendedTags()
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String tags = Repository.getAdvancedFilterRecommendedTagsHtml();

            return PartialView("Html", (object)tags);
        }


        public ActionResult GetNumLikes(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Kit kit = Repository.GetKit(id, UserID);

            return PartialView("Html", (object)kit.NumLikes.ToString());
        }

        public ActionResult GetNumFollows(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            if (id != UserID)
            {
                String value = String.Empty;

                return PartialView("Html", (object)value.ToString());
            }
            else
            {
                Int64 NumFollowings = 0;

                UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);
                NumFollowings = urRP.GetNumUserRelations(UserID, 1);

                return PartialView("Html", (object)NumFollowings.ToString());
            }
        }

        public ActionResult GetNumFollowersOther(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Int64 NumFollowings = 0;

            UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);
            NumFollowings = urRP.GetNumUserRelations(id, 2);

            return PartialView("Html", (object)NumFollowings.ToString());
        }

        public ActionResult GetFollowersContainerOther(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            String ProfileFollowers = String.Empty;

            UserRelationsRepository urRP = new UserRelationsRepository(Repository.Database);

            ProfileFollowers = urRP.GetHtmlProfileRelations(id, 2, UserID);

            return PartialView("Html", (object)ProfileFollowers);
        }

        public ActionResult GetNumKitComments(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Kit kit = Repository.GetKit(id, UserID);

            return PartialView("Html", (object)kit.NumComments.ToString());
        }

        public ActionResult GetNumReKits(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Kit kit = Repository.GetKit(id, UserID);

            return PartialView("Html", (object)kit.NumKits.ToString());
        }


        [Authorize]
        public ActionResult About(Int64 id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            StaticPagesRepository hRP = new StaticPagesRepository(Repository.Database);

            if ((HttpContext.Session["culture"] != null) && (HttpContext.Session["culture"].ToString() == "ca"))
            {
                id = id + 4;            
            }

            StaticPage static_page = hRP.GetStaticPage(id);

            //Recalculem el número d'interaccions
            GetNumUserInteractions();

            return View(static_page);
        }


        public ActionResult TimelineCounter(String date, String Search, String filter, String tags)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            var counter = Repository.GetTimelineCounter(date, UserID, Search, filter, tags).ToString();
            return PartialView("Html", (object)counter);
        }

        public ActionResult KitAccessLog()
        {
            AccessLog al = new AccessLog();
            AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            al.DeviceType = DeviceType.Web;
            al.ObjectType = AccesLogObjectType.Kit;
            al.OperationType = OperationType.OpenKitForm;

            User user = new User
            {
                ID = UserID
            };

            al.User = user;
            al.RefID = 0;
            al.IP = String.Empty;
            al.SessionID = String.Empty;
            al.Observations = String.Empty;

            alRP.CreateAccessLog(al);

            String result = String.Empty;

            return PartialView("Html", (object)result);
        }


        public ActionResult GetSocialKitText(Int64 id,String type)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            Kit kit = Repository.GetKit(id, UserID);

            String text = String.Empty;

            if(type == "1")
                text = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_TWITTER_TEXT, kit.Product.Name);
            else if(type == "2")
                text = String.Format(LikeKitWebsite.Resources.LikeKitWebsiteLocal.STR_KIT_FACEBOOK_TEXT);

            return PartialView("Html", (object)text);
        }

        public ActionResult LoQuiero(Int64 id)
        {
            AccessLog al = new AccessLog();
            AccessLogsRepository alRP = new AccessLogsRepository(Repository.Database);

            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());

            al.DeviceType = DeviceType.Web;
            al.ObjectType = AccesLogObjectType.Product;
            al.OperationType = OperationType.LoQuiero;

            User user = new User
            {
                ID = UserID
            };

            al.User = user;
            al.RefID = id;
            al.IP = String.Empty;
            al.SessionID = String.Empty;
            al.Observations = String.Empty;

            alRP.CreateAccessLog(al);

            String result = String.Empty;

            return PartialView("Html", (object)result);
        }


        public ActionResult LoadLoQuiero(Int64 product_id)
        {
            Int64 UserID = 0;
            if (Session[Globals.SessionUserID] != null)
                UserID = Int64.Parse(Session[Globals.SessionUserID].ToString());


            String result = String.Empty;

            KitsRepository kRP = new KitsRepository(Repository.Database);
            result = kRP.GetLoquieroHTML(product_id, 0);

            return PartialView("Html", (object)result);
        }

    }
}
